Might and Magic VI reboot, technical demo. The goal of this project is a non-commercial demo which will be distributed to the public.
The purpose is to discover the difficulty of implementing many of the core, base features from Might and Magic VI to determine if it's feasible to move ahead and design a new, original game based on the concept.

The current project runs on Unreal Engine 4.13. Binaries and screenshots can be downloaded at https://hapro.itch.io/newnewsorpigal

YOU MAY NOT USE ANY ART ASSETS WITHOUT EXPLICIT PERMISSION FROM THEIR OWNER.

The following free packs are required (excluded from the repository because of size):
- Starter Content
- Infinity Blade Adversaries
- Infinity Blade Grasslands
- Mixamo Animation Pack (imported through 4.10 version)
- Maybe one or two other things?

Instructions for level creation:
Adding an NPC - Blueprints/NPCs/HumanNPC, look at Dialog category
Adding an enemy - Blueprints/Enemies/..., no customizations needed, just drop in world
Adding items - Blueprints/Items/..., look at Gameplay category - if loot, set Gold. if not loot, check Create New Item
Adding chests - Blueprints/BP_Chest, Blueprints/BP_Barrel, Items category and Traps category
Adding fountains - Blueprints/BP_Fountain, Gameplay category
Adding buildings - First, add the static mesh in the world. Then, go in C++ Classes/NewNewSorpigal/Public/*Building. Modify attributes in Shop, Dialog, and Gameplay categories
Everything else should just be static meshes and landscape features. All other features are modifiable in code only, including quests, spells, etc.


Controls listed in in-game menu.