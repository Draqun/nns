// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "Chest.h"

#include "DataStructs.h"
#include "EquippableItem.h"
#include "GoldItem.h"
#include "Inventory.h"
#include "ItemGenerator.h"
#include "NNSCharacter.h"
#include "NNSGameInstance.h"
#include "NNSHUD.h"
#include "PlayerPartyCharacter.h"

const int32 AChest::kExpertPenalty = 25;
const int32 AChest::kMasterPenalty = 50;

// Sets default values
AChest::AChest(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	static ConstructorHelpers::FObjectFinder<UParticleSystem> FireParticlesFinder(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));
	FireTrapParticles = FireParticlesFinder.Object;
	ParticleMap.Emplace(ETrapTypes::TrapFire, FireTrapParticles);

	static ConstructorHelpers::FObjectFinder<UParticleSystem> AirParticlesFinder(TEXT("ParticleSystem'/Game/Particles/P_ExplosionSparks.P_ExplosionSparks'"));
	AirTrapParticles = AirParticlesFinder.Object;
	ParticleMap.Emplace(ETrapTypes::TrapAir, AirTrapParticles);

	static ConstructorHelpers::FObjectFinder<USoundWave> FireSoundFinder(TEXT("SoundWave'/Game/Sounds/244345__willlewis__musket-explosion.244345__willlewis__musket-explosion'"));
	SoundMap.Emplace(ETrapTypes::TrapFire, FireSoundFinder.Object);

	static ConstructorHelpers::FObjectFinder<USoundWave> AirSoundFinder(TEXT("SoundWave'/Game/Sounds/189630__elliottmoo__spark.189630__elliottmoo__spark'"));
	SoundMap.Emplace(ETrapTypes::TrapAir, AirSoundFinder.Object);

	static ConstructorHelpers::FObjectFinder<USoundWave> OpenFinder(TEXT("SoundWave'/Game/Sounds/chest_open.chest_open'"));
	OpenSound = OpenFinder.Object;

	bIsInitialized = false;

	Trap = ETrapTypes::TrapNone;
	TrapLevel = ETrapLevel::TrapBasic;

	Tier = 1;
	NumItems = 0;

	Tags.Add(ActorTags::kInteractableTag);
}

void AChest::SaveLoadCommon(FArchive& Archive)
{
	Super::SaveLoadCommon(Archive);

	Archive << bIsInitialized;
	Archive << Trap;
	Archive << TrapLevel;
}

void AChest::Save(FArchive& Archive)
{
	Super::Save(Archive);

	Inventory->Save(Archive);
}

void AChest::Load(FArchive& Archive)
{
	Super::Load(Archive);

	Inventory->Load(Archive);
}

// Called when the game starts or when spawned
void AChest::BeginPlay()
{
	Super::BeginPlay();

	// If this is in constructor, all Chests share the same inventory...?
	Inventory = NewObject<UInventory>();
}

// Called every frame
void AChest::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AChest::InitItems()
{
	if (!bIsInitialized) {
		// If the item is too big to fit in the remaining space, we'll try a few more times with new items just in case they're smaller
		for (int32 i = 0; i < NumItems; ++i) {
			for (int32 Tries = 0; Tries < 5; ++Tries) {
				UItem* Item = Cast<UNNSGameInstance>(GetGameInstance())->ItemGeneratorObject->GenerateItem(Tier);
				if (Item != nullptr && Inventory->AddItemToFirstOpenSlot(Item)) {
					break;
				}
			}
		}

		bIsInitialized = true;
	}
}

void AChest::Interact(ANNSCharacter* Character)
{
	Super::Interact(Character);

	InitItems();
	if (Trap != ETrapTypes::TrapNone) {
		int32 Penalty = 0;
		int32 DamageMin = 5;
		int32 DamageMax = 10;
		// Add penalty and set damage based on trap level
		if (TrapLevel == ETrapLevel::TrapExpert) {
			DamageMin = 8;
			DamageMax = 17;
			Penalty += kExpertPenalty;
		} else if (TrapLevel == ETrapLevel::TrapMaster) {
			DamageMin = 15;
			DamageMax = 40;
			Penalty += kMasterPenalty;
		}

		int32 Level = 0;
		EMastery Mastery = EMastery::MasteryBasic;
		Character->GetPartySkill(ESkills::SkillDisarmTrap, Level, Mastery);
		// Remove penalty if appropriate, based on skill level
		if (Mastery == EMastery::MasteryMaster) {
			Penalty -= kMasterPenalty;
			Level -= EnumMaps::SkillCosts[EMastery::MasteryMaster].SkillPoints;
		} else if (Mastery == EMastery::MasteryExpert) {
			Penalty -= kExpertPenalty;
			Level -= EnumMaps::SkillCosts[EMastery::MasteryExpert].SkillPoints;
		} else {
			Level -= 1;
		}

		// The goal here is to make it so that a lv1 opening a basic chest, a lv5 expert opening an expert chest, and a lv10 master opening a master chest
		// all have a 50% chance of succeeding. Increasing skill will increase that chance by 5% per point. If you don't have the necessary expertise, the
		// chance to succeed drops significantly. This should probably be rewritten better at some point.
		if (Level < 0 || (FMath::RandRange(Level * 5, 99) < FMath::Min(100, 50 + Penalty))) {
			UDamageData* Damage = UDamageData::Create(EnumMaps::TrapDamages[Trap], FMath::RandRange(DamageMin, DamageMax), -1, EDamageDistance::DamageInstant, TEXT("Chest"), this);
			Character->DealDamage(Damage, true);
			if (Character->IsA<APlayerPartyCharacter>()) {
				Cast<APlayerPartyCharacter>(Character)->TriggerParticlesAtCamera(ParticleMap[Trap]);
				Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect2D(SoundMap[Trap]);
			}
			Trap = ETrapTypes::TrapNone;
			// Don't open the chest if a trap triggers. They'll have to interact again.
			return;
		} else {
			Trap = ETrapTypes::TrapNone;
		}
	}
	if (OpenSound) {
		Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect2D(OpenSound);
	}
	Cast<UNNSGameInstance>(GetGameInstance())->HUD->OpenChest(this);
}
