// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "NNSHUD.h"

#include "Blueprint/UserWidget.h"

#include "BaseEnemyCharacter.h"
#include "CharacterSheet.h"
#include "Chest.h"
#include "DataStructs.h"
#include "DialogComponent.h"
#include "EquippableItem.h"
#include "Inventory.h"
#include "Item.h"
#include "NNSGameInstance.h"
#include "NNSPlayerController.h"
#include "NPCBuilding.h"
#include "Pickup.h"
#include "PlayerPartyCharacter.h"
#include "PotionItem.h"
#include "QuestManager.h"
#include "ShopBuilding.h"
#include "Spellbook.h"
#include "StaticInteractable.h"
#include "TavernBuilding.h"
#include "TempleBuilding.h"
#include "TimeOfDayManager.h"

const FLinearColor ANNSHUD::kWhiteColor = FLinearColor(1.0f, 1.0f, 1.0f);
const FLinearColor ANNSHUD::kBlackColor = FLinearColor(0.0f, 0.0f, 0.0f);
const FLinearColor ANNSHUD::kRedColor = FLinearColor(1.0f, 0.0f, 0.0f);
const FLinearColor ANNSHUD::kGreenColor = FLinearColor(0.0f, 1.0f, 0.0f);
const FLinearColor ANNSHUD::kBlueColor = FLinearColor(0.0f, 0.0f, 1.0f);
const FLinearColor ANNSHUD::kGoldColor = FLinearColor(0.405f, 0.303f, 0.032f);
const FLinearColor ANNSHUD::kBrownColor = FLinearColor(0.5f, 0.25f, 0.1f);
const FLinearColor ANNSHUD::kLightGrayColor = FLinearColor(0.7f, 0.7f, 0.7f);
const FLinearColor ANNSHUD::kMediumGrayColor = FLinearColor(0.3f, 0.3f, 0.3f);
const FLinearColor ANNSHUD::kDarkGrayColor = FLinearColor(0.15f, 0.15f, 0.15f);

const int32 ANNSHUD::InventoryRows = UInventory::kMaxHeight;
const int32 ANNSHUD::InventoryColumns = UInventory::kMaxWidth;
const float ANNSHUD::InventoryBoxSize = 50.0f;
const float ANNSHUD::TypeWidth = InventoryColumns * InventoryBoxSize + InventoryColumns + 1;
const float ANNSHUD::TypeHeight = InventoryRows * InventoryBoxSize + InventoryRows + 1;

const int32 ANNSHUD::NumStatusRows = 4;
const FString ANNSHUD::StatusEffectTooltip = TEXT("%s\n \nStrength: %i\nDuration: %s");

const float ANNSHUD::CharacterWidth = 400.0f;
const float ANNSHUD::CharacterHeight = 600.0f;

const float ANNSHUD::DialogSidebarWidth = 300.0f;
const float ANNSHUD::DialogSidebarSpacing = 16.0f;
const float ANNSHUD::DialogSidebarPortraitSize = 100.0f;
const float ANNSHUD::DialogTextScale = 1.2f;
const float ANNSHUD::DialogBackgroundWidth = 500.0f;
const float ANNSHUD::DialogBackgroundHeight = 200.0f;
const FName ANNSHUD::DialogExit = FName(TEXT("Exit (Escape)"));
const FName ANNSHUD::StoreBuy = FName(TEXT("Buy"));
const FName ANNSHUD::StoreInventory = FName(TEXT("Inventory"));
const FName ANNSHUD::StoreSell = FName(TEXT("Sell"));
const FName ANNSHUD::StoreIdentify = FName(TEXT("Identify"));
const FName ANNSHUD::StoreSkills = FName(TEXT("Learn Skills"));

const FString ANNSHUD::DialogSkillLearn = TEXT("Learn %s - %u gold");
const FString ANNSHUD::DialogSkillTrain = TEXT("Become %s %s - %u gold");
const FString ANNSHUD::DialogSkillTrainTooHigh = TEXT("You are already a master in %s");
const FString ANNSHUD::DialogSkillCannotTrain = TEXT("Cannot learn any more about %s");
const FName ANNSHUD::DialogSkillBasic = FName(TEXT("basic"));
const FName ANNSHUD::DialogSkillExpert = FName(TEXT("expert"));
const FName ANNSHUD::DialogSkillMaster = FName(TEXT("master"));

const FString ANNSHUD::TempleHeal = TEXT("Heal %s for %i gold");
const FString ANNSHUD::TempleHealFull = TEXT("%s is already full health");

const FName ANNSHUD::TavernRestName = FName(TEXT("rest"));
const FString ANNSHUD::TavernRestString = TEXT("Rest 8 hours for %i gold");

const FString ANNSHUD::StoreWrongType = TEXT("I'm not interested in that");
const FString ANNSHUD::StoreBuyPrice = TEXT("Buy %s for %u gold");
const FString ANNSHUD::StoreSellPrice = TEXT("Sell %s for %u gold");
const FString ANNSHUD::StoreIdentifyPrice = TEXT("Identify this item for %u gold");

const float ANNSHUD::SpellbookSpellSize = 200.0f;
const float ANNSHUD::SpellbookWidth = SpellbookSpellSize * 3 + Margin * 2;
const float ANNSHUD::SpellbookHeight = SpellbookSpellSize * 3 + Margin * 2;
const float ANNSHUD::SpellbookButtonWidth = 100.0f;
const float ANNSHUD::SpellbookButtonHeight = 20.0f;

const int32 ANNSHUD::kMaxLogLines = 8;
const float ANNSHUD::LogWidth = 400.0f;

const float ANNSHUD::MinimapY = Margin + 1;
const float ANNSHUD::MinimapWidth = 200.0f;
const float ANNSHUD::MinimapHeight = 200.0f;
const float ANNSHUD::MapSize = 600.0f;

const float ANNSHUD::TimeWidth = 50.0f;
const float ANNSHUD::TimeY = MinimapY + MinimapHeight + 1;

const float ANNSHUD::Margin = 8.0f;
const float ANNSHUD::SmallMargin = 4.0f;

const FName ANNSHUD::ScreenHitbox = FName(TEXT("Screen"));

const float ANNSHUD::ChestCloseWidth = 50.0f;
const float ANNSHUD::ChestCloseHeight = 20.0f;
const FName ANNSHUD::ChestCloseName = FName(TEXT("Close"));

const float ANNSHUD::StoreWidth = 1000.0f;
const float ANNSHUD::StoreHeight = 600.0f;

const float ANNSHUD::PlayerXStart = 20.0f;
const float ANNSHUD::PlayerYStart = 30.0f;
const float ANNSHUD::PlayerSpacing = 10.0f;
const float ANNSHUD::PlayerPortraitSize = 100.0f;
const float ANNSHUD::PlayerTitleHeight = 16.0f;
const float ANNSHUD::PlayerStatusHeight = 16.0f;
const float ANNSHUD::PlayerBarWidth = 10.0f;
const float ANNSHUD::PlayerBackgroundWidth = PlayerPortraitSize + SmallMargin * 4.0f + PlayerBarWidth * 2.0f - 1.0f;
const float ANNSHUD::PlayerBackgroundHeight = PlayerPortraitSize + PlayerTitleHeight + PlayerStatusHeight + SmallMargin * 4.0f;

TArray<FString> ANNSHUD::StatNames;
TArray<FString> ANNSHUD::SkillNames;
TArray<FString> ANNSHUD::ClassNames;
TArray<FString> ANNSHUD::StatusEffectNames;
TArray<FString> ANNSHUD::PartyStatusEffectNames;

ANNSHUD::ANNSHUD(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Font = nullptr;

	static ConstructorHelpers::FObjectFinder<UTexture2D> ArrowObject(TEXT("Texture2D'/Game/Maps/arrow.arrow'"));
	Arrow = ArrowObject.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TurnIconObject(TEXT("Texture2D'/Game/UI/turn_icon.turn_icon'"));
	TurnIcon = TurnIconObject.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> LevelUpObject(TEXT("Texture2D'/Game/UI/level_up.level_up'"));
	LevelUpIcon = LevelUpObject.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> DeathIconObject(TEXT("Texture2D'/Game/UI/dead.dead'"));
	DeathIcon = DeathIconObject.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> NPCPortraitObject(TEXT("Texture2D'/Game/UI/portrait.portrait'"));
	NPCPortrait = NPCPortraitObject.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> UnconsciousIconObject(TEXT("Texture2D'/Game/UI/unconscious.unconscious'"));
	UnconsciousIcon = UnconsciousIconObject.Object;

	static ConstructorHelpers::FClassFinder<UUserWidget> QuestsFinder(TEXT("WidgetBlueprint'/Game/Blueprints/UI/BP_QuestsMenu.BP_QuestsMenu_C'"));
	QuestsMenuClass = QuestsFinder.Class;

	static ConstructorHelpers::FObjectFinder<USoundWave> DoorFinder(TEXT("SoundWave'/Game/Sounds/door.door'"));
	DoorSound = DoorFinder.Object;

	StatusIcons.Emplace(EStatusEffects::StatusPoison, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_poison.status_poison'")).Object);
	StatusIcons.Emplace(EStatusEffects::StatusRegen, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_regen.status_regen'")).Object);
	StatusIcons.Emplace(EStatusEffects::StatusResistFire, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_fireshield.status_fireshield'")).Object);
	StatusIcons.Emplace(EStatusEffects::StatusResistAir, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_airshield.status_airshield'")).Object);
	StatusIcons.Emplace(EStatusEffects::StatusFireSpikes, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_firespikes.status_firespikes'")).Object);
	StatusIcons.Emplace(EStatusEffects::StatusHeroism, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_heroism.status_heroism'")).Object);
	StatusIcons.Emplace(EStatusEffects::StatusStatsUp, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_statsup.status_statsup'")).Object);
	StatusIcons.Emplace(EStatusEffects::StatusInWater, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_inwater.status_inwater'")).Object);

	PartyStatusIcons.Emplace(EPartyStatusEffects::StatusWizardsEye, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_wizardseye.status_wizardseye'")).Object);
	PartyStatusIcons.Emplace(EPartyStatusEffects::StatusWaterWalk, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_waterwalk.status_waterwalk'")).Object);
	PartyStatusIcons.Emplace(EPartyStatusEffects::StatusFly, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_fly.status_fly'")).Object);
	PartyStatusIcons.Emplace(EPartyStatusEffects::StatusFeatherFall, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_featherfall.status_featherfall'")).Object);
	PartyStatusIcons.Emplace(EPartyStatusEffects::StatusTorchLight, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_torchlight.status_torchlight'")).Object);
	PartyStatusIcons.Emplace(EPartyStatusEffects::StatusInCombat, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_incombat.status_incombat'")).Object);
	PartyStatusIcons.Emplace(EPartyStatusEffects::StatusFlee, ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/UI/Status/status_flee.status_flee'")).Object);


	MenuType = EMenuType::MenuNone;
	SpellbookSchool = ESkills::SkillTotalSkills;
	SelectedSpell = ESpells::SpellTotalSpells;
	TargetMode = ETargetMode::TargetNone;
	Building = nullptr;
	CurrentDialogKey = FName();

	MinimapSize = 1.0f;
	MinimapOrigin = FVector(0.0f, 0.0f, 0.0f);

	BackgroundMaterial = nullptr;
	InnerBackgroundMaterial = nullptr;
	SlotBackgroundMaterial = nullptr;
	LogBackgroundMaterial = nullptr;
	SpellbookBackgroundMaterial = nullptr;
	PlayerBackgroundMaterial = nullptr;
	TooltipBackgroundMaterial = nullptr;
	DialogBackgroundMaterial = nullptr;

	InnerBorderMaterial = nullptr;
	SlotBorderMaterial = nullptr;
	LogBorderMaterial = nullptr;
	SpellbookBorderMaterial = nullptr;
	PlayerBorderMaterial = nullptr;
	TooltipBorderMaterial = nullptr;
	DialogBorderMaterial = nullptr;

	InnerBorderWidth = 3.0f;
	SlotBorderWidth = 1.0f;
	LogBorderWidth = 1.0f;
	SpellbookBorderWidth = 1.0f;
	TooltipBorderWidth = 2.0f;
	DialogBorderWidth = 1.0f;

	FontColor = kWhiteColor;
	HeaderColor = kGoldColor;

	ActiveCharacterWhenPaused = 0;

	HoveredHitbox = FName();

	EquipPositions.Emplace(EEquipType::EquipMainHand, HUDPosition(TEXT("Weapon"), 5.0f, 200.0f, 100.0f, 200.0f));
	EquipPositions.Emplace(EEquipType::EquipOffHand, HUDPosition(TEXT("Shield"), 245.0f, 175.0f, 150.0f, 250.0f));
	EquipPositions.Emplace(EEquipType::EquipRanged, HUDPosition(TEXT("Bow"), 95.0f, 55.0f, 50.0f, 300.0f));
	EquipPositions.Emplace(EEquipType::EquipHead, HUDPosition(TEXT("Head"), 150.0f, 5.0f, 100.0f, 100.0f));
	EquipPositions.Emplace(EEquipType::EquipBody, HUDPosition(TEXT("Body"), 100.0f, 180.0f, 200.0f, 300.0f));
	//EquipPositions.Emplace(EEquipType::EquipHands, HUDPosition(0, 0, 0, 0));
	//EquipPositions.Emplace(EEquipType::EquipLegs, HUDPosition(0, 0, 0, 0));
	EquipPositions.Emplace(EEquipType::EquipFeet, HUDPosition(TEXT("Feet"), 150.0f, 495.0f, 100.0f, 100.0f));
	//EquipPositions.Emplace(EEquipType::EquipBack, HUDPosition(0, 0, 0, 0));
	//EquipPositions.Emplace(EEquipType::EquipNeck, HUDPosition(0, 0, 0, 0));
	//EquipPositions.Emplace(EEquipType::EquipFinger1, HUDPosition(0, 0, 0, 0));
	//EquipPositions.Emplace(EEquipType::EquipFinger2, HUDPosition(0, 0, 0, 0));
	//EquipPositions.Emplace(EEquipType::EquipFinger3, HUDPosition(0, 0, 0, 0));
	//EquipPositions.Emplace(EEquipType::EquipFinger4, HUDPosition(0, 0, 0, 0));

	//EquipOrder.Add(EEquipType::EquipBack);
	//EquipOrder.Add(EEquipType::EquipLegs);
	EquipOrder.Add(EEquipType::EquipRanged);
	EquipOrder.Add(EEquipType::EquipBody);
	EquipOrder.Add(EEquipType::EquipHead);
	EquipOrder.Add(EEquipType::EquipFeet);
	//EquipOrder.Add(EEquipType::EquipNeck);
	//EquipOrder.Add(EEquipType::EquipHands);
	EquipOrder.Add(EEquipType::EquipMainHand);
	EquipOrder.Add(EEquipType::EquipOffHand);
	//EquipOrder.Add(EEquipType::EquipFinger1);
	//EquipOrder.Add(EEquipType::EquipFinger2);
	//EquipOrder.Add(EEquipType::EquipFinger3);
	//EquipOrder.Add(EEquipType::EquipFinger4);

	// Init stat strings
	StatNames.AddZeroed((int32)EStats::StatTotalStats);
	StatNames[(int32)EStats::StatStrength] = TEXT("Strength");
	StatNames[(int32)EStats::StatIntelligence] = TEXT("Intelligence");
	StatNames[(int32)EStats::StatDexterity] = TEXT("Dexterity");
	StatNames[(int32)EStats::StatSpeed] = TEXT("Speed");
	StatNames[(int32)EStats::StatVitality] = TEXT("Vitality");
	StatNames[(int32)EStats::StatLuck] = TEXT("Luck");
	StatNames[(int32)EStats::StatHealth] = TEXT("Max Health");
	StatNames[(int32)EStats::StatMana] = TEXT("Max Mana");
	StatNames[(int32)EStats::StatEvasion] = TEXT("Accuracy");
	StatNames[(int32)EStats::StatAccuracy] = TEXT("Evasion");
	StatNames[(int32)EStats::StatResistFire] = TEXT("Resist Fire");
	StatNames[(int32)EStats::StatResistAir] = TEXT("Resist Air");

	StatTooltips.AddZeroed((int32)EStats::StatTotalStats);
	StatTooltips[(int32)EStats::StatStrength] =
		TEXT("Strength controls how strong this character.\nEvery 4 points after 10 increases your maximum damage by 1.");
	StatTooltips[(int32)EStats::StatIntelligence] =
		TEXT("Intelligence commands the power of magic.\nDirectly controls starting mana.\nIncreases the effect of many spells.");
	StatTooltips[(int32)EStats::StatDexterity] =
		TEXT("Dexterity governs the character's accuracy and precision.\nEvery 8 points after 10 increases your minimum damage by 1.\nEvery 2 points increases accuracy by 1.");
	StatTooltips[(int32)EStats::StatSpeed] =
		TEXT("Speed indicates how quick the character is.\nDecreases base action delay.\nEvery 2 points increases evasion by 1.");
	StatTooltips[(int32)EStats::StatVitality] =
		TEXT("Vitality shows the character's fortitude.\nDirectly controls starting health.\nEvery 5 points after 10 reduces raw damage taken by 1.");
	StatTooltips[(int32)EStats::StatLuck] =
		TEXT("Luck is an enigma.\nIt has minor effects on many different things, such as\ndrop rates or critical multiplier.");
	StatTooltips[(int32)EStats::StatHealth] =
		TEXT("The maximum number of hit points the character can have.");
	StatTooltips[(int32)EStats::StatMana] =
		TEXT("The maximum number of spell points the character can have.");
	StatTooltips[(int32)EStats::StatAccuracy] =
		TEXT("How likely a melee attack is to hit an enemy.\nEvery 2 points increases chance to hit by 1%.");
	StatTooltips[(int32)EStats::StatEvasion] =
		TEXT("How likely a character is able to dodge a melee attack.\nEvery 2 points increases chance to dodge by 1%.");
	StatTooltips[(int32)EStats::StatResistFire] =
		TEXT("Resistance to fire damage.");
	StatTooltips[(int32)EStats::StatResistAir] =
		TEXT("Resistance to air damage.");

	// Init skill strings
	SkillNames.AddZeroed((int32)ESkills::SkillTotalSkills);
	SkillNames[(int32)ESkills::SkillWeaponSword] = TEXT("Sword");
	SkillNames[(int32)ESkills::SkillWeaponAxe] = TEXT("Axe");
	SkillNames[(int32)ESkills::SkillWeaponDagger] = TEXT("Dagger");
	SkillNames[(int32)ESkills::SkillWeaponBow] = TEXT("Bow");
	SkillNames[(int32)ESkills::SkillArmorShield] = TEXT("Shield");
	SkillNames[(int32)ESkills::SkillArmorLeather] = TEXT("Leather");
	SkillNames[(int32)ESkills::SkillArmorChain] = TEXT("Chain");
	SkillNames[(int32)ESkills::SkillArmorPlate] = TEXT("Plate");
	SkillNames[(int32)ESkills::SkillAlchemy] = TEXT("Alchemy");
	SkillNames[(int32)ESkills::SkillArmsmaster] = TEXT("Armsmaster");
	SkillNames[(int32)ESkills::SkillBodybuilding] = TEXT("Bodybuilding");
	SkillNames[(int32)ESkills::SkillDisarmTrap] = TEXT("Disarm Trap");
	SkillNames[(int32)ESkills::SkillIdentifyItem] = TEXT("Identify Item");
	SkillNames[(int32)ESkills::SkillIdentifyMonster] = TEXT("Identify Monster");
	SkillNames[(int32)ESkills::SkillLearning] = TEXT("Learning");
	SkillNames[(int32)ESkills::SkillMeditation] = TEXT("Meditation");
	SkillNames[(int32)ESkills::SkillMerchant] = TEXT("Merchant");
	SkillNames[(int32)ESkills::SkillSwimming] = TEXT("Swimming");
	SkillNames[(int32)ESkills::SkillFireMagic] = TEXT("Fire Magic");
	SkillNames[(int32)ESkills::SkillAirMagic] = TEXT("Air Magic");
	SkillNames[(int32)ESkills::SkillBodyMagic] = TEXT("Body Magic");

	SkillTooltips.AddZeroed((int32)ESkills::SkillTotalSkills);
	SkillTooltips[(int32)ESkills::SkillWeaponSword] = MasteryTooltip(
		TEXT("Skill when wielding a sword. Swords are balanced weapons that provide a moderate damage range, and moderate defenses."),
		TEXT("Reduces weapon delay by 3% per skill point"),
		TEXT("Skill added to armor"),
		TEXT("30% chance to deal double weapon damage"));
	SkillTooltips[(int32)ESkills::SkillWeaponAxe] = MasteryTooltip(
		TEXT("Skill when wielding an axe. Axes are offensive weapons that have wide damage ranges, and no defense bonuses."),
		TEXT("Adds one bonus damage to weapon per 2 skill points"),
		TEXT("Add extra attack die to weapon"),
		TEXT("Chance to deal triple weapon damage equal to 1% per skill point"));
	SkillTooltips[(int32)ESkills::SkillWeaponDagger] = MasteryTooltip(
		TEXT("Skill when wielding a dagger. Daggers are quick, but not very strong. Their light weight allows mages to use them."),
		TEXT("Skill added to armor"),
		TEXT("Chance to deal double damage each to 10% per skill point"),
		TEXT("All attacks deal triple damage"));
	SkillTooltips[(int32)ESkills::SkillWeaponBow] = MasteryTooltip(
		TEXT("Skill when firing a bow. A talented archer fires shots quickly and precisely."),
		TEXT("Reduces weapon delay by 3% per skill point"),
		TEXT("Adds one bonus damage to weapon per skill point"),
		TEXT("Fires two arrows at once"));
	SkillTooltips[(int32)ESkills::SkillArmorShield] = MasteryTooltip(
		TEXT("Skill when holding a shield in the off hand. A shield will increase physical defense, and may give a chance to deflect a blow."),
		TEXT("Skill added to armor"),
		TEXT("50% chance to reduce physical damage by 10%"),
		TEXT("10% chance to negate physical damage"));
	SkillTooltips[(int32)ESkills::SkillArmorLeather] = MasteryTooltip(
		TEXT("Ability to wear leather armor effectively. Defense values are typically low, and small recovery penalty (.05s) per item worn."),
		TEXT("Base delay is +0.05s per item worn"),
		TEXT("Base delay penalty is eliminated"),
		TEXT("1/4th of skill points added to armor class per item worn"));
	SkillTooltips[(int32)ESkills::SkillArmorChain] = MasteryTooltip(
		TEXT("Ability to wear chain armor effectively. Strong defense values, while delay is increased by .1s per item worn."),
		TEXT("Base delay is +0.1s per item worn"),
		TEXT("Half of skill points added to armor class per item worn"),
		TEXT("Base delay penalty is eliminated"));
	SkillTooltips[(int32)ESkills::SkillArmorPlate] = MasteryTooltip(
		TEXT("Ability to wear plate armor effectively. Extremely strong defensive capabilities, but heavy penalty (.25s) per item worn."),
		TEXT("Skill points added to armor class per item worn"),
		TEXT("Base delay penalty is halved"),
		TEXT("Base delay penalty is eliminated"));
	SkillTooltips[(int32)ESkills::SkillAlchemy] = MasteryTooltip(
		TEXT("Learn how to mix reagants to create powerful potions. Advancing alchemy will allow a character to create more complex potions."),
		TEXT("Able to create tier 1 potions, and mix tier 2"),
		TEXT("Able to mix tier 3 and tier 4 potions"),
		TEXT("Able to mix tier 5 potions"));
	SkillTooltips[(int32)ESkills::SkillArmsmaster] = MasteryTooltip(
		TEXT("An armsmaster excels in melee combat. Training will reduce the recovery delay from the currently-held melee weapon."),
		TEXT("Reduces weapon delay by 1% per point"),
		TEXT("Doubles the effect"),
		TEXT("Quadrouples the effect"));
	SkillTooltips[(int32)ESkills::SkillBodybuilding] = MasteryTooltip(
		TEXT("Bodybuilding enhances the character's physical toughness. Raises maximum HP."),
		TEXT("+5 HP per point"),
		TEXT("Doubles the effect"),
		TEXT("Quadrouples the effect"));
	SkillTooltips[(int32)ESkills::SkillDisarmTrap] = MasteryTooltip(
		TEXT("Ability to disarm traps in chests and other objects. Success is not guaranteed."),
		TEXT("Increases chance to disarm successfully"),
		TEXT("Removes penalty for expert chests"),
		TEXT("Removes penalty for master chests"));
	SkillTooltips[(int32)ESkills::SkillIdentifyItem] = MasteryTooltip(
		TEXT("Ability to understand the specifics of items found in the world."),
		TEXT("Basic items can be identified"),
		TEXT("More items can be identified"),
		TEXT("All items can be identified"));
	SkillTooltips[(int32)ESkills::SkillIdentifyMonster] = MasteryTooltip(
		TEXT("Knowledge of enemies allows understanding of stats, resistances, attack types, and more."),
		TEXT("Able to see current and maximum health of enemy"),
		TEXT("Stats and resistances are displayed"),
		TEXT("All monster information is shown"));
	SkillTooltips[(int32)ESkills::SkillLearning] = MasteryTooltip(
		TEXT("Becoming a better student results in more experience points gained."),
		TEXT("Earn a base 5% + 1% per skill point more experience"),
		TEXT("Skill point bonus increased to 2% per skill point"),
		TEXT("Skill point bonus increased to 3% per skill point"));
	SkillTooltips[(int32)ESkills::SkillMeditation] = MasteryTooltip(
		TEXT("Meditation clears the mind of a spellcaster, allowing them to channel more energy into spellcasting. Increases maximum MP."),
		TEXT("+5 MP per point"),
		TEXT("Doubles the effect"),
		TEXT("Quadrouples the effect"));
	SkillTooltips[(int32)ESkills::SkillMerchant] = MasteryTooltip(
		TEXT("Barter your way to lower prices!"),
		TEXT("Prices for buying, selling, and identifying are 1% lower/higher per skill point, up to 50%"),
		TEXT("Doubles the effect"),
		TEXT("Triples the effect"));
	SkillTooltips[(int32)ESkills::SkillSwimming] = MasteryTooltip(
		TEXT("Reduces the burden of equipment while in water, reducing or eliminating drowning damage. Wearing leather,\nchain, and plate armor, as well as having a packed inventory, will both contribute to drowning."),
		TEXT("Eliminates weight penalty from 5 pieces of leather armor, and 8% of inventory space per skill point"),
		TEXT("Eliminates weight penalty from 5 pieces of chain mail"),
		TEXT("Eliminates weight penalty from 5 pieces of plate mail"));
	SkillTooltips[(int32)ESkills::SkillFireMagic] = MasteryTooltip(
		TEXT("Skill using fire-based magic."),
		TEXT("Learn and cast basic spells"),
		TEXT("Learn and cast expert spells, and unlock expert tier of all other spells"),
		TEXT("Learn and cast master spells, and unlock master tier of all other spells"));
	SkillTooltips[(int32)ESkills::SkillAirMagic] = MasteryTooltip(
		TEXT("Skill using air-based magic."),
		TEXT("Learn and cast basic spells"),
		TEXT("Learn and cast expert spells, and unlock expert tier of all other spells"),
		TEXT("Learn and cast master spells, and unlock master tier of all other spells"));
	SkillTooltips[(int32)ESkills::SkillBodyMagic] = MasteryTooltip(
		TEXT("Skill using body-based magic."),
		TEXT("Learn and cast basic spells"),
		TEXT("Learn and cast expert spells, and unlock expert tier of all other spells"),
		TEXT("Learn and cast master spells, and unlock master tier of all other spells"));

	// Init spell strings
	SpellTooltips.AddZeroed((int32)ESpells::SpellTotalSpells);
	SpellTooltips[(int32)ESpells::SpellTorchLight] = MasteryTooltip(
		TEXT("Illuminates the area around the party."),
		TEXT("Normal light that lasts 1 hour per point of skill"),
		TEXT("Bright light that lasts 2 hour per point of skill"),
		TEXT("Brightest light that lasts 3 hour per point of skill"));
	SpellTooltips[(int32)ESpells::SpellFireShield] = MasteryTooltip(
		TEXT("Creates a forcefield around each party member, increasing their resistance to fire."),
		TEXT("Increases resistance by 3 + 1 per skill point, and lasts 1 hour per point of skill"),
		TEXT("Increases resistance by 5 + 2 per skill point"),
		TEXT("Increases resistance by 7 + 3 per skill point"));
	SpellTooltips[(int32)ESpells::SpellFlameBolt] = MasteryTooltip(
		TEXT("Throws a small bolt of fire at a single target. The target takes 1d4 +x damage,\nwhere x is the number of skill points in Fire Magic."),
		TEXT("Deals 1d4 base damage"),
		TEXT("Deals 1d10 base damage"),
		TEXT("Deals 2d10 base damage"));
	SpellTooltips[(int32)ESpells::SpellFireSpikes] = MasteryTooltip(
		TEXT("When a character is attacked (physical, magical, melee, projectile), the fire spikes trigger\nan explosion, dealing damage to all characters except the caster's party within a 4m radius.\nThe explosion deals 2d(5+x) damage, where x is skill points in Fire Magic."),
		TEXT("N/A"),
		TEXT("Lasts 5 minutes per point of skill"),
		TEXT("Lasts 1 hour per point of skill"));
	SpellTooltips[(int32)ESpells::SpellFireBall] = MasteryTooltip(
		TEXT("Launches a fire ball that deals damage to everything within 5 meters on contact.\nEach target takes 2d(4+x) damage, where x is the number of skill points in Fire Magic."),
		TEXT("N/A"),
		TEXT("Two damage dice are rolled"),
		TEXT("One extra damage die is rolled, making the total damage 3d(4+x)"));
	SpellTooltips[(int32)ESpells::SpellWizardsEye] = MasteryTooltip(
		TEXT("Reveals notable objects on the minimap. A higher mastery reveals more types of objects."),
		TEXT("Lasts 1 hour per point of skill, and reveals friendly and enemy NPCs"),
		TEXT("Lasts 5 hours per point of skill, and also reveals items"),
		TEXT("Eliminates spell delay (but not base delay), and also reveals chests"));
	SpellTooltips[(int32)ESpells::SpellAirShield] = MasteryTooltip(
		TEXT("Creates a forcefield around each party member, increasing their resistance to air."),
		TEXT("Increases resistance by 3 + 1 per skill point"),
		TEXT("Increases resistance by 5 + 2 per skill point"),
		TEXT("Increases resistance by 7 + 3 per skill point"));
	SpellTooltips[(int32)ESpells::SpellShock] = MasteryTooltip(
		TEXT("Zaps an enemy with a fixed amount of air damage."),
		TEXT("Deals 5d1 damage"),
		TEXT("Deals 5d2 damage"),
		TEXT("Deals 5d3 damage"));
	SpellTooltips[(int32)ESpells::SpellFeatherFall] = MasteryTooltip(
		TEXT("When falling from great distances, slows your descent so that you land softly on the ground."),
		TEXT("N/A"),
		TEXT("Lasts 5 minutes per point of skill"),
		TEXT("Lasts 1 hour per point of skill"));
	SpellTooltips[(int32)ESpells::SpellWaterWalk] = MasteryTooltip(
		TEXT("Allows the party to levitate centimeters above water, thus avoiding the need to swim."),
		TEXT("N/A"),
		TEXT("Lasts 5 minutes per point of skill"),
		TEXT("Lasts 1 hour per point of skill"));
	SpellTooltips[(int32)ESpells::SpellJump] = MasteryTooltip(
		TEXT("Leap through the air at incredible heights. Be careful: the impact when landing could be deadly!"),
		TEXT("N/A"),
		TEXT("Launches the party with an extreme upwards force"),
		TEXT("Automatically casts Feather Fall for free when jumping, even without having learned it"));
	SpellTooltips[(int32)ESpells::SpellFly] = MasteryTooltip(
		TEXT("Gain the power of magical flight. While flying, you move faster and cannot take fall damage. You cannot fly indoors."),
		TEXT("N/A"),
		TEXT("N/A"),
		TEXT("Lasts 30 minutes per point of skill"));
	SpellTooltips[(int32)ESpells::SpellTeleportTown] = MasteryTooltip(
		TEXT("Teleport back to the main fountain in the town."),
		TEXT("N/A"),
		TEXT("N/A"),
		TEXT("Allows teleportation"));
	SpellTooltips[(int32)ESpells::SpellCureLight] = MasteryTooltip(
		TEXT("Lightly heals a damaged character's wounds. Has no effect on dead characters.\nRecovers 1d(4+x) + 3 health, where x is the number of skill points in Body Magic."),
		TEXT("One damage die is rolled"),
		TEXT("One extra die is rolled, making the total health recovered 2d(4+x) + 3"),
		TEXT("Two extra dice are rolled, making the total health recovered 3d(4+x) + 3"));
	SpellTooltips[(int32)ESpells::SpellHeroism] = MasteryTooltip(
		TEXT("Imbue a party member with tremendous strength, increasing their melee damage bonus by 2 per point of skill in Body Magic."),
		TEXT("Lasts 10 minutes per point of skill"),
		TEXT("Lasts 1 hour per point of skill"),
		TEXT("Effects entire party upon casting"));
	SpellTooltips[(int32)ESpells::SpellCauseLight] = MasteryTooltip(
		TEXT("Fires a small physical projectile that deals 1d4 +x damage where x is skill points in Body Magic."),
		TEXT("Deals 1d4 +x damage"),
		TEXT("Faster recovery time"),
		TEXT("Eliminates spell delay (but not base delay)"));
	SpellTooltips[(int32)ESpells::SpellFlee] = MasteryTooltip(
		TEXT("Flees from combat, removing the 'In Combat' status and preventing it until Flee wears off or you attack an enemy."),
		TEXT("Lasts 2 minutes + 1 minute extra per point of skill"),
		TEXT("Lasts 2 minutes + 2 minutes extra per point of skill"),
		TEXT("Lasts 2 minutes + 3 minute extra per point of skill"));
	SpellTooltips[(int32)ESpells::SpellRegen] = MasteryTooltip(
		TEXT("Regenerates a character's health over time. Every in-game minute, they recover 2 health."),
		TEXT("N/A"),
		TEXT("Lasts 10 minutes per point of skill"),
		TEXT("Lasts 1 hour per point of skill"));
	SpellTooltips[(int32)ESpells::SpellCureHeavy] = MasteryTooltip(
		TEXT("Heal a character's dire wounds instantly. Has no effect on dead characters.\nRecovers 5d(5+x) health, where x is the number of skill points in Body Magic."),
		TEXT("N/A"),
		TEXT("N/A"),
		TEXT("Heals 5d(5+x)"));
	SpellTooltips[(int32)ESpells::SpellCauseHeavy] = MasteryTooltip(
		TEXT("Fires a devastating physical projectile that deals 5d5 +x damage where x is skill points in Body Magic."),
		TEXT("N/A"),
		TEXT("N/A"),
		TEXT("Deals 5d5 +x damage"));

	StatusEffectNames.Add(TEXT("Poison"));
	StatusEffectTooltips.Add(TEXT("Slowly deals damage over time."));
	StatusEffectNames.Add(TEXT("Regen"));
	StatusEffectTooltips.Add(TEXT("Recovers health over time."));
	StatusEffectNames.Add(TEXT("Fire Shield"));
	StatusEffectTooltips.Add(TEXT("Increases resistance against fire damage."));
	StatusEffectNames.Add(TEXT("Air Shield"));
	StatusEffectTooltips.Add(TEXT("Increases resistance against air damage."));
	StatusEffectNames.Add(TEXT("Fire Spikes"));
	StatusEffectTooltips.Add(TEXT("Upon receiving damage, triggers an explosion dealing fire damage to all enemies within 4m."));
	StatusEffectNames.Add(TEXT("Heroism"));
	StatusEffectTooltips.Add(TEXT("Increases melee attack power."));
	StatusEffectNames.Add(TEXT("Stats Up"));
	StatusEffectTooltips.Add(TEXT("Increases all stats."));
	StatusEffectNames.Add(TEXT("In Water"));
	StatusEffectTooltips.Add(TEXT("You're currently in water. Without water walking or a high enough swimming skill,\nyou will begin drowning and take damage. Unconscious characters cannot swim."));

	PartyStatusEffectNames.Add(TEXT("Wizard's Eye"));
	PartyStatusEffectTooltips.Add(TEXT("Shows interesting things on the minimap."));

	PartyStatusEffectNames.Add(TEXT("Water Walk"));
	PartyStatusEffectTooltips.Add(TEXT("Can walk on water as if it were land, taking no drowning damage."));

	PartyStatusEffectNames.Add(TEXT("Fly"));
	PartyStatusEffectTooltips.Add(TEXT("Can fly through the air, unless indoors."));

	PartyStatusEffectNames.Add(TEXT("Feather Fall"));
	PartyStatusEffectTooltips.Add(TEXT("Take no damage when falling from great heights."));

	PartyStatusEffectNames.Add(TEXT("Torch Light"));
	PartyStatusEffectTooltips.Add(TEXT("Illuminates the surrounding area."));

	PartyStatusEffectNames.Add(TEXT("In Combat"));
	PartyStatusEffectTooltips.Add(TEXT("While in combat, all characters receive +5 armor and +10 evasion, but movement speed is halved."));

	PartyStatusEffectNames.Add(TEXT("Flee"));
	PartyStatusEffectTooltips.Add(TEXT("While fleeing, you cannot gain the 'In Combat' status until this status expires or you attack an enemy. Fleeing grants -5 armor and -10 evasion."));

	ClassNames.AddZeroed((int32)EClass::ClassTotalClasses + 1);
	ClassNames[(int32)EClass::ClassKnight] = TEXT("Knight");
	ClassNames[(int32)EClass::ClassPaladin] = TEXT("Paladin");
	ClassNames[(int32)EClass::ClassArcher] = TEXT("Archer");
	ClassNames[(int32)EClass::ClassDruid] = TEXT("Druid");
	ClassNames[(int32)EClass::ClassCleric] = TEXT("Cleric");
	ClassNames[(int32)EClass::ClassSorcerer] = TEXT("Sorcerer");
	ClassNames[(int32)EClass::ClassTotalClasses] = TEXT("Adventurer");
}

void ANNSHUD::Reset()
{
	LogLines.Empty();
}

void ANNSHUD::BeginPlay()
{
	Super::BeginPlay();

	if (Font == nullptr) {
		Font = GEngine->GetMediumFont();
	}

	Minimap = NewObject<UTextureRenderTarget2D>();
	Minimap->CompressionSettings = TextureCompressionSettings::TC_EditorIcon;
	Minimap->LODGroup = TextureGroup::TEXTUREGROUP_UI;
	Minimap->InitAutoFormat(600, 600);

	PlayerParty = Cast<APlayerPartyCharacter>(GetOwningPawn());
	GameInstance = Cast<UNNSGameInstance>(GetGameInstance());
	GameInstance->HUD = this;

	QuestsMenu = CreateWidget<UUserWidget>(Cast<APlayerController>(PlayerParty->GetController()), QuestsMenuClass);

	TextHeight = Font->GetMaxCharHeight();
	LogHeight = TextHeight * kMaxLogLines;
	DialogSidebarKeyY = 1 + TextHeight + DialogSidebarPortraitSize + Margin * 3 + DialogSidebarSpacing;
	DialogSidebarExitY = DialogSidebarKeyY + (DialogSidebarSpacing + TextHeight * DialogTextScale) * 6.0f;
}

void ANNSHUD::UpdateFromViewportCoordinates()
{
	int32 ScreenWidth, ScreenHeight;
	Cast<APlayerController>(PlayerParty->GetController())->GetViewportSize(ScreenWidth, ScreenHeight);

	const float OldBackgroundX = BackgroundX;

	LogX = ScreenWidth - LogWidth - Margin * 2 - 1;
	LogY = ScreenHeight - LogHeight - Margin * 2 - 1;

	MinimapX = ScreenWidth - MinimapWidth - Margin - 1;
	TimeX = MinimapX + (MinimapWidth / 2) - (TimeWidth / 2);
	TimeHeight = TextHeight + Margin * 2;

	MapX = (ScreenWidth - MapSize) / 2;
	MapY = (ScreenHeight - MapSize) / 2;

	CrosshairX = ScreenWidth / 2;
	CrosshairY = ScreenHeight / 2;

	BackgroundWidth = TypeWidth + CharacterWidth + Margin * 7 + 4;
	BackgroundHeight = CharacterHeight + Margin * 4 + 2;
	BackgroundX = (ScreenWidth - BackgroundWidth) / 2;
	BackgroundY = (ScreenHeight - BackgroundHeight) / 2;
	TypeX = BackgroundX + Margin * 2 + 1;
	TypeY = BackgroundY + Margin * 2 + 1;
	CharacterX = BackgroundX + BackgroundWidth - CharacterWidth - Margin * 2 - 1;
	CharacterY = BackgroundY + Margin * 2 + 1;
	StatusX = BackgroundX + Margin;
	StatusY = TypeY + TypeHeight + Margin * 2 + 1;

	DialogSidebarHeight = ScreenHeight - 2;
	DialogSidebarX = ScreenWidth - DialogSidebarWidth - 1;
	DialogBackgroundX = ((DialogSidebarX - 1) - DialogBackgroundWidth) / 2;
	DialogBackgroundY = ScreenHeight / 2.0f;

	ChestX = (ScreenWidth - TypeWidth) / 2;
	ChestY = (ScreenHeight - TypeHeight) / 2;
	ChestCloseX = ChestX + TypeWidth + 2;
	ChestCloseY = ChestY;

	StoreX = (ScreenWidth - (DialogSidebarWidth + 1) - StoreWidth) / 2;
	StoreY = (ScreenHeight - StoreHeight) / 2;

	SpellbookX = (ScreenWidth - SpellbookWidth) / 2;
	SpellbookY = (ScreenHeight - SpellbookHeight) / 2;
	SpellbookButtonX = SpellbookX + SpellbookWidth + Margin;

	HotkeyHintX = ScreenWidth - 150.0f;
	HotkeyHintY = LogY - (TextHeight + SmallMargin) * 10.0f - 10.0f;

	if (OldBackgroundX != BackgroundX) {
		RefreshHitboxes();
	}
}

void ANNSHUD::Pause(bool SetPaused)
{
	ANNSPlayerController* Controller = Cast<ANNSPlayerController>(PlayerParty->GetController());
	if (SetPaused) {
		PlayerParty->SetEnableMotionBlur(false);
		// In case the user changed the active character during a menu, reset it to an active one
		PlayerParty->AdvanceActiveCharacter();
		Controller->SetPause(true);
		// Pausing seems to disable clicks? So re-enable it here
		Controller->bEnableClickEvents = true;
		Controller->bShowMouseCursor = true;
		Controller->bLockMouseInCenter = false;
		FInputModeGameAndUI Mode;
		Mode.SetHideCursorDuringCapture(false);
		Controller->SetInputMode(Mode);
	} else {
		Controller->bShowMouseCursor = false;
		Controller->bLockMouseInCenter = true;
		// Setting input mode refocuses game. Set flag so cursor doesn't disappear in UI mode
		FInputModeGameOnly Mode;
		Mode.SetConsumeCaptureMouseDown(false);
		Controller->SetInputMode(Mode);
		Controller->SetPause(false);
		PlayerParty->SetEnableMotionBlur(true);
	}
}

void ANNSHUD::ToggleHUDMenuType(EMenuType Type, bool bPlaySound)
{
	if (Building.IsValid() || DialogComponent.IsValid() || Chest.IsValid() || (Type != EMenuType::MenuNone && MenuType == EMenuType::MenuSpellbook)) return;

	if (MenuType == EMenuType::MenuSpellbook) {
		ResetSpell();
	} else if (Type == EMenuType::MenuSpellbook) {
		// Set the spellbook to start on a page that the caster actually knows
		if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->LastSpellbookPage == ESkills::SkillTotalSkills) {
			for (auto& School : EnumMaps::SchoolSpells) {
				if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->GetSkill(School.Key) > 0) {
					PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->LastSpellbookPage = School.Key;
					break;
				}
			}
		}
		SpellbookSchool = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->LastSpellbookPage;
	}

	// Switch to menu, or back to no menu
	const EMenuType BeforeType = MenuType;
	if (MenuType != Type) {
		MenuType = Type;
	} else {
		MenuType = EMenuType::MenuNone;
	}	


	if (BeforeType == EMenuType::MenuQuests || BeforeType == EMenuType::MenuBestiary) {
		QuestsMenu->RemoveFromParent();
	}
	if (MenuType == EMenuType::MenuQuests || MenuType == EMenuType::MenuBestiary) {
		QuestsMenu->AddToViewport();
	}

	// If menu has appeared or disappeared, pause game/etc.
	if (MenuType == EMenuType::MenuNone && BeforeType != EMenuType::MenuNone) {
		if (GameInstance->bIsInTurnMode && BeforeType != EMenuType::MenuSpellbook) {
			PlayerParty->ActiveCharacter = ActiveCharacterWhenPaused;
		} else {
			PlayerParty->AdvanceActiveCharacter();
		}
		Pause(false);
	} else if (MenuType != EMenuType::MenuNone && BeforeType == EMenuType::MenuNone) {
		ActiveCharacterWhenPaused = PlayerParty->ActiveCharacter;
		Pause(true);
	}

	if (bPlaySound) {
		if (MenuType == EMenuType::MenuNone) {
			GameInstance->PlaySoundEffect2D(CloseMenuSound);
		} else {
			GameInstance->PlaySoundEffect2D(OpenMenuSound);
		}
	}

	RefreshHitboxes();
}

bool ANNSHUD::Escape()
{
	if (MenuType != EMenuType::MenuNone) {
		ToggleHUDMenuType(EMenuType::MenuNone);
		return true;
	} else if (Chest.IsValid()) {
		Chest = nullptr;
		RefreshHitboxes();
		Pause(false);
		return true;
	} else if (Building.IsValid() || DialogComponent.IsValid()) {
		if (CurrentDialogKey.IsEqual(StoreBuy) || CurrentDialogKey.IsEqual(StoreInventory) || CurrentDialogKey.IsEqual(StoreSell)
				|| CurrentDialogKey.IsEqual(StoreIdentify) || CurrentDialogKey.IsEqual(StoreSkills)) {
			CurrentDialogKey = FName();
			RefreshHitboxes();
		} else {
			CurrentDialogKey = FName();
			DialogComponent = nullptr;
			Building = nullptr;
			RefreshHitboxes();
			Pause(false);
		}
		return true;
	}
	return false;
}

void ANNSHUD::EnterDialog(UDialogComponent* Dialog)
{
	DialogComponent = Dialog;
	Pause(true);
	RefreshHitboxes();

	const float CenterX = DialogSidebarX + DialogSidebarWidth / 2.0f;
	const float CenterY = DialogSidebarExitY + (TextHeight * DialogTextScale) / 2.0f;
	Cast<ULocalPlayer>(Cast<APlayerController>(PlayerParty->GetController())->GetLocalPlayer())->ViewportClient->Viewport->SetMouse(CenterX, CenterY);
}

void ANNSHUD::OpenChest(AChest* InChest)
{
	Chest = InChest;
	Pause(true);
	RefreshHitboxes();
}

void ANNSHUD::EnterBuilding(AStaticInteractable* InBuilding)
{
	if (DoorSound) {
		GameInstance->PlaySoundEffect2D(DoorSound);
	}

	if (!InBuilding->IsA<ATavernBuilding>() && InBuilding->IsA<ANPCBuilding>()) {
		EnterDialog(Cast<ANPCBuilding>(InBuilding)->DialogComponent);
	} else {
		Building = InBuilding;
		Pause(true);
		RefreshHitboxes();

		const float CenterX = DialogSidebarX + DialogSidebarWidth / 2.0f;
		const float CenterY = DialogSidebarExitY + (TextHeight * DialogTextScale) / 2.0f;
		Cast<ULocalPlayer>(Cast<APlayerController>(PlayerParty->GetController())->GetLocalPlayer())->ViewportClient->Viewport->SetMouse(CenterX, CenterY);
	}
}

void ANNSHUD::DrawHUD()
{
	// Check for game window resizing every frame... not great but not sure where resize event is
	UpdateFromViewportCoordinates();

	static const FLinearColor ActiveCharacterColor = kGoldColor;
	static const FLinearColor HealthColor = kRedColor;
	static const FLinearColor ManaColor = kBlueColor;
	static const FLinearColor EmptyBarColor = kMediumGrayColor;
	static const FLinearColor TurnValidColor = kGreenColor;
	static const FLinearColor TurnInvalidColor = kRedColor;

	float CurrentY = PlayerYStart;

	for (auto& Member : PlayerParty->PartyMembers) {
		// Draw player border box
		const bool bIsTurn = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter] == Member;
		DrawBorder(PlayerBackgroundMaterial, nullptr, nullptr, PlayerBorderMaterial, bIsTurn ? 2.0f : 0.0f, PlayerXStart, CurrentY, PlayerBackgroundWidth, PlayerBackgroundHeight);

		// Draw name
		CurrentY += SmallMargin;
		const FString Name = FString::Printf(TEXT("%s"), *Member->Name/*, *ClassNames[(int32)Member->Class]*/);
		DrawText(Name, kWhiteColor, PlayerXStart + SmallMargin, CurrentY + (PlayerTitleHeight - TextHeight) / 2.0f, Font);

		// Draw turn box
		const FLinearColor PlayerTurnColor = Member->bCanAct ? TurnValidColor : TurnInvalidColor;
		DrawTexture(TurnIcon, PlayerXStart + PlayerBackgroundWidth - SmallMargin - 16.0f, CurrentY, 16.0f, 16.0f, 0.0f, 0.0f, 1.0f, 1.0f, PlayerTurnColor);

		// Draw skill points
		if (Member->SkillPoints > 0 || Member->StatPoints > 0) {
			DrawTextureSimple(LevelUpIcon, PlayerXStart + PlayerBackgroundWidth - SmallMargin * 2.0f - 32.0f, CurrentY);
		}
		CurrentY += PlayerTitleHeight + SmallMargin;

		// Draw Portrait
		DrawTexture(Member->Portrait, PlayerXStart + SmallMargin, CurrentY, PlayerPortraitSize, PlayerPortraitSize, 0.0f, 0.0f, 1.0f, 1.0f);
		if (Member->IsDead()) {
			DrawTextureSimple(DeathIcon, PlayerXStart + SmallMargin, CurrentY);
		} else if (Member->IsUnconscious()) {
			DrawTextureSimple(UnconsciousIcon, PlayerXStart + SmallMargin, CurrentY);
		}

		// Draw health bar
		const float HealthPixels = FMath::Max(0.0f, Member->CurrentHealth) / Member->GetStat(EStats::StatHealth) * (PlayerPortraitSize - 2);
		static const float HealthX = PlayerXStart + PlayerPortraitSize + SmallMargin * 3.0f;
		DrawRect(kLightGrayColor, HealthX, CurrentY, PlayerBarWidth, PlayerPortraitSize);
		DrawRect(kMediumGrayColor, HealthX + 1, CurrentY + 1, PlayerBarWidth - 2, PlayerPortraitSize - 2);
		DrawRect(kRedColor, HealthX + 1, CurrentY + (PlayerPortraitSize - 1) - HealthPixels, PlayerBarWidth - 2, HealthPixels);

		// Draw mana bar
		const float ManaPixels = FMath::Max(0.0f, Member->CurrentMana) / Member->GetStat(EStats::StatMana) * (PlayerPortraitSize - 2);
		static const float ManaX = PlayerXStart + PlayerPortraitSize + SmallMargin * 3.0f + PlayerBarWidth - 1.0f;
		DrawRect(kLightGrayColor, ManaX, CurrentY, PlayerBarWidth, PlayerPortraitSize);
		DrawRect(kMediumGrayColor, ManaX + 1, CurrentY + 1, PlayerBarWidth - 2, PlayerPortraitSize - 2);
		DrawRect(kBlueColor, ManaX + 1, CurrentY + (PlayerPortraitSize - 1) - ManaPixels, PlayerBarWidth - 2, ManaPixels);
		CurrentY += PlayerPortraitSize + SmallMargin;

		// Draw status effects
		float CurrentX = PlayerXStart + SmallMargin;
		for (int32 i = 0; i < (int32)EStatusEffects::StatusTotalStatusEffects; ++i) {
			if (Member->HasStatus((EStatusEffects)i)) {
				DrawTextureSimple(StatusIcons[(EStatusEffects)i], CurrentX, CurrentY);
			}
			CurrentX += 16.0f;
		}
		CurrentY += TextHeight + SmallMargin;

		CurrentY += PlayerSpacing;
	}

	// Current Gold
	DrawBorder(PlayerBackgroundMaterial, nullptr, nullptr, InnerBorderMaterial, 1.0f, PlayerXStart, CurrentY, PlayerBackgroundWidth, TextHeight + 2.0f * SmallMargin);
	CurrentY += SmallMargin;
	DrawText(FString::Printf(TEXT("Gold: %u"), GameInstance->PlayerGold), kGoldColor, PlayerXStart + SmallMargin, CurrentY, Font);
	CurrentY += SmallMargin + TextHeight;

	// Turn based mode
	if (GameInstance->bIsInTurnMode) {
		static const float TurnModeSize = 32.0f;
		float CharWidth, CharHeight;
		int32 ScreenWidth, ScreenHeight;
		Font->GetCharSize(TCHAR('T'), CharWidth, CharHeight);
		Cast<APlayerController>(PlayerParty->GetController())->GetViewportSize(ScreenWidth, ScreenHeight);

		DrawRect(kRedColor, PlayerXStart, CurrentY + SmallMargin, TurnModeSize, TurnModeSize);
		DrawText(TEXT("T"), kWhiteColor, PlayerXStart + (TurnModeSize - CharWidth) / 2, CurrentY + SmallMargin + (TurnModeSize - CharHeight) / 2, Font);
	}
	
	// Draw text log
	DrawBorder(LogBackgroundMaterial, nullptr, nullptr, LogBorderMaterial, LogBorderWidth, LogX - Margin, LogY - Margin, LogWidth + 2 * Margin, LogHeight + 2 * Margin);
	CurrentY = LogY;
	for (auto& Line : LogLines) {
		DrawText(Line, FontColor, LogX, CurrentY, Font);
		CurrentY += TextHeight;
	}

	// Draw minimap
	DrawBorder(BackgroundMaterial, nullptr, nullptr, InnerBorderMaterial, 1.0f, MinimapX, MinimapY, MinimapWidth, MinimapHeight);
	if (Minimap != nullptr) {
		static const float Sixth = 1.0f / 6.0f;
		static const float Third = 1.0f / 3.0f;
		const FVector HalfExtents = FVector(MinimapSize * Sixth, MinimapSize * Sixth, 40000.0f);
		const float HalfMinimapSize = MinimapSize / 2.0f;		
		const float LocationX = ((-PlayerParty->GetActorLocation().X + MinimapOrigin.X) + HalfMinimapSize) / MinimapSize;
		const float LocationY = ((PlayerParty->GetActorLocation().Y - MinimapOrigin.Y) + HalfMinimapSize) / MinimapSize;
		const float MinX = (LocationX - Sixth) * MinimapSize;
		const float MinY = (LocationY - Sixth) * MinimapSize;
		const float MaxX = (LocationX + Sixth) * MinimapSize;
		const float MaxY = (LocationY + Sixth) * MinimapSize;
		const float Rotation = PlayerParty->GetActorRotation().Yaw;

		// Adjust U/V and drawing coordinates to avoid wrapping
		const float U = FMath::Max(0.0f, LocationY - Sixth);
		float UWidth = Third - (U - (LocationY - Sixth));
		if (U + UWidth > 1.0f) {
			UWidth = 1.0f - U;
		}
		const float XOffset = (U - (LocationY - Sixth)) / Third * MinimapWidth;
		const float WidthOffset = (Third - UWidth) / Third * MinimapWidth;

		const float V = FMath::Max(0.0f, LocationX - Sixth);
		float VHeight = Third - (V - (LocationX - Sixth));
		if (V + VHeight > 1.0f) {
			VHeight = 1.0f - V;
		}
		const float YOffset = (V - (LocationX - Sixth)) / Third * MinimapHeight;
		const float HeightOffset = (Third - VHeight) / Third * MinimapHeight;
		DrawTexture(Minimap, MinimapX + XOffset, MinimapY + YOffset, MinimapWidth - WidthOffset, MinimapHeight - HeightOffset, U, V, UWidth, VHeight, FLinearColor::White, EBlendMode::BLEND_Opaque);

		if (PlayerParty->HasStatus(EPartyStatusEffects::StatusWizardsEye)) {
			TArray<FOverlapResult> AllActorsInRadius;
			if (GetWorld()->OverlapMultiByChannel(AllActorsInRadius, PlayerParty->GetActorLocation(), FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, FCollisionShape::MakeBox(HalfExtents))) {
				for (auto& OverlapResult : AllActorsInRadius) {
					// Make sure we're only testing the collision capsule
					if (!OverlapResult.GetComponent()->IsA<UShapeComponent>()) continue;

					AActor* Actor = OverlapResult.GetActor();

					FLinearColor Color = FLinearColor::Black;
					if (Actor->ActorHasTag(ActorTags::kFriendlyTag)) {
						Color = FLinearColor::Green;
					} else if (Actor->ActorHasTag(ActorTags::kEnemyTag)) {
						if (Actor->ActorHasTag(ActorTags::kVisibleTag)) {
							Color = FLinearColor::Red;
						}
					} else if (Actor->ActorHasTag(ActorTags::kPickupableTag)) {
						if (PlayerParty->StatusEffects[EPartyStatusEffects::StatusWizardsEye].Strength >= (int32)EMastery::MasteryExpert) {
							Color = FLinearColor::Blue;
						}
					} else if (Actor->IsA<AChest>()) {
						if (PlayerParty->StatusEffects[EPartyStatusEffects::StatusWizardsEye].Strength >= (int32)EMastery::MasteryMaster) {
							Color = FLinearColor(0.4f, 0.2f, 0.0f);
						}
					}

					if (Color != FLinearColor::Black) {
						const float ActorX = ((-Actor->GetActorLocation().X + MinimapOrigin.X) + HalfMinimapSize - MinX) / (MaxX - MinX);
						const float ActorY = ((Actor->GetActorLocation().Y - MinimapOrigin.Y) + HalfMinimapSize - MinY) / (MaxY - MinY);
						DrawRect(Color, MinimapX + (MinimapWidth * ActorY) - 1.0f, MinimapY + (MinimapHeight * ActorX) - 1.0f, 3.0f, 3.0f);
					}
				}
			}
		}

		DrawTexture(Arrow, MinimapX + MinimapWidth / 2.0f - 4.0f, MinimapY + MinimapHeight / 2.0f - 4.0f, 9.0f, 9.0f, 0.0f, 0.0f, 1.0f, 1.0f,
					FLinearColor::White, EBlendMode::BLEND_Translucent, 1.0, false, Rotation, FVector2D(0.5f, 0.5f));
	}

	// Draw clock
	DrawBorder(PlayerBackgroundMaterial, nullptr, nullptr, InnerBorderMaterial, 1.0f, TimeX, TimeY, TimeWidth, TimeHeight);
	{
		const FString Time = GameInstance->TimeOfDayManager->GetAsString();
		int32 Width, Height;
		Font->GetStringHeightAndWidth(*Time, Height, Width);
		DrawText(Time, kWhiteColor, TimeX + (TimeWidth - Width) / 2.0f, TimeY + (TimeHeight - Height) / 2.0f, Font);
	}
	
	// Draw party status effects
	DrawBorder(PlayerBackgroundMaterial, nullptr, nullptr, InnerBorderMaterial, 1.0f, MinimapX - 17.0f, MinimapY, 16.0f, MinimapHeight);
	{
		CurrentY = MinimapY;
		for (int32 i = 0; i < (int32)EPartyStatusEffects::StatusTotalPartyStatusEffects; ++i) {
			if (PlayerParty->HasStatus((EPartyStatusEffects)i)) {
				DrawTextureSimple(PartyStatusIcons[(EPartyStatusEffects)i], MinimapX - 17.0f, CurrentY);
			}
			CurrentY += 17.0f;
		}
	}

	// Draw hotkey hints
	if (GameInstance->bDrawHotkeysOnScreen) {
		int HintCounter = 0;
		DrawText(TEXT("E - Attack"), FontColor, HotkeyHintX, HotkeyHintY + HintCounter++ * (TextHeight + SmallMargin), Font);
		DrawText(TEXT("C - Cast Spell"), FontColor, HotkeyHintX, HotkeyHintY + HintCounter++ * (TextHeight + SmallMargin), Font);
		DrawText(TEXT("Enter - Turn-based Mode"), FontColor, HotkeyHintX, HotkeyHintY + HintCounter++ * (TextHeight + SmallMargin), Font);
		HintCounter++;
		DrawText(TEXT("I - Inventory"), FontColor, HotkeyHintX, HotkeyHintY + HintCounter++ * (TextHeight + SmallMargin), Font);
		DrawText(TEXT("O - Stats"), FontColor, HotkeyHintX, HotkeyHintY + HintCounter++ * (TextHeight + SmallMargin), Font);
		DrawText(TEXT("P - Skills"), FontColor, HotkeyHintX, HotkeyHintY + HintCounter++ * (TextHeight + SmallMargin), Font);
		DrawText(TEXT("J - Journal"), FontColor, HotkeyHintX, HotkeyHintY + HintCounter++ * (TextHeight + SmallMargin), Font);
		DrawText(TEXT("L - Bestiary Log"), FontColor, HotkeyHintX, HotkeyHintY + HintCounter++ * (TextHeight + SmallMargin), Font);
	}

	// If talking to an NPC, display the dialog HUD
	if (DialogComponent.IsValid()) {
		DrawDialog();
	} else if (Chest.IsValid()) {
		DrawChest();
	} else if (Building.IsValid()) {
		if (Building->IsA<AShopBuilding>()) {
			DrawShop();
		} else if (Building->IsA<ATempleBuilding>()) {
			DrawTemple();
		} else if (Building->IsA<ATavernBuilding>()) {
			DrawTavern();
		}
	} else if (MenuType == EMenuType::MenuSpellbook) {
		DrawSpellbook();
	} else if (MenuType != EMenuType::MenuNone && MenuType != EMenuType::MenuQuests && MenuType != EMenuType::MenuBestiary) {
		// If a menu is open, overlay it on top of the game HUD
		DrawHUDBackground();
		DrawCharacter();
		if (MenuType == EMenuType::MenuInventory) {
			DrawInventory(TypeX, TypeY, PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->Inventory);
		} else if (MenuType == EMenuType::MenuStats) {
			DrawStats();
		} else if (MenuType == EMenuType::MenuSkills) {
			DrawSkills();
		}
	} else {
		// Draw the "cursor" in the center of the screen
		DrawRect(kWhiteColor, CrosshairX - 2, CrosshairY - 2, 4, 4);
		DrawRect(kWhiteColor, CrosshairX - 1, CrosshairY - 1, 2, 2);

		if (IsRightMousePressed()) {
			AActor* NearestActor = nullptr;
			float NearestDistance = 0.0f;
			PlayerParty->GetActorUnderMouse(ActorTags::kIdentifiableTag, 20000.0f, NearestActor, NearestDistance);
			if (NearestActor != nullptr) {
				if (NearestActor->IsA<APickup>()) {
					DrawTooltip(Cast<APickup>(NearestActor));
				} else if (NearestActor->IsA<ABaseEnemyCharacter>()) {
					DrawTooltip(Cast<ABaseEnemyCharacter>(NearestActor));
				}
			}
		}

		// Draw the map on top, but only if not in a menu
		if (bDrawMap) {
			DrawMap();
		}
	}

	// Draw the Player's held item on top of everything
	if (PlayerParty->HeldItem != nullptr) {
		float X, Y;
		GetOwningPlayerController()->GetMousePosition(X, Y);
		DrawTextureSimple(GameInstance->GetTexture(PlayerParty->HeldItem->Icon), X - InventoryBoxSize / 2, Y - InventoryBoxSize / 2);
	}
}

void ANNSHUD::Log(const FString& Line)
{
	LogLines.Add(Line);
	LogLines.RemoveAt(0, FMath::Max(0, LogLines.Num() - kMaxLogLines));
}

void ANNSHUD::Log(UCharacterSheet* Character, float Amount, const FString& Source)
{
	if (Character->OwnerCharacter->GetDistanceTo(PlayerParty.Get()) > APlayerPartyCharacter::kMaxTargetDistance) return;

	FString Line = FString::Printf(TEXT("%s takes %i damage from %s"), *Character->Name, FMath::RoundToInt(Amount), *Source);
	if (Character->IsDead()) {
		Line.Append(TEXT(", and is dead."));
	} else if (Character->IsUnconscious()) {
		Line.Append(TEXT(", and is unconscious."));
	} else {
		Line.Append(TEXT("."));
	}
	Log(Line);
}

void ANNSHUD::DrawDialogSidebarBase(const FString& Name)
{
	static const FLinearColor SidebarUnhoveredTextColor = kWhiteColor;

	// Draw sidebar background
	DrawBorder(DialogBackgroundMaterial, nullptr, nullptr, DialogBorderMaterial, DialogBorderWidth, DialogSidebarX, 1, DialogSidebarWidth, DialogSidebarHeight);

	// Draw sidebar name
	int32 NameWidth = Font->GetStringSize(*Name);
	float SidebarY = 1 + Margin;
	DrawText(Name, SidebarUnhoveredTextColor, DialogSidebarX + (DialogSidebarWidth - NameWidth) / 2, SidebarY, Font);
	SidebarY += TextHeight + Margin;
	DrawTextureSimple(NPCPortrait, DialogSidebarX + (DialogSidebarWidth - DialogSidebarPortraitSize) / 2, SidebarY);
}

float ANNSHUD::DrawDialogSidebarText(const FString& Name, float Y, bool Available, bool Hovered)
{
	static const FLinearColor SidebarUnhoveredTextColor = kWhiteColor;
	static const FLinearColor SidebarHoveredTextColor = kBlueColor;
	static const FLinearColor SidebarUnavailableTextColor = kMediumGrayColor;

	FLinearColor Color = SidebarUnavailableTextColor;
	if (Available) {
		if (Hovered) {
			Color = SidebarHoveredTextColor;
		} else {
			Color = SidebarUnhoveredTextColor;
		}
	}

	int32 KeyWidth = Font->GetStringSize(*Name) * DialogTextScale;
	DrawText(Name, Color, DialogSidebarX + (DialogSidebarWidth - KeyWidth) / 2, Y, Font, DialogTextScale);
	return Y + DialogSidebarSpacing + TextHeight * DialogTextScale;
}

float ANNSHUD::DrawDialogComponent(UDialogComponent* Dialog, float Y)
{
	static const FLinearColor DialogTextColor = kWhiteColor;

	int32 KeyWidth = 0;
	// Sidebar dialog
	for (auto& DialogLine : Dialog->DialogLines) {
		if (DialogLine.Flag == EQuestFlags::QuestTotalQuests || GameInstance->QuestManager->IsCurrentOrPreviousStepCharacter(DialogLine.Flag, Dialog->GetOwner()->GetName(), DialogLine.FlagNum)) {
			Y = DrawDialogSidebarText(DialogLine.Key.ToString(), Y, true, HoveredHitbox.IsEqual(DialogLine.Key));
		}
	}
	// Sidebar skills
	if (Dialog->Skill != ESkills::SkillTotalSkills) {
		UCharacterSheet* ActiveCharacter = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter];
		if (!UNNSGameInstance::ClassSkillMap[ActiveCharacter->Class].Masteries.Contains(Dialog->Skill) || (EMastery)UNNSGameInstance::ClassSkillMap[ActiveCharacter->Class].Masteries[Dialog->Skill] <= ActiveCharacter->Mastery(Dialog->Skill)) {
			if (ActiveCharacter->IsMaster(Dialog->Skill)) {
				// Already master
				const FString SkillKey = FString::Printf(*DialogSkillTrainTooHigh, *SkillNames[(int32)Dialog->Skill]);
				Y = DrawDialogSidebarText(SkillKey, Y, false);
			} else {
				// Can't learn any more
				const FString SkillKey = FString::Printf(*DialogSkillCannotTrain, *SkillNames[(int32)Dialog->Skill]);
				Y = DrawDialogSidebarText(SkillKey, Y, false);
			}
		} else if (!(ActiveCharacter->GetBaseSkill(Dialog->Skill) > 0)) {
			// Basic, only draw if not already learned
			const FString SkillKey = FString::Printf(*DialogSkillLearn, *SkillNames[(int32)Dialog->Skill], EnumMaps::SkillCosts[EMastery::MasteryBasic].Gold);
			Y = DrawDialogSidebarText(SkillKey, Y, ActiveCharacter->CanLearnSkill(Dialog->Skill), HoveredHitbox.IsEqual(DialogSkillBasic));
		} else {
			// Expert
			if (!ActiveCharacter->IsExpert(Dialog->Skill)) {
				const FString SkillKey = FString::Printf(*DialogSkillTrain, *SkillNames[(int32)Dialog->Skill], *(DialogSkillExpert.ToString()), EnumMaps::SkillCosts[EMastery::MasteryExpert].Gold);
				Y = DrawDialogSidebarText(SkillKey, Y, ActiveCharacter->CanBecomeExpert(Dialog->Skill), HoveredHitbox.IsEqual(DialogSkillExpert));
			} else {
				// Master
				if (!ActiveCharacter->IsMaster(Dialog->Skill)) {
					const FString SkillKey = FString::Printf(*DialogSkillTrain, *SkillNames[(int32)Dialog->Skill], *(DialogSkillMaster.ToString()), EnumMaps::SkillCosts[EMastery::MasteryMaster].Gold);
					Y = DrawDialogSidebarText(SkillKey, Y, ActiveCharacter->CanBecomeMaster(Dialog->Skill), HoveredHitbox.IsEqual(DialogSkillMaster));
				}
			}
		}
	}

	// Draw text background
	DrawBorder(DialogBackgroundMaterial, nullptr, nullptr, DialogBorderMaterial, DialogBorderWidth, DialogBackgroundX, DialogBackgroundY, DialogBackgroundWidth, DialogBackgroundHeight);

	// Draw text
	FText* CurrentDialog = nullptr;
	if (CurrentDialogKey.IsNone()) {
		CurrentDialog = &Dialog->Greeting;
	} else {
		for (auto& DialogLine : Dialog->DialogLines) {
			if (DialogLine.Key.IsEqual(CurrentDialogKey)) {
				// Display the alt dialog only if the quest is finished
				if (DialogLine.AltDialog.IsEmpty() || DialogLine.Flag == EQuestFlags::QuestTotalQuests || !GameInstance->QuestManager->IsCharacterDialogFinished(DialogLine.Flag, Dialog->GetOwner()->GetName(), DialogLine.FlagNum)) {
					CurrentDialog = &DialogLine.Dialog;
				} else {
					CurrentDialog = &DialogLine.AltDialog;
				}
				break;
			}
		}
	}
	DrawText(CurrentDialog->ToString(), DialogTextColor, DialogBackgroundX + Margin, DialogBackgroundY + Margin, Font, DialogTextScale);

	return Y;
}

void ANNSHUD::DrawDialog()
{
	const FString& CharacterName = DialogComponent->GetOwner()->GetName();
	DrawDialogSidebarBase(CharacterName);

	// Draw sidebar options
	DrawDialogComponent(DialogComponent.Get(), DialogSidebarKeyY);

	// Sidebar exit
	DrawDialogSidebarText(DialogExit.ToString(), DialogSidebarExitY, true, HoveredHitbox.IsEqual(DialogExit));
}

void ANNSHUD::DrawChest()
{
	static const FLinearColor BackgroundColor = kBrownColor;
	static const FLinearColor BorderColor = kWhiteColor;
	static const FLinearColor TextColor = kWhiteColor;
	// Draw border
	DrawRect(BorderColor, ChestX - Margin - 1, ChestY - Margin - 1, TypeWidth + Margin * 2 + 2, TypeHeight + Margin * 2 + 2);

	// Draw background
	DrawRect(BackgroundColor, ChestX - Margin, ChestY - Margin, TypeWidth + Margin * 2, TypeHeight + Margin * 2);

	// Draw inventory
	DrawInventory(ChestX, ChestY, Chest->Inventory);

	// Draw close button
	DrawRect(BackgroundColor, ChestCloseX, ChestCloseY, ChestCloseWidth, ChestCloseHeight);
	const FString CloseString = ChestCloseName.ToString();
	int32 Height, Width;
	Font->GetStringHeightAndWidth(CloseString, Height, Width);
	DrawText(CloseString, TextColor, ChestCloseX + (ChestCloseWidth - Width) / 2, ChestCloseY + (ChestCloseHeight - Height) / 2, Font);
}

void ANNSHUD::DrawShop()
{
	static const FLinearColor BackgroundColor = kMediumGrayColor;
	static const FLinearColor BorderColor = kLightGrayColor;

	AShopBuilding* Shop = Cast<AShopBuilding>(Building.Get());

	const FString& CharacterName = Shop->GetName();
	DrawDialogSidebarBase(CharacterName);
	
	float SidebarY = DialogSidebarKeyY;
	if (CurrentDialogKey.IsNone()) {
		SidebarY = DrawDialogSidebarText(StoreBuy.ToString(), SidebarY, true, HoveredHitbox.IsEqual(StoreBuy));
		SidebarY = DrawDialogSidebarText(StoreInventory.ToString(), SidebarY, true, HoveredHitbox.IsEqual(StoreInventory));
		SidebarY = DrawDialogSidebarText(StoreSkills.ToString(), SidebarY, true, HoveredHitbox.IsEqual(StoreSkills));
	} else if (CurrentDialogKey.IsEqual(StoreBuy)) {
		DrawBorder(InnerBackgroundMaterial, nullptr, nullptr, InnerBorderMaterial, InnerBorderWidth, StoreX, StoreY, StoreWidth, StoreHeight);
		const int32 Rows = Shop->Items.Num() >= 8 ? 2 : 1;
		const int32 ItemsPerRow = FMath::CeilToInt(Shop->Items.Num() / Rows);
		const float ItemWidth = StoreWidth / ItemsPerRow;
		const float ItemHeight = StoreHeight / Rows;
		for (int32 CurrentRow = 0; CurrentRow < Rows; ++CurrentRow) {
			for (int32 i = 0; i < ItemsPerRow && (i + ItemsPerRow * CurrentRow < Shop->Items.Num()); ++i) {
				const float CenterY = StoreY + StoreHeight / Rows * CurrentRow + ItemHeight / 2;
				const float CenterX = StoreX + ItemWidth * i + ItemWidth / 2;
				UItem* Item = Shop->Items[i + (ItemsPerRow * CurrentRow)];
				if (Item != nullptr) {
					UTexture* Texture = GameInstance->GetTexture(Item->Icon);
					DrawTextureSimple(Texture, CenterX - Texture->GetSurfaceWidth() / 2, CenterY - Texture->GetSurfaceHeight() / 2);
				}
			}
		}
		if (HoveredHitbox.ToString().IsNumeric()) {
			UItem* Item = Shop->Items[FCString::Atoi(*(HoveredHitbox.ToString()))];
			uint32 Cost = PlayerParty->ConvertCost(Item->GetBuyCost(), false);
			const FString BuyString = FString::Printf(*StoreBuyPrice, *Item->Name.ToString(), Cost);
			SidebarY = DrawDialogSidebarText(BuyString, SidebarY, PlayerParty->GetGold() >= Cost);

			if (IsRightMousePressed()) {
				DrawTooltip(Item);
			}
		}
	} else if (CurrentDialogKey.IsEqual(StoreInventory) || CurrentDialogKey.IsEqual(StoreSell) || CurrentDialogKey.IsEqual(StoreIdentify)) {
		DrawBorder(InnerBackgroundMaterial, nullptr, nullptr, InnerBorderMaterial, InnerBorderWidth, ChestX - Margin, ChestY - Margin, TypeWidth + Margin * 2, TypeHeight + Margin * 2);
		DrawInventory(ChestX, ChestY, PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->Inventory);
		if (CurrentDialogKey.IsEqual(StoreInventory)) {
			SidebarY = DrawDialogSidebarText(StoreSell.ToString(), SidebarY, true, HoveredHitbox.IsEqual(StoreSell));
			SidebarY = DrawDialogSidebarText(StoreIdentify.ToString(), SidebarY, true, HoveredHitbox.IsEqual(StoreIdentify));
		} else if (HoveredHitbox.ToString().Contains(TEXT(","))) {
			TPair<int32, int32> Position = NameToCoords(HoveredHitbox);
			UItem* Item = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->Inventory->GetItem(Position.Key, Position.Value);
			if (Item != nullptr) {
				if (!Item->MatchesShop(Shop->ShopType)) {
					SidebarY = DrawDialogSidebarText(StoreWrongType, SidebarY);
				} else if (CurrentDialogKey.IsEqual(StoreSell)) {
					const FString ItemName = Item->GetName().ToString();
					const FString SellString = FString::Printf(*StoreSellPrice, *ItemName, PlayerParty->ConvertCost(Item->GetSellCost(), true));
					SidebarY = DrawDialogSidebarText(SellString, SidebarY);
				} else if (CurrentDialogKey.IsEqual(StoreIdentify)) {
					uint32 Cost = PlayerParty->ConvertCost(Item->GetIDCost(), false);
					const FString IDString = Item->bIsIdentified ? TEXT("Already Identified") : FString::Printf(*StoreIdentifyPrice, Cost);
					SidebarY = DrawDialogSidebarText(IDString, SidebarY, PlayerParty->GetGold() >= Cost);
				}
			}
		}
	} else if (CurrentDialogKey.IsEqual(StoreSkills)) {
		for (const auto& ShopSkill : EnumMaps::ShopSkills[Shop->ShopType]) {
			const uint32& GoldCost = EnumMaps::SkillCosts[EMastery::MasteryBasic].Gold;
			if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->CanLearnSkill(ShopSkill, false)) {
				const FString& Skill = FString::Printf(*DialogSkillLearn, *SkillNames[(int32)ShopSkill], GoldCost);
				SidebarY = DrawDialogSidebarText(*Skill, SidebarY, PlayerParty->GetGold() >= GoldCost, ShopSkill == (ESkills)FCString::Atoi(*(HoveredHitbox.ToString())));
			}
		}
	}

	// Sidebar exit
	DrawDialogSidebarText(DialogExit.ToString(), DialogSidebarExitY, true, HoveredHitbox.IsEqual(DialogExit));
}

void ANNSHUD::DrawTemple()
{
	ATempleBuilding* Temple = Cast<ATempleBuilding>(Building.Get());
	const FString& CharacterName = Temple->GetName();
	DrawDialogSidebarBase(CharacterName);

	float SidebarY = DialogSidebarKeyY;
	if (CurrentDialogKey.IsEqual(StoreSkills)) {
		for (const auto& ShopSkill : EnumMaps::ShopSkills[EShopTypes::ShopTemple]) {
			const uint32& GoldCost = EnumMaps::SkillCosts[EMastery::MasteryBasic].Gold;
			if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->CanLearnSkill(ShopSkill, false)) {
				const FString& Skill = FString::Printf(*DialogSkillLearn, *SkillNames[(int32)ShopSkill], GoldCost);
				SidebarY = DrawDialogSidebarText(*Skill, SidebarY, PlayerParty->GetGold() >= GoldCost, ShopSkill == (ESkills)FCString::Atoi(*(HoveredHitbox.ToString())));
			}
		}
	} else {
		for (int32 i = 0; i < PlayerParty->PartyMembers.Num(); ++i) {
			UCharacterSheet* Character = PlayerParty->PartyMembers[i];
			const uint32 HealCost = FMath::RoundToInt(Character->GetHealCost() * Temple->Multiplier);
			const bool HitboxOverlap = (i + 1) == FCString::Atoi(*(HoveredHitbox.ToString()));
			if (HealCost > 0) {
				SidebarY = DrawDialogSidebarText(FString::Printf(*TempleHeal, *Character->Name, (int32)HealCost), SidebarY, PlayerParty->GetGold() >= HealCost, HitboxOverlap);
			} else {
				SidebarY = DrawDialogSidebarText(FString::Printf(*TempleHealFull, *Character->Name), SidebarY, false, HitboxOverlap);
			}
		}
		SidebarY = DrawDialogSidebarText(StoreSkills.ToString(), SidebarY, true, HoveredHitbox.IsEqual(StoreSkills));
	}

	// Sidebar exit
	DrawDialogSidebarText(DialogExit.ToString(), DialogSidebarExitY, true, HoveredHitbox.IsEqual(DialogExit));
}

void ANNSHUD::DrawTavern()
{
	ATavernBuilding* Tavern = Cast<ATavernBuilding>(Building.Get());
	const FString& CharacterName = Tavern->GetName();
	DrawDialogSidebarBase(CharacterName);

	float SidebarY = DialogSidebarKeyY;

	if (CurrentDialogKey.IsEqual(StoreSkills)) {
		for (const auto& ShopSkill : EnumMaps::ShopSkills[EShopTypes::ShopTavern]) {
			const uint32& GoldCost = EnumMaps::SkillCosts[EMastery::MasteryBasic].Gold;
			if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->CanLearnSkill(ShopSkill, false)) {
				const FString& Skill = FString::Printf(*DialogSkillLearn, *SkillNames[(int32)ShopSkill], GoldCost);
				SidebarY = DrawDialogSidebarText(*Skill, SidebarY, PlayerParty->GetGold() >= GoldCost, ShopSkill == (ESkills)FCString::Atoi(*(HoveredHitbox.ToString())));
			}
		}
	} else {
		SidebarY = DrawDialogSidebarText(FString::Printf(*TavernRestString, (uint32)Tavern->RestCost), SidebarY, PlayerParty->GetGold() >= (uint32)Tavern->RestCost, HoveredHitbox.IsEqual(TavernRestName));
		SidebarY = DrawDialogComponent(Tavern->DialogComponent, SidebarY);
		SidebarY = DrawDialogSidebarText(StoreSkills.ToString(), SidebarY, true, HoveredHitbox.IsEqual(StoreSkills));
	}

	// Sidebar exit
	DrawDialogSidebarText(DialogExit.ToString(), DialogSidebarExitY, true, HoveredHitbox.IsEqual(DialogExit));
}

void ANNSHUD::DrawHUDBackground()
{
	// Draw background
	//DrawBorder(BackgroundMaterial, BorderTopLeftCornerTexture, BorderTopRightCornerTexture, BackgroundBorderLineMat, BackgroundBorderWidth, BackgroundX, BackgroundY, BackgroundWidth, BackgroundHeight);
	int32 ScreenWidth, ScreenHeight;
	Cast<APlayerController>(PlayerParty->GetController())->GetViewportSize(ScreenWidth, ScreenHeight);
	DrawMaterialSimple(BackgroundMaterial, 0.0f, 0.0f, (float)ScreenWidth, (float)ScreenHeight);
	
	// Draw dynamic type slot
	float X = BackgroundX + Margin + 1;
	float Y = BackgroundY + Margin + 1;
	float Width = TypeWidth + Margin * 2;
	float Height = TypeHeight + Margin * 2;
	DrawBorder(InnerBackgroundMaterial, nullptr, nullptr, InnerBorderMaterial, InnerBorderWidth, X, Y, Width, Height);

	// Draw character slot
	X += Width + Margin + 2;
	Width = CharacterWidth + Margin * 2;
	Height = CharacterHeight + Margin * 2;
	DrawBorder(InnerBackgroundMaterial, nullptr, nullptr, InnerBorderMaterial, InnerBorderWidth, X, Y, Width, Height);

	// Draw status effects
	X = BackgroundX + Margin + 1;
	Y = StatusY - Margin;
	Width = TypeWidth + Margin * 2;
	Height = CharacterHeight - TypeHeight - 1;
	DrawBorder(InnerBackgroundMaterial, nullptr, nullptr, InnerBorderMaterial, InnerBorderWidth, X, Y, Width, Height);
	int32 StatusCounter = 0;
	for (auto& Effect : PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->StatusEffects) {
		X = StatusX + (StatusCounter / NumStatusRows) * 80.0f;
		Y = StatusY + (StatusCounter % NumStatusRows) * (TextHeight + Margin);
		DrawText(StatusEffectNames[(int32)Effect.Key], FontColor, X, Y, Font);
		++StatusCounter;
	}
	if (IsRightMousePressed() && HoveredHitbox.ToString().Contains(TEXT("_status"))) {
		EStatusEffects Effect = (EStatusEffects)FCString::Atoi(*HoveredHitbox.ToString().RightChop(7));
		DrawTooltip(Effect);
	}

	StatusCounter = 0;
	for (auto& Effect : PlayerParty->StatusEffects) {
		X = StatusX + 400.0f + (StatusCounter / NumStatusRows) * 80.0f;
		Y = StatusY + (StatusCounter % NumStatusRows) * (TextHeight + Margin);
		DrawText(PartyStatusEffectNames[(int32)Effect.Key], FontColor, X, Y, Font);
		++StatusCounter;
	}
	if (IsRightMousePressed() && HoveredHitbox.ToString().Contains(TEXT("_partystatus"))) {
		EPartyStatusEffects Effect = (EPartyStatusEffects)FCString::Atoi(*HoveredHitbox.ToString().RightChop(12));
		DrawTooltip(Effect);
	}
}

void ANNSHUD::DrawBorder(UMaterialInterface* Background, UTexture2D* TLCorner, UTexture2D* TRCorner, UMaterialInterface* Line, float LineWidth, float X, float Y, float Width, float Height)
{
	if (Background != nullptr) {
		DrawMaterialSimple(Background, X, Y, Width, Height);
	}

	if (LineWidth > 0.0f && Line != nullptr) {
		const float ActualWidth = Width + 2 * LineWidth;
		const float ActualHeight = Height + 2 * LineWidth;
		const float ActualX = X - LineWidth;
		const float ActualY = Y - LineWidth;

		DrawMaterial(Line, ActualX, ActualY, ActualWidth, LineWidth, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, false, 0.0f);
		DrawMaterial(Line, ActualX + ActualWidth, ActualY, ActualHeight, LineWidth, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, false, 90.0f);
		DrawMaterial(Line, ActualX + ActualWidth, ActualY + ActualHeight, ActualWidth, LineWidth, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, false, 180.0f);
		DrawMaterial(Line, ActualX, ActualY + ActualHeight, ActualHeight, LineWidth, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, false, 270.0f);
	}

	if (TLCorner != nullptr && TRCorner != nullptr) {
		const float CornerWidth = TLCorner->GetSurfaceWidth();
		const float CornerHeight = TLCorner->GetSurfaceHeight();

		DrawTexture(TLCorner, X - CornerWidth / 2.0f - LineWidth / 2.0f, Y - CornerHeight / 2.0f - LineWidth / 2.0f, CornerWidth, CornerHeight, 0.0f, 0.0f, 1.0f, 1.0f, FLinearColor::White, EBlendMode::BLEND_Translucent, 1.0f, false, 0.0f, FVector2D(0.5f, 0.5f));
		DrawTexture(TRCorner, X + Width - CornerWidth / 2.0f + LineWidth / 2.0f, Y - CornerHeight / 2.0f - LineWidth / 2.0f, CornerWidth, CornerHeight, 0.0f, 0.0f, 1.0f, 1.0f, FLinearColor::White, EBlendMode::BLEND_Translucent, 1.0f, false, 0.0f, FVector2D(0.5f, 0.5f));
		DrawTexture(TLCorner, X + Width - CornerWidth / 2.0f + LineWidth / 2.0f, Y + Height - CornerHeight / 2.0f + LineWidth / 2.0f, CornerWidth, CornerHeight, 0.0f, 0.0f, 1.0f, 1.0f, FLinearColor::White, EBlendMode::BLEND_Translucent, 1.0f, false, 180.0f, FVector2D(0.5f, 0.5f));
		DrawTexture(TRCorner, X - CornerWidth / 2.0f - LineWidth / 2.0f, Y + Height - CornerHeight / 2.0f + LineWidth / 2.0f, CornerWidth, CornerHeight, 0.0f, 0.0f, 1.0f, 1.0f, FLinearColor::White, EBlendMode::BLEND_Translucent, 1.0f, false, 180.0f, FVector2D(0.5f, 0.5f));
	}
}

void ANNSHUD::DrawInventory(float X, float Y, UInventory* Inventory)
{
	static const FLinearColor LineColor = kDarkGrayColor;

	// Draw grid lines
	const float RowLineEndX = X + TypeWidth;
	for (int32 i = 0; i < InventoryRows + 1; ++i) {
		// A floating point bug requires adding to all values, despite the values being correct
		const float RowLineY = Y + i * (InventoryBoxSize + 1) + 0.1f;
		DrawLine(X, RowLineY, RowLineEndX, RowLineY, LineColor);
	}

	const float ColumnLineEndY = Y + TypeHeight;
	for (int32 i = 0; i < InventoryColumns + 1; ++i) {
		const float ColumnLineX = X + i * (InventoryBoxSize + 1) + 0.1f;
		DrawLine(ColumnLineX, Y, ColumnLineX, ColumnLineEndY, LineColor);
	}

	// Draw item textures
	for (auto& Item : Inventory->PositionMap) {
		// Offset textures by 1 to account for the grid line
		const float ItemX = X + Item.Value.Key * (InventoryBoxSize + 1) + 1.0f;
		const float ItemY = Y + Item.Value.Value * (InventoryBoxSize + 1) + 1.0f;
		DrawTextureSimple(GameInstance->GetTexture(Item.Key->Icon), ItemX, ItemY);
	}

	// Draw tooltip
	if (IsRightMousePressed() && HoveredHitbox.ToString().Contains(TEXT(","))) {
		TPair<int32, int32> Position = NameToCoords(HoveredHitbox);
		UItem* Item = Inventory->GetItem(Position.Key, Position.Value);

		if (Item != nullptr) {
			DrawTooltip(Item);
		}
	}
}

void ANNSHUD::DrawCharacter()
{
	// Draw slots
	for (int32 i = 0; i < EquipOrder.Num(); ++i) {
		const HUDPosition& Position = EquipPositions[EquipOrder[i]];
		DrawBorder(SlotBackgroundMaterial, nullptr, nullptr, SlotBorderMaterial, SlotBorderWidth, CharacterX + Position.X, CharacterY + Position.Y, Position.Width, Position.Height);

		UItem* Item = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->Equipment[(int32)EquipOrder[i]];
		if (Item != nullptr) {
			UTexture* Texture = GameInstance->GetTexture(Item->Icon);
			float X = CharacterX + Position.X + (Position.Width - Texture->GetSurfaceWidth()) / 2;
			float Y = CharacterY + Position.Y + (Position.Height - Texture->GetSurfaceHeight()) / 2;
			DrawTextureSimple(Texture, X, Y);
		} else {
			int32 Height, Width;
			Font->GetStringHeightAndWidth(*Position.Name, Height, Width);
			float X = CharacterX + Position.X + (Position.Width - Width) / 2;
			float Y = CharacterY + Position.Y + (Position.Height - Height) / 2;
			DrawText(Position.Name, FontColor, X, Y, Font);
		}
	}

	// Draw tooltip
	if (IsRightMousePressed() && HoveredHitbox.ToString().IsNumeric()) {
		int32 Slot = FCString::Atoi(*(HoveredHitbox.ToString()));
		if (Slot != (int32)EEquipType::EquipTotalSlots) {
			UItem* Item = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->Equipment[Slot];

			if (Item != nullptr) {
				DrawTooltip(Item);
			}
		}
	}
}

void ANNSHUD::DrawStat(EStats Stat, float X, float Y)
{
	static const FLinearColor UpgradeHoverTextColor = kBlueColor;
	static const float kBaseOffset = 100.0f;
	static const float kAdjustedOffset = 150.0f;

	const UCharacterSheet* ActiveCharacter = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter];

	FString Text = FString::Printf(TEXT("%s"), *StatNames[(int32)Stat]);
	FLinearColor Color = FontColor;
	if (HoveredHitbox == FName(*(StatNames[(int32)Stat])) && Stat <= EStats::StatMana && ActiveCharacter->StatPoints > 0) {
		Color = UpgradeHoverTextColor;
	}
	DrawText(Text, Color, X, Y, Font);

	const int32 BaseStat = ActiveCharacter->GetBaseStat(Stat);
	const int32 AdjustedStat = ActiveCharacter->GetStat(Stat);

	Text = FString::FromInt(BaseStat);
	DrawText(Text, FontColor, X + kBaseOffset, Y, Font);

	Text = FString::FromInt(AdjustedStat);
	if (BaseStat > AdjustedStat) {
		Color = kRedColor;
	} else if (BaseStat < AdjustedStat) {
		Color = kGreenColor;
	} else {
		Color = FontColor;
	}
	DrawText(Text, Color, X + kAdjustedOffset, Y, Font);
}

void ANNSHUD::DrawStats()
{
	static const float kBaseOffset = 100.0f;
	static const float kAdjustedOffset = 150.0f;
	static const float kRowStartOffset = 60.0f;
	static const float kSecondColumn = 300.0f;
	static const float kHeaderScale = 1.3f;

	const UCharacterSheet* ActiveCharacter = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter];

	FString Text = FString::Printf(TEXT("Available Stat Points: %i"), ActiveCharacter->StatPoints);
	DrawText(Text, FontColor, TypeX + TypeWidth - Margin - Font->GetStringSize(*Text), TypeY, Font);
	
	// Draw Name
	float X = TypeX;
	float Y = TypeY;
	Text = FString::Printf(TEXT("%s - Level %i %s"), *ActiveCharacter->Name, ActiveCharacter->Level, *ClassNames[(int32)ActiveCharacter->Class]);
	DrawText(Text, FontColor, X, Y, Font, kHeaderScale);
	Y += TextHeight * kHeaderScale + 1.0f;
	Text = FString::Printf(TEXT("%i / %i Experience"), ActiveCharacter->Experience, ActiveCharacter->ExperienceToLevel);
	DrawText(Text, FontColor, X, Y, Font);

	// Draw core stats
	Y = TypeY + kRowStartOffset;
	DrawText(TEXT("Core Stats"), HeaderColor, X, Y, Font, kHeaderScale);
	DrawText(TEXT("Base"), HeaderColor, X + kBaseOffset, Y, Font, kHeaderScale);
	DrawText(TEXT("Adjusted"), HeaderColor, X + kAdjustedOffset, Y, Font, kHeaderScale);
	Y += TextHeight + Margin * 2.0f;

	for (int32 i = 0; i <= (int32)EStats::StatMana; ++i) {
		DrawStat((EStats)i, X, Y);
		Y += TextHeight + Margin;
	}

	// Draw other stats
	X = TypeX + kSecondColumn;
	Y = TypeY + kRowStartOffset;
	DrawText(TEXT("Other Stats"), HeaderColor, X, Y, Font, kHeaderScale);
	DrawText(TEXT("Base"), HeaderColor, X + kBaseOffset, Y, Font, kHeaderScale);
	DrawText(TEXT("Adjusted"), HeaderColor, X + kAdjustedOffset, Y, Font, kHeaderScale);
	Y += TextHeight + Margin * 2.0f;

	DrawStat(EStats::StatAccuracy, X, Y);
	Y += TextHeight + Margin;
	DrawStat(EStats::StatEvasion, X, Y);
	Y += (TextHeight + Margin) * 3.0f;

	// Draw resistances
	DrawText(TEXT("Resistances"), HeaderColor, X, Y, Font, kHeaderScale);
	DrawText(TEXT("Base"), HeaderColor, X + kBaseOffset, Y, Font, kHeaderScale);
	DrawText(TEXT("Adjusted"), HeaderColor, X + kAdjustedOffset, Y, Font, kHeaderScale);
	Y += TextHeight + Margin * 2.0f;
	DrawStat(EStats::StatResistFire, X, Y);
	Y += TextHeight + Margin;
	DrawStat(EStats::StatResistAir, X, Y);
	Y += TextHeight + Margin;

	// Draw tooltip
	if (IsRightMousePressed() && !HoveredHitbox.ToString().IsNumeric()) {
		DrawTooltip(StatFromName(HoveredHitbox.ToString()));
	}
}

bool ANNSHUD::DrawSkill(ESkills Skill, float X, float Y)
{
	static const FLinearColor UpgradeHoverTextColor = kBlueColor;
	static const FLinearColor DisabledTextColor = kLightGrayColor;
	static const float kLevelOffset = 175.0f;

	const UCharacterSheet* ActiveCharacter = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter];

	if (ActiveCharacter->GetBaseSkill(Skill) == 0) return false;

	FString Text = FString::Printf(TEXT("%s"), *SkillNames[(int32)Skill]);
	if (ActiveCharacter->IsMaster(Skill)) {
		Text.Append(FString::Printf(TEXT(" (%s)"), *(DialogSkillMaster.ToString())));
	} else if (ActiveCharacter->IsExpert(Skill)) {
		Text.Append(FString::Printf(TEXT(" (%s)"), *(DialogSkillExpert.ToString())));
	}
	// Color skill differently depending on available skill points
	FLinearColor Color;
	if (ActiveCharacter->CanUpgradeBaseSkill(Skill)) {
		if (HoveredHitbox == FName(*(SkillNames[(int32)Skill]))) {
			Color = UpgradeHoverTextColor;
		} else {
			Color = FontColor;
		}
	} else {
		Color = DisabledTextColor;
	}
	DrawText(Text, Color, X, Y, Font);

	const int32 BaseSkill = ActiveCharacter->GetBaseSkill(Skill);
	const int32 AdjSkill = ActiveCharacter->GetSkill(Skill);
	if (BaseSkill != AdjSkill) {
		Text = FString::Printf(TEXT("%i (%i)"), BaseSkill, AdjSkill);
	} else {
		Text = FString::FromInt(BaseSkill);
	}
	DrawText(Text, Color, X + kLevelOffset, Y, Font);

	return true;
}

void ANNSHUD::DrawSkills()
{
	static const float kRowStartOffset = 60.0f;
	static const float kSecondColumn = 275.0f;
	static const float kHeaderScale = 1.3f;
	static const float kLevelOffset = 175.0f;

	const UCharacterSheet* ActiveCharacter = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter];

	// Draw skill points
	FString Text = FString::Printf(TEXT("Available Skill Points: %i"), ActiveCharacter->SkillPoints);
	DrawText(Text, FontColor, TypeX + TypeWidth - Margin - Font->GetStringSize(*Text), TypeY, Font);

	// Draw Name
	float X = TypeX;
	float Y = TypeY;
	Text = FString::Printf(TEXT("%s - Level %i %s"), *ActiveCharacter->Name, ActiveCharacter->Level, *ClassNames[(int32)ActiveCharacter->Class]);
	DrawText(Text, FontColor, X, Y, Font, kHeaderScale);
	Y += TextHeight * kHeaderScale + 1.0f;
	Text = FString::Printf(TEXT("%i / %i Experience"), ActiveCharacter->Experience, ActiveCharacter->ExperienceToLevel);
	DrawText(Text, FontColor, X, Y, Font);

	// Weapon skills
	Y = TypeY + kRowStartOffset;
	DrawText(TEXT("Weapon Skills"), HeaderColor, X, Y, Font, kHeaderScale);
	DrawText(TEXT("Level"), HeaderColor, X + kLevelOffset, Y, Font, kHeaderScale);
	Y += TextHeight + Margin * 2.0f;

	for (int32 i = (int32)ESkills::SkillWeaponSword; i <= (int32)ESkills::SkillWeaponBow; ++i) {
		if (DrawSkill((ESkills)i, X, Y)) {
			Y += TextHeight + Margin;
		}
	}

	// Armor skills
	Y = TypeY + kRowStartOffset + (TextHeight + Margin) * 7.0f;
	DrawText(TEXT("Armor Skills"), HeaderColor, X, Y, Font, kHeaderScale);
	DrawText(TEXT("Level"), HeaderColor, X + kLevelOffset, Y, Font, kHeaderScale);
	Y += TextHeight + Margin * 2.0f;

	for (int32 i = (int32)ESkills::SkillArmorShield; i <= (int32)ESkills::SkillArmorPlate; ++i) {
		if (DrawSkill((ESkills)i, X, Y)) {
			Y += TextHeight + Margin;
		}
	}

	// Magic skills
	X = TypeX + kSecondColumn;
	Y = TypeY + kRowStartOffset;
	DrawText(TEXT("Magic Skills"), HeaderColor, X, Y, Font, kHeaderScale);
	DrawText(TEXT("Level"), HeaderColor, X + kLevelOffset, Y, Font, kHeaderScale);
	Y += TextHeight + Margin * 2.0f;

	for (int32 i = (int32)ESkills::SkillFireMagic; i <= (int32)ESkills::SkillBodyMagic; ++i) {
		if (DrawSkill((ESkills)i, X, Y)) {
			Y += TextHeight + Margin;
		}
	}

	// Misc skills
	X = TypeX + kSecondColumn * 2.0f;
	Y = TypeY + kRowStartOffset;
	DrawText(TEXT("Misc Skills"), HeaderColor, X, Y, Font, kHeaderScale);
	DrawText(TEXT("Level"), HeaderColor, X + kLevelOffset, Y, Font, kHeaderScale);
	Y += TextHeight + Margin * 2.0f;

	for (int32 i = (int32)ESkills::SkillAlchemy; i <= (int32)ESkills::SkillSwimming; ++i) {
		if (DrawSkill((ESkills)i, X, Y)) {
			Y += TextHeight + Margin;
		}
	}

	// Draw tooltip
	if (IsRightMousePressed() && !HoveredHitbox.ToString().IsNumeric()) {
		DrawTooltip(SkillFromName(HoveredHitbox.ToString()));
	}
}

bool ANNSHUD::IsRightMousePressed() const
{
	return Cast<APlayerController>(PlayerParty->GetController())->IsInputKeyDown(EKeys::RightMouseButton);
}

void ANNSHUD::DrawSpellbook()
{
	static const FLinearColor ButtonColor = kBlueColor;
	static const FLinearColor HighlightColor = kBlueColor;

	if (TargetMode != ETargetMode::TargetNone) {
		// For targeting, don't draw anything, and let the user click on what they want to target based on the mode
		int32 ScreenWidth, ScreenHeight;
		Cast<APlayerController>(PlayerParty->GetController())->GetViewportSize(ScreenWidth, ScreenHeight);

		const FString TargetText = FString::Printf(TEXT("Select a%s target"), (TargetMode == ETargetMode::TargetAlly ? TEXT(" friendly") : TEXT("n enemy")));
		static const float TextScale = 3.0f;
		const int32 TextWidth = Font->GetStringSize(*TargetText) * TextScale;
		DrawText(TargetText, kRedColor, (ScreenWidth - TextWidth) / 2, 20, Font, TextScale);

		// Draw tooltip if right-clicking, needed to see enemy health
		if (IsRightMousePressed()) {
			AActor* NearestActor = nullptr;
			float NearestDistance = 0.0f;
			if (PlayerParty->GetActorUnderMouse(ActorTags::kIdentifiableTag, APlayerPartyCharacter::kMaxTargetDistance, NearestActor, NearestDistance)) {
				if (NearestActor->IsA<APickup>()) {
					DrawTooltip(Cast<APickup>(NearestActor));
				} else if (NearestActor->IsA<ABaseEnemyCharacter>()) {
					DrawTooltip(Cast<ABaseEnemyCharacter>(NearestActor));
				}
			}
		}
	} else {
		// Draw border
		DrawBorder(SpellbookBorderMaterial, nullptr, nullptr, SpellbookBorderMaterial, SpellbookBorderWidth, SpellbookX - Margin, SpellbookY - Margin, SpellbookWidth + Margin * 2.0f, SpellbookHeight + Margin * 2.0f);

		// Draw spells
		if (SpellbookSchool != ESkills::SkillTotalSkills) {
			for (int32 i = 0; i < EnumMaps::SchoolSpells[SpellbookSchool].Num(); ++i) {
				const ESpells Spell = EnumMaps::SchoolSpells[SpellbookSchool][i];
				if (!PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->HasLearnedSpell(Spell)) continue;

				const float X = SpellbookX + ((i % 3) * (SpellbookSpellSize + Margin));
				const float Y = SpellbookY + ((i / 3) * (SpellbookSpellSize + Margin));
				DrawTextureSimple(GameInstance->GetTexture(EnumMaps::SpellInfo[Spell].SpellbookTexture), X, Y);
				if (HoveredHitbox == FName(*(EnumMaps::SpellInfo[Spell].Name))) {
					DrawLine(X, Y + SpellbookSpellSize + 1, X + SpellbookSpellSize, Y + SpellbookSpellSize + 1, HighlightColor);
				}
			}
		}

		// Draw buttons
		float Y = SpellbookY;
		for (auto& School : EnumMaps::SchoolSpells) {
			if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->GetSkill(School.Key) > 0) {
				// If so, draw the tab
				if (School.Key == SpellbookSchool) {
					DrawRect(ButtonColor, SpellbookButtonX, Y, SpellbookButtonWidth, SpellbookButtonHeight);
				} else {
					DrawRect(ButtonColor, SpellbookButtonX + 1, Y, SpellbookButtonWidth - 1, SpellbookButtonHeight);
				}
				DrawText(SkillNames[(int32)School.Key], FontColor, SpellbookButtonX + Margin, Y + (SpellbookButtonHeight - TextHeight) / 2, Font);
			}

			Y += SpellbookButtonHeight + 1;
		}

		// Draw close button
		DrawRect(ButtonColor, SpellbookButtonX, SpellbookY + SpellbookHeight - SpellbookButtonHeight, SpellbookButtonWidth, SpellbookButtonHeight);
		DrawText(ChestCloseName.ToString(), FontColor, SpellbookButtonX + Margin, SpellbookY + SpellbookHeight - (3 * SpellbookButtonHeight - TextHeight) / 2, Font);

		// Draw tooltip
		if (IsRightMousePressed() && !HoveredHitbox.ToString().IsNumeric() && HoveredHitbox != ChestCloseName) {
			DrawTooltip(SpellFromName(HoveredHitbox.ToString()));
		}
	}
}

void ANNSHUD::DrawMap()
{
	if (Minimap != nullptr) {
		DrawTexture(Minimap, MapX, MapY, 600.0f, 600.0f, 0.0f, 0.0f, 1.0f, 1.0f, FLinearColor::White, EBlendMode::BLEND_Opaque);
	}

	static const float Sixth = 1.0f / 6.0f;
	static const float Third = 1.0f / 3.0f;
	const float HalfMinimapSize = MinimapSize / 2.0f;
	const FVector HalfExtents = FVector(MinimapSize * Sixth, MinimapSize * Sixth, 40000.0f);
	const float LocationX = ((-PlayerParty->GetActorLocation().X + MinimapOrigin.X) + HalfMinimapSize) / MinimapSize;
	const float LocationY = ((PlayerParty->GetActorLocation().Y - MinimapOrigin.Y) + HalfMinimapSize) / MinimapSize;
	const float Rotation = PlayerParty->GetActorRotation().Yaw;

	if (PlayerParty->HasStatus(EPartyStatusEffects::StatusWizardsEye)) {
		TArray<FOverlapResult> AllActorsInRadius;
		if (GetWorld()->OverlapMultiByChannel(AllActorsInRadius, PlayerParty->GetActorLocation(), FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, FCollisionShape::MakeBox(HalfExtents))) {
			for (auto& OverlapResult : AllActorsInRadius) {
				// Make sure we're only testing the collision capsule
				if (!OverlapResult.GetComponent()->IsA<UShapeComponent>()) continue;

				AActor* Actor = OverlapResult.GetActor();
				FLinearColor Color = FLinearColor::Black;
				if (Actor->ActorHasTag(ActorTags::kFriendlyTag)) {
					Color = FLinearColor::Green;
				} else if (Actor->ActorHasTag(ActorTags::kEnemyTag)) {
					if (Actor->ActorHasTag(ActorTags::kVisibleTag)) {
						Color = FLinearColor::Red;
					}
				} else if (Actor->ActorHasTag(ActorTags::kPickupableTag)) {
					if (PlayerParty->StatusEffects[EPartyStatusEffects::StatusWizardsEye].Strength >= (int32)EMastery::MasteryExpert) {
						Color = FLinearColor::Blue;
					}
				} else if (Actor->IsA<AChest>()) {
					if (PlayerParty->StatusEffects[EPartyStatusEffects::StatusWizardsEye].Strength >= (int32)EMastery::MasteryMaster) {
						Color = FLinearColor(0.4f, 0.2f, 0.0f);
					}
				}

				if (Color != FLinearColor::Black) {
					const float ActorX = ((-Actor->GetActorLocation().X + MinimapOrigin.X) + HalfMinimapSize) / MinimapSize;
					const float ActorY = ((Actor->GetActorLocation().Y - MinimapOrigin.Y) + HalfMinimapSize) / MinimapSize;
					DrawRect(Color, MapX + (MapSize * ActorY) - 1.0f, MapY + (MapSize * ActorX) - 1.0f, 3.0f, 3.0f);
				}
			}
		}
	}

	DrawTexture(Arrow, MapX + LocationY * MapSize - 4.0f, MapY + LocationX * MapSize - 4.0f, 9.0f, 9.0f, 0.0f, 0.0f, 1.0f, 1.0f,
		FLinearColor::White, EBlendMode::BLEND_Translucent, 1.0, false, Rotation, FVector2D(0.5f, 0.5f));
}

void ANNSHUD::DrawTooltip(UItem* Item)
{
	if (Item == nullptr) return;
	int32 SkillValue = 0;
	EMastery Mastery = EMastery::MasteryBasic;
	PlayerParty->GetPartySkill(ESkills::SkillIdentifyItem, SkillValue, Mastery);
	Item->Identify(SkillValue, Mastery);

	static const FLinearColor TextColor = kWhiteColor;

	if (Item->bIsIdentified && Item->IsA<USpellbook>()) {
		DrawTooltip(Cast<USpellbook>(Item)->Spell);
	} else {
		const FString& Name = Item->GetName().ToString();
		TArray<FString> DescriptionLines = Item->ToString();

		// First calculate the width/height needed
		int32 NameHeight, NameWidth;
		Font->GetStringHeightAndWidth(*Name, NameHeight, NameWidth);
		int32 Height = NameHeight + Margin, Width = NameWidth;
		for (auto& Line : DescriptionLines) {
			int32 H, W;
			Font->GetStringHeightAndWidth(*Line, H, W);
			Height += H;
			if (W > Width) Width = W;

		}

		float X, Y;
		// Draw the border
		DrawTooltipBorder(Width, Height, X, Y);
		// Draw the tooltip text
		DrawText(Name, TextColor, X + (Width - NameWidth) / 2, Y, Font);
		Y += NameHeight + Margin;
		for (auto& Line : DescriptionLines) {
			DrawText(Line, TextColor, X, Y, Font);
			Y += NameHeight;
		}
	}
}

void ANNSHUD::DrawTooltip(EStats Stat)
{
	if (Stat == EStats::StatTotalStats) return;
	static const FLinearColor TextColor = kWhiteColor;

	const FString& Name = StatNames[(int32)Stat];
	const FString& Description = StatTooltips[(int32)Stat];

	TArray<FString> DescriptionLines;
	Description.ParseIntoArrayLines(DescriptionLines);

	// First calculate the width/height needed
	int32 NameHeight, NameWidth;
	Font->GetStringHeightAndWidth(*Name, NameHeight, NameWidth);
	int32 Height = NameHeight + Margin, Width = NameWidth;
	for (auto& Line : DescriptionLines) {
		int32 H, W;
		Font->GetStringHeightAndWidth(*Line, H, W);
		Height += H;
		if (W > Width) Width = W;
		
	}

	float X, Y;
	// Draw the border
	DrawTooltipBorder(Width, Height, X, Y);
	// Draw the tooltip text
	DrawText(Name, TextColor, X + (Width - NameWidth) / 2, Y, Font);
	DrawText(Description, TextColor, X, Y + NameHeight + Margin, Font);
}

void ANNSHUD::DrawMasteryTooltip(const FString& Name, const MasteryTooltip& Tooltip, const EMastery& Mastery, const EMastery& MaxAllowed)
{
	DrawMasteryTooltip(Name, Tooltip, Mastery, (int32)MaxAllowed);
}

void ANNSHUD::DrawMasteryTooltip(const FString& Name, const MasteryTooltip& Tooltip, const EMastery& Mastery, int32 MaxAllowed)
{
	static const FLinearColor TextColor = kWhiteColor;
	static const FLinearColor UnavailableTextColor = kLightGrayColor;
	static const FString ClassRestriction = TEXT("Not available to this class");

	const FString& Description = Tooltip.Description;
	const FString Basic = FString::Printf(TEXT("Novice: %s"), MaxAllowed >= (int32)EMastery::MasteryBasic ? *Tooltip.Basic : *ClassRestriction);
	const FString Expert = FString::Printf(TEXT("Expert: %s"), MaxAllowed >= (int32)EMastery::MasteryExpert ? *Tooltip.Expert : *ClassRestriction);
	const FString Master = FString::Printf(TEXT("Master: %s"), MaxAllowed >= (int32)EMastery::MasteryMaster ? *Tooltip.Master : *ClassRestriction);

	TArray<FString> DescriptionLines;
	Description.ParseIntoArrayLines(DescriptionLines);

	// First calculate the width/height needed
	int32 NameHeight, NameWidth;
	Font->GetStringHeightAndWidth(*Name, NameHeight, NameWidth);
	int32 Width = NameWidth;

	int32 DescriptionHeight = 0;
	for (auto& Line : DescriptionLines) {
		int32 H, W;
		Font->GetStringHeightAndWidth(*Line, H, W);
		DescriptionHeight += H;
		Width = FMath::Max(W, Width);
	}
	int32 Height = NameHeight + Margin * 2 + DescriptionHeight;

	DescriptionLines.Empty();
	DescriptionLines.Add(Basic);
	DescriptionLines.Add(Expert);
	DescriptionLines.Add(Master);
	for (auto& Line : DescriptionLines) {
		int32 H, W;
		Font->GetStringHeightAndWidth(*Line, H, W);
		Height += H;
		Width = FMath::Max(W, Width);
	}

	float X, Y;
	// Draw the border
	DrawTooltipBorder(Width, Height, X, Y);
	// Draw the tooltip text
	DrawText(Name, TextColor, X + (Width - NameWidth) / 2, Y, Font);
	Y += NameHeight + Margin;
	DrawText(Description, TextColor, X, Y, Font);
	Y += DescriptionHeight + Margin;
	DrawText(Basic, MaxAllowed >= (int32)EMastery::MasteryBasic ? TextColor : UnavailableTextColor, X, Y, Font);
	Y += NameHeight;

	DrawText(Expert, Mastery >= EMastery::MasteryExpert ? TextColor : UnavailableTextColor, X, Y, Font);
	Y += NameHeight;
	DrawText(Master, Mastery >= EMastery::MasteryMaster ? TextColor : UnavailableTextColor, X, Y, Font);
}

void ANNSHUD::DrawTooltip(ESkills Skill)
{
	if (Skill == ESkills::SkillTotalSkills) return;

	DrawMasteryTooltip(SkillNames[(int32)Skill], SkillTooltips[(int32)Skill], PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->Mastery(Skill),
					   UNNSGameInstance::ClassSkillMap[PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->Class].Masteries[Skill]);
}

void ANNSHUD::DrawTooltip(ESpells Spell)
{
	if (Spell == ESpells::SpellTotalSpells) return;

	DrawMasteryTooltip(EnumMaps::SpellInfo[Spell].Name, SpellTooltips[(int32)Spell], PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->Mastery(EnumMaps::SpellInfo[Spell].Skill),
					   PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->GetMasteryAllowed(Spell));
}

void ANNSHUD::DrawTooltip(EStatusEffects Effect)
{
	if (Effect == EStatusEffects::StatusTotalStatusEffects) return;
	DrawEffectTooltip((int32)Effect, PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->StatusEffects[Effect], false);
}

void ANNSHUD::DrawTooltip(EPartyStatusEffects Effect)
{
	if (Effect == EPartyStatusEffects::StatusTotalPartyStatusEffects) return;
	DrawEffectTooltip((int32)Effect, PlayerParty->StatusEffects[Effect], true);
}

void ANNSHUD::DrawEffectTooltip(int32 Status, const FStatusEffect& Effect, bool bPartyEffect)
{
	static const FLinearColor TextColor = kWhiteColor;

	const FString& Name = bPartyEffect ? PartyStatusEffectNames[Status] : StatusEffectNames[Status];

	FString DurationString = TEXT("");
	if (Effect.IsPermanent()) {
		DurationString = TEXT("Forever");
	} else {
		const int32 Hours = Effect.Duration / 60;
		const int32 Minutes = Effect.Duration - Hours * 60;
		FString HourString = FString::Printf(TEXT("%i hour%s"), Hours, (Hours > 1) ? TEXT("s") : TEXT(""));
		FString MinuteString = FString::Printf(TEXT("%i minutes"), Minutes);
		if (Hours > 0) {
			DurationString.Append(HourString);
			DurationString.Append(TEXT(" "));
		}
		DurationString.Append(MinuteString);
	}

	const FString Description = FString::Printf(*StatusEffectTooltip, bPartyEffect ? *PartyStatusEffectTooltips[Status]  : *StatusEffectTooltips[Status], Effect.Strength, *DurationString);

	TArray<FString> DescriptionLines;
	Description.ParseIntoArrayLines(DescriptionLines);

	// First calculate the width/height needed
	int32 NameHeight, NameWidth;
	Font->GetStringHeightAndWidth(*Name, NameHeight, NameWidth);
	int32 Height = NameHeight + Margin, Width = NameWidth;
	for (auto& Line : DescriptionLines) {
		int32 H, W;
		Font->GetStringHeightAndWidth(*Line, H, W);
		Height += H;
		if (W > Width) Width = W;
	}

	float X, Y;
	// Draw the border
	DrawTooltipBorder(Width, Height, X, Y);
	// Draw the tooltip text
	DrawText(Name, TextColor, X + (Width - NameWidth) / 2, Y, Font);
	DrawText(Description, TextColor, X, Y + NameHeight + Margin, Font);
}

void ANNSHUD::DrawTooltip(APickup* Pickup)
{
	if (Pickup->Item != nullptr) {
		// If there's an item, draw it. Otherwise, just draw the gold
		DrawTooltip(Pickup->Item);
	} else {
		static const FLinearColor TextColor = kWhiteColor;
		int32 Height, Width;
		const FString Gold = FString::Printf(TEXT("%i Gold"), Pickup->Gold);
		Font->GetStringHeightAndWidth(Gold, Height, Width);
		float X, Y;
		// Draw the border
		DrawTooltipBorder(Width, Height, X, Y);
		// Draw the tooltip text
		DrawText(Gold, TextColor, X, Y, Font);
	}
}

void ANNSHUD::DrawTooltip(ABaseEnemyCharacter* Character)
{
	static const FLinearColor TextColor = kWhiteColor;
	static const FLinearColor HeaderTextColor = kDarkGrayColor;
	static const float BGWidth = 200.0f;
	static const float BGHeight = 310.0f;

	// Get ID Monster skill information
	int32 Level = 0;
	EMastery Mastery = EMastery::MasteryBasic;
	PlayerParty->GetPartySkill(ESkills::SkillIdentifyMonster, Level, Mastery);

	float X, Y;
	// Draw the border
	DrawTooltipBorder(BGWidth, BGHeight, X, Y);

	UCharacterSheet* PartyMember = Character->PartyMembers[0];

	// Draw the name
	int32 NameWidth = Font->GetStringSize(*Character->DataTableName.ToString());
	DrawText(Character->DataTableName.ToString(), TextColor, X + (BGWidth - NameWidth * 2) / 2, Y, Font, 2.0f);
	Y += TextHeight * 2 + Margin;
	
	// Draw health
	FString HealthString;
	if (Level > 0) {
		HealthString = FString::Printf(TEXT("Health: %i / %i"), FMath::CeilToInt(PartyMember->CurrentHealth), PartyMember->GetStat(EStats::StatHealth));
	} else {
		HealthString = FString::Printf(TEXT("Health: ?? / ??"));
	}
	int32 HealthWidth = Font->GetStringSize(*HealthString);
	DrawText(HealthString, TextColor, X + (BGWidth - HealthWidth) / 2, Y, Font);
	Y += TextHeight + Margin;

	// Draw weapon attributes
	DrawText(TEXT("Equipment:"), HeaderTextColor, X, Y, Font);
	float EquipY = Y + TextHeight + Margin / 2;

	FString DamageString, ArmorString, DelayString;
	if (Mastery == EMastery::MasteryMaster) {
		UEquippableItem* MainHand = PartyMember->Equipment[(int32)EEquipType::EquipMainHand];
		if (MainHand != nullptr) {
			DamageString = FString::Printf(TEXT("Damage: %id%i +%i"), MainHand->Dice, MainHand->DiceSides, MainHand->Damage);
			ArmorString = FString::Printf(TEXT("Armor: %i"), MainHand->Armor);
		} else {
			DamageString = TEXT("Damage: ??");
			ArmorString = TEXT("Armor: ??");
		}
		DelayString = FString::Printf(TEXT("Delay: %.2f"));
	} else {
		DamageString = TEXT("Damage: ??");
		ArmorString = TEXT("Armor: ??");
		DelayString = TEXT("Delay: ??");
	}

	DrawText(DamageString, TextColor, X, EquipY, Font);
	EquipY += TextHeight + Margin / 2;
	DrawText(ArmorString, TextColor, X, EquipY, Font);
	EquipY += TextHeight + Margin / 2;
	DrawText(DelayString, TextColor, X, EquipY, Font);
	EquipY += TextHeight + Margin;

	// Draw status effects
	DrawText(TEXT("Status Effects:"), HeaderTextColor, X, EquipY, Font);
	EquipY += TextHeight + Margin / 2;
	for (auto& Effect : PartyMember->StatusEffects) {
		DrawText(StatusEffectNames[(int32)Effect.Key], TextColor, X, EquipY, Font);
		EquipY += TextHeight + Margin / 2;
	}

	// Draw stats
	DrawText(TEXT("Stats:"), HeaderTextColor, X + BGWidth / 2, Y, Font);
	float StatY = Y + TextHeight + Margin / 2;
	for (int32 i = 0; i < (int32)EStats::StatTotalStats; ++i) {
		FString Text;
		if (Mastery >= EMastery::MasteryExpert) {
			Text = FString::Printf(TEXT("%s: %i"), *StatNames[i], PartyMember->GetStat((EStats)i));
		} else {
			Text = FString::Printf(TEXT("%s: ??"), *StatNames[i]);
		}
		DrawText(Text, TextColor, X + BGWidth / 2, StatY, Font);
		StatY += TextHeight + Margin / 2;
	}
}

void ANNSHUD::DrawTooltipBorder(float Width, float Height, float& OutX, float& OutY)
{
	float MouseX, MouseY;
	Cast<APlayerController>(PlayerParty->GetController())->GetMousePosition(MouseX, MouseY);
	// Offset from mouse position slightly so cursor doesn't obscure
	MouseX += 16.0f;
	MouseY += 16.0f;

	int32 ScreenWidth, ScreenHeight;
	Cast<APlayerController>(PlayerParty->GetController())->GetViewportSize(ScreenWidth, ScreenHeight);

	const float BorderWidth = Width + 2 * Margin + 2;
	const float BorderHeight = Height + 2 * Margin + 2;

	// Force the tooltip to stay on the screen if it's too big
	MouseX -= FMath::Max(0.0f, MouseX + BorderWidth - ScreenWidth);
	MouseY -= FMath::Max(0.0f, MouseY + BorderHeight - ScreenHeight);

	// Draw the border
	DrawBorder(TooltipBackgroundMaterial, nullptr, nullptr, TooltipBorderMaterial, TooltipBorderWidth, MouseX, MouseY, BorderWidth, BorderHeight);

	// Return position to start drawing text
	OutX = MouseX + Margin + 1;
	OutY = MouseY + Margin + 1;
}

float ANNSHUD::AddDialogHitBox(const FName& BoxName, float Y)
{
	AddHitBox(FVector2D(DialogSidebarX, Y), FVector2D(DialogSidebarWidth, TextHeight * DialogTextScale), BoxName, true);
	return Y + DialogSidebarSpacing + TextHeight * DialogTextScale;
}

float ANNSHUD::AddDialogComponentHitBox(UDialogComponent* Dialog, float Y)
{
	// Add hitboxes for every dialog option
	const FString& CharacterName = Dialog->GetOwner()->GetName();
	for (auto& DialogLine : Dialog->DialogLines) {
		if (DialogLine.Flag == EQuestFlags::QuestTotalQuests || GameInstance->QuestManager->IsCurrentOrPreviousStepCharacter(DialogLine.Flag, CharacterName, DialogLine.FlagNum)) {
			Y = AddDialogHitBox(DialogLine.Key, Y);
		}
	}
	// Add hitboxes for skills
	if (Dialog->Skill != ESkills::SkillTotalSkills) {
		UCharacterSheet* ActiveCharacter = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter];

		// Basic
		if (!(ActiveCharacter->GetBaseSkill(Dialog->Skill) > 0)) {
			Y = AddDialogHitBox(DialogSkillBasic, Y);
		} else {
			// Expert
			if (!ActiveCharacter->IsExpert(Dialog->Skill)) {
				Y = AddDialogHitBox(DialogSkillExpert, Y);
			} else {
				// Master, add even if already master (will do nothing in that case)
				Y = AddDialogHitBox(DialogSkillMaster, Y);
			}
		}
	}
	return Y;
}

void ANNSHUD::AddInventoryHitBox(float StartX, float StartY)
{
	static const FVector2D InventoryHitboxSize = FVector2D(InventoryBoxSize, InventoryBoxSize);
	// Add a hitbox for every inventory slot
	for (int32 i = 0; i < InventoryColumns; ++i) {
		for (int32 j = 0; j < InventoryRows; ++j) {
			const FVector2D Position = FVector2D(StartX + InventoryBoxSize * i + i, StartY + InventoryBoxSize * j + j);
			AddHitBox(Position, InventoryHitboxSize, CoordsToName(TPairInitializer<int32, int32>(i, j)), true);
		}
	}
}

void ANNSHUD::RefreshHitboxes()
{
	// Clear all existing hitboxes
	HitBoxMap.Empty();
	HoveredHitbox = FName();

	int32 ScreenWidth, ScreenHeight;
	Cast<APlayerController>(PlayerParty->GetController())->GetViewportSize(ScreenWidth, ScreenHeight);
	AddHitBox(FVector2D(0, 0), FVector2D(ScreenWidth, ScreenHeight), ScreenHitbox, true, -1);

	if (DialogComponent.IsValid()) {
		// Add hitboxes for every dialog option
		float Y = DialogSidebarKeyY;
		Y = AddDialogComponentHitBox(DialogComponent.Get(), Y);
		Y = AddDialogHitBox(DialogExit, DialogSidebarExitY);
	} else if (Building.IsValid()) {
		float Y = DialogSidebarKeyY;
		if (Building->IsA<AShopBuilding>()) {
			AShopBuilding* Shop = Cast<AShopBuilding>(Building.Get());
			if (CurrentDialogKey.IsNone()) {
				Y = AddDialogHitBox(StoreBuy, Y);
				Y = AddDialogHitBox(StoreInventory, Y);
				Y = AddDialogHitBox(StoreSkills, Y);
			} else if (CurrentDialogKey.IsEqual(StoreBuy)) {
				const int32 Rows = Shop->Items.Num() >= 8 ? 2 : 1;
				const int32 ItemsPerRow = FMath::CeilToInt(Shop->Items.Num() / Rows);
				const float ItemWidth = StoreWidth / ItemsPerRow;
				const float ItemHeight = StoreHeight / Rows;
				for (int32 CurrentRow = 0; CurrentRow < Rows; ++CurrentRow) {
					for (int32 i = 0; i < ItemsPerRow && (i + ItemsPerRow * CurrentRow < Shop->Items.Num()); ++i) {
						const float CenterY = StoreY + StoreHeight / Rows * CurrentRow + ItemHeight / 2;
						const float CenterX = StoreX + ItemWidth * i + ItemWidth / 2;
						UItem* Item = Shop->Items[i + (ItemsPerRow * CurrentRow)];
						if (Item != nullptr) {
							const float IconWidth = GameInstance->GetTexture(Item->Icon)->GetSurfaceWidth();
							const float IconHeight = GameInstance->GetTexture(Item->Icon)->GetSurfaceHeight();
							const FVector2D Position = FVector2D(CenterX - (IconWidth / 2), CenterY - (IconHeight / 2));
							AddHitBox(Position, FVector2D(IconWidth, IconHeight), FName(*FString::FromInt(i + ItemsPerRow * CurrentRow)), true);
						}
					}
				}
			} else if (CurrentDialogKey.IsEqual(StoreInventory)) {
				AddInventoryHitBox(ChestX, ChestY);
				Y = AddDialogHitBox(StoreSell, Y);
				Y = AddDialogHitBox(StoreIdentify, Y);
			} else if (CurrentDialogKey.IsEqual(StoreSell)) {
				AddInventoryHitBox(ChestX, ChestY);
			} else if (CurrentDialogKey.IsEqual(StoreIdentify)) {
				AddInventoryHitBox(ChestX, ChestY);
			} else if (CurrentDialogKey.IsEqual(StoreSkills)) {
				for (const auto& ShopSkill : EnumMaps::ShopSkills[Shop->ShopType]) {
					if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->CanLearnSkill(ShopSkill)) {
						Y = AddDialogHitBox(FName(*FString::FromInt((int32)ShopSkill)), Y);
					}
				}
			}
		} else if (Building->IsA<ATempleBuilding>()) {
			float SidebarY = DialogSidebarKeyY;
			if (CurrentDialogKey.IsEqual(StoreSkills)) {
				for (const auto& ShopSkill : EnumMaps::ShopSkills[EShopTypes::ShopTemple]) {
					if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->CanLearnSkill(ShopSkill)) {
						Y = AddDialogHitBox(FName(*FString::FromInt((int32)ShopSkill)), Y);
					}
				}
			} else {
				for (int32 i = 0; i < PlayerParty->PartyMembers.Num(); ++i) {
					Y = AddDialogHitBox(FName(*FString::FromInt(i+1)), Y);
				}
				Y = AddDialogHitBox(StoreSkills, Y);
			}
		} else if (Building->IsA<ATavernBuilding>()) {
			ATavernBuilding* Tavern = Cast<ATavernBuilding>(Building.Get());
			if (CurrentDialogKey.IsEqual(StoreSkills)) {
				for (const auto& ShopSkill : EnumMaps::ShopSkills[EShopTypes::ShopTavern]) {
					if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->CanLearnSkill(ShopSkill)) {
						Y = AddDialogHitBox(FName(*FString::FromInt((int32)ShopSkill)), Y);
					}
				}
			} else {
				Y = AddDialogHitBox(TavernRestName, Y);
				Y = AddDialogComponentHitBox(Tavern->DialogComponent, Y);
				Y = AddDialogHitBox(StoreSkills, Y);
			}
		}
		AddDialogHitBox(DialogExit, DialogSidebarExitY);
	} else if (Chest.IsValid()) {
		AddInventoryHitBox(ChestX, ChestY);
		// Add hitbox for close button
		AddHitBox(FVector2D(ChestCloseX, ChestCloseY), FVector2D(ChestCloseWidth, ChestCloseHeight), ChestCloseName, true);
	} else if (MenuType == EMenuType::MenuSpellbook) {
		if (TargetMode != ETargetMode::TargetNone) {
			if (TargetMode == ETargetMode::TargetAlly) {
				float CurrentY = PlayerYStart;
				for (int32 i = 0; i < UNNSGameInstance::kCharactersInParty; ++i) {
					// Hitbox per player character, set priority higher than hitbox above
					AddHitBox(FVector2D(PlayerXStart, CurrentY), FVector2D(PlayerBackgroundWidth, PlayerBackgroundHeight), FName(*FString::FromInt(i)), true, 1);
					CurrentY += PlayerBackgroundHeight + PlayerSpacing;
				}
			}
		} else {
			// Add spell hitboxes
			if (SpellbookSchool != ESkills::SkillTotalSkills) {
				for (int32 i = 0; i < EnumMaps::SchoolSpells[SpellbookSchool].Num(); ++i) {
					const ESpells Spell = EnumMaps::SchoolSpells[SpellbookSchool][i];
					if (!PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->HasLearnedSpell(Spell)) continue;

					const float X = SpellbookX + ((i % 3) * (SpellbookSpellSize + Margin));
					const float Y = SpellbookY + ((i / 3) * (SpellbookSpellSize + Margin));
					AddHitBox(FVector2D(X, Y), FVector2D(SpellbookSpellSize, SpellbookSpellSize), FName(*EnumMaps::SpellInfo[Spell].Name), true);
				}
			}

			// Add hitbox for spellbook pages
			float Y = SpellbookY;
			for (auto& School : EnumMaps::SchoolSpells) {
				if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->GetSkill(School.Key) > 0) {
					if (School.Key != SpellbookSchool) {
						AddHitBox(FVector2D(SpellbookButtonX + 1, Y), FVector2D(SpellbookButtonWidth - 1, SpellbookButtonHeight), FName(*FString::FromInt((int32)School.Key)), true);
					}
				}

				Y += SpellbookButtonHeight + 1;
			}

			// Draw close button
			AddHitBox(FVector2D(SpellbookButtonX, SpellbookY + SpellbookHeight - SpellbookButtonHeight), FVector2D(SpellbookButtonWidth, SpellbookButtonHeight), ChestCloseName, true);
		}
	} else if (MenuType != EMenuType::MenuNone && MenuType != EMenuType::MenuQuests && MenuType != EMenuType::MenuBestiary) {
		// Add a hitbox for every equipment slot
		AddHitBox(FVector2D(CharacterX, CharacterY), FVector2D(CharacterWidth, CharacterHeight), FName(*FString::FromInt((int32)EEquipType::EquipTotalSlots)), true, 0);
		// Add hitboxes for status effects
		int32 StatusCounter = 0;
		for (auto& Effect : PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->StatusEffects) {
			const float X = StatusX + (StatusCounter / NumStatusRows) * 80.0f;
			const float Y = StatusY + (StatusCounter % NumStatusRows) * (TextHeight + Margin);
			AddHitBox(FVector2D(X, Y), FVector2D(75.0f, TextHeight), FName(*FString::Printf(TEXT("_status%i"), (int32)Effect.Key)), true);
			++StatusCounter;
		}
		StatusCounter = 0;
		for (auto& Effect : PlayerParty->StatusEffects) {
			const float X = 400.0f + StatusX + (StatusCounter / NumStatusRows) * 80.0f;
			const float Y = StatusY + (StatusCounter % NumStatusRows) * (TextHeight + Margin);
			AddHitBox(FVector2D(X, Y), FVector2D(75.0f, TextHeight), FName(*FString::Printf(TEXT("_partystatus%i"), (int32)Effect.Key)), true);
			++StatusCounter;
		}

		for (int32 i = 0; i < EquipOrder.Num(); ++i) {
			const HUDPosition& Position = EquipPositions[EquipOrder[i]];
			const FName Slot = FName(*FString::FromInt((int32)EquipOrder[i]));
			AddHitBox(FVector2D(CharacterX + Position.X, CharacterY + Position.Y), FVector2D(Position.Width, Position.Height), Slot, true, i + 1);
		}

		if (MenuType == EMenuType::MenuInventory) {
			AddInventoryHitBox(TypeX, TypeY);
		} else if (MenuType == EMenuType::MenuStats) {
			float Y = TypeY + 60.0f + TextHeight + Margin * 2.0f;
			float X = TypeX;

			for (int32 i = 0; i <= (int32)EStats::StatMana; ++i) {
				AddHitBox(FVector2D(X, Y), FVector2D(200.0f, TextHeight), FName(*StatNames[i]), true);
				Y += TextHeight + Margin;
			}

			X = TypeX + 300.0f;
			Y = TypeY + 60.0f + TextHeight + Margin * 2.0f;

			AddHitBox(FVector2D(X, Y), FVector2D(200.0f, TextHeight), FName(*StatNames[(int32)EStats::StatAccuracy]), true);
			Y += TextHeight + Margin;
			AddHitBox(FVector2D(X, Y), FVector2D(200.0f, TextHeight), FName(*StatNames[(int32)EStats::StatEvasion]), true);
			Y += (TextHeight + Margin) * 3.0f;

			Y += TextHeight + Margin * 2.0f;
			AddHitBox(FVector2D(X, Y), FVector2D(200.0f, TextHeight), FName(*StatNames[(int32)EStats::StatResistFire]), true);
			Y += TextHeight + Margin;
			AddHitBox(FVector2D(X, Y), FVector2D(200.0f, TextHeight), FName(*StatNames[(int32)EStats::StatResistAir]), true);
		} else if (MenuType == EMenuType::MenuSkills) {
			static const float kRowOffset = 60.0f;
			static const float kLevelOffset = 175.0f;
			static const float kSecondColumn = 275.0f;

			float X = TypeX;
			float Y = TypeY + kRowOffset + TextHeight + Margin * 2.0f;
			for (int32 i = (int32)ESkills::SkillWeaponSword; i <= (int32)ESkills::SkillWeaponBow; ++i) {
				if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->GetBaseSkill((ESkills)i) == 0) continue;
				AddHitBox(FVector2D(X, Y), FVector2D(kLevelOffset, TextHeight), FName(*SkillNames[i]), true);
				Y += TextHeight + Margin;
			}

			Y = TypeY + kRowOffset + (TextHeight + Margin) * 7.0f + TextHeight + Margin * 2.0f;
			for (int32 i = (int32)ESkills::SkillArmorShield; i <= (int32)ESkills::SkillArmorPlate; ++i) {
				if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->GetBaseSkill((ESkills)i) == 0) continue;
				AddHitBox(FVector2D(X, Y), FVector2D(kLevelOffset, TextHeight), FName(*SkillNames[i]), true);
				Y += TextHeight + Margin;
			}

			X = TypeX + kSecondColumn;
			Y = TypeY + kRowOffset + TextHeight + Margin * 2.0f;
			for (int32 i = (int32)ESkills::SkillFireMagic; i <= (int32)ESkills::SkillBodyMagic; ++i) {
				if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->GetBaseSkill((ESkills)i) == 0) continue;
				AddHitBox(FVector2D(X, Y), FVector2D(kLevelOffset, TextHeight), FName(*SkillNames[i]), true);
				Y += TextHeight + Margin;
			}

			X = TypeX + kSecondColumn * 2.0f;
			Y = TypeY + kRowOffset + TextHeight + Margin * 2.0f;
			for (int32 i = (int32)ESkills::SkillAlchemy; i <= (int32)ESkills::SkillSwimming; ++i) {
				if (PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->GetBaseSkill((ESkills)i) == 0) continue;
				AddHitBox(FVector2D(X, Y), FVector2D(kLevelOffset, TextHeight), FName(*SkillNames[i]), true);
				Y += TextHeight + Margin;
			}
		}
	}

	for (const auto& HitBox : HitBoxMap) {
		float X, Y;
		GetOwningPlayerController()->GetMousePosition(X, Y);
		if (HitBox.Contains(FVector2D(X, Y))) {
			HoveredHitbox = HitBox.GetName();
			break;
		}
	}
}

FName ANNSHUD::CoordsToName(const TPair<int32, int32>& Coords)
{
	return FName(*FString::Printf(TEXT("%i,%i"), Coords.Key, Coords.Value));
}

TPair<int32, int32> ANNSHUD::NameToCoords(const FName& Name)
{
	TArray<FString> Tokens;
	Name.ToString().ParseIntoArray(Tokens, TEXT(","));

	return TPairInitializer<int32, int32>(FCString::Atoi(*(Tokens[0])), FCString::Atoi(*(Tokens[1])));
}

void ANNSHUD::NotifyHitBoxClick(FName BoxName)
{
	ClickedHitbox = BoxName;
	if (BoxName.IsEqual(ScreenHitbox)) {
		// Ignore background clicks
		return;
	} else if (Chest.IsValid()) {
		if (!BoxName.IsEqual(ChestCloseName)) {
			TPair<int32, int32> Position = NameToCoords(BoxName);
			UItem* Item = Chest->Inventory->GetItem(Position.Key, Position.Value);
			if (Item == nullptr) {
				if (PlayerParty->HeldItem != nullptr) {
					UItem* SwapItem = nullptr;
					if (Chest->Inventory->DropOrSwap(Position.Key, Position.Value, PlayerParty->HeldItem, SwapItem)) {
						PlayerParty->HeldItem = SwapItem;
					}
				}
			} else {
				if (PlayerParty->AddItemToInventory(Item)) {
					Chest->Inventory->RemoveItem(Item);
				}
			}
		}
	} else if (Building.IsValid()) {
		if (Building->IsA<AShopBuilding>()) {
			AShopBuilding* Shop = Cast<AShopBuilding>(Building.Get());
			if (CurrentDialogKey.IsEqual(StoreBuy) && BoxName.ToString().IsNumeric()) {
				const int32 Index = FCString::Atoi(*(BoxName.ToString()));
				UItem* Item = Shop->Items[Index];
				const uint32 Cost = PlayerParty->ConvertCost(Item->GetBuyCost(), false);
				if (PlayerParty->GetGold() >= Cost && PlayerParty->AddItemToInventory(Item)) {
					Shop->Items[Index] = nullptr;
					PlayerParty->RemoveGold(Cost);
					RefreshHitboxes();
				}
			} else if (BoxName.ToString().Contains(TEXT(","))) {
				if (CurrentDialogKey.IsEqual(StoreSell)) {
					UInventory* ActiveInventory = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->Inventory;
					TPair<int32, int32> Position = NameToCoords(BoxName);
					UItem* Item = ActiveInventory->GetItem(Position.Key, Position.Value);
					if (Item != nullptr && Item->MatchesShop(Shop->ShopType) && ActiveInventory->RemoveItem(Item)) {
						PlayerParty->AddGold(PlayerParty->ConvertCost(Item->GetSellCost(), true));
						RefreshHitboxes();
					}
				} else if (CurrentDialogKey.IsEqual(StoreIdentify)) {
					UInventory* ActiveInventory = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->Inventory;
					TPair<int32, int32> Position = NameToCoords(BoxName);
					UItem* Item = ActiveInventory->GetItem(Position.Key, Position.Value);
					if (Item != nullptr) {
						const uint32 Cost = PlayerParty->ConvertCost(Item->GetIDCost(), false);
						if (!Item->bIsIdentified && PlayerParty->GetGold() >= Cost) {
							Item->bIsIdentified = true;
							PlayerParty->RemoveGold(Cost);
						}
					}
				}
			}
		}
	} else if (MenuType != EMenuType::MenuNone && MenuType != EMenuType::MenuSpellbook && MenuType != EMenuType::MenuQuests && MenuType != EMenuType::MenuBestiary) {
		// Equipment hitboxes are a plain number
		if (BoxName.ToString().IsNumeric()) {
			EEquipType Slot = (EEquipType)FCString::Atoi(*(BoxName.ToString()));
			// Equip/unequip item if possible
			if (PlayerParty->HeldItem == nullptr) {
				PlayerParty->HeldItem = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->UnequipItem(Slot);
			} else {
				if (PlayerParty->HeldItem->IsA(UEquippableItem::StaticClass())) {
					PlayerParty->HeldItem = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->EquipItem(Cast<UEquippableItem>(PlayerParty->HeldItem), Slot);
				} else if (PlayerParty->HeldItem->IsA(UConsumableItem::StaticClass())) {
					if (Cast<UConsumableItem>(PlayerParty->HeldItem)->Consume(PlayerParty->PartyMembers[PlayerParty->ActiveCharacter])) {
						PlayerParty->HeldItem = nullptr;
						RefreshHitboxes();
					}
				}
			}
		} else if (BoxName.ToString().Contains(TEXT("_status"))) {
		} else if (MenuType == EMenuType::MenuInventory) {
			UInventory* ActiveInventory = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->Inventory;
			TPair<int32, int32> Position = NameToCoords(BoxName);
			if (PlayerParty->HeldItem == nullptr) {
				// If not holding item, pick up whatever is there
				UItem* Item = ActiveInventory->GetItem(Position.Key, Position.Value);
				if (Item != nullptr && ActiveInventory->RemoveItem(Item)) {
					PlayerParty->HeldItem = Item;
				}
			} else {
				// If holding item, try to drop it or swap it with something
				UItem* SwapItem = nullptr;
				if (ActiveInventory->DropOrSwap(Position.Key, Position.Value, PlayerParty->HeldItem, SwapItem)) {
					PlayerParty->HeldItem = SwapItem;
				}
			}
		} else if (MenuType == EMenuType::MenuStats) {
			// Upgrade stat
			const EStats Stat = StatFromName(BoxName.ToString());
			if (Stat <= EStats::StatMana) {
				PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->UpgradeBaseStat(Stat);
			}
		} else if (MenuType == EMenuType::MenuSkills) {
			// Upgrade skill
			PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->UpgradeBaseSkill(SkillFromName(BoxName.ToString()));
		}
	}
}

void ANNSHUD::NotifyHitBoxRightClick(FName BoxName)
{
	if (BoxName.IsEqual(ScreenHitbox)) {
		// ignore
	} else if (MenuType == EMenuType::MenuInventory) {
		if (PlayerParty->HeldItem != nullptr && PlayerParty->HeldItem->IsA<UPotionItem>()) {
			UInventory* ActiveInventory = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->Inventory;
			TPair<int32, int32> Position = NameToCoords(BoxName);
			UItem* Item = ActiveInventory->GetItem(Position.Key, Position.Value);
			if (Item != nullptr && Item->IsA<UPotionItem>()) {
				if (!Cast<UPotionItem>(PlayerParty->HeldItem)->Mix(PlayerParty.Get(), Cast<UPotionItem>(Item))) {
					ToggleHUDMenuType(EMenuType::MenuNone);
				}
				return;
			}
		}
	}
}

void ANNSHUD::HandleDialogHitbox(UDialogComponent* Dialog, const FName& BoxName)
{
	if (BoxName.IsEqual(ScreenHitbox)) {
		// Ignore background clicks
		return;
	} else if (BoxName.IsEqual(DialogExit)) {
		CurrentDialogKey = FName();
		DialogComponent = nullptr;
		Building = nullptr;
		RefreshHitboxes();
		Pause(false);
	} else if (BoxName.IsEqual(DialogSkillBasic)) {
		UCharacterSheet* Character = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter];
		if (Character->CanLearnSkill(Dialog->Skill)) {
			Character->LearnBaseSkill(Dialog->Skill);
			RefreshHitboxes();
		}
	} else if (BoxName.IsEqual(DialogSkillExpert)) {
		UCharacterSheet* Character = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter];
		if (Character->CanBecomeExpert(Dialog->Skill)) {
			Character->BecomeExpert(Dialog->Skill);
			RefreshHitboxes();
		}
	} else if (BoxName.IsEqual(DialogSkillMaster)) {
		UCharacterSheet* Character = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter];
		if (Character->CanBecomeMaster(Dialog->Skill)) {
			Character->BecomeMaster(Dialog->Skill);
			RefreshHitboxes();
		}
	} else {
		CurrentDialogKey = BoxName;
		const FString& CharacterName = Dialog->GetOwner()->GetName();
		for (auto& DialogLine : Dialog->DialogLines) {
			if (DialogLine.Key.IsEqual(CurrentDialogKey)) {
				if (DialogLine.Flag != EQuestFlags::QuestTotalQuests) {
					GameInstance->QuestManager->UpdateQuestFromDialog(DialogLine.Flag, CharacterName);
				}
				break;
			}
		}
		RefreshHitboxes();
	}
}

void ANNSHUD::NotifyHitBoxRelease(FName BoxName)
{
	if (!ClickedHitbox.IsEqual(BoxName)) {
		ClickedHitbox = FName();
		return;
	}
	// Detect dialog changes on mouse release, because as soon as game is unpaused, mouse press triggers Interact event
	// Spellbook is first because it actually uses the background hitbox, so we have to check before the background hitbox is skipped
	if (MenuType == EMenuType::MenuSpellbook) {
		if (TargetMode == ETargetMode::TargetAlly) {
			if (BoxName.ToString().IsNumeric()) {
				// A party member was chosen
				const int32 Character = FCString::Atoi(*(BoxName.ToString()));
				PlayerParty->CastSpell(SelectedSpell, PlayerParty->PartyMembers[Character]);
				ToggleHUDMenuType(EMenuType::MenuNone);
			} else {
				// Cancel back to spellbook
				ResetSpell();
				RefreshHitboxes();
			}
		} else if (TargetMode == ETargetMode::TargetEnemy) {
			AActor* NearestActor = nullptr;
			float NearestDistance = 0;
			static const float Distance = APlayerPartyCharacter::kMaxTargetDistance;
			if (PlayerParty->GetActorUnderMouse(ActorTags::kAttackableTag, Distance, NearestActor, NearestDistance)) {
				// An enemy was chosen
				PlayerParty->CastSpell(SelectedSpell, Cast<ANNSCharacter>(NearestActor)->PartyMembers[0]);
				ToggleHUDMenuType(EMenuType::MenuNone);
			} else {
				// Cancel back to spellbook
				ResetSpell();
				RefreshHitboxes();
			}
		} else {
			if (BoxName.IsEqual(ChestCloseName)) {
				// Close the menu
				ToggleHUDMenuType(EMenuType::MenuNone);
			} else if (BoxName.ToString().IsNumeric()) {
				// Select a new page in the spellbook
				SpellbookSchool = (ESkills)FCString::Atoi(*(BoxName.ToString()));
				PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->LastSpellbookPage = SpellbookSchool;
				GameInstance->PlaySoundEffect2D(OpenMenuSound);
				RefreshHitboxes();
			} else if (!BoxName.IsEqual(ScreenHitbox)) {
				// A spell was chosen. Enter target mode
				SelectedSpell = SpellFromName(BoxName.ToString());
				TargetMode = EnumMaps::SpellInfo[SelectedSpell].TargetMode;
				// Master heroism effects the whole party
				if (SelectedSpell == ESpells::SpellHeroism && PlayerParty->PartyMembers[PlayerParty->ActiveCharacter]->IsMaster(ESkills::SkillBodyMagic)) {
					TargetMode = ETargetMode::TargetNone;
				}
				if (TargetMode == ETargetMode::TargetNone) {
					PlayerParty->CastSpell(SelectedSpell);
					ToggleHUDMenuType(EMenuType::MenuNone);
				} else {
					RefreshHitboxes();
				}
			}
		}
	} else if (BoxName.IsEqual(ScreenHitbox)) {
		// Ignore background clicks
	} else if (DialogComponent.IsValid()) {
		HandleDialogHitbox(DialogComponent.Get(), BoxName);
	} else if (Chest.IsValid()) {
		if (BoxName.IsEqual(ChestCloseName)) {
			Chest = nullptr;
			RefreshHitboxes();
			Pause(false);
		}
	} else if (Building.IsValid()) {
		if (BoxName.IsEqual(DialogExit)) {
			if (CurrentDialogKey.IsNone() || (Building->IsA<ATavernBuilding>() && !CurrentDialogKey.IsEqual(StoreSkills))) {
				AShopBuilding* Shop = Cast<AShopBuilding>(Building.Get());
				if (Shop != nullptr && Shop->Goodbye != nullptr) {
					GameInstance->PlaySoundEffect2D(Shop->Goodbye);
				}
				CurrentDialogKey = FName();
				Building = nullptr;
				RefreshHitboxes();
				Pause(false);
			} else {
				CurrentDialogKey = FName();
				RefreshHitboxes();
			}
		} else if (CurrentDialogKey.IsEqual(StoreSkills)) {
			UCharacterSheet* Character = PlayerParty->PartyMembers[PlayerParty->ActiveCharacter];
			const ESkills Skill = (ESkills)FCString::Atoi(*(BoxName.ToString()));
			if (Character->CanLearnSkill(Skill)) {
				Character->LearnBaseSkill(Skill);
				RefreshHitboxes();
			}
		} else if (BoxName.IsEqual(StoreSkills)) {
			CurrentDialogKey = BoxName;
			RefreshHitboxes();
		} else if (Building->IsA<AShopBuilding>()) {
			if (!CurrentDialogKey.IsEqual(StoreBuy) && !BoxName.ToString().Contains(TEXT(","))) {
				CurrentDialogKey = BoxName;
				RefreshHitboxes();
			}
		} else if (Building->IsA<ATempleBuilding>()) {
			ATempleBuilding* Temple = Cast<ATempleBuilding>(Building.Get());
			UCharacterSheet* Character = PlayerParty->PartyMembers[FCString::Atoi(*(BoxName.ToString())) - 1];
			const uint32 HealCost = FMath::RoundToInt(Character->GetHealCost() * Temple->Multiplier);
			if (HealCost > 0 && PlayerParty->GetGold() >= HealCost) {
				Character->FullHeal();
				PlayerParty->RemoveGold(HealCost);
			}
		} else if (Building->IsA<ATavernBuilding>()) {
			ATavernBuilding* Tavern = Cast<ATavernBuilding>(Building.Get());
			if (BoxName.IsEqual(TavernRestName)) {
				if (PlayerParty->GetGold() >= (uint32)Tavern->RestCost) {
					for (const auto& Character : PlayerParty->PartyMembers) {
						Character->Heal(Character->GetStat(EStats::StatHealth) - (int32)Character->DeathBarrier);
						Character->UpdateCurrentMana(Character->GetStat(EStats::StatMana));
					}
					PlayerParty->RemoveGold((uint32)Tavern->RestCost);
					GameInstance->TimeOfDayManager->AdvanceTimeOfDay(60.0f * 8.0f);
				}
			} else {
				HandleDialogHitbox(Tavern->DialogComponent, BoxName);
			}
		}
	}
	ClickedHitbox = FName();
}

void ANNSHUD::ResetSpell()
{
	SelectedSpell = ESpells::SpellTotalSpells;
	TargetMode = ETargetMode::TargetNone;
}

void ANNSHUD::NotifyHitBoxBeginCursorOver(FName BoxName)
{
	HoveredHitbox = BoxName;
}

void ANNSHUD::NotifyHitBoxEndCursorOver(FName BoxName)
{
	HoveredHitbox = FName(TEXT(""));
}

EStats ANNSHUD::StatFromName(const FString& Name)
{
	for (int32 i = 0; i < StatNames.Num(); ++i) {
		if (StatNames[i].Equals(Name)) {
			return (EStats)i;
		}
	}
	return EStats::StatTotalStats;
}

ESkills ANNSHUD::SkillFromName(const FString& Name)
{
	for (int32 i = 0; i < SkillNames.Num(); ++i) {
		if (SkillNames[i].Equals(Name)) {
			return (ESkills)i;
		}
	}
	return ESkills::SkillTotalSkills;
}

ESpells ANNSHUD::SpellFromName(const FString& Name)
{
	for (auto& Spell : EnumMaps::SpellInfo) {
		if (Spell.Value.Name == Name) {
			return Spell.Key;
		}
	}
	return ESpells::SpellTotalSpells;
}

// NOTE: ALL FUNCTIONS BELOW COPIED FROM AHUD.cpp
// UpdateAndDispatchHitBoxRightClickEvents() is a copy of UpdateAndDispatchHitBoxClickEvents, but forwards
// the hitbxoes to NotifyHitBoxRightClick() instead of NotifyHitBoxClick()
bool ANNSHUD::UpdateAndDispatchHitBoxRightClickEvents(FVector2D ClickLocation, const EInputEvent InEventType)
{
	const bool bIsClickEvent = (InEventType == IE_Pressed || InEventType == IE_DoubleClick);

	// Early out to avoid unnecessary expense of calling GetCoordinateOffset
	if ((bIsClickEvent && HitBoxMap.Num() == 0) || (!bIsClickEvent && HitBoxHits.Num() == 0))
	{
		return false;
	}

	ClickLocation += GetCoordinateOffset();

	bool bHit = false;

	// If this is a click event we may not have the hit box in the hit list yet (particularly for touch events) so we need to check all HitBoxes
	if (bIsClickEvent)
	{
		for (FHUDHitBox& HitBox : HitBoxMap)
		{
			if (HitBox.Contains(ClickLocation))
			{
				bHit = true;

				NotifyHitBoxRightClick(HitBox.GetName());

				if (HitBox.ConsumesInput())
				{
					break;	//Early out if this box consumed the click
				}
			}
		}
	} else
	{
		// Disabled since no longer using hitboxes to check for tooltips
		//for (FHUDHitBox* HitBoxHit : HitBoxHits)
		//{
		//	if (HitBoxHit->Contains(ClickLocation))
		//	{
		//		bHit = true;

		//		if (InEventType == IE_Released)
		//		{
		//			NotifyHitBoxRightRelease(HitBoxHit->GetName());
		//		}

		//		if (HitBoxHit->ConsumesInput() == true)
		//		{
		//			break;	//Early out if this box consumed the click
		//		}
		//	}
		//}
	}
	return bHit;
}

// This is a direct copy of a private function in AHUD
FVector2D ANNSHUD::GetCoordinateOffset() const
{
	FVector2D Offset(0.f, 0.f);

	ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(GetOwningPlayerController()->Player);

	if (LocalPlayer)
	{
		// Create a view family for the game viewport
		FSceneViewFamilyContext ViewFamily(FSceneViewFamily::ConstructionValues(
			LocalPlayer->ViewportClient->Viewport,
			GetWorld()->Scene,
			LocalPlayer->ViewportClient->EngineShowFlags)
			.SetRealtimeUpdate(true));

		// Calculate a view where the player is to update the streaming from the players start location
		FVector ViewLocation;
		FRotator ViewRotation;
		FSceneView* SceneView = LocalPlayer->CalcSceneView(&ViewFamily, /*out*/ ViewLocation, /*out*/ ViewRotation, LocalPlayer->ViewportClient->Viewport);

		if (SceneView)
		{
			Offset.X = (SceneView->ViewRect.Min.X - SceneView->UnscaledViewRect.Min.X) // This accounts for the borders when the aspect ratio is locked
				- SceneView->UnscaledViewRect.Min.X;						// And this will deal with the viewport offset if its a split screen

			Offset.Y = (SceneView->ViewRect.Min.Y - SceneView->UnscaledViewRect.Min.Y)
				- SceneView->UnscaledViewRect.Min.Y;
		}
	}

	return Offset;
}
