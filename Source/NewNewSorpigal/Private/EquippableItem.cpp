// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "EquippableItem.h"

#include "DataStructs.h"
#include "Globals.h"
#include "NNSGameInstance.h"
#include "NNSHUD.h"

UEquippableItem::UEquippableItem()
	: Super()
{
	StatModifiers.AddZeroed((int32)EStats::StatTotalStats);
	SkillModifiers.AddZeroed((int32)ESkills::SkillTotalSkills);
}

UEquippableItem* UEquippableItem::Create(const FEnemyData& Data)
{
	// Enemy weapons don't need icons/names since they're never displayed anywhere
	// Enemies also have their full delay in the UCharacterSheet's BaseDelay, so it's 0 here
	UEquippableItem* Item = NewObject<UEquippableItem>();
	Item->Dice = Data.Dice;
	Item->DiceSides = Data.DiceSides;
	Item->Damage = Data.Damage;
	Item->Delay = 0;
	Item->Armor = Data.Armor;
	Item->Slot = EEquipType::EquipMainHand;
	Item->Style = EEquipStyle::EquipOther;

	return Item;
}

UEquippableItem* UEquippableItem::Create(const FName& BaseName)
{
	UEquippableItem* Item = NewObject<UEquippableItem>();
	Item->Name = BaseName;
	Item->Init();

	return Item;
}

void UEquippableItem::Init()
{
	// Name must be set before this is called
	const FEquippableItemData* Data = UNNSGameInstance::EquippableItemDataTable->FindRow<FEquippableItemData>(Name, FString(TEXT("EquippableItemData")));

	Dice = Data->Dice;
	DiceSides = Data->DiceSides;
	Damage = Data->Damage;
	Delay = Data->Delay;
	Armor = Data->Armor;
	Slot = Data->Slot;
	Style  = Data->Style;

	Tier = Data->Tier;
	Icon = Data->Icon;
	IconWidth = Data->IconWidth;
	IconHeight = Data->IconHeight;
	PickupClass = Data->PickupClass;

	if (Name.IsEqual(FName(TEXT("Excalibur")))) {
		StatModifiers[(int32)EStats::StatStrength] = 20;
		StatModifiers[(int32)EStats::StatDexterity] = 20;
		StatModifiers[(int32)EStats::StatAccuracy] = 20;
		SkillModifiers[(int32)ESkills::SkillWeaponSword] = 5;
	} else if (Name.IsEqual(FName(TEXT("MinotaurKingsAxe")))) {
		StatModifiers[(int32)EStats::StatStrength] = 30;
		StatModifiers[(int32)EStats::StatResistFire] = 20;
		SkillModifiers[(int32)ESkills::SkillWeaponAxe] = 5;
		SkillModifiers[(int32)ESkills::SkillArmsmaster] = 5;
	} else if (Name.IsEqual(FName(TEXT("VorpalBlade")))) {
		StatModifiers[(int32)EStats::StatDexterity] = 20;
		StatModifiers[(int32)EStats::StatSpeed] = 40;
		StatModifiers[(int32)EStats::StatMana] = 100;
		SkillModifiers[(int32)ESkills::SkillWeaponDagger] = 5;
		SkillModifiers[(int32)ESkills::SkillIdentifyMonster] = 5;
	} else if (Name.IsEqual(FName(TEXT("EurytusBow")))) {
		StatModifiers[(int32)EStats::StatDexterity] = 40;
		StatModifiers[(int32)EStats::StatResistAir] = 10;
		SkillModifiers[(int32)ESkills::SkillWeaponBow] = 5;
		SkillModifiers[(int32)ESkills::SkillAirMagic] = 5;
	} else if (Name.IsEqual(FName(TEXT("Seven-leagueBoots")))) {
		StatModifiers[(int32)EStats::StatSpeed] = 60;
		StatModifiers[(int32)EStats::StatEvasion] = 10;
		SkillModifiers[(int32)ESkills::SkillArmorLeather] = 5;
		SkillModifiers[(int32)ESkills::SkillSwimming] = 5;
	} else if (Name.IsEqual(FName(TEXT("MithrilChainMail")))) {
		StatModifiers[(int32)EStats::StatVitality] = 20;
		StatModifiers[(int32)EStats::StatLuck] = 40;
		SkillModifiers[(int32)ESkills::SkillArmorChain] = 5;
		SkillModifiers[(int32)ESkills::SkillMerchant] = 5;
	} else if (Name.IsEqual(FName(TEXT("AncientKingsMask")))) {
		StatModifiers[(int32)EStats::StatVitality] = 20;
		StatModifiers[(int32)EStats::StatHealth] = 200;
		SkillModifiers[(int32)ESkills::SkillArmorPlate] = 5;
		SkillModifiers[(int32)ESkills::SkillBodybuilding] = 5;
	} else if (Name.IsEqual(FName(TEXT("Aegis")))) {
		StatModifiers[(int32)EStats::StatVitality] = 15;
		StatModifiers[(int32)EStats::StatHealth] = 400;
		SkillModifiers[(int32)ESkills::SkillArmorShield] = 5;
		SkillModifiers[(int32)ESkills::SkillBodyMagic] = 5;
	}
}

void UEquippableItem::SaveLoadCommon(FArchive& Archive)
{
	Super::SaveLoadCommon(Archive);

	Archive << StatModifiers;
	Archive << SkillModifiers;
}

bool UEquippableItem::MatchesShop(EShopTypes ShopType)
{
	if (Tier == 6) return false;

	if (ShopType == EShopTypes::ShopWeapon) {
		return Slot == EEquipType::EquipMainHand || Slot == EEquipType::EquipOffHand || Slot == EEquipType::EquipRanged;
	} else if (ShopType == EShopTypes::ShopArmorChest) {
		return Slot == EEquipType::EquipBody;
	}
	return ShopType == EShopTypes::ShopArmorOther;
}

TArray<FString> UEquippableItem::ToString() const
{
	if (bIsIdentified) {
		TArray<FString> Lines;
		Lines.Add(FString::Printf(TEXT("Skill: %s"), *ANNSHUD::SkillNames[(int32)EnumMaps::StyleToSkill[Style]]));
		if (Dice > 0) {
			Lines.Add(FString::Printf(TEXT("Damage: %id%i +%i"), Dice, DiceSides, Damage));
		}
		if (Delay > 0) {
			Lines.Add(FString::Printf(TEXT("Delay: %.2f"), Delay));
		}
		if (Armor > 0) {
			Lines.Add(FString::Printf(TEXT("Armor: %i"), Armor));
		}
		bool bWroteStats = false;
		bool bWroteSkills = false;
		for (int32 i = 0; i < (int32)EStats::StatTotalStats; ++i) {
			if (StatModifiers[i] > 0) {
				if (!bWroteStats) {
					bWroteStats = true;
					Lines.Add(TEXT(" "));
					Lines.Add(TEXT("Stats:"));
				}
				Lines.Add(FString::Printf(TEXT("  %s: +%i"), *ANNSHUD::StatNames[i], StatModifiers[i]));
			}
		}
		for (int32 i = 0; i < (int32)ESkills::SkillTotalSkills; ++i) {
			if (SkillModifiers[i] > 0) {
				if (!bWroteSkills) {
					bWroteSkills = true;
					if (!bWroteStats) {
						Lines.Add(TEXT(" "));
					}
					Lines.Add(TEXT("Skills:"));
				}
				Lines.Add(FString::Printf(TEXT("  %s: +%i"), *ANNSHUD::SkillNames[i], SkillModifiers[i]));
			}
		}
		return Lines;
	} else {
		return Super::ToString();
	}
}

uint32 UEquippableItem::GetBuyCost() const
{
	uint32 Value = 10 + Dice * DiceSides + Damage + Armor;
	Value *= FMath::Sqrt(IconWidth * IconHeight);
	Value *= Tier;

	if (Style == EEquipStyle::EquipChain) Value *= 2;
	else if (Style == EEquipStyle::EquipPlate) Value *= 4;

	for (const int32 Modifier : StatModifiers) {
		if (Modifier > 0) {
			Value = FMath::CeilToInt((float)Value * (1.0f + Modifier * 0.03f));
		}
	}
	for (const int32 Modifier : SkillModifiers) {
		if (Modifier > 0) {
			Value = FMath::CeilToInt((float)Value * (1.0f + Modifier * 0.03f));
		}
	}

	return Value;
}

void UEquippableItem::Enchant()
{
	// Enchant up to 1 time for every tier
	const int32 NumEnchants = FMath::RandRange(1, Tier);
	for (int32 CurrentEnchant = 0; CurrentEnchant < NumEnchants; ++CurrentEnchant) {
		int32 EnchantTier = 1;
		for (; EnchantTier < Tier; ++EnchantTier) {
			// 50% chance for tier 1, 25% for tier 2, etc.
			if (FMath::RandBool()) break;
		}
		// Determine if buff is stat or skill
		if (FMath::RandBool()) {
			// Choose stat
			int32 Stat = FMath::RandRange(0, (int32)EStats::StatTotalStats - 1);
			StatModifiers[Stat] += FMath::RandRange(1, EnchantTier * 3);
		} else {
			// Choose skill
			int32 Skill = FMath::RandRange(0, (int32)ESkills::SkillTotalSkills - 1);
			SkillModifiers[Skill] += FMath::RandRange(1, EnchantTier);
		}
	}
}
