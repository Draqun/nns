// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "NPCBuilding.h"

#include "DialogComponent.h"
#include "NNSGameInstance.h"
#include "NNSHUD.h"

ANPCBuilding::ANPCBuilding(const FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{
	DialogComponent = ObjectInitializer.CreateDefaultSubobject<UDialogComponent>(this, TEXT("DialogComp"));
}

void ANPCBuilding::Interact(ANNSCharacter* Character)
{
	Super::Interact(Character);
	Cast<UNNSGameInstance>(GetGameInstance())->HUD->EnterBuilding(this);
}
