// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "NNSGameInstance.h"

#include "Runtime/Landscape/Classes/Landscape.h"
#include "Runtime/Landscape/Classes/LandscapeProxy.h"

#include "BaseEnemyCharacter.h"
#include "CharacterSheet.h"
#include "DataStructs.h"
#include "EquippableItem.h"
#include "Inventory.h"
#include "ItemGenerator.h"
#include "NNSCharacter.h"
#include "NNSHUD.h"
#include "Pickup.h"
#include "PlayerPartyCharacter.h"
#include "Projectile.h"
#include "QuestManager.h"
#include "Spellbook.h"
#include "StaticInteractable.h"
#include "TimeOfDayManager.h"

const FString UNNSGameInstance::BaseSaveDir = FString(FPlatformProcess::UserSettingsDir()) + TEXT("/NewNewSorpigalDemo/");
const FString UNNSGameInstance::SaveFilePattern = BaseSaveDir + TEXT("%s.sav");
const int32 UNNSGameInstance::QuicksaveSlots = 3;
const int32 UNNSGameInstance::AutosaveSlots = 3;
UDataTable* UNNSGameInstance::EnemyDataTable = nullptr;
UDataTable* UNNSGameInstance::EquippableItemDataTable = nullptr;
UDataTable* UNNSGameInstance::PotionItemDataTable = nullptr;
UDataTable* UNNSGameInstance::QuestItemDataTable = nullptr;
TMap<FName, UClass*> UNNSGameInstance::ClassMap;
TMap<EClass, FSkillsToMastery> UNNSGameInstance::ClassSkillMap;
int32 UNNSGameInstance::FieldOfView = 105;

UNNSGameInstance::UNNSGameInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, SavedPlayer(true)
{
	static ConstructorHelpers::FObjectFinder<UDataTable> EquippableItemDataTableFinder(TEXT("DataTable'/Game/DataTables/EquippableItemDataTable.EquippableItemDataTable'"));
	EquippableItemDataTable = EquippableItemDataTableFinder.Object;

	static ConstructorHelpers::FObjectFinder<UDataTable> PotionItemDataTableFinder(TEXT("DataTable'/Game/DataTables/PotionItemDataTable.PotionItemDataTable'"));
	PotionItemDataTable = PotionItemDataTableFinder.Object;

	static ConstructorHelpers::FObjectFinder<UDataTable> EnemyDataTableFinder(TEXT("DataTable'/Game/DataTables/EnemyDataTable.EnemyDataTable'"));
	EnemyDataTable = EnemyDataTableFinder.Object;

	static ConstructorHelpers::FObjectFinder<UDataTable> QuestItemDataTableFinder(TEXT("DataTable'/Game/DataTables/KeyItemDataTable.KeyItemDataTable'"));
	QuestItemDataTable = QuestItemDataTableFinder.Object;

	static ConstructorHelpers::FObjectFinder<UDataTable> ClassDataTableFinder(TEXT("DataTable'/Game/DataTables/ClassDataTable.ClassDataTable'"));
	UDataTable* ClassTable = ClassDataTableFinder.Object;
	TArray<FName> ClassNames = ClassTable->GetRowNames();
	for (const FName& Name : ClassNames) {
		FClassData* ClassInfo = ClassTable->FindRow<FClassData>(Name, TEXT("Class Skills"));
		int32 Index = -1;
		ANNSHUD::ClassNames.Find(Name.ToString(), Index);
		ClassSkillMap.Add((EClass)Index);
		TMap<ESkills, int32> SkillMap;
		SkillMap.Emplace(ESkills::SkillAirMagic, ClassInfo->AirMagic);
		SkillMap.Emplace(ESkills::SkillAlchemy, ClassInfo->Alchemy);
		SkillMap.Emplace(ESkills::SkillArmsmaster, ClassInfo->Armsmaster);
		SkillMap.Emplace(ESkills::SkillWeaponAxe, ClassInfo->Axe);
		SkillMap.Emplace(ESkills::SkillBodybuilding, ClassInfo->Bodybuilding);
		SkillMap.Emplace(ESkills::SkillBodyMagic, ClassInfo->BodyMagic);
		SkillMap.Emplace(ESkills::SkillWeaponBow, ClassInfo->Bow);
		SkillMap.Emplace(ESkills::SkillArmorChain, ClassInfo->Chain);
		SkillMap.Emplace(ESkills::SkillWeaponDagger, ClassInfo->Dagger);
		SkillMap.Emplace(ESkills::SkillDisarmTrap, ClassInfo->DisarmTrap);
		SkillMap.Emplace(ESkills::SkillFireMagic, ClassInfo->FireMagic);
		SkillMap.Emplace(ESkills::SkillIdentifyItem, ClassInfo->IdentifyItem);
		SkillMap.Emplace(ESkills::SkillIdentifyMonster, ClassInfo->IdentifyMonster);
		SkillMap.Emplace(ESkills::SkillLearning, ClassInfo->Learning);
		SkillMap.Emplace(ESkills::SkillArmorLeather, ClassInfo->Leather);
		SkillMap.Emplace(ESkills::SkillMeditation, ClassInfo->Meditation);
		SkillMap.Emplace(ESkills::SkillMerchant, ClassInfo->Merchant);
		SkillMap.Emplace(ESkills::SkillArmorPlate, ClassInfo->Plate);
		SkillMap.Emplace(ESkills::SkillArmorShield, ClassInfo->Shield);
		SkillMap.Emplace(ESkills::SkillSwimming, ClassInfo->Swimming);
		SkillMap.Emplace(ESkills::SkillWeaponSword, ClassInfo->Sword);
		for (const auto& Skill : SkillMap) {
			if (Skill.Value >= 0) {
				ClassSkillMap[(EClass)Index].Masteries.Emplace(Skill.Key, (EMastery)Skill.Value);
			}
		}
	}
	ClassSkillMap.Add(EClass::ClassTotalClasses);
	for (int32 i = 0; i < (int32)ESkills::SkillTotalSkills; ++i) {
		ClassSkillMap[EClass::ClassTotalClasses].Masteries.Emplace((ESkills)i, EMastery::MasteryMaster);
	}

	static ConstructorHelpers::FObjectFinder<UParticleSystem> BloodParticlesFinder(TEXT("ParticleSystem'/Game/Particles/P_Blood.P_Blood'"));
	BloodParticles = BloodParticlesFinder.Object;

	TArray<UObject*> PortraitObjects;
	EngineUtils::FindOrLoadAssetsByPath(TEXT("/Game/UI/Portraits"), PortraitObjects, EngineUtils::ATL_Regular);
	for (UObject* Obj : PortraitObjects) {
		Portraits.Add(Cast<UTexture2D>(Obj));
	}

	HUD = nullptr;

	static ConstructorHelpers::FObjectFinder<UParticleSystem> FireSpikesFinder(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));
	FireSpikeParticles = FireSpikesFinder.Object;

	bIgnorePlayerTransforms = false;
	bTeleportOnLoad = false;

	MotionBlurAmount = 0.2f;
	MouseSensitivity = 1.0f;

	MasterVolume = 1.0f;
	MusicVolume = 1.0f;
	SoundEffectVolume = 0.75f;

	bUseCombatMode = true;
	bUseBackwardsSpeed = true;

	bDrawHotkeysOnScreen = true;
}

void UNNSGameInstance::InitSpell(ESpells Spell, const FString& Name, int32 Mana, EMastery Mastery, float Delay, ESkills Skill, ETargetMode Target, const FString& Texture, const FString& Sound)
{
	if (!EnumMaps::SchoolSpells.Contains(Skill)) {
		EnumMaps::SchoolSpells.Add(Skill);
	}
	EnumMaps::SchoolSpells[Skill].Add(Spell);
	EnumMaps::SpellInfo.Emplace(Spell, FSpellInfo(Name, Mana, Mastery, Delay, Skill, Target, FStringAssetReference(Texture), FStringAssetReference(Sound)));
}

void UNNSGameInstance::Shutdown()
{
	TimeOfDayManager->EndTimer();
	SavedPlayer.FlushCache();
	SavedPlayer.Close();

	Super::Shutdown();
}

void UNNSGameInstance::Init()
{
	// Set up constant, global data
	EnumMaps::DamageResistances.Emplace(EDamageTypes::DamageFire, EStats::StatResistFire);
	EnumMaps::DamageResistances.Emplace(EDamageTypes::DamageAir, EStats::StatResistAir);
	EnumMaps::StatusResistances.Emplace(EStats::StatResistFire, EStatusEffects::StatusResistFire);
	EnumMaps::StatusResistances.Emplace(EStats::StatResistAir, EStatusEffects::StatusResistAir);

	EnumMaps::TrapDamages.Emplace(ETrapTypes::TrapFire, EDamageTypes::DamageFire);
	EnumMaps::TrapDamages.Emplace(ETrapTypes::TrapAir, EDamageTypes::DamageAir);

	EnumMaps::SkillCosts.Emplace(EMastery::MasteryBasic, FSkillCost(0, 400));
	EnumMaps::SkillCosts.Emplace(EMastery::MasteryExpert, FSkillCost(5, 700));
	EnumMaps::SkillCosts.Emplace(EMastery::MasteryMaster, FSkillCost(10, 2000));

	EnumMaps::ShopSkills.Emplace(EShopTypes::ShopWeapon, TArray<ESkills>());
	EnumMaps::ShopSkills[EShopTypes::ShopWeapon].Add(ESkills::SkillWeaponSword);
	EnumMaps::ShopSkills[EShopTypes::ShopWeapon].Add(ESkills::SkillWeaponAxe);
	EnumMaps::ShopSkills[EShopTypes::ShopWeapon].Add(ESkills::SkillWeaponBow);
	EnumMaps::ShopSkills[EShopTypes::ShopWeapon].Add(ESkills::SkillWeaponDagger);
	EnumMaps::ShopSkills.Emplace(EShopTypes::ShopArmorChest, TArray<ESkills>());
	EnumMaps::ShopSkills[EShopTypes::ShopArmorChest].Add(ESkills::SkillArmorLeather);
	EnumMaps::ShopSkills[EShopTypes::ShopArmorChest].Add(ESkills::SkillArmorChain);
	EnumMaps::ShopSkills[EShopTypes::ShopArmorChest].Add(ESkills::SkillArmorPlate);
	EnumMaps::ShopSkills.Emplace(EShopTypes::ShopArmorOther, TArray<ESkills>());
	EnumMaps::ShopSkills[EShopTypes::ShopArmorOther].Add(ESkills::SkillArmorShield);
	EnumMaps::ShopSkills[EShopTypes::ShopArmorOther].Add(ESkills::SkillArmorLeather);
	EnumMaps::ShopSkills[EShopTypes::ShopArmorOther].Add(ESkills::SkillArmorChain);
	EnumMaps::ShopSkills[EShopTypes::ShopArmorOther].Add(ESkills::SkillArmorPlate);
	EnumMaps::ShopSkills.Emplace(EShopTypes::ShopSpellbook, TArray<ESkills>());
	EnumMaps::ShopSkills[EShopTypes::ShopSpellbook].Add(ESkills::SkillAirMagic);
	EnumMaps::ShopSkills[EShopTypes::ShopSpellbook].Add(ESkills::SkillBodyMagic);
	EnumMaps::ShopSkills[EShopTypes::ShopSpellbook].Add(ESkills::SkillFireMagic);
	EnumMaps::ShopSkills[EShopTypes::ShopSpellbook].Add(ESkills::SkillMeditation);
	EnumMaps::ShopSkills.Emplace(EShopTypes::ShopAlchemy, TArray<ESkills>());
	EnumMaps::ShopSkills[EShopTypes::ShopAlchemy].Add(ESkills::SkillAlchemy);
	EnumMaps::ShopSkills.Emplace(EShopTypes::ShopTavern, TArray<ESkills>());
	EnumMaps::ShopSkills[EShopTypes::ShopTavern].Add(ESkills::SkillArmsmaster);
	EnumMaps::ShopSkills[EShopTypes::ShopTavern].Add(ESkills::SkillBodybuilding);
	EnumMaps::ShopSkills[EShopTypes::ShopTavern].Add(ESkills::SkillMerchant);
	EnumMaps::ShopSkills[EShopTypes::ShopTavern].Add(ESkills::SkillDisarmTrap);
	EnumMaps::ShopSkills[EShopTypes::ShopTavern].Add(ESkills::SkillSwimming);
	EnumMaps::ShopSkills.Emplace(EShopTypes::ShopTemple, TArray<ESkills>());
	EnumMaps::ShopSkills[EShopTypes::ShopTemple].Add(ESkills::SkillIdentifyMonster);
	EnumMaps::ShopSkills[EShopTypes::ShopTemple].Add(ESkills::SkillIdentifyItem);
	EnumMaps::ShopSkills[EShopTypes::ShopTemple].Add(ESkills::SkillLearning);

	EnumMaps::StyleToSkill.Emplace(EEquipStyle::EquipSword, ESkills::SkillWeaponSword);
	EnumMaps::StyleToSkill.Emplace(EEquipStyle::EquipAxe, ESkills::SkillWeaponAxe);
	EnumMaps::StyleToSkill.Emplace(EEquipStyle::EquipDagger, ESkills::SkillWeaponDagger);
	EnumMaps::StyleToSkill.Emplace(EEquipStyle::EquipBow, ESkills::SkillWeaponBow);
	EnumMaps::StyleToSkill.Emplace(EEquipStyle::EquipShield, ESkills::SkillArmorShield);
	EnumMaps::StyleToSkill.Emplace(EEquipStyle::EquipLeather, ESkills::SkillArmorLeather);
	EnumMaps::StyleToSkill.Emplace(EEquipStyle::EquipChain, ESkills::SkillArmorChain);
	EnumMaps::StyleToSkill.Emplace(EEquipStyle::EquipPlate, ESkills::SkillArmorPlate);
	EnumMaps::StyleToSkill.Emplace(EEquipStyle::EquipOther, ESkills::SkillTotalSkills);
	
	// This empty is only necessary for PIE mode, cause it will just keep adding every time it runs...
	EnumMaps::SchoolSpells.Empty();
	// TODO: move this to data table?
	InitSpell(ESpells::SpellTorchLight, TEXT("Torch Light"), 2, EMastery::MasteryBasic, 1.0f, ESkills::SkillFireMagic, ETargetMode::TargetNone, TEXT("Texture2D'/Game/UI/spellbook_torchlight.spellbook_torchlight'"), TEXT("SoundWave'/Game/Sounds/spells/spell_torchlight.spell_torchlight'"));
	InitSpell(ESpells::SpellFireShield, TEXT("Protection from Fire"), 6, EMastery::MasteryBasic, 2.5f, ESkills::SkillFireMagic, ETargetMode::TargetNone, TEXT("Texture2D'/Game/UI/spellbook_fireshield.spellbook_fireshield'"), TEXT("SoundWave'/Game/Sounds/spells/spell_elementshield.spell_elementshield'"));
	InitSpell(ESpells::SpellFlameBolt, TEXT("Flame Bolt"), 3, EMastery::MasteryBasic, 2.0f, ESkills::SkillFireMagic, ETargetMode::TargetEnemy, TEXT("Texture2D'/Game/UI/spellbook_flamebolt2.spellbook_flamebolt2'"), TEXT("SoundWave'/Game/Sounds/spells/spell_flamebolt.spell_flamebolt'"));
	InitSpell(ESpells::SpellFireSpikes, TEXT("Fire Spikes"), 15, EMastery::MasteryExpert, 4.5f, ESkills::SkillFireMagic, ETargetMode::TargetAlly, TEXT("Texture2D'/Game/UI/spellbook_firespikes.spellbook_firespikes'"), TEXT("SoundWave'/Game/Sounds/spells/spell_firespikes.spell_firespikes'"));
	InitSpell(ESpells::SpellFireBall, TEXT("Fire Ball"), 13, EMastery::MasteryExpert, 4.0f, ESkills::SkillFireMagic, ETargetMode::TargetEnemy, TEXT("Texture2D'/Game/UI/spellbook_fireball.spellbook_fireball'"), TEXT("SoundWave'/Game/Sounds/spells/spell_fireball.spell_fireball'"));
	InitSpell(ESpells::SpellWizardsEye, TEXT("Wizard's Eye"), 1, EMastery::MasteryBasic, 1.0f, ESkills::SkillAirMagic, ETargetMode::TargetNone, TEXT("Texture2D'/Game/UI/spellbook_wizardseye.spellbook_wizardseye'"), TEXT("SoundWave'/Game/Sounds/spells/spell_wizardseye.spell_wizardseye'"));
	InitSpell(ESpells::SpellAirShield, TEXT("Protection from Air"), 6, EMastery::MasteryBasic, 2.5f, ESkills::SkillAirMagic, ETargetMode::TargetNone, TEXT("Texture2D'/Game/UI/spellbook_airshield.spellbook_airshield'"), TEXT("SoundWave'/Game/Sounds/spells/spell_elementshield.spell_elementshield'"));
	InitSpell(ESpells::SpellShock, TEXT("Shock"), 4, EMastery::MasteryBasic, 2.3f, ESkills::SkillAirMagic, ETargetMode::TargetEnemy, TEXT("Texture2D'/Game/UI/spellbook_shock.spellbook_shock'"), TEXT("SoundWave'/Game/Sounds/spells/spell_shock.spell_shock'"));
	InitSpell(ESpells::SpellFeatherFall, TEXT("Feather Fall"), 7, EMastery::MasteryExpert, 4.5f, ESkills::SkillAirMagic, ETargetMode::TargetNone, TEXT("Texture2D'/Game/UI/spellbook_featherfall.spellbook_featherfall'"), TEXT("SoundWave'/Game/Sounds/spells/spell_featherfall.spell_featherfall'"));
	InitSpell(ESpells::SpellWaterWalk, TEXT("Water Walk"), 15, EMastery::MasteryExpert, 7.0f, ESkills::SkillAirMagic, ETargetMode::TargetNone, TEXT("Texture2D'/Game/UI/spellbook_waterwalk.spellbook_waterwalk'"), TEXT("SoundWave'/Game/Sounds/spells/spell_waterwalk.spell_waterwalk'"));
	InitSpell(ESpells::SpellJump, TEXT("Jump"), 10, EMastery::MasteryExpert, 5.0f, ESkills::SkillAirMagic, ETargetMode::TargetNone, TEXT("Texture2D'/Game/UI/spellbook_jump.spellbook_jump'"), TEXT("SoundWave'/Game/Sounds/spells/spell_jump.spell_jump'"));
	InitSpell(ESpells::SpellFly, TEXT("Fly"), 40, EMastery::MasteryMaster, 10.0f, ESkills::SkillAirMagic, ETargetMode::TargetNone, TEXT("Texture2D'/Game/UI/spellbook_fly.spellbook_fly'"), TEXT("SoundWave'/Game/Sounds/spells/spell_fly.spell_fly'"));
	InitSpell(ESpells::SpellTeleportTown, TEXT("Teleport Town"), 35, EMastery::MasteryMaster, 9.0f, ESkills::SkillAirMagic, ETargetMode::TargetNone, TEXT("Texture2D'/Game/UI/spellbook_teleporttown.spellbook_teleporttown'"), TEXT("SoundWave'/Game/Sounds/spells/spell_teleport.spell_teleport'"));
	InitSpell(ESpells::SpellCureLight, TEXT("Cure Light Wounds"), 3, EMastery::MasteryBasic, 1.5f, ESkills::SkillBodyMagic, ETargetMode::TargetAlly, TEXT("Texture2D'/Game/UI/spellbook_curelight.spellbook_curelight'"), TEXT("SoundWave'/Game/Sounds/spells/spell_curelight.spell_curelight'"));
	InitSpell(ESpells::SpellHeroism, TEXT("Heroism"), 5, EMastery::MasteryBasic, 2.0f, ESkills::SkillBodyMagic, ETargetMode::TargetAlly, TEXT("Texture2D'/Game/UI/spellbook_heroism.spellbook_heroism'"), TEXT("SoundWave'/Game/Sounds/spells/spell_heroism.spell_heroism'"));
	InitSpell(ESpells::SpellCauseLight, TEXT("Cause Light Wounds"), 4, EMastery::MasteryBasic, 2.1f, ESkills::SkillBodyMagic, ETargetMode::TargetEnemy, TEXT("Texture2D'/Game/UI/spellbook_causelight.spellbook_causelight'"), TEXT("SoundWave'/Game/Sounds/spells/spell_causelight.spell_causelight'"));
	InitSpell(ESpells::SpellFlee, TEXT("Flee"), 2, EMastery::MasteryBasic, 5.0f, ESkills::SkillBodyMagic, ETargetMode::TargetNone, TEXT("Texture2D'/Game/UI/spellbook_flee.spellbook_flee'"), TEXT("SoundWave'/Game/Sounds/spells/spell_flee.spell_flee'"));
	InitSpell(ESpells::SpellRegen, TEXT("Regen"), 9, EMastery::MasteryExpert, 2.5f, ESkills::SkillBodyMagic, ETargetMode::TargetAlly, TEXT("Texture2D'/Game/UI/spellbook_regen.spellbook_regen'"), TEXT("SoundWave'/Game/Sounds/spells/spell_regen.spell_regen'"));
	InitSpell(ESpells::SpellCureHeavy, TEXT("Cure Heavy Wounds"), 12, EMastery::MasteryMaster, 2.5f, ESkills::SkillBodyMagic, ETargetMode::TargetAlly, TEXT("Texture2D'/Game/UI/spellbook_cureheavy.spellbook_cureheavy'"), TEXT("SoundWave'/Game/Sounds/spells/spell_cureheavy.spell_cureheavy'"));
	InitSpell(ESpells::SpellCauseHeavy, TEXT("Cause Heavy Wounds"), 13, EMastery::MasteryMaster, 4.0f, ESkills::SkillBodyMagic, ETargetMode::TargetEnemy, TEXT("Texture2D'/Game/UI/spellbook_causeheavy.spellbook_causeheavy'"), TEXT("SoundWave'/Game/Sounds/spells/spell_causeheavy.spell_causeheavy'"));

	// PIE Only?
	SavedLevels.Empty();

	QuestManager = NewObject<UQuestManager>();
	QuestManager->GameInstance = this;
	TimeOfDayManager = NewObject<UTimeOfDayManager>();

	ItemGeneratorObject = TSharedPtr<ItemGenerator>(new ItemGenerator());

	// Default characters for debugging
	{
		for (int32 i = 0; i < kCharactersInParty; ++i) {
			PlayerPartyMembers.Add(NewObject<UCharacterSheet>());
			PlayerPartyMembers[i]->DeathBarrier = -15.0f;
		}

		TArray<FCharacterCreatorInfo> Characters;
		Characters.AddDefaulted(kCharactersInParty);

		for (int32 i = 0; i < kCharactersInParty; ++i) {
			Characters[i].Strength = 10;
			Characters[i].Intelligence = 10;
			Characters[i].Dexterity = 10;
			Characters[i].Speed = 10;
			Characters[i].Vitality = 10;
			Characters[i].Luck = 10;
		}
		Characters[0].Name = TEXT("Toorum");
		Characters[0].Class = EClass::ClassTotalClasses;
		//Characters[0].Class = EClass::ClassPaladin;
		//Characters[0].Skills.Add(ESkills::SkillWeaponSword);
		//Characters[0].Skills.Add(ESkills::SkillArmorShield);
		//Characters[0].Skills.Add(ESkills::SkillArmorChain);
		//Characters[0].Skills.Add(ESkills::SkillBodyMagic);
		Characters[0].Portrait = Portraits[3];
		Characters[0].Voice = 0;

		Characters[1].Name = TEXT("Strelok");
		Characters[1].Class = EClass::ClassArcher;
		Characters[1].Skills.Add(ESkills::SkillArmorLeather);
		Characters[1].Skills.Add(ESkills::SkillArmorPlate);
		Characters[1].Skills.Add(ESkills::SkillWeaponSword);
		Characters[1].Skills.Add(ESkills::SkillWeaponBow);
		Characters[1].Skills.Add(ESkills::SkillArmorShield);
		Characters[1].Skills.Add(ESkills::SkillBodybuilding);
		Characters[1].Portrait = Portraits[4];
		Characters[1].Voice = 1;

		Characters[2].Name = TEXT("Kevin Graham");
		Characters[2].Class = EClass::ClassCleric;
		Characters[2].Skills.Add(ESkills::SkillArmorLeather);
		Characters[2].Skills.Add(ESkills::SkillWeaponAxe);
		Characters[2].Skills.Add(ESkills::SkillBodyMagic);
		Characters[2].Skills.Add(ESkills::SkillMeditation);
		Characters[2].Skills.Add(ESkills::SkillLearning);
		Characters[2].Portrait = Portraits[5];
		Characters[2].Voice = 2;

		Characters[3].Name = TEXT("Gothmog");
		Characters[3].Class = EClass::ClassSorcerer;
		Characters[3].Skills.Add(ESkills::SkillArmorLeather);
		Characters[3].Skills.Add(ESkills::SkillWeaponDagger);
		Characters[3].Skills.Add(ESkills::SkillFireMagic);
		Characters[3].Skills.Add(ESkills::SkillAirMagic);
		Characters[3].Skills.Add(ESkills::SkillIdentifyItem);
		Characters[3].Skills.Add(ESkills::SkillIdentifyMonster);
		Characters[3].Portrait = Portraits[0];
		Characters[3].Voice = 3;
		
		InitPartyMembers(Characters);

		for (int32 i = 0; i < (int32)ESkills::SkillTotalSkills; ++i) {
			PlayerPartyMembers[0]->SkillPoints = 100;
			PlayerPartyMembers[0]->StatPoints = 100;
			PlayerPartyMembers[0]->BaseSkills[i] = 10;
			PlayerPartyMembers[0]->ExpertSkills[i] = true;
			PlayerPartyMembers[0]->MasterSkills[i] = true;
		}
		PlayerPartyMembers[0]->AddItemToInventory(UEquippableItem::Create(FName(TEXT("WornSword"))));
		PlayerPartyMembers[0]->AddItemToInventory(UEquippableItem::Create(FName(TEXT("WornAxe"))));
		PlayerPartyMembers[0]->AddItemToInventory(UEquippableItem::Create(FName(TEXT("Shortbow"))));
		PlayerPartyMembers[0]->AddItemToInventory(UEquippableItem::Create(FName(TEXT("WornLeatherArmor"))));
		for (int32 i = 0; i < (int32)ESpells::SpellTotalSpells; ++i) {
			PlayerPartyMembers[0]->LearnSpell((ESpells)i);
		}
		PlayerPartyMembers[0]->Init();
	}
}

UTexture* UNNSGameInstance::GetTexture(TAssetPtr<UTexture>& Texture)
{
	if (!Texture.IsValid() || Texture.IsPending()) {
		// It's a fairly small image, so SynchronousLoad should be fine
		Texture = Cast<UTexture>(AssetLoader.SynchronousLoad(Texture.ToStringReference()));
	}
	return Texture.Get();
}

USoundWave* UNNSGameInstance::GetSound(TAssetPtr<USoundWave>& Sound)
{
	if (!Sound.IsValid() || Sound.IsPending()) {
		// It's a fairly small sound, so SynchronousLoad should be fine
		Sound = Cast<USoundWave>(AssetLoader.SynchronousLoad(Sound.ToStringReference()));
	}
	return Sound.Get();
}

bool UNNSGameInstance::ToggleTurnMode()
{
	return SetTurnMode(!bIsInTurnMode);
}

bool UNNSGameInstance::SetTurnMode(bool bEnable, bool bForce)
{
	if (!bIsInTurnMode) {
		bIsInTurnMode = true;
		for (auto PawnIterator = GetWorld()->GetPawnIterator(); PawnIterator; ++PawnIterator) {
			ANNSCharacter* Character = Cast<ANNSCharacter>(PawnIterator->Get());
			if (Character->bIsDead) continue;
			Character->ConvertTimerToTurnCount();
			Character->UpdateWalkSpeed();

			if (Character->IsA(APlayerPartyCharacter::StaticClass())) {
				for (auto& Member : Character->PartyMembers) {
					if (!Member->IsUnconscious()) {
						InsertPlayerIntoTurnOrder(Member);
					}
				}
			} else {
				Cast<ABaseEnemyCharacter>(Character)->bStartedTurnModeInRadius = Character->GetDistanceTo(PlayerPartyMembers[0]->OwnerCharacter.Get()) <= 3500.0f;
				for (auto& Member : Character->PartyMembers) {
					if (!Member->IsUnconscious()) {
						InsertIntoTurnOrder(Member);
					}
				}
			}
		}
		TimeOfDayManager->EndTimer();
		CheckTurnOrder();
		return true;
	} else if (bIsInTurnMode && (bForce || TurnOrder[0]->OwnerCharacter->IsA(APlayerPartyCharacter::StaticClass()))) {
		bIsInTurnMode = false;
		for (auto PawnIterator = GetWorld()->GetPawnIterator(); PawnIterator; ++PawnIterator) {
			ANNSCharacter* Character = Cast<ANNSCharacter>(PawnIterator->Get());
			Character->ConvertTurnCountToTimer();
			Character->UpdateWalkSpeed();
		}
		TurnOrder.Empty();
		TimeOfDayManager->StartTimer();
		return true;
	}
	return false;
}

void UNNSGameInstance::CheckTurnOrder()
{
	if (TurnOrder[0]->TurnCount <= 0.0f) {
		// Reorder other party members right after this one
		int32 Counter = 0;
		for (auto& Member : TurnOrder[0]->OwnerCharacter->PartyMembers) {
			if (Member == TurnOrder[0] || Member->IsUnconscious()) continue;

			if (Member->TurnCount <= 0.0f) {
				TurnOrder.Remove(Member);
				TurnOrder.Insert(Member, ++Counter);
			}
		}

		// If someone can act, let them go
		TurnOrder[0]->OwnerCharacter->StartTurn();
	} else {
		// Otherwise, subtract 0.5 from everyone's timer. This will
		// result in multiple characters being able to move, but we
		// process them one at a time.
		for (auto& Member : TurnOrder) {
			if (!Member->IsUnconscious()) {
				Member->TurnCount -= 0.5f;
			}
		}
		TimeOfDayManager->AdvanceTurnTimer(0.5f);
		CheckTurnOrder();
	}
}

void UNNSGameInstance::EndTurn()
{
	if (!bIsInTurnMode) return;

	const bool IsPlayer = TurnOrder[0]->OwnerCharacter->IsA(APlayerPartyCharacter::StaticClass());

	// Remove the character who just acted and re-insert them in the turn order
	// according to their new turn delay
	TArray<UCharacterSheet*>& PartyMembers = TurnOrder[0]->OwnerCharacter->PartyMembers;
	for (auto& Member : PartyMembers) {
		if (!Member->IsUnconscious()) {
			TurnOrder.Remove(Member);
		}
	}

	if (IsPlayer) {
		for (auto& Member : PartyMembers) {
			if (!Member->IsUnconscious()) {
				InsertPlayerIntoTurnOrder(Member);
			}
		}
	} else {
		for (auto& Member : PartyMembers) {
			if (!Member->IsUnconscious()) {
				InsertIntoTurnOrder(Member);
			}
		}
	}

	CheckTurnOrder();
}

void UNNSGameInstance::NotifyDeath(UCharacterSheet* Character)
{
	if (bIsInTurnMode) {
		const bool bFirst = TurnOrder[0] == Character;
		TurnOrder.Remove(Character);

		if (bFirst) {
			CheckTurnOrder();
		}
	}
}

void UNNSGameInstance::InsertIntoTurnOrder(UCharacterSheet* Member)
{
	// Enemies are added to the end of the list, so they appear
	// after any character with the same turn delay
	for (int32 i = TurnOrder.Num() - 1; i >= 0; --i) {
		if (TurnOrder[i]->TurnCount <= Member->TurnCount) {
			TurnOrder.Insert(Member, i + 1);
			return;
		}
	}
	TurnOrder.Insert(Member, 0);
}

void UNNSGameInstance::InsertPlayerIntoTurnOrder(UCharacterSheet* Member)
{
	// Players are added to the front of the list, so they appear
	// before any character with the same turn delay
	// We cheat a little for the player :)
	for (int32 i = 0; i < TurnOrder.Num(); ++i) {
		if (TurnOrder[i]->TurnCount >= Member->TurnCount) {
			TurnOrder.Insert(Member, i);
			return;
		}
	}
	TurnOrder.Add(Member);
}

UClass* UNNSGameInstance::GetClassFromMap(const FName& Name)
{
	// Loads a class, or retrieves it from the ClassMap
	UClass* C = nullptr;
	if (ClassMap.Contains(Name)) {
		C = ClassMap[Name];
	} else {
		C = LoadObject<UClass>(GetTransientPackage(), *Name.ToString());
		ClassMap.Emplace(Name, C);
	}
	return C;
}

void UNNSGameInstance::SaveLoadCommon(FArchive& Archive)
{
	Archive << PlayerGold;
	Archive << bIsInTurnMode;
}

void UNNSGameInstance::Save(FArchive& Archive)
{
	SaveLoadCommon(Archive);
	QuestManager->Save(Archive);
	TimeOfDayManager->Save(Archive);

	// Save player to a separate archive so we can read it in in LoadCurrentLevel()
	SavedPlayer.Empty();
	SavedPlayer.Seek(0);
	PlayerPartyMembers[0]->OwnerCharacter->Save(SavedPlayer);
	Archive << SavedPlayer;

	FName CurrentLevelName = FName(*GetWorld()->GetName());
	Archive << CurrentLevelName;

	// Save the current level, then add all saved levels to the archive
	// This lets us persist all levels even if they aren't loaded
	SaveCurrentLevel();

	Archive << SavedLevels;
}

void UNNSGameInstance::SaveCurrentLevel()
{
	const FString LevelName = GetWorld()->GetName();
	// Had a weird bug where trying to re-use the existing FBufferArchive would corrupt the data
	if (SavedLevels.Contains(LevelName)) {
		SavedLevels[LevelName].FlushCache();
		SavedLevels[LevelName].Close();
		SavedLevels.Remove(LevelName);
	}
	FBufferArchive ToLevelBuffer(true);
	ToLevelBuffer.Seek(0);

	TArray<AActor*> AllActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AActor::StaticClass(), AllActors);

	TArray<ABaseEnemyCharacter*> AllNPCs;
	TArray<APickup*> AllPickups;
	TArray<AStaticInteractable*> AllStaticInteractables;
	TArray<FString> VisibleGeometry;
	// TODO: combine these into a single loop somehow? might need an ISavable interface or something
	for (auto& Actor : AllActors) {
		if (Actor->IsPendingKill()) continue;

		if (Actor->IsA<ABaseEnemyCharacter>()) {
			if (!Cast<ABaseEnemyCharacter>(Actor)->bIsDead) {
				AllNPCs.Add(Cast<ABaseEnemyCharacter>(Actor));
			}
		} else if (Actor->IsA<APickup>()) {
			AllPickups.Add(Cast<APickup>(Actor));
		} else if (Actor->IsA<AStaticInteractable>()) {
			AllStaticInteractables.Add(Cast<AStaticInteractable>(Actor));
		} else if (bIsIndoors && Actor->ActorHasTag(ActorTags::kVisibleTag)) {
			VisibleGeometry.Add(Actor->GetName());
		}
	}

	if (bIsIndoors) {
		ToLevelBuffer << VisibleGeometry;
	}
	
	{
		int32 NumNPCs = AllNPCs.Num();
		ToLevelBuffer << NumNPCs;
		for (auto& NPC : AllNPCs) {
			NPC->Save(ToLevelBuffer);
		}
	}
	
	{
		int32 NumPickups = AllPickups.Num();
		ToLevelBuffer << NumPickups;
		for (auto& Pickup : AllPickups) {
			FName ClassName = FName(*Pickup->GetClass()->GetPathName());
			ToLevelBuffer << ClassName;
			Pickup->Save(ToLevelBuffer);
		}
	}
	
	for (auto& StaticInteractable : AllStaticInteractables) {
		StaticInteractable->Save(ToLevelBuffer);
	}
	
	SavedLevels.Emplace(LevelName, ToLevelBuffer);
}

void UNNSGameInstance::Load(FArchive& Archive)
{
	SaveLoadCommon(Archive);
	QuestManager->Load(Archive);
	TimeOfDayManager->Load(Archive);

	// Don't load the player yet, do that after the level loads in case we need to change levels first
	Archive << SavedPlayer;

	FName LevelName;
	Archive << LevelName;

	SavedLevels.Empty();
	Archive << SavedLevels;

	OpenLevel(LevelName);

	if (HUD != nullptr) {
		HUD->Reset();
	}
}

void UNNSGameInstance::LoadCurrentLevel()
{
	const FString LevelName = GetWorld()->GetName();

	// Clear the turn order, as we'll re-initialize it after everything is loaded
	TurnOrder.Empty();

	// If SavedLevels isn't populated, this must be the first time entering this level
	if (SavedLevels.Contains(LevelName)) {
		// Destroy all existing pickups, then spawn them again. Is there a better way?
		TArray<AActor*> AllActorsOfType;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), APickup::StaticClass(), AllActorsOfType);
		for (auto& Actor : AllActorsOfType) {
			if (Actor->IsPendingKill()) continue;

			Actor->Destroy();
		}

		FMemoryReader Archive(SavedLevels[LevelName], true);
		Archive.Seek(0);

		// If indoors, reset visibility of static meshes
		if (bIsIndoors) {
			TArray<FString> VisibleGeometry;
			Archive << VisibleGeometry;
			
			if (VisibleGeometry.Num() > 0) {
				UGameplayStatics::GetAllActorsOfClass(GetWorld(), AActor::StaticClass(), AllActorsOfType);
				for (AActor* Actor : AllActorsOfType) {
					int32 Index = -1;
					if (VisibleGeometry.Find(Actor->GetName(), Index)) {
						Actor->Tags.Add(ActorTags::kVisibleTag);
						VisibleGeometry.RemoveAt(Index);
					}
				}
			}
		}

		// Re-init all NPCs (create name map first)
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABaseEnemyCharacter::StaticClass(), AllActorsOfType);
		TMap<FString, ABaseEnemyCharacter*> NameMap;
		for (AActor* NPC : AllActorsOfType) {
			NameMap.Emplace(NPC->GetName(), Cast<ABaseEnemyCharacter>(NPC));
		}
		int32 NumActors = 0;
		Archive << NumActors;
		for (int32 i = 0; i < NumActors; ++i) {
			FString Name;
			Archive << Name;

			if (NameMap.Contains(Name)) {
				NameMap[Name]->Load(Archive);
				NameMap.Remove(Name);
			}
		}

		// Destroy NPCs that weren't reloaded
		for (auto& NPC : NameMap) {
			if (NPC.Value->IsPendingKill()) continue;

			NPC.Value->CustomDestroy();
		}

		// Spawn new actors from the saved data
		Archive << NumActors;
		for (int32 i = 0; i < NumActors; ++i) {
			FName ClassName;
			Archive << ClassName;

			FTransform Transform;
			Archive << Transform;

			APickup* Actor = GetWorld()->SpawnActor<APickup>(GetClassFromMap(ClassName), Transform);
			Actor->Load(Archive);
		}

		// Re-init all static world objects
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AStaticInteractable::StaticClass(), AllActorsOfType);
		for (int32 i = 0; i < AllActorsOfType.Num(); ++i) {
			FString Name;
			Archive << Name;

			for (auto& Actor : AllActorsOfType) {
				if (Actor->GetName().Equals(Name)) {
					Cast<AStaticInteractable>(Actor)->Load(Archive);
					break;
				}
			}
		}

		// TODO: load projectiles?
	}

	// Update the minimap (before the player because we want to apply wizard's eye after the minimap)
	FVector Origin, Extent;
	GetLevelBoundingBox(GetWorld(), bIsIndoors, Origin, Extent);
	MinimapCam = GetWorld()->SpawnActor<ASceneCapture2D>(FVector(Origin.X, Origin.Y, Origin.Z + Extent.Z + 5000.0f), FRotator(-90.0f, 0.0f, 0.0f));
	MinimapCam->GetCaptureComponent2D()->ShowFlags.SetSkeletalMeshes(false);
	MinimapCam->GetCaptureComponent2D()->ShowFlags.SetParticles(false);
	MinimapCam->GetCaptureComponent2D()->ShowFlags.SetBloom(false);
	MinimapCam->GetCaptureComponent2D()->ShowFlags.SetEyeAdaptation(false);
	MinimapCam->GetCaptureComponent2D()->ShowFlags.SetAmbientOcclusion(false);
	MinimapCam->GetCaptureComponent2D()->ShowFlags.SetDynamicShadows(false);
	MinimapCam->GetCaptureComponent2D()->ShowFlags.SetScreenSpaceReflections(false);
	MinimapCam->GetCaptureComponent2D()->ShowFlags.SetReflectionEnvironment(false);
	MinimapCam->GetCaptureComponent2D()->ShowFlags.SetWireframe(bIsIndoors);
	MinimapCam->GetCaptureComponent2D()->ShowFlags.SetLighting(false);
	MinimapCam->GetCaptureComponent2D()->ShowFlags.SetDiffuse(true);
	MinimapCam->GetCaptureComponent2D()->ProjectionType = ECameraProjectionMode::Orthographic;
	MinimapCam->GetCaptureComponent2D()->OrthoWidth = FMath::Max(Extent.X, Extent.Y) * 2.0f;
	MinimapCam->GetCaptureComponent2D()->TextureTarget = HUD->Minimap;
	MinimapCam->GetCaptureComponent2D()->bCaptureEveryFrame = false;
	MinimapCam->GetCaptureComponent2D()->bCaptureOnMovement = false;
	for (TActorIterator<AActor> Iter(GetWorld()); Iter; ++Iter) {
		if (Iter->ActorHasTag(ActorTags::kExcludeTag) || (bIsIndoors && !Iter->ActorHasTag(ActorTags::kVisibleTag))) {
			MinimapCam->GetCaptureComponent2D()->HiddenActors.Add(*Iter);
		}

		if (bIsIndoors) {
			UStaticMeshComponent* Mesh = Cast<UStaticMeshComponent>(Iter->GetComponentByClass(UStaticMeshComponent::StaticClass()));
			if (Mesh != nullptr) {
				Mesh->bOverrideWireframeColor = true;
				Mesh->WireframeColorOverride = FColor(200, 200, 200);
				Mesh->MarkRenderStateDirty();
			}
		}
	}
	MinimapCam->GetCaptureComponent2D()->CaptureScene();
	HUD->MinimapSize = MinimapCam->GetCaptureComponent2D()->OrthoWidth;
	HUD->MinimapOrigin = Origin;

	// Re-initialize the player
	if (SavedPlayer.Num() > 0) {
		FMemoryReader PlayerReader(SavedPlayer);
		PlayerReader.Seek(0);
		Cast<APlayerPartyCharacter>(PlayerPartyMembers[0]->OwnerCharacter.Get())->Load(PlayerReader, bIgnorePlayerTransforms);
		if (bTeleportOnLoad) {
			bTeleportOnLoad = false;
			if (!TeleportObject.IsNone()) {
				for (TActorIterator<AActor> Iter(GetWorld()); Iter; ++Iter) {
					if (Iter->GetName().Equals(TeleportObject.ToString())) {
						const float Offset = PlayerPartyMembers[0]->OwnerCharacter->GetCapsuleComponent()->GetScaledCapsuleHalfHeight() - Iter->GetSimpleCollisionHalfHeight();
						PlayerPartyMembers[0]->OwnerCharacter->SetActorLocation(Iter->GetActorLocation() + Iter->GetActorForwardVector() * 200.0f + FVector(0.0, 0.0, Offset));
						PlayerPartyMembers[0]->OwnerCharacter->GetController()->SetControlRotation(Iter->GetActorRotation());
						break;
					}
				}
				TeleportObject = FName();
			} else {
				PlayerPartyMembers[0]->OwnerCharacter->SetActorLocation(TeleportLocation);
				PlayerPartyMembers[0]->OwnerCharacter->GetController()->SetControlRotation(TeleportRotation);
				TeleportLocation = FVector::ZeroVector;
				TeleportRotation = FRotator::ZeroRotator;
			}
		}
		bIgnorePlayerTransforms = false;
	}

	// Give Time of Day Manager reference to new World
	TimeOfDayManager->World = GetWorld();

	// Re-initialize the turn timers/turn state
	if (bIsInTurnMode) {
		// Flip is so that SetTurnMode() doesn't think nothing changed
		bIsInTurnMode = !bIsInTurnMode;
		SetTurnMode(true);
	} else {
		for (auto PawnIterator = GetWorld()->GetPawnIterator(); PawnIterator; ++PawnIterator) {
			ANNSCharacter* Character = Cast<ANNSCharacter>(PawnIterator->Get());
			const int32 Active = Character->ActiveCharacter;
			for (auto& Member : Character->PartyMembers) {
				Member->ConvertTurnCountToTimer();
			}
			// AdvanceActiveCharacter() may be called a bunch... so manually force the active character back to the saved value
			Character->ActiveCharacter = Active;
		}
		TimeOfDayManager->StartTimer();
	}
}

void UNNSGameInstance::GetLevelBoundingBox(UWorld* World, bool bIsIndoors, FVector& Origin, FVector& Extent)
{
	FVector MinValues, MaxValues;
	for (int32 i = 0; i < 3; ++i) {
		MinValues[i] = TNumericLimits<float>::Max();
		MaxValues[i] = TNumericLimits<float>::Lowest();
	}
	TArray<AActor*> Actors;
	// Get bounds of Landscapes, Static Meshes, and Brushes (ignore all else)
	if (!bIsIndoors) {
		for (TActorIterator<ALandscape> Iter(World); Iter; ++Iter) {
			Actors.Add(*Iter);
		}
	} else {
		for (TActorIterator<AStaticMeshActor> Iter(World); Iter; ++Iter) {
			Actors.Add(*Iter);
		}
		for (TActorIterator<ABrush> Iter(World); Iter; ++Iter) {
			if (!Iter->IsA<AVolume>()) {
				Actors.Add(*Iter);
			}
		}
	}
	for (const AActor* Actor : Actors) {
		if (Actor->ActorHasTag(ActorTags::kExcludeTag)) continue;

		FVector ActorOrigin, ActorExtent;
		Actor->GetActorBounds(true, ActorOrigin, ActorExtent);
		for (int32 i = 0; i < 3; ++i) {
			if (ActorOrigin[i] + ActorExtent[i] > MaxValues[i]) MaxValues[i] = ActorOrigin[i] + ActorExtent[i];
			if (ActorOrigin[i] - ActorExtent[i] < MinValues[i]) MinValues[i] = ActorOrigin[i] - ActorExtent[i];
		}
	}

	for (int32 i = 0; i < 3; ++i) {
		Extent[i] = (MaxValues[i] - MinValues[i]) / 2;
		Origin[i] = Extent[i] + MinValues[i];
	}
}

void UNNSGameInstance::ShowWireframeColors(bool bShowColors)
{
	if (bIsIndoors) {
		for (TActorIterator<AActor> Iter(GetWorld()); Iter; ++Iter) {
			UStaticMeshComponent* Mesh = Cast<UStaticMeshComponent>(Iter->GetComponentByClass(UStaticMeshComponent::StaticClass()));
			if (Mesh != nullptr && Mesh->bOverrideWireframeColor == bShowColors) {
				Mesh->bOverrideWireframeColor = !bShowColors;
				Mesh->MarkRenderStateDirty();
			}
		}
		MinimapCam->GetCaptureComponent2D()->CaptureScene();
	}
}

UAudioComponent* UNNSGameInstance::PlayMusic(USoundBase* Sound, float FadeTime)
{
	UAudioComponent* Audio = UGameplayStatics::CreateSound2D(GetWorld(), Sound, MasterVolume * MusicVolume);
	PlayingMusic.Add(Audio);
	Audio->FadeIn(FadeTime);
	return Audio;
}

void UNNSGameInstance::StopMusic(UAudioComponent* Sound, float FadeTime)
{
	if (Sound != nullptr) {
		Sound->FadeOut(FadeTime, 0.0f);
		PlayingMusic.Remove(Sound);
	}
}

void UNNSGameInstance::PlaySoundEffect2D(USoundBase* Sound)
{
	UGameplayStatics::PlaySound2D(GetWorld(), Sound, MasterVolume * SoundEffectVolume);
}

void UNNSGameInstance::PlaySoundEffect3D(USoundBase* Sound, const FVector& Location)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, Location, MasterVolume * SoundEffectVolume);
}

void UNNSGameInstance::RegisterAmbientSound(UAudioComponent* Sound)
{
	PlayingSounds.Add(Sound);
	Sound->SetVolumeMultiplier(MasterVolume * SoundEffectVolume);
}

void UNNSGameInstance::SetMasterVolume(float Volume)
{
	MasterVolume = Volume;
	AdjustPlayingVolumes(true, true);
}

void UNNSGameInstance::SetMusicVolume(float Volume)
{
	MusicVolume = Volume;
	AdjustPlayingVolumes(true, false);
}

void UNNSGameInstance::SetSoundEffectVolume(float Volume)
{
	SoundEffectVolume = Volume;
	AdjustPlayingVolumes(false, true);
}

void UNNSGameInstance::AdjustPlayingVolumes(bool bMusic, bool bSounds)
{
	if (bMusic) {
		for (auto& Sound : PlayingMusic) {
			Sound->SetVolumeMultiplier(MasterVolume * MusicVolume);
		}
	}
	if (bSounds) {
		for (auto& Sound : PlayingSounds) {
			Sound->SetVolumeMultiplier(MasterVolume * SoundEffectVolume);
		}
	}
}

void UNNSGameInstance::SetFieldOfView(int32 FOV)
{
	FieldOfView = FOV;
	if (PlayerPartyMembers[0]->OwnerCharacter.IsValid()) {
		Cast<APlayerPartyCharacter>(PlayerPartyMembers[0]->OwnerCharacter.Get())->FirstPersonCameraComponent->FieldOfView = FieldOfView;
	}
}

void UNNSGameInstance::SetUseBackwardsWalkSpeed(bool bUse)
{
	bUseBackwardsSpeed = bUse;
	if (PlayerPartyMembers[0]->OwnerCharacter.IsValid()) {
		PlayerPartyMembers[0]->OwnerCharacter->UpdateWalkSpeed();
	}
}

void UNNSGameInstance::SaveGameToFile(const FString& FileName)
{
	FString FinalFileName = FileName;
	// For quicksave/autosave, there are multiple slots - save to the oldest slot (or fill a slot if it's empty)
	if (FileName == "quicksave" || FileName == "autosave") {
		IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();
		const int32& NumSlots = (FileName == "quicksave" ? QuicksaveSlots : AutosaveSlots);
		TArray<int64> Timestamps;
		for (int32 i = 0; i < NumSlots; ++i) {
			const FString TempFile = FString::Printf(*SaveFilePattern, *FString::Printf(TEXT("%s_%i"), *FileName, i + 1));
			if (PlatformFile.FileExists(*TempFile)) {
				Timestamps.Add(PlatformFile.GetTimeStamp(*TempFile).ToUnixTimestamp());
			} else {
				Timestamps.Add(0);
				break;
			}
		}
		int32 Index = 0;
		FMath::Min<int64>(Timestamps, &Index);
		FinalFileName = FString::Printf(TEXT("%s_%i"), *FileName, Index + 1);
	}
	const FString FullPath = FString::Printf(*SaveFilePattern, *FinalFileName);

	FBufferArchive ToBinary(true);
	Save(ToBinary);

	if (ToBinary.Num() == 0) return;

	FFileHelper::SaveArrayToFile(ToBinary, *FullPath);
	ToBinary.FlushCache();
	ToBinary.Close();
}

void UNNSGameInstance::LoadGameFromFile(const FString& FileName)
{
	FString FinalFileName = FileName;
	// For quicksave/autosave, there are multiple slots - load from the newest slot
	if (FileName == "quicksave" || FileName == "autosave") {
		IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();
		const int32& NumSlots = (FileName == "quicksave" ? QuicksaveSlots : AutosaveSlots);
		TArray<int64> Timestamps;
		for (int32 i = 0; i < NumSlots; ++i) {
			const FString TempFile = FString::Printf(*SaveFilePattern, *FString::Printf(TEXT("%s_%i"), *FileName, i + 1));
			if (PlatformFile.FileExists(*TempFile)) {
				Timestamps.Add(PlatformFile.GetTimeStamp(*TempFile).ToUnixTimestamp());
			} else {
				break;
			}
		}
		int32 Index = 0;
		FMath::Max<int64>(Timestamps, &Index);
		FinalFileName = FString::Printf(TEXT("%s_%i"), *FileName, Index + 1);
	}
	const FString FullPath = FString::Printf(*SaveFilePattern, *FinalFileName);

	TArray<uint8> Bytes;
	if (!FFileHelper::LoadFileToArray(Bytes, *FullPath) || Bytes.Num() == 0) {
		UE_LOG(LogTemp, Warning, TEXT("Failed to load game from: %s"), *FullPath);
		return;
	}

	FMemoryReader FromBinary(Bytes, true);
	FromBinary.Seek(0);

	Load(FromBinary);

	FromBinary.FlushCache();
	FromBinary.Close();
}

TArray<FString> UNNSGameInstance::GetSaveFiles(bool bIgnoreAutoQuick)
{
	TArray<FString> Files;
	IFileManager::Get().FindFiles(Files, *FString::Printf(*SaveFilePattern, TEXT("*")), true, false);
	TArray<TPair<FString, int64> > FilesAndTimes;
	for (int32 i = 0; i < Files.Num(); ++i) {
		if (bIgnoreAutoQuick && (Files[i].StartsWith(TEXT("quicksave")) || Files[i].StartsWith(TEXT("autosave")))) continue;

		TPair<FString, int64> Pair;
		Pair.Key = Files[i].LeftChop(4);
		Pair.Value = IFileManager::Get().GetTimeStamp(*FString::Printf(*SaveFilePattern, *Pair.Key)).ToUnixTimestamp();
		FilesAndTimes.Add(Pair);
	}
	FilesAndTimes.Sort([](const TPair<FString, int64>& One, const TPair<FString, int64>& Two) {
		return One.Value > Two.Value;
	});
	Files.Empty();
	for (int32 i = 0; i < FilesAndTimes.Num(); ++i) {
		Files.Add(FilesAndTimes[i].Key);
	}
	return Files;
}

void UNNSGameInstance::SwitchLevel(const FName& LevelName)
{
	SaveGameToFile(TEXT("autosave"));
	// If walking to a new level, spawn at spawn location (and ignore saved transforms)
	// For inter-level loading, we do want to spawn with saved transforms
	bIgnorePlayerTransforms = true;
	OpenLevel(LevelName);
	// Reload level in blueprint? Not sure how to get level load callback
}

void UNNSGameInstance::OpenLevel(const FName& LevelName)
{
	PlayingMusic.Empty();
	PlayingSounds.Empty();
	UGameplayStatics::OpenLevel(GetWorld(), LevelName);
}

void UNNSGameInstance::Teleport(const FName& LevelName, const FVector& Location, const FRotator& Rotation)
{
	if (LevelName.ToString() == GetWorld()->GetName()) {
		PlayerPartyMembers[0]->OwnerCharacter->SetActorLocation(Location);
		PlayerPartyMembers[0]->OwnerCharacter->GetController()->SetControlRotation(Rotation);
	} else {
		bTeleportOnLoad = true;
		TeleportLocation = Location;
		TeleportRotation = Rotation;
		SaveGameToFile(TEXT("autosave"));
		OpenLevel(LevelName);
	}
}

void UNNSGameInstance::Respawn()
{
	UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(0.0f, 1.0f, 2.0f, FLinearColor::Black, false, true);
	FTimerHandle Handle;
	GetWorld()->GetTimerManager().SetTimer(Handle, this, &UNNSGameInstance::RespawnPrivate, 4.0f);
}

void UNNSGameInstance::RespawnPrivate()
{
	static const FVector kLocation(-20700.0f, 15766.0f, 471.0f);
	static const FRotator kRotation(0.0f, -90.0f, 0.0f);
	static const FName kLevelName = FName(TEXT("Level_Landscape"));

	PlayerPartyMembers[0]->OwnerCharacter->bIsDead = false;
	for (UCharacterSheet* Member : PlayerPartyMembers) {
		Member->FullHeal();
	}

	if (bIsInTurnMode) {
		SetTurnMode(false);
	}

	if (GetWorld()->GetName() == kLevelName.ToString()) {
		PlayerPartyMembers[0]->OwnerCharacter->SetActorLocation(kLocation);
		PlayerPartyMembers[0]->OwnerCharacter->GetController()->SetControlRotation(kRotation);
		PlayerPartyMembers[0]->OwnerCharacter->EnableInput(Cast<APlayerController>(PlayerPartyMembers[0]->OwnerCharacter->GetController()));
		UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(1.0f, 0.0f, 2.0f, FLinearColor::Black);
	} else {
		bTeleportOnLoad = true;
		TeleportLocation = kLocation;
		TeleportRotation = kRotation;
		
		FBufferArchive ToBinary(true);
		Save(ToBinary);
		// Don't actually save to file

		OpenLevel(kLevelName);
	}
}

void UNNSGameInstance::TeleportToObject(const FName& LevelName, const FName& OtherSideObjectName)
{
	if (LevelName.ToString() == GetWorld()->GetName()) {
		for (TActorIterator<AActor> Iter(GetWorld()); Iter; ++Iter) {
			if (Iter->GetName().Equals(OtherSideObjectName.ToString())) {
				const float Offset = PlayerPartyMembers[0]->OwnerCharacter->GetCapsuleComponent()->GetScaledCapsuleHalfHeight() - Iter->GetSimpleCollisionHalfHeight();
				PlayerPartyMembers[0]->OwnerCharacter->SetActorLocation(Iter->GetActorLocation() + Iter->GetActorForwardVector() * 200.0f + FVector(0.0, 0.0, Offset));
				PlayerPartyMembers[0]->OwnerCharacter->GetController()->SetControlRotation(Iter->GetActorRotation());
				break;
			}
		}
	} else {
		bTeleportOnLoad = true;
		TeleportObject = OtherSideObjectName;
		SaveGameToFile(TEXT("autosave"));
		OpenLevel(LevelName);
	}
}

void UNNSGameInstance::QuitToMainMenu()
{
	SavedPlayer.Empty();
	SavedLevels.Empty();
	OpenLevel(TEXT("Level_MainMenu"));
}

void UNNSGameInstance::BeginGame(TArray<FCharacterCreatorInfo> Characters)
{
	InitPartyMembers(Characters);
	TimeOfDayManager->SetTime(12, 0);
	// Load first level
	OpenLevel(TEXT("Level_Landscape"));
}

void UNNSGameInstance::InitPartyMembers(const TArray<FCharacterCreatorInfo>& Characters)
{
	PlayerPartyMembers.Empty();
	for (int32 i = 0; i < kCharactersInParty; ++i) {
		PlayerPartyMembers.Add(NewObject<UCharacterSheet>());
		PlayerPartyMembers[i]->DeathBarrier = -10.0f;

		// Assign stats
		PlayerPartyMembers[i]->BaseStats[(int32)EStats::StatStrength] = Characters[i].Strength;
		PlayerPartyMembers[i]->BaseStats[(int32)EStats::StatIntelligence] = Characters[i].Intelligence;
		PlayerPartyMembers[i]->BaseStats[(int32)EStats::StatDexterity] = Characters[i].Dexterity;
		PlayerPartyMembers[i]->BaseStats[(int32)EStats::StatSpeed] = Characters[i].Speed;
		PlayerPartyMembers[i]->BaseStats[(int32)EStats::StatVitality] = Characters[i].Vitality;
		PlayerPartyMembers[i]->BaseStats[(int32)EStats::StatLuck] = Characters[i].Luck;

		PlayerPartyMembers[i]->BaseStats[(int32)EStats::StatHealth] = Characters[i].Vitality * 5;
		PlayerPartyMembers[i]->BaseStats[(int32)EStats::StatMana] = Characters[i].Intelligence * 3;
		PlayerPartyMembers[i]->BaseStats[(int32)EStats::StatAccuracy] = Characters[i].Dexterity;
		PlayerPartyMembers[i]->BaseStats[(int32)EStats::StatEvasion] = Characters[i].Speed;
		PlayerPartyMembers[i]->BaseStats[(int32)EStats::StatResistAir] = Characters[i].Intelligence / 2;
		PlayerPartyMembers[i]->BaseStats[(int32)EStats::StatResistFire] = Characters[i].Intelligence / 2;
		PlayerPartyMembers[i]->Class = Characters[i].Class;

		PlayerPartyMembers[i]->Portrait = Characters[i].Portrait;

		// Assign skills
		for (const auto& Skill : Characters[i].Skills) {
			PlayerPartyMembers[i]->BaseSkills[(int32)Skill] = 1;

			// Give default items
			switch (Skill) {
			case ESkills::SkillAirMagic:
				PlayerPartyMembers[i]->AddItemToInventory(USpellbook::Create(ESpells::SpellWizardsEye));
				PlayerPartyMembers[i]->AddItemToInventory(USpellbook::Create(ESpells::SpellShock));
				break;
			case ESkills::SkillAlchemy:
				PlayerPartyMembers[i]->AddItemToInventory(UPotionItem::Create(UPotionItem::kEmptyString));
				PlayerPartyMembers[i]->AddItemToInventory(UPotionItem::Create(UPotionItem::kEmptyString));
				PlayerPartyMembers[i]->AddItemToInventory(UPotionItem::Create(UPotionItem::kEmptyString));
				PlayerPartyMembers[i]->AddItemToInventory(UPotionItem::Create(UPotionItem::kBlueReagentString));
				PlayerPartyMembers[i]->AddItemToInventory(UPotionItem::Create(UPotionItem::kRedReagentString));
				PlayerPartyMembers[i]->AddItemToInventory(UPotionItem::Create(UPotionItem::kYellowReagentString));
				break;
			case ESkills::SkillArmorChain:
				PlayerPartyMembers[i]->AddItemToInventory(UEquippableItem::Create(TEXT("ChainMail")));
				break;
			case ESkills::SkillArmorLeather:
				PlayerPartyMembers[i]->AddItemToInventory(UEquippableItem::Create(TEXT("WornLeatherArmor")));
				break;
			case ESkills::SkillArmorPlate:
				PlayerPartyMembers[i]->AddItemToInventory(UEquippableItem::Create(TEXT("RustyPlateMail")));
				break;
			case ESkills::SkillArmorShield:
				PlayerPartyMembers[i]->AddItemToInventory(UEquippableItem::Create(TEXT("Buckler")));
				break;
			case ESkills::SkillBodyMagic:
				PlayerPartyMembers[i]->AddItemToInventory(USpellbook::Create(ESpells::SpellCureLight));
				PlayerPartyMembers[i]->AddItemToInventory(USpellbook::Create(ESpells::SpellFlee));
				break;
			case ESkills::SkillFireMagic:
				PlayerPartyMembers[i]->AddItemToInventory(USpellbook::Create(ESpells::SpellTorchLight));
				PlayerPartyMembers[i]->AddItemToInventory(USpellbook::Create(ESpells::SpellFlameBolt));
				break;
			case ESkills::SkillWeaponAxe:
				PlayerPartyMembers[i]->AddItemToInventory(UEquippableItem::Create(TEXT("WornAxe")));
				break;
			case ESkills::SkillWeaponBow:
				PlayerPartyMembers[i]->AddItemToInventory(UEquippableItem::Create(TEXT("Shortbow")));
				break;
			case ESkills::SkillWeaponDagger:
				PlayerPartyMembers[i]->AddItemToInventory(UEquippableItem::Create(TEXT("WornDagger")));
				break;
			case ESkills::SkillWeaponSword:
				PlayerPartyMembers[i]->AddItemToInventory(UEquippableItem::Create(TEXT("WornSword")));
				break;
			}
		}

		for (auto& Item : PlayerPartyMembers[i]->Inventory->PositionMap) {
			Item.Key->bIsIdentified = true;
		}

		PlayerPartyMembers[i]->Voice = Characters[i].Voice;

		// Set name
		PlayerPartyMembers[i]->Name = Characters[i].Name;

		PlayerPartyMembers[i]->Init();
	}

	PlayerGold = 1000;
}
