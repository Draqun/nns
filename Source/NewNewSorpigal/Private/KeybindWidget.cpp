// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "KeybindWidget.h"

UKeybindWidget::UKeybindWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

TArray<FInputActionKeyMapping> UKeybindWidget::GetBindings()
{
	return GetDefault<UInputSettings>()->ActionMappings;
}
