// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "GoldItem.h"

UGoldItem::UGoldItem()
	: Super()
{
	static ConstructorHelpers::FObjectFinder<UTexture2D> IconFinder(TEXT("Texture2D'/Game/Models/Chest/chest.chest'"));
	Icon = TAssetPtr<UTexture>(IconFinder.Object);
	Name = FName(TEXT("Gold"));
	IconWidth = 2;
	IconHeight = 1;
	bIsIdentified = true;
}

void UGoldItem::SaveLoadCommon(FArchive& Archive)
{
	Super::SaveLoadCommon(Archive);
	Archive << Amount;
}

UGoldItem* UGoldItem::Create(uint32 Amount)
{
	UGoldItem* Item = NewObject<UGoldItem>();
	Item->Amount = Amount;
	return Item;
}

TArray<FString> UGoldItem::ToString() const
{
	TArray<FString> Lines;
	Lines.Add(FString::Printf(TEXT("Amount: %u"), Amount));
	return Lines;
}
