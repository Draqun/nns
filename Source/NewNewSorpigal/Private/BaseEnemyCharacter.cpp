// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "BaseEnemyCharacter.h"

#include "CharacterSheet.h"
#include "DataStructs.h"
#include "EquippableItem.h"
#include "Globals.h"
#include "ItemGenerator.h"
#include "NNSCharacterAnimInstance.h"
#include "NNSGameInstance.h"
#include "Pickup.h"
#include "PlayerPartyCharacter.h"
#include "TimeOfDayManager.h"
#include "QuestManager.h"

// Sets default values
ABaseEnemyCharacter::ABaseEnemyCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<APickup> Loot(TEXT("Blueprint'/Game/Blueprints/Items/LootPickup.LootPickup_C'"));
	LootPickupClass = Loot.Class;

	AttackAnimation = nullptr;

	SensingComponent = ObjectInitializer.CreateDefaultSubobject<UPawnSensingComponent>(this, TEXT("Sensing"));
	SensingComponent->HearingThreshold = 0.0f;
	SensingComponent->LOSHearingThreshold = 2500.0f;
	SensingComponent->SightRadius = 3500.0f;
	SensingComponent->OnSeePawn.AddDynamic(this, &ABaseEnemyCharacter::OnSeePawn);
	SensingComponent->OnHearNoise.AddDynamic(this, &ABaseEnemyCharacter::OnHearPawn);

	Tags.Add(ActorTags::kEnemyTag);
	Tags.Add(ActorTags::kIdentifiableTag);
	Tags.Add(ActorTags::kAttackableTag);

	// Using FClassFinder on the AI Blueprint doesn't work for some reason...
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	TimeLastNoticed = -10000.0f;
	bLastNoticedSight = false;
	TimeCanCastNextSpellAt = -10000.0f;

	bStartedTurnModeInRadius = false;
}

void ABaseEnemyCharacter::SaveLoadCommon(FArchive & Archive)
{
	Super::SaveLoadCommon(Archive);

	Archive << SpawnLocation;
	Archive << bStartedTurnModeInRadius;
}

void ABaseEnemyCharacter::Save(FArchive & Archive)
{
	// Must be loaded in UNNSGameInstance
	FString Name = GetName();
	Archive << Name;

	Super::Save(Archive);

	for (auto& Character : PartyMembers) {
		Character->Save(Archive, true);
	}
}

void ABaseEnemyCharacter::Load(FArchive & Archive, bool bIgnoreTransforms)
{
	FTransform PlayerTransform;
	Archive << PlayerTransform;
	SetActorTransform(PlayerTransform);

	Super::Load(Archive);

	for (auto& Character : PartyMembers) {
		Character->Load(Archive, true);
	}
}

// Called when the game starts or when spawned
void ABaseEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();

	SpawnLocation = GetActorLocation();

	// Currently, all enemies have a party size of 1
	PartyMembers.Add(NewObject<UCharacterSheet>());

	// Init enemy stats. MUST have DataTableName set to something in the EnemyDataTable or it will crash
	FEnemyData* EnemyData = Cast<UNNSGameInstance>(GetGameInstance())->EnemyDataTable->FindRow<FEnemyData>(DataTableName, FString(TEXT("EnemyData")));
	GetCharacterMovement()->MaxWalkSpeed = EnemyData->MaxSpeed;
	MaxWalkDistance = EnemyData->MaxSpeed * PassTime;
	StatusOnHit = EnemyData->Status;
	for (auto& Member : PartyMembers) {
		Member->OwnerCharacter = this;
		Member->Name = DataTableName.ToString();
		Member->InitEnemy(*EnemyData);
	}
}

// Called every frame
void ABaseEnemyCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ABaseEnemyCharacter::OnDeath()
{
	Super::OnDeath();

	AController* CurrentController = GetController();
	if (CurrentController != nullptr) {
		// Stop movement so the death animation plays immediately
		CurrentController->StopMovement();
		// Unpossess to stop AI
		CurrentController->UnPossess();
		// Destroy the controller, since it's not part of the enemy anymore
		CurrentController->Destroy();
	}
	// Disable collision so arrows penetrate during death animation
	SetActorEnableCollision(false);

	if (GetMesh() && GetMesh()->GetAnimInstance() && AttackAnimation != nullptr && GetMesh()->GetAnimInstance()->Montage_IsPlaying(AttackAnimation)) {
		GetMesh()->GetAnimInstance()->Montage_Stop(0.0f, AttackAnimation);
	}

	bIsMyTurn = false;

	Cast<UNNSGameInstance>(GetGameInstance())->QuestManager->UpdateQuestsFromKill(DataTableName.ToString());

	Cast<APlayerPartyCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter())->GiveExperience(GetExperience());

	// Drop loot if the enemy has anything to drop
	FEnemyData* EnemyData = UNNSGameInstance::EnemyDataTable->FindRow<FEnemyData>(DataTableName, FString(TEXT("EnemyData")));
	uint32 Gold = FMath::RandRange(EnemyData->GoldMin, EnemyData->GoldMax);
	const int32 PlayerLuck = Cast<APlayerPartyCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter())->GetPartyCombinedStat(EStats::StatLuck);
	if (FMath::RandRange(0.0f, 1.0f) < (PlayerLuck - 40) * .01f) {
		Gold *= 3;
	}
	UItem* Item = nullptr;
	if (FMath::RandRange(0.0f, 1.0f) < (PlayerLuck) * .0005f + .03f) {
		Item = Cast<UNNSGameInstance>(GetGameInstance())->ItemGeneratorObject->GenerateItem(FMath::Clamp(FMath::FloorToInt(PartyMembers[0]->Level / 10) + 1, 1, 5));
	}

	if (Gold > 0 || Item != nullptr) {
		APickup* Loot = GetWorld()->SpawnActor<APickup>(LootPickupClass, GetActorLocation(), GetActorRotation(), FActorSpawnParameters());
		Loot->Gold = Gold;
		Loot->Item = Item;
	}

	Tags.Empty();

	// Queue function to be called after death animation finishes
	FTimerHandle DeathAnimationHandle;
	GetWorldTimerManager().SetTimer(DeathAnimationHandle, this, &ABaseEnemyCharacter::OnDeathAnimationFinished, 2.5f, false);
}

void ABaseEnemyCharacter::UpdateWalkSpeed()
{
	FEnemyData* Data = UNNSGameInstance::EnemyDataTable->FindRow<FEnemyData>(DataTableName, TEXT("ResetSpeed"));
	GetCharacterMovement()->MaxWalkSpeed = Data->MaxSpeed * (Cast<UNNSGameInstance>(GetGameInstance())->bIsInTurnMode ? 2 : 1);
}

void ABaseEnemyCharacter::FinishAttack()
{
	EndTurn();
}

void ABaseEnemyCharacter::EndTurn()
{
	Super::EndTurn();

	GetWorldTimerManager().ClearTimer(TurnLimitHandle);
}

void ABaseEnemyCharacter::StartTurn()
{
	Super::StartTurn();

	if (!bStartedTurnModeInRadius) {
		for (auto& Member : PartyMembers) {
			if (Member->bCanAct) {
				Pass();
			}
		}
	}

	GetWorldTimerManager().SetTimer(TurnLimitHandle, this, &ABaseEnemyCharacter::OnTurnLimitExpired, 2.0f);
}

void ABaseEnemyCharacter::OnTurnLimitExpired()
{
	if (GetMesh() && GetMesh()->GetAnimInstance() && AttackAnimation != nullptr && GetMesh()->GetAnimInstance()->Montage_IsPlaying(AttackAnimation)) {
		return;
	}
	Pass();
}

TArray<ESpells> ABaseEnemyCharacter::GetCastableSpells(bool bExitAfterFirst) const
{
	TArray<ESpells> CastableSpells;

	UCharacterSheet* Character = PartyMembers[ActiveCharacter];
	const float MinutesSinceGameStart = Cast<UNNSGameInstance>(GetGameInstance())->TimeOfDayManager->GetMinutesSinceGameStart();
	if (Character->bCanAct && MinutesSinceGameStart >= TimeCanCastNextSpellAt) {
		for (int32 i = 0; i < (int32)ESpells::SpellTotalSpells; ++i) {
			const ESpells Spell = (ESpells)i;
			const FSpellInfo& SpellInfo = EnumMaps::SpellInfo[Spell];
			if (Character->HasLearnedSpell(Spell) && Character->CurrentMana >= SpellInfo.ManaCost) {
				bool bCanCast = false;
				if (SpellInfo.TargetMode == ETargetMode::TargetAlly || SpellInfo.TargetMode == ETargetMode::TargetNone) {
					switch (Spell) {
					case ESpells::SpellFireShield:
						bCanCast = !Character->HasStatus(EStatusEffects::StatusResistFire);
					case ESpells::SpellAirShield:
						bCanCast = !Character->HasStatus(EStatusEffects::StatusResistAir);
					case ESpells::SpellRegen:
						bCanCast = !Character->HasStatus(EStatusEffects::StatusRegen);
					case ESpells::SpellHeroism:
						bCanCast = !Character->HasStatus(EStatusEffects::StatusHeroism);
					case ESpells::SpellCureLight:
					case ESpells::SpellCureHeavy:
						bCanCast = Character->CurrentHealth < (Character->GetStat(EStats::StatHealth) / 2.0f);
					default:
						bCanCast = true;
					}
				} else if (SpellInfo.TargetMode == ETargetMode::TargetEnemy) {
					bCanCast = bLastNoticedSight && (MinutesSinceGameStart - TimeLastNoticed) <= 1;
				}
				if (bCanCast) {
					CastableSpells.Add(Spell);
					if (bExitAfterFirst) {
						break;
					}
				}
			}
		}
	}

	return CastableSpells;
}

bool ABaseEnemyCharacter::CanCastSpell() const
{
	return GetCastableSpells(true).Num() > 0;
}

void ABaseEnemyCharacter::CastRandomSpell()
{
	UCharacterSheet* Character = PartyMembers[ActiveCharacter];
	if (Character->bCanAct) {
		TArray<ESpells> CastableSpells = GetCastableSpells(false);
		const int32 Index = FMath::RandRange(0, CastableSpells.Num() - 1);
		const FSpellInfo& SpellInfo = EnumMaps::SpellInfo[CastableSpells[Index]];
		if (SpellInfo.TargetMode == ETargetMode::TargetAlly) {
			CastSpell(CastableSpells[Index], Character);
		} else if (SpellInfo.TargetMode == ETargetMode::TargetNone) {
			CastSpell(CastableSpells[Index]);
		} else if (SpellInfo.TargetMode == ETargetMode::TargetEnemy) {
			CastSpell(CastableSpells[Index], Cast<ANNSCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter())->PartyMembers[0]);
		}
		TimeCanCastNextSpellAt = Cast<UNNSGameInstance>(GetGameInstance())->TimeOfDayManager->GetMinutesSinceGameStart() + FMath::RandRange(2, 5);
		bool bEndTurn = true;
		if (GetMesh() && GetMesh()->GetAnimInstance() && AttackAnimation != nullptr) {
			const float Duration = GetMesh()->GetAnimInstance()->Montage_Play(AttackAnimation, 1.0f, EMontagePlayReturnType::Duration);
			bEndTurn = !Cast<UNNSGameInstance>(GetGameInstance())->bIsInTurnMode;

			if (!bEndTurn) {
				FTimerHandle Handle;
				GetWorld()->GetTimerManager().SetTimer(Handle, this, &ABaseEnemyCharacter::FinishAttack, Duration, false);
			}
		}
		PartyMembers[ActiveCharacter]->StartTurnTimer(PartyMembers[ActiveCharacter]->GetDelay(PartyMembers[ActiveCharacter]->GetAttackDelay(EEquipType::EquipMainHand)), bEndTurn);
	}
}

int32 ABaseEnemyCharacter::GetExperience() const
{
	// Add all the party members together for the final value
	int32 Experience = 0;
	for (auto& Member : PartyMembers) {
		Experience += Member->Experience;
	}
	return Experience;
}

void ABaseEnemyCharacter::Attack()
{
	if (PartyMembers[ActiveCharacter]->bCanAct) {
		ANNSCharacter* PlayerCharacter = Cast<ANNSCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter());
		if (GetDistanceTo(PlayerCharacter) <= MaxMeleeDistance) {
			UDamageData* Damage = UDamageData::Create(EDamageTypes::DamagePhysical, PartyMembers[ActiveCharacter]->GetDamage(EEquipType::EquipMainHand),
													  PartyMembers[ActiveCharacter]->GetStat(EStats::StatAccuracy), EDamageDistance::DamageMelee, PartyMembers[ActiveCharacter]->Name, this,
													  false, FMath::RandRange(1, 100) <= 5 ? StatusOnHit : EStatusEffects::StatusTotalStatusEffects, FStatusEffect(FMath::RandRange(3, 12), FMath::FloorToInt(PartyMembers[ActiveCharacter]->Level / 10) + 1));
			PlayerCharacter->DealDamage(Damage);

			// If we're in turn mode, we don't want to end the turn right now. Wait for attack end callback for that.
			bool bEndTurn = true;
			if (GetMesh() && GetMesh()->GetAnimInstance() && AttackAnimation != nullptr) {
				const float Duration = GetMesh()->GetAnimInstance()->Montage_Play(AttackAnimation, 1.0f, EMontagePlayReturnType::Duration);
				bEndTurn = !Cast<UNNSGameInstance>(GetGameInstance())->bIsInTurnMode;

				if (!bEndTurn) {
					FTimerHandle Handle;
					GetWorld()->GetTimerManager().SetTimer(Handle, this, &ABaseEnemyCharacter::FinishAttack, Duration, false);
				}
			}

			PartyMembers[ActiveCharacter]->StartTurnTimer(PartyMembers[ActiveCharacter]->GetDelay(PartyMembers[ActiveCharacter]->GetAttackDelay(EEquipType::EquipMainHand)), bEndTurn);
			if (Cast<UNNSGameInstance>(GetGameInstance())->bUseCombatMode) {
				const bool bAboutToTick = Cast<UNNSGameInstance>(GetGameInstance())->TimeOfDayManager->TimeRemainingUntilTick() <= 0.5f;
				PlayerCharacter->AddStatusEffect(EPartyStatusEffects::StatusInCombat, bAboutToTick ? 3 : 2);
			}

			PlaySound(AttackSounds, ActiveCharacter);
		}
	}
}

void ABaseEnemyCharacter::DealDamage(UDamageData* Damage, bool DamageAll)
{
	Super::DealDamage(Damage, DamageAll);

	APlayerPartyCharacter* Player = Cast<APlayerPartyCharacter>(Damage->SourceActor);
	if (Player != nullptr) {
		OnSeePawn(Player);
	}
}

void ABaseEnemyCharacter::DealDamage(UDamageData* Damage, UCharacterSheet* Character)
{
	Super::DealDamage(Damage, Character);

	APlayerPartyCharacter* Player = Cast<APlayerPartyCharacter>(Damage->SourceActor);
	if (Player != nullptr) {
		OnSeePawn(Player);
	}
}

void ABaseEnemyCharacter::OnDeathAnimationFinished()
{
	// Make enemy fall through floor to "disappear"
	GetMesh()->SetSimulatePhysics(true);

	// After falling through the floor, we have to destroy the actor
	FTimerHandle DespawnHandle;
	GetWorldTimerManager().SetTimer(DespawnHandle, this, &ABaseEnemyCharacter::OnFallThroughFloorFinished, 0.5f, false);
}

void ABaseEnemyCharacter::OnFallThroughFloorFinished()
{
	CustomDestroy();
}

void ABaseEnemyCharacter::OnSeePawn(APawn* Pawn)
{
	TimeLastNoticed = Cast<UNNSGameInstance>(GetGameInstance())->TimeOfDayManager->GetMinutesSinceGameStart();
	LocationLastNoticed = Pawn->GetActorLocation();
	bLastNoticedSight = true;
	if (TimeCanCastNextSpellAt < 0) {
		TimeCanCastNextSpellAt = TimeLastNoticed + FMath::RandRange(0, 5);
	}
}

void ABaseEnemyCharacter::OnHearPawn(APawn* Pawn, const FVector & Location, float Volume)
{
	TimeLastNoticed = Cast<UNNSGameInstance>(GetGameInstance())->TimeOfDayManager->GetMinutesSinceGameStart();
	LocationLastNoticed = Location;
	bLastNoticedSight = false;
}
