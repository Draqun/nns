// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "TavernBuilding.h"

#include "NNSGameInstance.h"
#include "NNSHUD.h"

ATavernBuilding::ATavernBuilding(const FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{
	RestCost = 100;
}

void ATavernBuilding::Interact(ANNSCharacter* Character)
{
	Super::Interact(Character);
	Cast<UNNSGameInstance>(GetGameInstance())->HUD->EnterBuilding(this);
}
