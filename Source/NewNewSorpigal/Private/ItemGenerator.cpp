// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "ItemGenerator.h"

#include "DataStructs.h"
#include "EquippableItem.h"
#include "Globals.h"
#include "GoldItem.h"
#include "Item.h"
#include "NNSGameInstance.h"
#include "PotionItem.h"
#include "Spellbook.h"

const int32 ItemGenerator::kEnchantChance = 30;

ItemGenerator::ItemGenerator()
{
	// Init database
	for (int32 i = 0; i < (int32)ItemTotalItemTypes; ++i) {
		Database.Add((ItemType)i);
		for (int32 Tiers = 1; Tiers <= 6; ++Tiers) {
			Database[(ItemType)i].Add(Tiers);
		}
	}

	// Add gold
	for (int32 Tiers = 1; Tiers <= 5; ++Tiers) {
		// Key == amount of gold
		Database[ItemGold][Tiers].Add(FString::FromInt(Tiers * Tiers * 100));
	}

	// Add spellbooks
	for (int32 i = 0; i < (int32)ESpells::SpellTotalSpells; ++i) {
		const FSpellInfo& SpellInfo = EnumMaps::SpellInfo[(ESpells)i];
		if (SpellInfo.RequiredMastery == EMastery::MasteryBasic) {
			// Key == Spell enumeration
			Database[ItemSpellbook][1].Add(FString::FromInt(i));
			Database[ItemSpellbook][2].Add(FString::FromInt(i));
		} else if (SpellInfo.RequiredMastery == EMastery::MasteryExpert) {
			Database[ItemSpellbook][2].Add(FString::FromInt(i));
			Database[ItemSpellbook][3].Add(FString::FromInt(i));
			Database[ItemSpellbook][4].Add(FString::FromInt(i));
		} else if (SpellInfo.RequiredMastery == EMastery::MasteryMaster) {
			Database[ItemSpellbook][4].Add(FString::FromInt(i));
			Database[ItemSpellbook][5].Add(FString::FromInt(i));
		}
	}

	// Add equipment
	for (const auto& Row : UNNSGameInstance::EquippableItemDataTable->GetRowNames()) {
		const FEquippableItemData* Data = UNNSGameInstance::EquippableItemDataTable->FindRow<FEquippableItemData>(Row, FString(TEXT("ItemGenerator")));
		// Key == data table row name
		if (Data->Slot == EEquipType::EquipMainHand || Data->Slot == EEquipType::EquipRanged) {
			Database[ItemWeapon][Data->Tier].Add(Row.ToString());
		} else {
			Database[ItemArmor][Data->Tier].Add(Row.ToString());
		}
	}

	// Add potions
	Database[ItemPotion][1].Add(UPotionItem::kRedReagentString.ToString());
	Database[ItemPotion][1].Add(UPotionItem::kYellowReagentString.ToString());
	Database[ItemPotion][1].Add(UPotionItem::kBlueReagentString.ToString());
	Database[ItemPotion][1].Add(UPotionItem::kEmptyString.ToString());
	for (const auto& Row : UNNSGameInstance::PotionItemDataTable->GetRowNames()) {
		const FPotionData* Data = UNNSGameInstance::PotionItemDataTable->FindRow<FPotionData>(Row, FString(TEXT("ItemGenerator")));
		// Key == data table row name
		Database[ItemPotion][Data->Tier].Add(Row.ToString());
	}
}

ItemGenerator::~ItemGenerator()
{
}

UItem* ItemGenerator::GenerateEquippableItem(const FName& Name)
{
	UEquippableItem* Item = UEquippableItem::Create(Name);
	if (FMath::RandRange(1, 100) <= 30) {
		Item->Enchant();
	}
	return Item;
}

UItem* ItemGenerator::GenerateItem(int32 ItemLevel)
{
	UItem* Item = nullptr;

	// Generate an item by selecting a type (all types have equal chance), and then selecting a random
	// item for the requested tier (all items have equal chance). If a type has no items at that tier, try
	// again until we find something that exists.
	for (int32 Tries = 0; Tries < 20 && Item == nullptr; ++Tries) {
		const ItemType Type = (ItemType)FMath::RandRange(0, (int32)ItemTotalItemTypes - 1);
		if (Database[Type][ItemLevel].Num() == 0) continue;

		const int32 ItemIndex = FMath::RandRange(0, Database[Type][ItemLevel].Num() - 1);
		const FString& Data = Database[Type][ItemLevel][ItemIndex];

		switch (Type) {
		case ItemGold:
			Item = UGoldItem::Create((uint32)FCString::Atoi(*Data));
			break;
		case ItemWeapon:
		case ItemArmor:
			Item = GenerateEquippableItem(FName(*Data));
			break;
		case ItemSpellbook:
			Item = USpellbook::Create((ESpells)FCString::Atoi(*Data));
			break;
		case ItemPotion:
			Item = UPotionItem::Create(FName(*Data));
			break;
		}
	}

	return Item;
}

UItem* ItemGenerator::GenerateWeapon(int32 ItemLevel)
{
	if (Database[ItemWeapon][ItemLevel].Num() == 0) return nullptr;

	const int32 ItemIndex = FMath::RandRange(0, Database[ItemWeapon][ItemLevel].Num() - 1);
	const FString& Data = Database[ItemWeapon][ItemLevel][ItemIndex];
	return GenerateEquippableItem(FName(*Data));
}

UItem* ItemGenerator::GenerateArmorChest(int32 ItemLevel)
{
	if (Database[ItemArmor][ItemLevel].Num() == 0) return nullptr;

	UItem* Item = nullptr;

	for (int32 i = 0; i < 20 && !Item; ++i) {
		const int32 ItemIndex = FMath::RandRange(0, Database[ItemArmor][ItemLevel].Num() - 1);
		const FName Name = FName(*Database[ItemArmor][ItemLevel][ItemIndex]);
		const FEquippableItemData* Data = UNNSGameInstance::EquippableItemDataTable->FindRow<FEquippableItemData>(Name, FString(TEXT("EquippableItemData")));
		if (Data->Slot == EEquipType::EquipBody) {
			Item = GenerateEquippableItem(Name);
		}
	}
	return Item;
}

UItem* ItemGenerator::GenerateArmorOther(int32 ItemLevel)
{
	if (Database[ItemArmor][ItemLevel].Num() == 0) return nullptr;

	UItem* Item = nullptr;

	for (int32 i = 0; i < 20 && !Item; ++i) {
		const int32 ItemIndex = FMath::RandRange(0, Database[ItemArmor][ItemLevel].Num() - 1);
		const FName Name = FName(*Database[ItemArmor][ItemLevel][ItemIndex]);
		const FEquippableItemData* Data = UNNSGameInstance::EquippableItemDataTable->FindRow<FEquippableItemData>(Name, FString(TEXT("EquippableItemData")));
		if (Data->Slot != EEquipType::EquipBody) {
			Item = GenerateEquippableItem(Name);
		}
	}
	return Item;
}

UItem* ItemGenerator::GenerateSpellbook(int32 ItemLevel)
{
	if (Database[ItemSpellbook][ItemLevel].Num() == 0) return nullptr;

	const int32 ItemIndex = FMath::RandRange(0, Database[ItemSpellbook][ItemLevel].Num() - 1);
	const FString& Data = Database[ItemSpellbook][ItemLevel][ItemIndex];
	return USpellbook::Create((ESpells)FCString::Atoi(*Data));
}

UItem* ItemGenerator::GeneratePotion(int32 ItemLevel)
{
	if (Database[ItemPotion][ItemLevel].Num() == 0) return nullptr;

	const int32 ItemIndex = FMath::RandRange(0, Database[ItemPotion][ItemLevel].Num() - 1);
	const FName Name = FName(*Database[ItemPotion][ItemLevel][ItemIndex]);
	return UPotionItem::Create(Name);
}
