// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "Item.h"

#include "NNSGameInstance.h"

UItem::UItem()
	: Super()
{
	bIsIdentified = false;
	Tier = 0;
}

void UItem::Save(FArchive& Archive)
{
	FName ClassName = FName(*GetClass()->GetPathName());
	Archive << ClassName;

	SaveLoadCommon(Archive);
}

void UItem::SaveLoadCommon(FArchive& Archive)
{
	Archive << Name;
	Archive << bIsIdentified;
}

UItem* UItem::Load(FArchive& Archive)
{
	FName ClassName;
	Archive << ClassName;

	UItem* Item = NewObject<UItem>(GetTransientPackage(), UNNSGameInstance::GetClassFromMap(ClassName));
	Item->SaveLoadCommon(Archive);
	Item->Init();
	return Item;
}

TArray<FString> UItem::ToString() const
{
	TArray<FString> Lines;
	if (!bIsIdentified) {
		Lines.Add(TEXT("Could not identify item"));
	}
	return Lines;
}

void UItem::Identify(int SkillValue, EMastery Mastery)
{
	if (!bIsIdentified) {
		bIsIdentified = Mastery == EMastery::MasteryMaster || SkillValue * ((int32)Mastery + 1) >= Tier * Tier;
	}
}

bool UItem::MatchesShop(EShopTypes ShopType)
{
	return false;
}

uint32 UItem::GetBuyCost() const
{
	return 1000;
}

uint32 UItem::GetSellCost() const
{
	return GetBuyCost() / 3;
}

uint32 UItem::GetIDCost() const
{
	return GetBuyCost() / 10;
}

FName UItem::GetName() const
{
	return bIsIdentified ? Name : FName(TEXT("Unknown"));
}
