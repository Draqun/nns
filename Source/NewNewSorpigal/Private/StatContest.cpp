// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "StatContest.h"

#include "CharacterSheet.h"
#include "NNSCharacter.h"
#include "NNSGameInstance.h"
#include "NNSHUD.h"

AStatContest::AStatContest(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FObjectFinder<USoundWave> WinFinder(TEXT("SoundWave'/Game/Sounds/320657__rhodesmas__level-up-03.320657__rhodesmas__level-up-03'"));
	WinSound = WinFinder.Object;

	SkillPoints = 10;
	RequiredStat = 50;
	StatTested = EStats::StatStrength;
	AlreadyWon.AddZeroed(UNNSGameInstance::kCharactersInParty);
}

void AStatContest::SaveLoadCommon(FArchive & Archive)
{
	Super::SaveLoadCommon(Archive);

	Archive << AlreadyWon;

	if (Archive.IsLoading() && AllWon()) {
		Tags.Empty();
	}
}

void AStatContest::Interact(ANNSCharacter* Character)
{
	const FString CharName = *Character->PartyMembers[Character->ActiveCharacter]->Name;
	if (AlreadyWon[Character->ActiveCharacter]) {
		Cast<UNNSGameInstance>(GetGameInstance())->HUD->Log(FString::Printf(TEXT("%s has already won!"), *CharName));
	} else if (Character->PartyMembers[Character->ActiveCharacter]->GetStat(StatTested) >= RequiredStat) {
		Cast<UNNSGameInstance>(GetGameInstance())->HUD->Log(FString::Printf(TEXT("%s won! Gained %i skill points."), *CharName, SkillPoints));
		AlreadyWon[Character->ActiveCharacter] = true;
		Character->PartyMembers[Character->ActiveCharacter]->SkillPoints += SkillPoints;

		if (WinSound) {
			Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect2D(WinSound);
		}
		
		if (AllWon()) {
			// Make non-interactable after everyone won
			Tags.Empty();
		}
	} else {
		Cast<UNNSGameInstance>(GetGameInstance())->HUD->Log(FString::Printf(TEXT("%s lost. Your %s must be at least %i."), *CharName, *ANNSHUD::StatNames[(int32)StatTested], RequiredStat));
	}
}

bool AStatContest::AllWon() const
{
	for (int32 i = 0; i < AlreadyWon.Num(); ++i) {
		if (!AlreadyWon[i]) return false;
	}
	return true;
}
