// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "ZoneChangerInteractable.h"

#include "NNSGameInstance.h"

AZoneChangerInteractable::AZoneChangerInteractable(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	LevelName = TEXT("");
}

void AZoneChangerInteractable::Interact(ANNSCharacter* Character)
{
	Super::Interact(Character);

	Cast<UNNSGameInstance>(GetGameInstance())->TeleportToObject(LevelName, OtherSideObjectName);
}
