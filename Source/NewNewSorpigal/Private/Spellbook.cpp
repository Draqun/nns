// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "Spellbook.h"

#include "CharacterSheet.h"
#include "Globals.h"
#include "NNSCharacter.h"
#include "NNSGameInstance.h"
#include "Pickup.h"

const FStringAssetReference USpellbook::BasicIcon = TEXT("Texture2D'/Game/UI/spellbook_basic.spellbook_basic'");
const FStringAssetReference USpellbook::ExpertIcon = TEXT("Texture2D'/Game/UI/spellbook_expert.spellbook_expert'");
const FStringAssetReference USpellbook::MasterIcon = TEXT("Texture2D'/Game/UI/spellbook_master.spellbook_master'");

USpellbook::USpellbook()
	: Super()
{
	static ConstructorHelpers::FClassFinder<APickup> ClassFinder(TEXT("Blueprint'/Game/Blueprints/Items/BookPickup.BookPickup_C'"));
	PickupClass = ClassFinder.Class;

	static ConstructorHelpers::FObjectFinder<USoundWave> SpellLearnFinder(TEXT("SoundWave'/Game/Sounds/320657__rhodesmas__level-up-03.320657__rhodesmas__level-up-03'"));
	SpellLearnSound = SpellLearnFinder.Object;

	IconWidth = 2;
	IconHeight = 2;
}

void USpellbook::SaveLoadCommon(FArchive& Archive)
{
	Super::SaveLoadCommon(Archive);
	Archive << Spell;
}

USpellbook* USpellbook::Create(ESpells Spell)
{
	USpellbook* Item = NewObject<USpellbook>();
	Item->Spell = Spell;
	Item->Init();

	return Item;
}

void USpellbook::Init()
{
	const FSpellInfo& SpellInfo = EnumMaps::SpellInfo[Spell];
	Mastery = SpellInfo.RequiredMastery;
	Skill = SpellInfo.Skill;

	if (Mastery == EMastery::MasteryMaster) {
		Icon = MasterIcon;
		Tier = 5;
	} else if (Mastery == EMastery::MasteryExpert) {
		Icon = ExpertIcon;
		Tier = 3;
	} else {
		Icon = BasicIcon;
		Tier = 1;
	}

	Name = FName(*FString::Printf(TEXT("Spellbook of %s"), *SpellInfo.Name));
}

bool USpellbook::MatchesShop(EShopTypes ShopType)
{
	return ShopType == EShopTypes::ShopSpellbook;
}

TArray<FString> USpellbook::ToString() const
{
	// HUD is hardcoded to display spell tooltip instead
	return Super::ToString();
}

bool USpellbook::Consume(UCharacterSheet* Character)
{
	if (Character->LearnSpell(Spell)) {
		if (SpellLearnSound) {
			Cast<UNNSGameInstance>(Character->OwnerCharacter->GetGameInstance())->PlaySoundEffect2D(SpellLearnSound);
		}
		return true;
	}
	return false;
}

uint32 USpellbook::GetBuyCost() const
{
	return 150 * FMath::Pow(Tier, 2);
}
