// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "PotionItem.h"

#include "CharacterSheet.h"
#include "DataStructs.h"
#include "Globals.h"
#include "Inventory.h"
#include "NNSGameInstance.h"
#include "Pickup.h"
#include "PlayerPartyCharacter.h"

const FStringAssetReference UPotionItem::kRedReagentIcon = TEXT("Texture2D'/Game/UI/red_reagent.red_reagent'");
const FStringAssetReference UPotionItem::kYellowReagentIcon = TEXT("Texture2D'/Game/UI/yellow_reagent.yellow_reagent'");
const FStringAssetReference UPotionItem::kBlueReagentIcon = TEXT("Texture2D'/Game/UI/blue_reagent.blue_reagent'");
const FStringAssetReference UPotionItem::kEmptyIcon = TEXT("Texture2D'/Game/UI/empty_potion.empty_potion'");
const FName UPotionItem::kRedReagentString = TEXT("Red Reagent");
const FName UPotionItem::kYellowReagentString = TEXT("Yellow Reagent");
const FName UPotionItem::kBlueReagentString = TEXT("Blue Reagent");
const FName UPotionItem::kEmptyString = TEXT("Empty Bottle");

UPotionItem::UPotionItem()
	: Super()
{
	static ConstructorHelpers::FClassFinder<APickup> ClassFinder(TEXT("Blueprint'/Game/Blueprints/Items/PotionPickup.PotionPickup_C'"));
	PickupClass = ClassFinder.Class;

	static ConstructorHelpers::FClassFinder<APickup> ClassFinderRed(TEXT("Blueprint'/Game/Blueprints/Items/RedReagentPickup.RedReagentPickup_C'"));
	RedReagentPickupClass = ClassFinderRed.Class;

	static ConstructorHelpers::FClassFinder<APickup> ClassFinderYellow(TEXT("Blueprint'/Game/Blueprints/Items/YellowReagentPickup.YellowReagentPickup_C'"));
	YellowReagentPickupClass = ClassFinderYellow.Class;

	static ConstructorHelpers::FClassFinder<APickup> ClassFinderBlue(TEXT("Blueprint'/Game/Blueprints/Items/BlueReagentPickup.BlueReagentPickup_C'"));
	BlueReagentPickupClass = ClassFinderBlue.Class;

	static ConstructorHelpers::FObjectFinder<UParticleSystem> ExplosionFinder(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));
	Explosion = ExplosionFinder.Object;

	static ConstructorHelpers::FObjectFinder<USoundWave> MixFinder(TEXT("SoundWave'/Game/Sounds/bubble.bubble'"));
	MixSound = MixFinder.Object;

	static ConstructorHelpers::FObjectFinder<USoundWave> DrinkFinder(TEXT("SoundWave'/Game/Sounds/bottle.bottle'"));
	DrinkSound = DrinkFinder.Object;

	static ConstructorHelpers::FObjectFinder<USoundWave> ExplodeFinder(TEXT("SoundWave'/Game/Sounds/244345__willlewis__musket-explosion.244345__willlewis__musket-explosion'"));
	ExplodeSound = ExplodeFinder.Object;
}

UPotionItem* UPotionItem::Create(const FName& Potion)
{
	UPotionItem* Item = NewObject<UPotionItem>();
	Item->Name = Potion;
	Item->Init();

	return Item;
}

void UPotionItem::Init()
{
	IconWidth = 1;
	IconHeight = 1;
	Tier = 0;

	// Name must be set before this is called
	if (Name.IsEqual(kRedReagentString)) {
		Icon = kRedReagentIcon;
		PickupClass = RedReagentPickupClass;
		Type = EPotionTypes::PotionReagentRed;
		bIsIdentified = true;
		Tooltip = TEXT("A small, red plant - used for mixing into a potion.");
	} else if (Name.IsEqual(kYellowReagentString)) {
		Icon = kYellowReagentIcon;
		PickupClass = YellowReagentPickupClass;
		Type = EPotionTypes::PotionReagentYellow;
		bIsIdentified = true;
		Tooltip = TEXT("A small, yellow plant - used for mixing into a potion.");
	} else if (Name.IsEqual(kBlueReagentString)) {
		Icon = kBlueReagentIcon;
		PickupClass = BlueReagentPickupClass;
		Type = EPotionTypes::PotionReagentBlue;
		bIsIdentified = true;
		Tooltip = TEXT("A small, blue plant - used for mixing into a potion.");
	} else {
		IconWidth = 1;
		IconHeight = 2;
		if (Name.IsEqual(kEmptyString)) {
			Icon = kEmptyIcon;
			Type = EPotionTypes::PotionEmptyBottle;
			bIsIdentified = true;
			Tooltip = TEXT("An empty potion bottle.");
		} else {
			const FPotionData* Data = UNNSGameInstance::PotionItemDataTable->FindRow<FPotionData>(Name, FString(TEXT("PotionItemData")));
			Icon = Data->Texture;
			Tier = Data->Tier;
			Type = Data->Enum;
			Tooltip = Data->Tooltip;
		}
	}
}

bool UPotionItem::Mix(APlayerPartyCharacter* Player, UPotionItem* Item)
{
	if (IsEmpty() || (IsReagent() && Item->IsReagent())) return true;

	for (const auto& Row : UNNSGameInstance::PotionItemDataTable->GetRowNames()) {
		const FPotionData* Data = UNNSGameInstance::PotionItemDataTable->FindRow<FPotionData>(Row, FString(TEXT("PotionMix")));
		if (Data->Item1 == Type && Data->Item2 == Item->Type || Data->Item2 == Type && Data->Item1 == Item->Type) {
			int32 Level = 0;
			EMastery Mastery = EMastery::MasteryBasic;
			Player->GetPartySkill(ESkills::SkillAlchemy, Level, Mastery);
			if (Mastery == EMastery::MasteryMaster || (Mastery == EMastery::MasteryExpert && Data->Tier <= 4) || (Mastery == EMastery::MasteryBasic && Data->Tier <= 2)) {
				Item->Name = Row;
				Item->Init();

				if (IsReagent()) {
					Player->HeldItem = nullptr;
				} else {
					Name = kEmptyString;
					Init();
				}
				if (MixSound) {
					Cast<UNNSGameInstance>(Player->GetGameInstance())->PlaySoundEffect2D(MixSound);
				}
				return true;
			}
		}
	}

	Player->HeldItem = nullptr;
	Player->PartyMembers[Player->ActiveCharacter]->Inventory->RemoveItem(Item);

	// Have to unpause game before playing particles or it doesn't work
	const float TierDamage = (Tier + Item->Tier) * 10.0f;
	UDamageData* Damage = UDamageData::Create(EDamageTypes::DamageFire, FMath::RandRange(TierDamage - TierDamage / 2, TierDamage + TierDamage / 2), -1, EDamageDistance::DamageInstant, TEXT("potions"), nullptr, true);
	Player->DealDamage(Damage, true);
	Player->TriggerParticlesAtCamera(Explosion);
	if (ExplodeSound) {
		Cast<UNNSGameInstance>(Player->GetGameInstance())->PlaySoundEffect2D(ExplodeSound);
	}
	return false;
}

bool UPotionItem::MatchesShop(EShopTypes ShopType)
{
	return ShopType == EShopTypes::ShopAlchemy;
}

TArray<FString> UPotionItem::ToString() const
{
	if (bIsIdentified) {
		TArray<FString> Lines;
		Tooltip.ParseIntoArrayLines(Lines);
		return Lines;
	} else {
		return Super::ToString();
	}
}

bool UPotionItem::Consume(UCharacterSheet* Character)
{
	if (IsEmpty() || Character->IsDead()) {
		return false;
	} else {
		if (DrinkSound) {
			Cast<UNNSGameInstance>(Character->OwnerCharacter->GetGameInstance())->PlaySoundEffect2D(DrinkSound);
		}

		switch (Type) {
		case EPotionTypes::PotionReagentRed:
			Character->Heal(5);
			break;
		case EPotionTypes::PotionReagentYellow:
			Character->AddStatusEffect(EStatusEffects::StatusStatsUp, 60, 1);
			break;
		case EPotionTypes::PotionReagentBlue:
			Character->UpdateCurrentMana(5);
			break;
		case EPotionTypes::PotionRed:
			Character->Heal(20);
			break;
		case EPotionTypes::PotionYellow:
			Character->AddStatusEffect(EStatusEffects::StatusStatsUp, 120, 2);
			break;
		case EPotionTypes::PotionBlue:
			Character->UpdateCurrentMana(20);
			break;
		case EPotionTypes::PotionDarkRed:
			Character->Heal(50);
			break;
		case EPotionTypes::PotionDarkYellow:
			Character->AddStatusEffect(EStatusEffects::StatusStatsUp, 300, 5);
			break;
		case EPotionTypes::PotionDarkBlue:
			Character->UpdateCurrentMana(50);
			break;
		case EPotionTypes::PotionOrange:
			Character->AddStatusEffect(EStatusEffects::StatusResistFire, 300, 10);
			break;
		case EPotionTypes::PotionGreen:
			Character->AddStatusEffect(EStatusEffects::StatusResistAir, 300, 10);
			break;
		case EPotionTypes::PotionPurple:
			Character->RemoveStatusEffect(EStatusEffects::StatusPoison);
			break;
		case EPotionTypes::PotionWhite1:
			Character->AddStatusEffect(EStatusEffects::StatusRegen, 120, 5);
			break;
		case EPotionTypes::PotionWhite2:
			Character->AddStatusEffect(EStatusEffects::StatusFireSpikes, 120, 5);
			break;
		case EPotionTypes::PotionWhite3:
			Character->AddStatusEffect(EStatusEffects::StatusHeroism, 120, 5);
			break;
		case EPotionTypes::PotionWhite4:
			Character->FullHeal();
			Character->ChangeBaseStat(-1, (EStats)FMath::RandRange(0, (int32)EStats::StatLuck));
			break;
		case EPotionTypes::PotionWhite5:
			if (Character->IsUnconscious()) {
				Character->CurrentHealth = 1;
				Character->RegainConsciousness();
			}
			break;
		case EPotionTypes::PotionWhite6:
			Character->OwnerCharacter->AddStatusEffect(EPartyStatusEffects::StatusTorchLight, 120, 2);
			break;
		case EPotionTypes::PotionBlack1:
			Character->ChangeBaseStat(1, EStats::StatStrength);
			break;
		case EPotionTypes::PotionBlack2:
			Character->ChangeBaseStat(1, EStats::StatIntelligence);
			break;
		case EPotionTypes::PotionBlack3:
			Character->ChangeBaseStat(1, EStats::StatDexterity);
			break;
		case EPotionTypes::PotionBlack4:
			Character->ChangeBaseStat(1, EStats::StatSpeed);
			break;
		case EPotionTypes::PotionBlack5:
			Character->ChangeBaseStat(1, EStats::StatVitality);
			break;
		case EPotionTypes::PotionBlack6:
			Character->ChangeBaseStat(1, EStats::StatLuck);
			break;
		}
		return true;
	}
}

uint32 UPotionItem::GetBuyCost() const
{
	return 5 * (FMath::Pow(Tier, 4) + 1);
}
