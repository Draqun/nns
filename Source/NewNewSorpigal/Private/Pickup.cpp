// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "Pickup.h"

#include "Globals.h"
#include "Item.h"
#include "PlayerPartyCharacter.h"

// Sets default values
APickup::APickup(const FObjectInitializer& ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	BoxComponent = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("BoxComp"));
	BoxComponent->BodyInstance.SetCollisionProfileName("Pickup");
	RootComponent = BoxComponent;

	MeshComponent = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("MeshComp"));
	MeshComponent->BodyInstance.SetCollisionProfileName("NoCollision");
	MeshComponent->SetupAttachment(RootComponent);

	Gold = 0;
	Item = nullptr;
	bCreateNewItem = false;

	Tags.Add(ActorTags::kIdentifiableTag);
	Tags.Add(ActorTags::kInteractableTag);
	Tags.Add(ActorTags::kPickupableTag);
}

void APickup::SaveLoadCommon(FArchive& Archive)
{
	Archive << Gold;
}

void APickup::Save(FArchive& Archive)
{
	FTransform Transform = GetTransform();
	Archive << Transform;

	SaveLoadCommon(Archive);

	bool HasItem = Item != nullptr;
	Archive << HasItem;
	if (HasItem) {
		Item->Save(Archive);
	}
}

void APickup::Load(FArchive& Archive)
{
	SaveLoadCommon(Archive);

	bool HasItem = false;
	Archive << HasItem;
	if (HasItem) {
		Item = UItem::Load(Archive);
	}
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();

	if (GEngine) {
		BoxComponent->SetMassOverrideInKg(NAME_None, 3.0f);
	}

	// Drop pickup with physics but only let it move up/down (Z axis)
	BoxComponent->SetSimulatePhysics(true);

	if (bCreateNewItem) {
		bCreateNewItem = false;
		Item = CreateItem();
	}

	// After some time, disable physics so it doesn't hurt performance
	FTimerHandle PhysicsHandle;
	GetWorldTimerManager().SetTimer(PhysicsHandle, this, &APickup::OnDisablePhysics, 5.0f, false);
}

// Called every frame
void APickup::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void APickup::AddToInventory(APlayerPartyCharacter* Player)
{
	Player->AddGold(Gold);
	Gold = 0;

	if (Item != nullptr) {
		if (Player->AddItemToInventory(Item)) {
			Item = nullptr;
		}
	}

	if (Item == nullptr) {
		Destroy();
	}
}

void APickup::OnDisablePhysics()
{
	BoxComponent->SetSimulatePhysics(false);
}
