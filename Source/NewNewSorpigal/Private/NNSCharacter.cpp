// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "NNSCharacter.h"

#include "CharacterSheet.h"
#include "DataStructs.h"
#include "NNSGameInstance.h"
#include "NNSHUD.h"
#include "Projectile.h"

const float ANNSCharacter::PassTime = 2.0f;

// Sets default values
ANNSCharacter::ANNSCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ProjectileTargetComponent = ObjectInitializer.CreateDefaultSubobject<UArrowComponent>(this, TEXT("ProjectileTarget"));
	ProjectileTargetComponent->SetupAttachment(GetCapsuleComponent());

	ProjectileSpawnComponent = ObjectInitializer.CreateDefaultSubobject<UArrowComponent>(this, TEXT("ProjectileSpawn"));
	ProjectileSpawnComponent->SetupAttachment(GetCapsuleComponent());

	ActiveCharacter = 0;
	MaxMeleeDistance = 400.0f;
	RegularJumpHeight = 420.0f;
	RegularAcceleration = 2048.0f;
	FallPeak = 0.0f;
	GetCharacterMovement()->MaxAcceleration = RegularAcceleration;
	GetCharacterMovement()->AirControl = 0.2f;
	GetCharacterMovement()->MaxFlySpeed = 1100.0f;
	GetCharacterMovement()->BrakingDecelerationFlying = 10000.0f;
	bIsMyTurn = false;

	// Load projectile classes (arrows and spells)
	static ConstructorHelpers::FClassFinder<AProjectile> ArrowFinder(TEXT("Blueprint'/Game/Blueprints/Projectiles/BP_ArrowProjectile.BP_ArrowProjectile_C'"));
	ArrowClass = ArrowFinder.Class;
	static ConstructorHelpers::FClassFinder<AProjectile> FireBallFinder(TEXT("Blueprint'/Game/Blueprints/Projectiles/BP_MagicFireBall.BP_MagicFireBall_C'"));
	FireBallClass = FireBallFinder.Class;
	static ConstructorHelpers::FClassFinder<AProjectile> FlameBoltFinder(TEXT("Blueprint'/Game/Blueprints/Projectiles/BP_MagicFlameBolt.BP_MagicFlameBolt_C'"));
	FlameBoltClass = FlameBoltFinder.Class;
	static ConstructorHelpers::FClassFinder<AProjectile> ShockFinder(TEXT("Blueprint'/Game/Blueprints/Projectiles/BP_MagicShock.BP_MagicShock_C'"));
	ShockClass = ShockFinder.Class;
	static ConstructorHelpers::FClassFinder<AProjectile> CauseLightFinder(TEXT("Blueprint'/Game/Blueprints/Projectiles/BP_MagicCauseLight.BP_MagicCauseLight_C'"));
	CauseLightClass = CauseLightFinder.Class;
	static ConstructorHelpers::FClassFinder<AProjectile> CauseHeavyFinder(TEXT("Blueprint'/Game/Blueprints/Projectiles/BP_MagicCauseHeavy.BP_MagicCauseHeavy_C'"));
	CauseHeavyClass = CauseHeavyFinder.Class;

	// Register spell function pointers with the appropriate enum
	SpellFunctions.Emplace(ESpells::SpellTorchLight, &ANNSCharacter::CastTorchLight);
	SpellFunctions.Emplace(ESpells::SpellFireShield, &ANNSCharacter::CastFireShield);
	SpellFunctions.Emplace(ESpells::SpellFlameBolt, &ANNSCharacter::CastFlameBolt);
	SpellFunctions.Emplace(ESpells::SpellFireSpikes, &ANNSCharacter::CastFireSpikes);
	SpellFunctions.Emplace(ESpells::SpellFireBall, &ANNSCharacter::CastFireBall);
	SpellFunctions.Emplace(ESpells::SpellWizardsEye, &ANNSCharacter::CastWizardsEye);
	SpellFunctions.Emplace(ESpells::SpellAirShield, &ANNSCharacter::CastAirShield);
	SpellFunctions.Emplace(ESpells::SpellShock, &ANNSCharacter::CastShock);
	SpellFunctions.Emplace(ESpells::SpellFeatherFall, &ANNSCharacter::CastFeatherFall);
	SpellFunctions.Emplace(ESpells::SpellWaterWalk, &ANNSCharacter::CastWaterWalk);
	SpellFunctions.Emplace(ESpells::SpellJump, &ANNSCharacter::CastJump);
	SpellFunctions.Emplace(ESpells::SpellFly, &ANNSCharacter::CastFly);
	SpellFunctions.Emplace(ESpells::SpellTeleportTown, &ANNSCharacter::CastTeleportTown);
	SpellFunctions.Emplace(ESpells::SpellCureLight, &ANNSCharacter::CastCureLight);
	SpellFunctions.Emplace(ESpells::SpellHeroism, &ANNSCharacter::CastHeroism);
	SpellFunctions.Emplace(ESpells::SpellCauseLight, &ANNSCharacter::CastCauseLight);
	SpellFunctions.Emplace(ESpells::SpellFlee, &ANNSCharacter::CastFlee);
	SpellFunctions.Emplace(ESpells::SpellRegen, &ANNSCharacter::CastRegen);
	SpellFunctions.Emplace(ESpells::SpellCureHeavy, &ANNSCharacter::CastCureHeavy);
	SpellFunctions.Emplace(ESpells::SpellCauseHeavy, &ANNSCharacter::CastCauseHeavy);
}

void ANNSCharacter::SaveLoadCommon(FArchive& Archive)
{
	Archive << ActiveCharacter;
	Archive << Tags;
}

void ANNSCharacter::Save(FArchive& Archive)
{
	// Must be loaded in subclass
	FTransform Transform = GetTransform();
	Archive << Transform;

	FVector Velocity = GetCharacterMovement()->Velocity;
	Archive << Velocity;

	TEnumAsByte<EMovementMode> MovementMode = GetCharacterMovement()->MovementMode;
	Archive << MovementMode;

	SaveLoadCommon(Archive);

	Archive << StatusEffects;
}

void ANNSCharacter::Load(FArchive& Archive, bool bIgnoreTransforms)
{
	FVector Velocity;
	Archive << Velocity;
	if (!bIgnoreTransforms) {
		GetCharacterMovement()->Velocity = Velocity;
	}

	TEnumAsByte<EMovementMode> MovementMode;
	Archive << MovementMode;
	if (MovementMode != EMovementMode::MOVE_Flying || !Cast<UNNSGameInstance>(GetGameInstance())->bIsIndoors) {
		GetCharacterMovement()->SetMovementMode(MovementMode);
	}

	SaveLoadCommon(Archive);
	
	TMap<EPartyStatusEffects, FStatusEffect> SavedEffects;
	Archive << SavedEffects;
	for (auto& Effect : SavedEffects) {
		AddStatusEffect(Effect.Key, Effect.Value.Duration, Effect.Value.Strength);
	}

	bIsMyTurn = false;
}

// Called when the game starts or when spawned
void ANNSCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ANNSCharacter::CustomDestroy()
{
	for (auto& Character : PartyMembers) {
		GetWorldTimerManager().ClearAllTimersForObject(Character);
	}
	Destroy();
}

// Called every frame
void ANNSCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (GetCharacterMovement()->MovementMode == EMovementMode::MOVE_Falling && FallPeak < GetActorLocation().Z && !HasStatus(EPartyStatusEffects::StatusFeatherFall)) {
		FallPeak = GetActorLocation().Z;
	}
}

void ANNSCharacter::Pass()
{
	if (PartyMembers[ActiveCharacter]->bCanAct) {
		if (Cast<UNNSGameInstance>(GetGameInstance())->bIsInTurnMode) {
			// Take a quick action in turn based
			PartyMembers[ActiveCharacter]->StartTurnTimer(PassTime);
		} else {
			// Advance to next character in list in realtime
			const int TempCharacter = ActiveCharacter;
			PartyMembers[TempCharacter]->bCanAct = false;
			AdvanceActiveCharacter();
			PartyMembers[TempCharacter]->bCanAct = true;
		}
	}
}

void ANNSCharacter::AdvanceActiveCharacter()
{
	// If our current member can still act, don't do anything
	if (PartyMembers[ActiveCharacter]->bCanAct) return;

	const int32 NumPartyMembers = PartyMembers.Num();
	for (int32 i = 1; i < NumPartyMembers; ++i) {
		ActiveCharacter = (ActiveCharacter + 1) % NumPartyMembers;
		if (PartyMembers[ActiveCharacter]->bCanAct) {
			break;
		}
	}
}

void ANNSCharacter::DealDamage(UDamageData* Damage, bool DamageAll)
{
	if (bIsDead) return;

	if (DamageAll) {
		for (auto& Member : PartyMembers) {
			Member->TakeDamage(Damage);
		}
	} else {
		// Hit a random character in the party that's not dead
		int32 CharacterHit = 0;
		int32 Counter = 0;
		do {
			CharacterHit = FMath::RandRange(0, PartyMembers.Num() - 1);
		} while (PartyMembers[CharacterHit]->IsUnconscious() && Counter++ < 50);
		PartyMembers[CharacterHit]->TakeDamage(Damage);
	}

	CheckIsCharacterDead();
}

void ANNSCharacter::DealDamage(UDamageData* Damage, UCharacterSheet* Character)
{
	Character->TakeDamage(Damage);
	CheckIsCharacterDead();
}

void ANNSCharacter::PlaySound(TArray<USoundWave*>& Sounds, int32 Index)
{
	if (Sounds.Num() > Index) {
		Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect3D(Sounds[Index], GetActorLocation());
	}
}

void ANNSCharacter::CheckIsCharacterDead()
{
	for (auto& Member : PartyMembers) {
		if (!Member->IsUnconscious()) {
			bIsDead = false;
			return;
		}
	}
	bIsDead = true;
	OnDeath();
}

bool ANNSCharacter::CanMemberAct()
{
	for (auto& Member : PartyMembers) {
		if (Member->bCanAct) {
			return true;
		}
	}
	return false;
}

void ANNSCharacter::ConvertTimerToTurnCount()
{
	for (auto& Member : PartyMembers) {
		Member->ConvertTimerToTurnCount();
	}
}

void ANNSCharacter::ConvertTurnCountToTimer()
{
	for (auto& Member : PartyMembers) {
		Member->ConvertTurnCountToTimer();
	}
	bIsMyTurn = false;
}

void ANNSCharacter::StartTurn()
{
	bIsMyTurn = true;
	for (auto& Member : PartyMembers) {
		// Instead of simply grabbing the first PartyMember,
		// enable all PartyMembers who can currently act
		if (!Member->IsUnconscious() && Member->TurnCount <= 0.0f) {
			Member->bCanAct = true;
		}
	}
	AdvanceActiveCharacter();
}

void ANNSCharacter::EndTurn()
{
	if (bIsMyTurn) {
		for (auto& Member : PartyMembers) {
			if (Member->bCanAct) {
				return;
			}
		}
		bIsMyTurn = false;
		Cast<UNNSGameInstance>(GetGameInstance())->EndTurn();
	}
}

void ANNSCharacter::AddStatusEffect(EPartyStatusEffects Status, int32 Duration, int32 Strength)
{
	if (!StatusEffects.Contains(Status) || (!StatusEffects[Status].IsPermanent() && StatusEffects[Status].Strength < Strength)) {
		StatusEffects.Emplace(Status, FStatusEffect(Duration, Strength));
		if (Status != EPartyStatusEffects::StatusInCombat) {
			Cast<UNNSGameInstance>(GetGameInstance())->HUD->Log(FString::Printf(TEXT("You gain the effect of %s."), *ANNSHUD::PartyStatusEffectNames[(int32)Status]));
		}
	}
	if (Status == EPartyStatusEffects::StatusFly && !Cast<UNNSGameInstance>(GetGameInstance())->bIsIndoors) {
		GetCharacterMovement()->GravityScale = 0.0f;
		GetCharacterMovement()->MaxAcceleration = 20000.0f;
		GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);
	}
}

void ANNSCharacter::RemoveStatusEffect(EPartyStatusEffects Status)
{
	if (StatusEffects.Contains(Status)) {
		StatusEffects.Remove(Status);
		if (Status != EPartyStatusEffects::StatusInCombat) {
			Cast<UNNSGameInstance>(GetGameInstance())->HUD->Log(FString::Printf(TEXT("You lose the effect of %s."), *ANNSHUD::PartyStatusEffectNames[(int32)Status]));
		}
		if (Status == EPartyStatusEffects::StatusFly) {
			GetCharacterMovement()->GravityScale = 1.0f;
			GetCharacterMovement()->MaxAcceleration = RegularAcceleration;
			GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Falling);
		}
	}
}

void ANNSCharacter::TickStatusEffects(int32 Duration)
{
	for (auto& Effect : StatusEffects) {
		Effect.Value.Duration -= Duration;
		if (Effect.Value.Duration <= 0) {
			RemoveStatusEffect(Effect.Key);
		}
	}
}

void ANNSCharacter::OnMovementModeChanged(EMovementMode PrevMovementMode, uint8 PrevCustomMode)
{
	Super::OnMovementModeChanged(PrevMovementMode, PrevCustomMode);

	if (GetCharacterMovement()->MovementMode == EMovementMode::MOVE_Falling) {
		FallPeak = GetActorLocation().Z;
	} else if (PrevMovementMode == EMovementMode::MOVE_Falling && GetCharacterMovement()->IsMovingOnGround()) {
		const float FallDistance = FallPeak - GetActorLocation().Z;
		if (!HasStatus(EPartyStatusEffects::StatusFeatherFall) && FallDistance > 600.0f) {
			DealDamage(UDamageData::Create(EDamageTypes::DamagePure, (FallDistance - 600.0f) / 20.0f, -1, EDamageDistance::DamageInstant, TEXT("falling"), nullptr, true), true);
		}
	}
}

int32 ANNSCharacter::GetPartyCombinedStat(EStats Stat) const
{
	int32 Value = 0;
	for (auto& Member : PartyMembers) {
		if (Member->IsUnconscious()) continue;

		Value += Member->GetStat(Stat);
	}
	return Value;
}

void ANNSCharacter::GetPartySkill(ESkills Skill, int32& Level, EMastery& Mastery) const
{
	Level = 0;
	Mastery = EMastery::MasteryBasic;
	for (auto& Member : PartyMembers) {
		if (Member->IsUnconscious()) continue;

		const int32 Value = Member->GetSkill(Skill);
		const EMastery MemberMastery = Member->Mastery(Skill);
		// A lv10 master is prioritized over a lv12 expert. Should warn about this in manual
		if (MemberMastery > Mastery || Value > Level) {
			Level = Value;
			Mastery = MemberMastery;
		}
	}
}

AProjectile* ANNSCharacter::SpawnProjectile(TSubclassOf<class AProjectile>& ProjectileClass)
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	SpawnParams.Instigator = this;
	// Spawn arrow at ProjectSpawnComponent's location and launch it at the nearest enemy
	return GetWorld()->SpawnActor<AProjectile>(ProjectileClass, ProjectileSpawnComponent->GetComponentLocation(), FRotator(0.0f), SpawnParams);
}

FVector ANNSCharacter::GetProjectileSpawnDirection() const
{
	return GetActorForwardVector();
}

void ANNSCharacter::LaunchProjectile(AProjectile* Projectile, ANNSCharacter* Target)
{
	FVector Direction = GetProjectileSpawnDirection();
	if (Target != nullptr) {
		// No idea why but if this if statement only sets and normalizes Direction, SpawnActor above
		// fails for whatever reason, resulting in a crash. Adding an extra line in fixes it?
		FVector TargetLocation = Cast<ANNSCharacter>(Target)->ProjectileTargetComponent->GetComponentLocation();
		Direction = TargetLocation - ProjectileSpawnComponent->GetComponentLocation();
		Direction.Normalize();
	}
	Projectile->InitVelocity(Direction);
}

bool ANNSCharacter::CastSpell(ESpells Spell, UCharacterSheet* Target)
{
	FSpellInfo& SpellInfo = EnumMaps::SpellInfo[Spell];
	// Only cast spell if mana is available
	// TODO: fizzle spell if not enough mana? still take turn?
	if (PartyMembers[ActiveCharacter]->CurrentMana >= SpellInfo.ManaCost) {
		if ((this->*(SpellFunctions[Spell]))(Target)) {
			Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect3D(Cast<UNNSGameInstance>(GetGameInstance())->GetSound(SpellInfo.Sound), ProjectileSpawnComponent->GetComponentLocation());
			PartyMembers[ActiveCharacter]->UpdateCurrentMana(-SpellInfo.ManaCost);
			PartyMembers[ActiveCharacter]->StartTurnTimer(PartyMembers[ActiveCharacter]->GetDelay(PartyMembers[ActiveCharacter]->GetSpellDelay(Spell)));
			return true;
		}
	}
	return false;
}

bool ANNSCharacter::CastTorchLight(UCharacterSheet * Target)
{
	const EMastery Mastery = PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillFireMagic);
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillFireMagic);
	const int32 Duration = Skill * 60 * ((int32)Mastery + 1);
	AddStatusEffect(EPartyStatusEffects::StatusTorchLight, Duration, (int32)Mastery);
	return true;
}

bool ANNSCharacter::CastFireShield(UCharacterSheet * Target)
{
	const EMastery Mastery = PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillFireMagic);
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillFireMagic);
	const int32 Duration = Skill * 60;
	for (auto& Member : PartyMembers) {
		Member->AddStatusEffect(EStatusEffects::StatusResistFire, Duration, 3 + (int32)Mastery * 2 + ((int32)Mastery + 1) * Skill);
	}
	return true;
}

bool ANNSCharacter::CastFlameBolt(UCharacterSheet* Target)
{
	AProjectile* Projectile = SpawnProjectile(FlameBoltClass);
	if (Projectile == nullptr) return false;
	
	// 1d4 for basic, 1d10 for expert, 2d10 for master, plus bonus fire magic skill
	const int32 Dice = 1 + PartyMembers[ActiveCharacter]->IsMaster(ESkills::SkillFireMagic) ? 1 : 0;
	const int Sides = PartyMembers[ActiveCharacter]->IsExpert(ESkills::SkillFireMagic) ? 10 : 4;
	const float Damage = UtilFunctions::RollDice(Dice, Sides) + PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillFireMagic);
	Projectile->Damage = UDamageData::Create(EDamageTypes::DamageFire, Damage, -1, EDamageDistance::DamageRanged, PartyMembers[ActiveCharacter]->Name, this);
	Projectile->Damage->DamageAmount += FMath::RandRange(0, PartyMembers[ActiveCharacter]->GetStat(EStats::StatIntelligence) / 5);

	ANNSCharacter* TargetCharacter = nullptr;
	if (Target != nullptr) {
		TargetCharacter = Target->OwnerCharacter.Get();
	}
	LaunchProjectile(Projectile, TargetCharacter);
	return true;
}

bool ANNSCharacter::CastFireSpikes(UCharacterSheet * Target)
{
	if (Target == nullptr) return false;

	const EMastery Mastery = PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillFireMagic);
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillFireMagic);
	const int32 Duration = Skill * (Mastery == EMastery::MasteryExpert ? 5 : 60);
	Target->AddStatusEffect(EStatusEffects::StatusFireSpikes, Duration, Skill + FMath::RandRange(0, PartyMembers[ActiveCharacter]->GetStat(EStats::StatIntelligence) / 10));
	return true;
}

bool ANNSCharacter::CastFireBall(UCharacterSheet* Target)
{
	AProjectile* Projectile = SpawnProjectile(FireBallClass);
	if (Projectile == nullptr) return false;

	// (1+x)d(4+y) where x == mastery and y == skill level
	const float Damage = UtilFunctions::RollDice(1 + (int32)PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillFireMagic), 4 + PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillFireMagic));
	Projectile->Damage = UDamageData::Create(EDamageTypes::DamageFire, Damage, -1, EDamageDistance::DamageRanged, PartyMembers[ActiveCharacter]->Name, this);
	Projectile->Damage->DamageAmount += FMath::RandRange(0, PartyMembers[ActiveCharacter]->GetStat(EStats::StatIntelligence) / 5);
	Projectile->DamageRadius = 500.0f;

	ANNSCharacter* TargetCharacter = nullptr;
	if (Target != nullptr) {
		TargetCharacter = Target->OwnerCharacter.Get();
	}
	LaunchProjectile(Projectile, TargetCharacter);
	return true;
}

bool ANNSCharacter::CastWizardsEye(UCharacterSheet* Target)
{
	const EMastery Mastery = PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillAirMagic);
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillAirMagic);
	const int32 Duration = Skill * (Mastery == EMastery::MasteryBasic ? 60 : 300);
	AddStatusEffect(EPartyStatusEffects::StatusWizardsEye, Duration, (int32)Mastery);
	return true;
}

bool ANNSCharacter::CastAirShield(UCharacterSheet * Target)
{
	const EMastery Mastery = PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillAirMagic);
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillAirMagic);
	const int32 Duration = Skill * 60;
	for (auto& Member : PartyMembers) {
		Member->AddStatusEffect(EStatusEffects::StatusResistAir, Duration, 3 + (int32)Mastery * 2 + ((int32)Mastery + 1) * Skill);
	}
	return true;
}

bool ANNSCharacter::CastShock(UCharacterSheet* Target)
{
	AProjectile* Projectile = SpawnProjectile(ShockClass);
	if (Projectile == nullptr) return false;

	// 5*(1+x)d1, where x is air magic mastery
	const float Damage = UtilFunctions::RollDice(5 * (1 + (int32)PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillAirMagic)), 1);
	Projectile->Damage = UDamageData::Create(EDamageTypes::DamageAir, Damage, -1, EDamageDistance::DamageRanged, PartyMembers[ActiveCharacter]->Name, this);
	Projectile->Damage->DamageAmount += FMath::RandRange(0, PartyMembers[ActiveCharacter]->GetStat(EStats::StatIntelligence) / 5);

	ANNSCharacter* TargetCharacter = nullptr;
	if (Target != nullptr) {
		TargetCharacter = Target->OwnerCharacter.Get();
	}
	LaunchProjectile(Projectile, TargetCharacter);
	return true;
}

bool ANNSCharacter::CastFeatherFall(UCharacterSheet * Target)
{
	const EMastery Mastery = PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillAirMagic);
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillAirMagic);
	const int32 Duration = Skill * (Mastery == EMastery::MasteryExpert ? 5 : 60);
	AddStatusEffect(EPartyStatusEffects::StatusFeatherFall, Duration);
	return true;
}

bool ANNSCharacter::CastWaterWalk(UCharacterSheet* Target)
{
	const EMastery Mastery = PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillAirMagic);
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillAirMagic);
	const int32 Duration = Skill * (Mastery == EMastery::MasteryExpert ? 5 : 60);
	AddStatusEffect(EPartyStatusEffects::StatusWaterWalk, Duration);
	return true;
}

bool ANNSCharacter::CastJump(UCharacterSheet * Target)
{
	if (Cast<UNNSGameInstance>(GetGameInstance())->bIsIndoors) return false;

	GetCharacterMovement()->JumpZVelocity = 2000.0f;
	GetCharacterMovement()->JumpZVelocity += PartyMembers[ActiveCharacter]->GetStat(EStats::StatIntelligence) * 3.0f;
	Jump();
	return true;
}

bool ANNSCharacter::CastFly(UCharacterSheet * Target)
{
	if (Cast<UNNSGameInstance>(GetGameInstance())->bIsIndoors) return false;

	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillAirMagic);
	const int32 Duration = Skill * 30;
	AddStatusEffect(EPartyStatusEffects::StatusFly, Duration);
	return true;
}

bool ANNSCharacter::CastTeleportTown(UCharacterSheet * Target)
{
	Cast<UNNSGameInstance>(GetGameInstance())->Teleport(FName(TEXT("Level_Landscape")), FVector(-20700.0f, 15766.0f, 471.0f), FRotator(0.0f, -90.0f, 0.0f));
	return true;
}

bool ANNSCharacter::CastCureLight(UCharacterSheet* Target)
{
	if (Target == nullptr) return false;

	// (1+x)d(4+y) where x == mastery and y == skill level
	float Recovered = UtilFunctions::RollDice(1 + (int32)PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillBodyMagic), 4 + PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillBodyMagic)) + 3.0f;
	Recovered += FMath::RandRange(0, PartyMembers[ActiveCharacter]->GetStat(EStats::StatIntelligence) / 5);
	Target->Heal(Recovered);

	return true;
}

bool ANNSCharacter::CastHeroism(UCharacterSheet * Target)
{
	const EMastery Mastery = PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillBodyMagic);
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillBodyMagic);
	const int32 Duration = Skill * (Mastery == EMastery::MasteryBasic ? 10 : 60);
	if (Target == nullptr) {
		for (auto& Member : PartyMembers) {
			Member->AddStatusEffect(EStatusEffects::StatusHeroism, Duration, 2 * Skill);
		}
	} else {
		Target->AddStatusEffect(EStatusEffects::StatusHeroism, Duration, 2 * Skill);
	}
	return true;
}

bool ANNSCharacter::CastCauseLight(UCharacterSheet * Target)
{
	AProjectile* Projectile = SpawnProjectile(CauseLightClass);
	if (Projectile == nullptr) return false;

	// 1d4 +x
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillBodyMagic);
	const float Damage = UtilFunctions::RollDice(1, 4) + Skill;
	Projectile->Damage = UDamageData::Create(EDamageTypes::DamagePhysical, Damage, -1, EDamageDistance::DamageRanged, PartyMembers[ActiveCharacter]->Name, this);
	Projectile->Damage->DamageAmount += FMath::RandRange(0, PartyMembers[ActiveCharacter]->GetStat(EStats::StatIntelligence) / 5);

	ANNSCharacter* TargetCharacter = nullptr;
	if (Target != nullptr) {
		TargetCharacter = Target->OwnerCharacter.Get();
	}
	LaunchProjectile(Projectile, TargetCharacter);
	return true;
}

bool ANNSCharacter::CastFlee(UCharacterSheet * Target)
{
	const EMastery Mastery = PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillBodyMagic);
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillBodyMagic);
	const int32 Duration = 2 + Skill * (Mastery == EMastery::MasteryMaster ? 1 : (Mastery == EMastery::MasteryExpert ? 2 : 3));
	AddStatusEffect(EPartyStatusEffects::StatusFlee, Duration);
	return true;
}

bool ANNSCharacter::CastRegen(UCharacterSheet * Target)
{
	if (Target == nullptr) return false;

	const EMastery Mastery = PartyMembers[ActiveCharacter]->Mastery(ESkills::SkillBodyMagic);
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillBodyMagic);
	const int32 Duration = Skill * (Mastery == EMastery::MasteryMaster ? 60 : 10);
	Target->AddStatusEffect(EStatusEffects::StatusRegen, Duration, 2);
	return true;
}

bool ANNSCharacter::CastCureHeavy(UCharacterSheet * Target)
{
	if (Target == nullptr) return false;

	// 5d(5+x)
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillBodyMagic);
	const float Recovered = UtilFunctions::RollDice(5, 5 + Skill) + FMath::RandRange(0, PartyMembers[ActiveCharacter]->GetStat(EStats::StatIntelligence) / 5);
	Target->Heal(Recovered);

	return true;
}

bool ANNSCharacter::CastCauseHeavy(UCharacterSheet * Target)
{
	AProjectile* Projectile = SpawnProjectile(CauseHeavyClass);
	if (Projectile == nullptr) return false;

	// 5d5 +x
	const int32 Skill = PartyMembers[ActiveCharacter]->GetSkill(ESkills::SkillBodyMagic);
	const float Damage = UtilFunctions::RollDice(5, 5) + Skill;
	Projectile->Damage = UDamageData::Create(EDamageTypes::DamagePhysical, Damage, -1, EDamageDistance::DamageRanged, PartyMembers[ActiveCharacter]->Name, this);
	Projectile->Damage->DamageAmount += FMath::RandRange(0, PartyMembers[ActiveCharacter]->GetStat(EStats::StatIntelligence) / 5);

	ANNSCharacter* TargetCharacter = nullptr;
	if (Target != nullptr) {
		TargetCharacter = Target->OwnerCharacter.Get();
	}
	LaunchProjectile(Projectile, TargetCharacter);
	return true;
}
