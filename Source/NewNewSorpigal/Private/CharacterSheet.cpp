// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "CharacterSheet.h"

#include "DataStructs.h"
#include "EquippableItem.h"
#include "Globals.h"
#include "Inventory.h"
#include "Item.h"
#include "NNSCharacter.h"
#include "NNSGameInstance.h"
#include "NNSHUD.h"
#include "PlayerPartyCharacter.h"

UCharacterSheet::UCharacterSheet(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bCanAct = true;

	DeathBarrier = 0.0f;
	BaseDelay = 1.0f;
	Name = TEXT("");
	Inventory = ObjectInitializer.CreateDefaultSubobject<UInventory>(this, TEXT("Inventory"));

	Equipment.AddZeroed((int32)EEquipType::EquipTotalSlots);
	// This might be unnecessary, but just to keep things modern
	for (int32 i = 0; i < (int32)EEquipType::EquipTotalSlots; ++i) {
		Equipment[i] = nullptr;
	}

	BaseStats.AddZeroed((int32)EStats::StatTotalStats);
	// + 1 for SkillTotalSkills to always be 0
	BaseSkills.AddZeroed((int32)ESkills::SkillTotalSkills + 1);
	ExpertSkills.AddZeroed((int32)ESkills::SkillTotalSkills + 1);
	MasterSkills.AddZeroed((int32)ESkills::SkillTotalSkills + 1);
	KnownSpells.AddZeroed((int32)ESpells::SpellTotalSpells);
	Level = 1;
	Experience = 0;
	// TODO: Base ExperienceToLevel off a curve somewhere
	ExperienceToLevel = 100;
	SkillPoints = 0;
	StatPoints = 0;

	Portrait = nullptr;

	LastSpellbookPage = ESkills::SkillTotalSkills;
	LastSpellCast = ESpells::SpellTotalSpells;

	bCanPlayDamageSound = true;
}

void UCharacterSheet::Save(FArchive & Archive, bool bBasic)
{
	SaveLoadCommon(Archive, bBasic);

	// Current attack timer
	float TempTurnCount = TurnCount;
	if (!Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance())->bIsInTurnMode) {
		TempTurnCount = OwnerCharacter->GetWorldTimerManager().GetTimerRemaining(TurnTimerHandle);
	}
	Archive << TempTurnCount;

	if (!bBasic) {
		// Inventory and equipment
		Inventory->Save(Archive);
		
		for (int32 i = 0; i < (int32)EEquipType::EquipTotalSlots; ++i) {
			bool Equipped = Equipment[i] != nullptr;
			Archive << Equipped;
			if (Equipped) {
				Equipment[i]->Save(Archive);
			}
		}

		FString PortraitName = Portrait->GetPathName();
		Archive << PortraitName;
	}
}

void UCharacterSheet::Load(FArchive & Archive, bool bBasic)
{
	bCanAct = false;

	// Attack timer
	SaveLoadCommon(Archive, bBasic);
	Archive << TurnCount;

	if (!bBasic) {
		// Inventory and equipment
		Inventory->Load(Archive);

		for (int32 i = 0; i < (int32)EEquipType::EquipTotalSlots; ++i) {
			bool Equipped = false;
			Archive << Equipped;
			UEquippableItem* Item = nullptr;
			if (Equipped) {
				Item = Cast<UEquippableItem>(UItem::Load(Archive));
			}
			Equipment[i] = Item;
		}

		FString PortraitName = TEXT("");
		Archive << PortraitName;

		TArray<UTexture2D*> Portraits = Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance())->Portraits;
		Portrait = nullptr;
		for (UTexture2D* Texture : Portraits) {
			if (Texture->GetPathName().Equals(PortraitName)) {
				Portrait = Texture;
				break;
			}
		}
		if (Portrait == nullptr) {
			Portrait = Portraits[FMath::RandRange(0, Portraits.Num() - 1)];
		}
	}
}

void UCharacterSheet::SaveLoadCommon(FArchive & Archive, bool bBasic)
{
	Archive << CurrentHealth;
	Archive << CurrentMana;
	Archive << StatusEffects;
	Archive << Voice;

	if (!bBasic) {
		Archive << Experience;
		Archive << ExperienceToLevel;
		Archive << Level;
		Archive << SkillPoints;
		Archive << StatPoints;
		Archive << BaseDelay;
		Archive << DeathBarrier;
		Archive << Name;
		Archive << BaseStats;
		Archive << BaseSkills;
		Archive << ExpertSkills;
		Archive << MasterSkills;
		Archive << KnownSpells;
		Archive << Class;
	}
}

void UCharacterSheet::InitEnemy(const FEnemyData& EnemyData)
{
	Level = EnemyData.Level;
	Experience = EnemyData.Experience;
	BaseStats[(int32)EStats::StatHealth] = EnemyData.MaxHealth;
	BaseStats[(int32)EStats::StatMana] = EnemyData.MaxMana;
	BaseStats[(int32)EStats::StatStrength] = EnemyData.Strength;
	BaseStats[(int32)EStats::StatIntelligence] = EnemyData.Intelligence;
	BaseStats[(int32)EStats::StatDexterity] = EnemyData.Dexterity;
	BaseStats[(int32)EStats::StatSpeed] = EnemyData.Speed;
	BaseStats[(int32)EStats::StatVitality] = EnemyData.Vitality;
	BaseStats[(int32)EStats::StatLuck] = EnemyData.Luck;
	BaseStats[(int32)EStats::StatResistFire] = EnemyData.ResistFire;
	BaseStats[(int32)EStats::StatResistAir] = EnemyData.ResistAir;
	BaseStats[(int32)EStats::StatAccuracy] = EnemyData.Accuracy;
	BaseStats[(int32)EStats::StatEvasion] = EnemyData.Evasion;
	BaseSkills[(int32)ESkills::SkillFireMagic] = EnemyData.FireMagicSkill;
	BaseSkills[(int32)ESkills::SkillAirMagic] = EnemyData.AirMagicSkill;
	BaseSkills[(int32)ESkills::SkillBodyMagic] = EnemyData.BodyMagicSkill;
	if (EnemyData.FireMagicMastery >= EMastery::MasteryExpert) {
		ExpertSkills[(int32)ESkills::SkillFireMagic] = true;
	}
	if (EnemyData.FireMagicMastery == EMastery::MasteryMaster) {
		MasterSkills[(int32)ESkills::SkillFireMagic] = true;
	}
	if (EnemyData.AirMagicMastery >= EMastery::MasteryExpert) {
		ExpertSkills[(int32)ESkills::SkillAirMagic] = true;
	}
	if (EnemyData.AirMagicMastery == EMastery::MasteryMaster) {
		MasterSkills[(int32)ESkills::SkillAirMagic] = true;
	}
	if (EnemyData.BodyMagicMastery >= EMastery::MasteryExpert) {
		ExpertSkills[(int32)ESkills::SkillBodyMagic] = true;
	}
	if (EnemyData.BodyMagicMastery == EMastery::MasteryMaster) {
		MasterSkills[(int32)ESkills::SkillBodyMagic] = true;
	}
	for (const ESpells Spell : EnemyData.SpellList) {
		KnownSpells[(int32)Spell] = true;
	}

	BaseDelay = EnemyData.BaseDelay;
	EquipItem(UEquippableItem::Create(EnemyData), EEquipType::EquipMainHand);

	Init();
}

void UCharacterSheet::Init()
{
	CurrentHealth = GetStat(EStats::StatHealth);
	CurrentMana = GetStat(EStats::StatMana);
}

void UCharacterSheet::StartTurnTimer(float Delay, bool bEndTurn)
{
	bCanAct = false;
	OwnerCharacter->AdvanceActiveCharacter();
	if (Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance())->bIsInTurnMode) {
		TurnCount += Delay;
	} else {
		OwnerCharacter->GetWorldTimerManager().SetTimer(TurnTimerHandle, this, &UCharacterSheet::OnTurnTimerFinished, Delay, false);
	}
	if (bEndTurn) {
		OwnerCharacter->EndTurn();
	}
}

void UCharacterSheet::OnTurnTimerFinished()
{
	if (!IsUnconscious()) {
		bCanAct = true;
		OwnerCharacter->AdvanceActiveCharacter();
	}
}

void UCharacterSheet::TakeDamage(const UDamageData* Damage)
{
	UNNSGameInstance* GameInstance = Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance());

	const bool bWasDead = IsDead();

	// Luck check
	if (Damage->DamageDistance == EDamageDistance::DamageRanged) {
		if (FMath::RandRange(0.0f, 1.0f) < GetStat(EStats::StatLuck) * .001f) {
			GameInstance->HUD->Log(FString::Printf(TEXT("%s gets lucky and evades a projectile"), *Name));
			return;
		}
	}

	// Evasion check
	if (Damage->DamageDistance == EDamageDistance::DamageMelee) {
		// Accuracy == Evasion -> 20% chance, +2 evasion = +1% chance
		const int32 DodgeChance = FMath::Clamp(FMath::RoundToInt((Damage->Accuracy - GetStat(EStats::StatEvasion)) / 2.0f), -70, 20);
		if (FMath::RandRange(1, 100) > (80 + DodgeChance)) {
			GameInstance->HUD->Log(FString::Printf(TEXT("%s dodges an attack"), *Name));
			return;
		}
	}

	float Amount = Damage->DamageAmount - FMath::FloorToInt(FMath::Max(0, GetStat(EStats::StatVitality) - 10) / 5.0f);
	if (Damage->DamageType == EDamageTypes::DamagePhysical) {
		Amount -= FMath::Pow(FMath::Log2(FMath::Max(1.0f, GetArmor())), 3.0f) * 0.1f;

		UEquippableItem* Shield = Equipment[(int32)EEquipType::EquipOffHand];
		if (Shield != nullptr && Shield->Style == EEquipStyle::EquipShield) {
			const ESkills Skill = EnumMaps::StyleToSkill[Shield->Style];
			if (IsMaster(Skill) && FMath::RandRange(1, 100) <= 10) {
				GameInstance->HUD->Log(FString::Printf(TEXT("%s nullified the attack with a shield"), *Name));
				return;
			} else if (IsExpert(Skill) && FMath::RandBool()) {
				Amount *= 0.9f;
			}
		}
	} else if (Damage->DamageType != EDamageTypes::DamagePure) {
		Amount -= FMath::Pow(FMath::Log2(FMath::Max(1.0f, (float)GetStat(EnumMaps::DamageResistances[Damage->DamageType]))), 3.0f) * 0.1f;
	}
	Amount = FMath::RoundToInt(FMath::Max(1.0f, Amount));
	CurrentHealth -= Amount;
	GameInstance->HUD->Log(this, Amount, Damage->DamageSource);

	if (!OwnerCharacter->IsA<APlayerPartyCharacter>() && Damage->SourceActor != nullptr && Damage->SourceActor->IsA<APlayerPartyCharacter>()) {
		UGameplayStatics::SpawnEmitterAtLocation(OwnerCharacter->GetWorld(), GameInstance->BloodParticles, OwnerCharacter->ProjectileSpawnComponent->GetComponentTransform());
	}

	if (!bWasDead) {
		if (Damage->DamageAmount > 0.0f && Damage->Status.Key != EStatusEffects::StatusTotalStatusEffects) {
			AddStatusEffect(Damage->Status.Key, Damage->Status.Value.Duration, Damage->Status.Value.Strength);
		}

		if (!Damage->bIsRetaliation && StatusEffects.Contains(EStatusEffects::StatusFireSpikes)) {
			// Deal damage to all characters in radius
			TArray<FOverlapResult> AllActorsInRadius;
			FCollisionObjectQueryParams Params(FCollisionObjectQueryParams::InitType::AllDynamicObjects);
			if (OwnerCharacter->GetWorld()->OverlapMultiByObjectType(AllActorsInRadius, OwnerCharacter->GetActorLocation(), FQuat::Identity, Params, FCollisionShape::MakeSphere(400.0f))) {
				for (auto& OverlapResult : AllActorsInRadius) {
					// Only count collision component, not mesh
					if (!OverlapResult.GetComponent()->IsA<UShapeComponent>()) continue;

					AActor* Actor = OverlapResult.GetActor();
					if (Actor != OwnerCharacter && Actor->IsA<ANNSCharacter>()) {
						UDamageData* SpikeDamage = UDamageData::Create(EDamageTypes::DamageFire, UtilFunctions::RollDice(1, StatusEffects[EStatusEffects::StatusFireSpikes].Strength), -1, EDamageDistance::DamageInstant, TEXT("fire spikes"), OwnerCharacter.Get(), true);
						Cast<ANNSCharacter>(Actor)->DealDamage(SpikeDamage);
						FTransform ParticleTransform = Actor->GetRootComponent()->GetComponentTransform();
						ParticleTransform.SetLocation(ParticleTransform.GetLocation() + Actor->GetActorForwardVector() * Cast<ANNSCharacter>(Actor)->GetCapsuleComponent()->GetScaledCapsuleRadius());
						UGameplayStatics::SpawnEmitterAtLocation(OwnerCharacter->GetWorld(), GameInstance->FireSpikeParticles, ParticleTransform);
					}
				}
			}
		}

		if (IsUnconscious()) {
			if (IsDead()) {
				TArray<EStatusEffects> Effects;
				StatusEffects.GetKeys(Effects);
				for (const EStatusEffects Effect : Effects) {
					StatusEffects.Remove(Effect);
				}
				OwnerCharacter->PlaySound(OwnerCharacter->DeathSounds, OwnerCharacter->CharacterIndex(this));
			} else {
				PlayDamageSound();
			}

			TurnCount = 0.0f;
			bCanAct = false;
			OwnerCharacter->AdvanceActiveCharacter();
			OwnerCharacter->EndTurn();
			Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance())->NotifyDeath(this);
		} else {
			PlayDamageSound();
		}
	}
}

void UCharacterSheet::PlayDamageSound()
{
	if (bCanPlayDamageSound) {
		bCanPlayDamageSound = false;

		OwnerCharacter->PlaySound(OwnerCharacter->DamageSounds, OwnerCharacter->CharacterIndex(this));

		FTimerDelegate TimerCallback;
		TimerCallback.BindLambda([this]
		{
			bCanPlayDamageSound = true;
		});

		FTimerHandle Handle;
		OwnerCharacter->GetWorldTimerManager().SetTimer(Handle, TimerCallback, 1.0f, false);
	}
}

bool UCharacterSheet::Heal(float Amount, bool bLog)
{
	// Can heal if unconscious, but not if dead
	if (IsDead()) return false;

	const bool WasUnconscious = IsUnconscious();

	CurrentHealth = FMath::Min(CurrentHealth + Amount, (float)GetStat(EStats::StatHealth));
	if (bLog) {
		Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance())->HUD->Log(FString::Printf(TEXT("%s was healed for %i health."), *Name, FMath::RoundToInt(Amount)));
	}

	if (IsUnconscious() != WasUnconscious) {
		RegainConsciousness();
	}

	return true;
}

void UCharacterSheet::FullHeal()
{
	const bool WasUnconscious = IsUnconscious();
	CurrentHealth = (float)GetStat(EStats::StatHealth);
	CurrentMana = (float)GetStat(EStats::StatMana);
	Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance())->HUD->Log(FString::Printf(TEXT("%s was fully healed."), *Name));

	if (IsUnconscious() != WasUnconscious) {
		RegainConsciousness();
	}
}

void UCharacterSheet::RegainConsciousness()
{
	// If regaining consciousness, we want to give them a delay before acting. Also, we need to insert back into the turn order
	StartTurnTimer(3.0f);
	UNNSGameInstance* GameInstance = Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance());
	if (GameInstance->bIsInTurnMode) {
		if (OwnerCharacter->IsA<APlayerPartyCharacter>()) {
			GameInstance->InsertPlayerIntoTurnOrder(this);
		} else {
			GameInstance->InsertIntoTurnOrder(this);
		}
	}
}

void UCharacterSheet::UpdateCurrentMana(float Delta)
{
	CurrentMana = FMath::Max(0.0f, FMath::Min((float)GetStat(EStats::StatMana), CurrentMana + Delta));
}

void UCharacterSheet::GiveExperience(float XP)
{
	const int32 LearningSkill = GetSkill(ESkills::SkillLearning);
	if (LearningSkill > 0) {
		float Multiplier = 1.05;
		Multiplier += ((int32)Mastery(ESkills::SkillLearning) + 1) * LearningSkill / 100.0f;
		XP *= Multiplier;
	}
	Experience += (int32)XP;
	while (Experience >= ExperienceToLevel) {
		LevelUp();
	}
}

void UCharacterSheet::LevelUp()
{
	Experience -= ExperienceToLevel;
	++Level;
	ExperienceToLevel = Level * 100;
	SkillPoints += FMath::CeilToInt((float)Level / 3.0f) + 3;
	StatPoints += (Level / 5) + 1;
}

bool UCharacterSheet::IsDead() const
{
	return CurrentHealth <= DeathBarrier;
}

bool UCharacterSheet::IsUnconscious() const
{
	return CurrentHealth <= 0.0f;
}

void UCharacterSheet::AddStatusEffect(EStatusEffects Status, int32 Duration, int32 Strength)
{
	if (IsDead() && Status != EStatusEffects::StatusInWater) return;
	if (!StatusEffects.Contains(Status) || (!StatusEffects[Status].IsPermanent() && StatusEffects[Status].Strength < Strength)) {
		StatusEffects.Emplace(Status, FStatusEffect(Duration, Strength));
		if (OwnerCharacter->GetDistanceTo(Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance())->PlayerPartyMembers[0]->OwnerCharacter.Get()) < APlayerPartyCharacter::kMaxTargetDistance) {
			Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance())->HUD->Log(FString::Printf(TEXT("%s gains the effect of %s."), *Name, *ANNSHUD::StatusEffectNames[(int32)Status]));
		}
	}
}

bool UCharacterSheet::RemoveStatusEffect(EStatusEffects Status)
{
	if (StatusEffects.Contains(Status)) {
		StatusEffects.Remove(Status);
		if (OwnerCharacter->GetDistanceTo(Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance())->PlayerPartyMembers[0]->OwnerCharacter.Get()) < APlayerPartyCharacter::kMaxTargetDistance) {
			Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance())->HUD->Log(FString::Printf(TEXT("%s loses the effect of %s."), *Name, *ANNSHUD::StatusEffectNames[(int32)Status]));
		}
		return true;
	}
	return false;
}

void UCharacterSheet::TickStatusEffects(int32 Duration)
{
	for (auto& Effect : StatusEffects) {
		Effect.Value.Duration -= Duration;
		if (Effect.Value.Duration <= 0) {
			RemoveStatusEffect(Effect.Key);
		}

		if (Effect.Key == EStatusEffects::StatusPoison) {
			OwnerCharacter->DealDamage(UDamageData::Create(EDamageTypes::DamagePure, Effect.Value.Strength, -1, EDamageDistance::DamageInstant, TEXT("poison"), nullptr, true), this);
			if (IsDead()) return;
		} else if (Effect.Key == EStatusEffects::StatusInWater) {
			if (!OwnerCharacter->HasStatus(EPartyStatusEffects::StatusWaterWalk)) {
				const int32 Skill = GetSkill(ESkills::SkillSwimming);

				int32 EquipWeight = 0;
				for (auto& Item : Equipment) {
					if (Item != nullptr) {
						if (Item->Style == EEquipStyle::EquipLeather) {
							EquipWeight += 1;
						} else if (Item->Style == EEquipStyle::EquipChain) {
							EquipWeight += 2;
						} else if (Item->Style == EEquipStyle::EquipPlate) {
							EquipWeight += 4;
						}
					}
				}

				int32 InventoryWeight = 0;
				for (auto& Item : Inventory->PositionMap) {
					InventoryWeight += Item.Key->IconWidth * Item.Key->IconHeight;
				}

				if (Skill > 0) {
					EquipWeight -= 5 * (IsMaster(ESkills::SkillSwimming) ? 4 : (IsExpert(ESkills::SkillSwimming) ? 2 : 1));
					EquipWeight = FMath::Max(0, EquipWeight);
					InventoryWeight -= UInventory::kMaxWidth * UInventory::kMaxHeight * Skill * 0.08f;
					InventoryWeight = FMath::Max(0, InventoryWeight);
				}

				const float Damage = (Skill > 0 ? 0 : 5) + EquipWeight + InventoryWeight / UInventory::kMaxHeight;

				OwnerCharacter->DealDamage(UDamageData::Create(EDamageTypes::DamagePure, Damage, -1, EDamageDistance::DamageInstant, TEXT("drowning"), nullptr, true), this);
				if (IsDead()) return;
			}
		} else if (Effect.Key == EStatusEffects::StatusRegen) {
			Heal((float)Effect.Value.Strength, false);
		}
	}
}

void UCharacterSheet::ConvertTimerToTurnCount()
{
	bCanAct = false;
	TurnCount = 0.0f;

	if (OwnerCharacter->GetWorldTimerManager().IsTimerActive(TurnTimerHandle)) {
		// Set turn delay equal to time remaining. We use a 1:1 conversion from
		// cooldown->turn delay, so 1.5 seconds is 1.5 turns.
		if (!IsUnconscious()) {
			TurnCount = OwnerCharacter->GetWorldTimerManager().GetTimerRemaining(TurnTimerHandle);
		}
		OwnerCharacter->GetWorldTimerManager().ClearTimer(TurnTimerHandle);
	}
}

void UCharacterSheet::ConvertTurnCountToTimer()
{
	if (TurnCount > 0.0f) {
		// Start a timer with the remaining turn count as the time
		bCanAct = false;
		OwnerCharacter->GetWorldTimerManager().SetTimer(TurnTimerHandle, this, &UCharacterSheet::OnTurnTimerFinished, TurnCount, false);
	} else {
		// If they are ready to act, just enable them immediately
		OnTurnTimerFinished();
	}
	TurnCount = 0.0f;
}

uint32 UCharacterSheet::GetHealCost() const
{
	uint32 Cost = GetStat(EStats::StatHealth) - (int32)CurrentHealth;
	if (IsDead()) {
		Cost *= 5;
	} else if (IsUnconscious()) {
		Cost *= 2;
	}
	return Cost;
}

int32 UCharacterSheet::GetStat(EStats Stat) const
{
	int32 Value = GetBaseStat(Stat);

	if (Stat == EStats::StatHealth) {
		Value += GetSkill(ESkills::SkillBodybuilding) * 5 * (IsMaster(ESkills::SkillBodybuilding) ? 4 : IsExpert(ESkills::SkillBodybuilding) ? 2 : 1);
	} else if (Stat == EStats::StatMana) {
		Value += GetSkill(ESkills::SkillMeditation) * 5 * (IsMaster(ESkills::SkillMeditation) ? 4 : IsExpert(ESkills::SkillMeditation) ? 2 : 1);
	} else if (EnumMaps::StatusResistances.Contains(Stat) && StatusEffects.Contains(EnumMaps::StatusResistances[Stat])) {
		Value += StatusEffects[EnumMaps::StatusResistances[Stat]].Strength;
	} else if (Stat == EStats::StatAccuracy) {
		Value += GetStat(EStats::StatDexterity) / 2;
	} else if (Stat == EStats::StatEvasion) {
		Value += GetStat(EStats::StatSpeed) / 2;
		if (OwnerCharacter->HasStatus(EPartyStatusEffects::StatusInCombat)) {
			Value += 10;
		} else if (OwnerCharacter->HasStatus(EPartyStatusEffects::StatusFlee)) {
			Value -= 10;
		}
	}

	for (auto& Item : Equipment) {
		if (Item != nullptr) {
			Value += Item->StatModifiers[(int32)Stat];
		}
	}

	if (HasStatus(EStatusEffects::StatusStatsUp)) {
		Value += StatusEffects[EStatusEffects::StatusStatsUp].Strength;
	}

	return FMath::Max(0, Value);
}

int32 UCharacterSheet::GetSkill(ESkills Skill) const
{
	int32 Value = GetBaseSkill(Skill);

	// Modifiers don't work for unlearned skills
	if (Value == 0) return Value;

	for (auto& Item : Equipment) {
		if (Item != nullptr) {
			Value += Item->SkillModifiers[(int32)Skill];
		}
	}

	return Value;
}

void UCharacterSheet::ChangeBaseStat(int32 Delta, EStats Stat)
{
	BaseStats[(int32)Stat] = FMath::Max(GetBaseStat(Stat) + Delta, 0);
	ANNSHUD* HUD = Cast<UNNSGameInstance>(OwnerCharacter->GetGameInstance())->HUD.Get();
	HUD->Log(FString::Printf(TEXT("%s's %s %screases by %i (now %i)."), *Name, *HUD->StatNames[(int32)Stat], (Delta > 0 ? TEXT("in") : TEXT("de")), (Delta > 0 ? Delta : -Delta), BaseStats[(int32)Stat]));
}

void UCharacterSheet::UpgradeBaseStat(EStats Stat)
{
	if (StatPoints <= 0) return;

	if (Stat == EStats::StatHealth || Stat == EStats::StatMana) {
		ChangeBaseStat(5, Stat);
	} else {
		ChangeBaseStat(1, Stat);
	}
	--StatPoints;
}

void UCharacterSheet::LearnBaseSkill(ESkills Skill, bool UseMoney)
{
	if (CanLearnSkill(Skill, UseMoney)) {
		++BaseSkills[(int32)Skill];
		if (UseMoney) {
			Cast<APlayerPartyCharacter>(OwnerCharacter.Get())->RemoveGold(EnumMaps::SkillCosts[EMastery::MasteryBasic].Gold);
		}
	}
}

void UCharacterSheet::UpgradeBaseSkill(ESkills Skill)
{
	if (CanUpgradeBaseSkill(Skill)) {
		SkillPoints -= GetBaseSkill(Skill);
		++BaseSkills[(int32)Skill];
	}
}

bool UCharacterSheet::CanUpgradeBaseSkill(ESkills Skill) const
{
	return GetBaseSkill(Skill) > 0 && GetBaseSkill(Skill) <= SkillPoints;
}

void UCharacterSheet::BecomeExpert(ESkills Skill)
{
	if (CanBecomeExpert(Skill)) {
		ExpertSkills[(int32)Skill] = true;
		Cast<APlayerPartyCharacter>(OwnerCharacter.Get())->RemoveGold(EnumMaps::SkillCosts[EMastery::MasteryExpert].Gold);
	}
}

void UCharacterSheet::BecomeMaster(ESkills Skill)
{
	if (CanBecomeMaster(Skill)) {
		MasterSkills[(int32)Skill] = true;
		Cast<APlayerPartyCharacter>(OwnerCharacter.Get())->RemoveGold(EnumMaps::SkillCosts[EMastery::MasteryMaster].Gold);
	}
}

bool UCharacterSheet::IsExpert(ESkills Skill) const
{
	return ExpertSkills[(int32)Skill];
}

bool UCharacterSheet::IsMaster(ESkills Skill) const
{
	return MasterSkills[(int32)Skill];
}

bool UCharacterSheet::CanLearnSkill(ESkills Skill, bool UseMoney) const
{
	const FSkillCost& Cost = EnumMaps::SkillCosts[EMastery::MasteryBasic];
	return UNNSGameInstance::ClassSkillMap[Class].Masteries.Contains(Skill) && GetBaseSkill(Skill) == Cost.SkillPoints && (!UseMoney || Cast<APlayerPartyCharacter>(OwnerCharacter.Get())->GetGold() >= Cost.Gold);
}

bool UCharacterSheet::CanBecomeExpert(ESkills Skill) const
{
	const FSkillCost& Cost = EnumMaps::SkillCosts[EMastery::MasteryExpert];
	return (EMastery)UNNSGameInstance::ClassSkillMap[Class].Masteries[Skill] >= EMastery::MasteryExpert && !IsExpert(Skill) && GetBaseSkill(Skill) >= Cost.SkillPoints && Cast<APlayerPartyCharacter>(OwnerCharacter.Get())->GetGold() >= Cost.Gold;
}

bool UCharacterSheet::CanBecomeMaster(ESkills Skill) const
{
	const FSkillCost& Cost = EnumMaps::SkillCosts[EMastery::MasteryMaster];
	return (EMastery)UNNSGameInstance::ClassSkillMap[Class].Masteries[Skill] >= EMastery::MasteryMaster && !IsMaster(Skill) && GetBaseSkill(Skill) >= Cost.SkillPoints && Cast<APlayerPartyCharacter>(OwnerCharacter.Get())->GetGold() >= Cost.Gold;
}

bool UCharacterSheet::HasLearnedSpell(ESpells Spell) const
{
	return KnownSpells[(int32)Spell];
}

bool UCharacterSheet::CanLearnSpell(ESpells Spell) const
{
	const FSpellInfo& SpellInfo = EnumMaps::SpellInfo[Spell];
	return !HasLearnedSpell(Spell) && GetSkill(SpellInfo.Skill) > 0 && Mastery(SpellInfo.Skill) >= SpellInfo.RequiredMastery;
}

bool UCharacterSheet::LearnSpell(ESpells Spell)
{
	if (CanLearnSpell(Spell)) {
		KnownSpells[(int32)Spell] = true;
		return true;
	}
	return false;
}

int32 UCharacterSheet::GetMasteryAllowed(ESpells Spell)
{
	const ESkills Skill = EnumMaps::SpellInfo[Spell].Skill;
	if (UNNSGameInstance::ClassSkillMap[Class].Masteries.Contains(Skill)) {
		return (int32)UNNSGameInstance::ClassSkillMap[Class].Masteries[Skill];
	} else {
		return -1;
	}
}

EMastery UCharacterSheet::Mastery(ESkills Skill) const
{
	if (IsMaster(Skill)) return EMastery::MasteryMaster;
	else if (IsExpert(Skill)) return EMastery::MasteryExpert;
	else return EMastery::MasteryBasic;
}

float UCharacterSheet::GetArmor() const
{
	float Value = 0.0f;

	for (auto& Item : Equipment) {
		if (Item != nullptr) {
			const ESkills Skill = EnumMaps::StyleToSkill[Item->Style];
			const int32 SkillValue = GetSkill(Skill);
			if (Item->Style == EEquipStyle::EquipSword && IsExpert(Skill)) {
				// Expert sword adds skill to armor
				Value += SkillValue;
			} else if (Item->Style == EEquipStyle::EquipDagger) {
				Value += SkillValue;
			} else if (Item->Style == EEquipStyle::EquipShield) {
				Value += SkillValue;
			} else if (Item->Style == EEquipStyle::EquipLeather && IsMaster(Skill)) {
				// Master leather adds skill/4 to each item
				Value += FMath::CeilToFloat(SkillValue / 4.0f);
			} else if (Item->Style == EEquipStyle::EquipChain && IsExpert(Skill)) {
				// Expert chain adds skill/2 to each item
				Value += FMath::CeilToFloat(SkillValue / 2.0f);
			} else if (Item->Style == EEquipStyle::EquipPlate) {
				// Plate adds skill to each item
				Value += SkillValue;
			}
			Value += Item->Armor;
		}
	}

	if (OwnerCharacter->HasStatus(EPartyStatusEffects::StatusInCombat)) {
		Value += 5;
	} else if (OwnerCharacter->HasStatus(EPartyStatusEffects::StatusFlee)) {
		Value -= 5;
	}

	return Value;
}

float UCharacterSheet::GetDamage(EEquipType Slot) const
{
	float Value = 0.0f;
	// Dexterity increases minimum damage at a factor of 1/8
	const int32 DexterityModifier = FMath::Max(0, (GetStat(EStats::StatDexterity) - 10) / 8);
	// Strength increases maximum damage by a factor of 1/4
	const int32 StrengthModifier = FMath::Max(0, (GetStat(EStats::StatStrength) - 10) / 4);

	Value += FMath::RandRange(DexterityModifier, DexterityModifier + StrengthModifier);

	UEquippableItem* Weapon = Equipment[(int32)Slot];
	if (Weapon == nullptr) {
		Value += UtilFunctions::RollDice(2, 4);
	} else {
		const ESkills Skill = EnumMaps::StyleToSkill[Weapon->Style];
		const int32 SkillValue = GetSkill(Skill);

		int32 Dice = Weapon->Dice;
		int32 DiceSides = Weapon->DiceSides;

		float Multiplier = 1.0f;

		if (Weapon->Style == EEquipStyle::EquipAxe && IsExpert(Skill)) {
			// Master axe adds an extra die
			++Dice;
		}

		Value += UtilFunctions::RollDice(Weapon->Dice, Weapon->DiceSides);
		// Then add static +damage bonus
		Value += Weapon->Damage;

		if (Weapon->Style == EEquipStyle::EquipAxe) {
			// Axe skill adds to raw damage
			Value += FMath::FloorToFloat((float)SkillValue / 2.0);
			if (IsMaster(Skill) && FMath::RandRange(1, 100) <= SkillValue) {
				// Master axe can triple damage
				Multiplier = 3.0f;
			}
		} else if (Weapon->Style == EEquipStyle::EquipDagger) {
			// Master dagger triples, expert dagger might double
			if (IsMaster(Skill)) {
				Multiplier = 3.0f;
			} else if (IsExpert(Skill) && FMath::RandRange(1, 100) <= (SkillValue * 10)) {
				Multiplier = 2.0f;
			}
		} else if (Weapon->Style == EEquipStyle::EquipSword && IsExpert(Skill)) {
			// Master sword has 30% chance of doing double damage
			if (FMath::RandRange(1, 100) <= 30) {
				Multiplier = 2.0f;
			}
		} else if (Weapon->Style == EEquipStyle::EquipBow && IsExpert(Skill)) {
			Value += SkillValue;
		}

		if (Multiplier > 1.0f) {
			Multiplier += GetStat(EStats::StatLuck) * .005f;
		}
		Value *= Multiplier;
	}
	// Heroism increases by spell strength
	if (Slot == EEquipType::EquipMainHand && StatusEffects.Contains(EStatusEffects::StatusHeroism)) {
		Value += StatusEffects[EStatusEffects::StatusHeroism].Strength;
	}

	Value = FMath::Max(0.0f, Value);

	return Value;
}

float UCharacterSheet::GetDelay(float AdditionalDelay) const
{
	float Value = BaseDelay + AdditionalDelay;

	for (auto& Item : Equipment) {
		if (Item != nullptr) {
			const ESkills Skill = EnumMaps::StyleToSkill[Item->Style];
			// Add delay for each armor piece, but reduce based on expertise
			switch (Item->Style) {
			case EEquipStyle::EquipLeather:
				Value += IsExpert(Skill) ? 0.0f : 0.05f;
			case EEquipStyle::EquipChain:
				Value += IsMaster(Skill) ? 0.0f : 0.1f;
			case EEquipStyle::EquipPlate:
				Value += IsMaster(Skill) ? 0.0f : IsExpert(Skill) ? 0.125f : 0.25f;
			}
		}
	}

	// Speed decreases total delay by a flat logarithmic value
	Value -= FMath::Loge(GetStat(EStats::StatSpeed)) / 7.0f;
	// But the total delay must be at least 0.5
	Value = FMath::Max(0.5f, Value);

	return Value;
}

float UCharacterSheet::GetAttackDelay(EEquipType Slot) const
{
	UEquippableItem* Weapon = Equipment[(int32)Slot];
	if (Weapon != nullptr) {
		const ESkills Skill = EnumMaps::StyleToSkill[Weapon->Style];

		float Delay = Weapon->Delay;
		if (Weapon->Style == EEquipStyle::EquipSword) {
			// Sword skill reduces weapon delay
			Delay *= 1.0f - GetSkill(Skill) * 0.03;
		}
		// Armsmaster reduces a percentage of weapon delay
		return Delay * (1.0f - (GetSkill(ESkills::SkillArmsmaster) * 0.01 * (IsMaster(ESkills::SkillArmsmaster) ? 4 : IsExpert(ESkills::SkillArmsmaster) ? 2 : 1)));
	}
	return 1.0f;
}

float UCharacterSheet::GetSpellDelay(ESpells Spell) const
{
	float Delay = EnumMaps::SpellInfo[Spell].Delay;

	if (Spell == ESpells::SpellWizardsEye && IsMaster(EnumMaps::SpellInfo[Spell].Skill)) return 0.0f;
	if (Spell == ESpells::SpellCauseLight) {
		if (IsMaster(EnumMaps::SpellInfo[Spell].Skill)) return 0.0f;
		else if (IsExpert(EnumMaps::SpellInfo[Spell].Skill)) Delay /= 2.0f;
	}

	return Delay;
}

UEquippableItem* UCharacterSheet::EquipItem(UEquippableItem* Item, EEquipType Slot)
{
	if (Slot == EEquipType::EquipTotalSlots) {
		Slot = Item->Slot;
	}

	if (Slot != Item->Slot || (Item->Style != EEquipStyle::EquipOther && GetSkill(EnumMaps::StyleToSkill[Item->Style]) == 0)) return Item;

	UEquippableItem* Previous = Equipment[(int32)Slot];
	Equipment[(int32)Slot] = Item;

	return Previous;
}

UEquippableItem* UCharacterSheet::UnequipItem(EEquipType Slot)
{
	if (Slot == EEquipType::EquipTotalSlots) return nullptr;

	UEquippableItem* Previous = Equipment[(int32)Slot];
	Equipment[(int32)Slot] = nullptr;

	return Previous;
}

bool UCharacterSheet::AddItemToInventory(UItem* Item)
{
	return Inventory->AddItemToFirstOpenSlot(Item);
}

bool UCharacterSheet::FindItem(const FName& ItemName, bool RemoveItem)
{
	for (auto& Item : Inventory->PositionMap) {
		if (Item.Key->Name.IsEqual(ItemName)) {
			if (RemoveItem) {
				Inventory->RemoveItem(Item.Key.Get());
			}
			return true;
		}
	}

	for (auto& Item : Equipment) {
		if (Item != nullptr && Item->Name.IsEqual(ItemName)) {
			if (RemoveItem) {
				UnequipItem(Cast<UEquippableItem>(Item)->Slot);
			}
			return true;
		}
	}

	return false;
}
