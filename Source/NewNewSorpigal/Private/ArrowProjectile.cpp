// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "ArrowProjectile.h"

#include "NNSCharacter.h"

// Sets default values
AArrowProjectile::AArrowProjectile(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AArrowProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AArrowProjectile::Tick( float DeltaTime )
{
	Super::Tick(DeltaTime);

}

void AArrowProjectile::OnOverlapMesh(ANNSCharacter* HitCharacter, FHitResult& Hit)
{
	// Spawn static arrow in its place
	AActor* StaticArrow = GetWorld()->SpawnActor<AActor>(StaticArrowClass, CollisionComp->GetComponentTransform());
	StaticArrow->AttachToComponent(Hit.GetComponent(), FAttachmentTransformRules::KeepWorldTransform, Hit.BoneName);

	Super::OnOverlapMesh(HitCharacter, Hit);
}

void AArrowProjectile::HitCallback(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr) {
		// Spawn static arrow in its place
		GetWorld()->SpawnActor<AActor>(StaticArrowClass, CollisionComp->GetComponentTransform());
		GetWorld()->DestroyActor(this);
	}
}
