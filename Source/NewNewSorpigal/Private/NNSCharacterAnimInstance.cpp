// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "NNSCharacterAnimInstance.h"

#include "NNSCharacter.h"

UNNSCharacterAnimInstance::UNNSCharacterAnimInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bIsMoving = false;
	bIsDead = false;
}

void UNNSCharacterAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	ANNSCharacter* Character = Cast<ANNSCharacter>(TryGetPawnOwner());

	if (Character != nullptr) {
		bIsDead = Character->bIsDead;
		bIsMoving = !Character->GetMovementComponent()->Velocity.IsNearlyZero();
	}
}
