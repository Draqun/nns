// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "NewNewSorpigalGameMode.h"

#include "CharacterSheet.h"
#include "NNSGameInstance.h"
#include "NNSPlayerController.h"
#include "PlayerPartyCharacter.h"

ANewNewSorpigalGameMode::ANewNewSorpigalGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<APawn> PlayerClass(TEXT("Blueprint'/Game/Blueprints/BP_PlayerParty.BP_PlayerParty_C'"));
	DefaultPawnClass = PlayerClass.Class;

	static ConstructorHelpers::FClassFinder<AHUD> BPHUD(TEXT("Blueprint'/Game/Blueprints/UI/BP_HUD.BP_HUD_C'"));
	HUDClass = BPHUD.Class;

	PlayerControllerClass = ANNSPlayerController::StaticClass();
}

void ANewNewSorpigalGameMode::BeginPlay()
{
	FTimerHandle LoadHandle;
	// Set timer for the first tick after game is fully loaded (to re-initialize game with saved data)
	GetWorldTimerManager().SetTimer(LoadHandle, Cast<UNNSGameInstance>(GetGameInstance()), &UNNSGameInstance::LoadCurrentLevel, 0.00001f, false);
}
