// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "NewNewSorpigalGameMode.generated.h"

class UCharacterSheet;

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API ANewNewSorpigalGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	ANewNewSorpigalGameMode(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;
};
