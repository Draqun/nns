// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Projectile.h"
#include "MagicProjectile.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API AMagicProjectile : public AProjectile
{
	GENERATED_BODY()
	
public:
	AMagicProjectile(const FObjectInitializer& ObjectInitializer);

	virtual void HitCallback(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	virtual void OnOverlapMesh(ANNSCharacter* HitCharacter);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
	UParticleSystem* HitParticles;

	UPROPERTY(EditDefaultsOnly, Category = "Audio")
	USoundWave* HitSound;

private:
	void PlayHitParticleAndSound();
};
