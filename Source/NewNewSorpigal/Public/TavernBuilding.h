// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "NPCBuilding.h"
#include "TavernBuilding.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API ATavernBuilding : public ANPCBuilding
{
	GENERATED_BODY()
	
public:
	ATavernBuilding(const FObjectInitializer& ObjectInitializer);

	virtual void Interact(ANNSCharacter* Character) override;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
	int32 RestCost;
};
