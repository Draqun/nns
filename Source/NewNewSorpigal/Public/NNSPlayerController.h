// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "NNSPlayerController.generated.h"

/**
 * Necessary class in order to get right-clicks working in the HUD
 * The base PlayerController does not forward right-clicks to the HUD's hitboxes,
 * so here we simply copy/paste the function and switch the mouse check from Left to Right
 */
UCLASS()
class NEWNEWSORPIGAL_API ANNSPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ANNSPlayerController();

	virtual void BeginPlay() override;

	virtual bool InputKey(FKey Key, EInputEvent EventType, float AmountDepressed, bool bGamepad) override;
	
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mouse")
	bool bLockMouseInCenter;
};
