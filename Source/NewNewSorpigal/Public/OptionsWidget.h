// Fillout your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "OptionsWidget.generated.h"

USTRUCT(BlueprintType)
struct FScreenResolution
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ScreenResolution)
	int32 Width;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ScreenResolution)
	int32 Height;

	FScreenResolution() : Width(0), Height(0) {}

	bool operator==(const FScreenResolution& Other) const {
		return Width == Other.Width && Height == Other.Height;
	}
};

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API UOptionsWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UOptionsWidget(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "Options")
	static void SetResolution(int32 Width, int32 Height);

	UFUNCTION(BlueprintCallable, Category = "Options")
	static void SetWindowMode(EWindowMode::Type Mode);

	UFUNCTION(BlueprintPure, Category="Options")
	static TArray<FScreenResolution> GetAvailableResolutions();

	UFUNCTION(BlueprintPure, Category = "Options")
	static FScreenResolution GetCurrentResolution();

	UFUNCTION(BlueprintPure, Category = "Options")
	static EWindowMode::Type IsFullscreen();

	UFUNCTION(BlueprintPure, Category = "Options")
	static void GetQualitySettings(int32& AntiAliasing, int32& Effects, int32& PostProcess, int32& Shadow, int32& Texture, int32& ViewDistance, int32& Foliage);
	UFUNCTION(BlueprintPure, Category = "Options")
	static int32 GetViewDistanceQuality();
	UFUNCTION(BlueprintPure, Category = "Options")
	static int32 GetAntiAliasingQuality();
	UFUNCTION(BlueprintPure, Category = "Options")
	static int32 GetShadowQuality();
	UFUNCTION(BlueprintPure, Category = "Options")
	static int32 GetPostProcessQuality();
	UFUNCTION(BlueprintPure, Category = "Options")
	static int32 GetTextureQuality();
	UFUNCTION(BlueprintPure, Category = "Options")
	static int32 GetEffectsQuality();
	UFUNCTION(BlueprintPure, Category = "Options")
	static int32 GetFoliageQuality();

	UFUNCTION(BlueprintCallable, Category = "Options")
	static void SetViewDistanceQuality(int32 Quality);
	UFUNCTION(BlueprintCallable, Category = "Options")
	static void SetAntiAliasingQuality(int32 Quality);
	UFUNCTION(BlueprintCallable, Category = "Options")
	static void SetShadowQuality(int32 Quality);
	UFUNCTION(BlueprintCallable, Category = "Options")
	static void SetPostProcessQuality(int32 Quality);
	UFUNCTION(BlueprintCallable, Category = "Options")
	static void SetTextureQuality(int32 Quality);
	UFUNCTION(BlueprintCallable, Category = "Options")
	static void SetEffectsQuality(int32 Quality);
	UFUNCTION(BlueprintCallable, Category = "Options")
	static void SetFoliageQuality(int32 Quality);

private:
	static UGameUserSettings* GetGameUserSettings();
};
