// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Globals.h"
#include "CharacterSheet.generated.h"

struct FEnemyData;
class ANNSCharacter;
class UDamageData;
class UEquippableItem;
class UInventory;
class UItem;

/**
 * Class handling the individual party member within a Character
 * Handles stats, health, status effects, etc.
 */
UCLASS()
class NEWNEWSORPIGAL_API UCharacterSheet : public UObject
{
	GENERATED_BODY()

public:
	UCharacterSheet(const FObjectInitializer& ObjectInitializer);

	void Save(FArchive& Archive, bool bBasic = false);
	void Load(FArchive& Archive, bool bBasic = false);
	void SaveLoadCommon(FArchive& Archive, bool bBasic = false);

	void InitEnemy(const FEnemyData& EnemyData);

	// Call as final step after setting all initial values
	UFUNCTION()
	void Init();

	// Member can no longer act until timer finishes. Call after performing an action
	UFUNCTION()
	void StartTurnTimer(float Delay, bool bEndTurn = true);

	// Allow the player to act again
	UFUNCTION()
	void OnTurnTimerFinished();

	UFUNCTION()
	void TakeDamage(const UDamageData* Damage);

	void PlayDamageSound();
	
	UPROPERTY()
	bool bCanPlayDamageSound;

	UFUNCTION()
	bool Heal(float Amount, bool bLog = true);

	UFUNCTION()
	void FullHeal();

	void RegainConsciousness();

	UFUNCTION()
	void UpdateCurrentMana(float Delta);

	// Increase Experience per living character and check for level ups
	UFUNCTION()
	void GiveExperience(float XP);

	// True if CurrentHealth is less than DeathBarrier
	UFUNCTION()
	bool IsDead() const;

	// True if CurrentHealth is less than 0
	UFUNCTION()
	bool IsUnconscious() const;

	void AddStatusEffect(EStatusEffects Status, int32 Duration = StatusEffectConstants::kMaxDuration, int32 Strength = 1);
	bool RemoveStatusEffect(EStatusEffects Status);
	void TickStatusEffects(int32 Duration);
	bool HasStatus(EStatusEffects Status) const { return StatusEffects.Contains(Status); }

	// Real->Turn
	UFUNCTION()
	void ConvertTimerToTurnCount();

	// Turn->Real
	UFUNCTION()
	void ConvertTurnCountToTimer();

	UFUNCTION()
	uint32 GetHealCost() const;

	// Get the base value of the stat, for cases like adding a percentage
	UFUNCTION()
	int32 GetBaseStat(EStats Stat) const;

	// Get the actual value of the stat, for use in calculations
	UFUNCTION()
	int32 GetStat(EStats Stat) const;

	// Get the base value of the skill, for cases like leveling the skill
	UFUNCTION()
	int32 GetBaseSkill(ESkills Skill) const;

	// Get the actual value of the skill, for use in calculations
	UFUNCTION()
	int32 GetSkill(ESkills Skill) const;

	UFUNCTION()
	void ChangeBaseStat(int32 Delta, EStats Stat);

	UFUNCTION()
	void UpgradeBaseStat(EStats Stat);

	// Learn a skill at 0. Skills at 0 can't be increased otherwise
	UFUNCTION()
	void LearnBaseSkill(ESkills Skill, bool UseMoney = true);

	// Upgrade a learned skill by 1
	UFUNCTION()
	void UpgradeBaseSkill(ESkills Skill);

	// Must have learned and have enough skill points to upgrade
	UFUNCTION()
	bool CanUpgradeBaseSkill(ESkills Skill) const;

	UFUNCTION()
	void BecomeExpert(ESkills Skill);

	UFUNCTION()
	void BecomeMaster(ESkills Skill);

	UFUNCTION()
	bool IsExpert(ESkills Skill) const;

	UFUNCTION()
	bool IsMaster(ESkills Skill) const;

	UFUNCTION()
	bool CanLearnSkill(ESkills Skill, bool UseMoney = true) const;

	UFUNCTION()
	bool CanBecomeExpert(ESkills Skill) const;

	UFUNCTION()
	bool CanBecomeMaster(ESkills Skill) const;

	UFUNCTION()
	bool HasLearnedSpell(ESpells Spell) const;

	UFUNCTION()
	bool CanLearnSpell(ESpells Spell) const;

	UFUNCTION()
	bool LearnSpell(ESpells Spell);

	UFUNCTION()
	int32 GetMasteryAllowed(ESpells Spell);

	UFUNCTION()
	EMastery Mastery(ESkills Skill) const;

	UFUNCTION()
	float GetArmor() const;

	// Gets a random damage value based on currently-equipped weapon
	UFUNCTION()
	float GetDamage(EEquipType Slot) const;

	// Gets base delay, after going through modifiers. Optionally pass in additional delay from weapon/spell
	UFUNCTION()
	float GetDelay(float AdditionalDelay = 0.0f) const;

	// Delay based on attacking with main hand weapon
	UFUNCTION()
	float GetAttackDelay(EEquipType Slot) const;

	// Delay based on casting a spell
	UFUNCTION()
	float GetSpellDelay(ESpells Spell) const;

	UPROPERTY(BlueprintReadWrite, Category = Gameplay)
	TWeakObjectPtr<ANNSCharacter> OwnerCharacter;
	
	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = Gameplay)
	float CurrentHealth;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = Gameplay)
	float CurrentMana;

	UPROPERTY(BlueprintReadOnly, Category = Gameplay)
	UInventory* Inventory;

	UPROPERTY()
	TArray<UEquippableItem*> Equipment;

	UFUNCTION()
	bool AddItemToInventory(UItem* Item);

	UFUNCTION()
	bool FindItem(const FName& ItemName, bool RemoveItem);

	// Equip item in requested slot. If no slot specified, auto-pick. Returns same item if fail, previous equipped item, or nullptr
	UFUNCTION()
	UEquippableItem* EquipItem(UEquippableItem* Item, EEquipType Slot = EEquipType::EquipTotalSlots);

	// Unequips and returns the item in the slot
	UFUNCTION()
	UEquippableItem* UnequipItem(EEquipType Slot);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	int32 Level;

	// For the Player, the amount of XP points towards the next level. For enemies, the amount of XP they give for dying
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	int32 Experience;

	// The amount of experience needed to level up again
	UPROPERTY()
	int32 ExperienceToLevel;

	// Current available skill points to upgrade skills
	UPROPERTY()
	int32 SkillPoints;

	// Current available stat points to upgrade stats
	UPROPERTY()
	int32 StatPoints;

	// If the character has no turn timer or turn delay, they can act
	UPROPERTY(BlueprintReadWrite, Category = Gameplay)
	bool bCanAct;

	// Time or turns until next attack
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay)
	float TurnCount;

	// Base delay all characters have, added on to weapon delay (therefore, this is unarmed delay)
	UPROPERTY()
	float BaseDelay;

	// Amount of health (should be negative) needed before considered dead
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	float DeathBarrier;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Gameplay)
	FString Name;

	UPROPERTY()
	FTimerHandle TurnTimerHandle;

	UPROPERTY()
	TArray<int32> BaseStats;

	UPROPERTY()
	TArray<int32> BaseSkills;

	UPROPERTY()
	TArray<bool> ExpertSkills;

	UPROPERTY()
	TArray<bool> MasterSkills;

	UPROPERTY()
	TMap<EStatusEffects, FStatusEffect> StatusEffects;

	UPROPERTY()
	EClass Class;

	UPROPERTY()
	UTexture2D* Portrait;

	UPROPERTY()
	ESkills LastSpellbookPage;

	UPROPERTY()
	ESpells LastSpellCast;

	UPROPERTY()
	int32 Voice;

private:
	UFUNCTION()
	void LevelUp();

	UPROPERTY()
	TArray<bool> KnownSpells;
};

inline int32 UCharacterSheet::GetBaseStat(EStats Stat) const
{
	return BaseStats[(int32)Stat];
}

inline int32 UCharacterSheet::GetBaseSkill(ESkills Skill) const
{
	return BaseSkills[(int32)Skill];
}
