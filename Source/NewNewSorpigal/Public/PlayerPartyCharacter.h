// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "NNSCharacter.h"
#include "PlayerPartyCharacter.generated.h"

class UItem;
class UPartyMember;

UCLASS()
class NEWNEWSORPIGAL_API APlayerPartyCharacter : public ANNSCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerPartyCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void SaveLoadCommon(FArchive& Archive) override;
	virtual void Save(FArchive& Archive) override;
	virtual void Load(FArchive& Archive, bool bIgnoreTransforms = false) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* Input) override;

	// Enable or disable motion blur (mainly used for disabling when paused)
	UFUNCTION()
	void SetEnableMotionBlur(bool bEnable);

	// The Player doesn't have a mesh, so we should never check collision with it
	UFUNCTION()
	virtual bool ShouldCheckMeshCollision() const override { return false; }

	void TriggerParticlesAtCamera(UParticleSystem* Particles);

	UFUNCTION()
	uint32 GetGold() const;

	UFUNCTION()
	void RemoveGold(uint32 Gold) const;

	UFUNCTION()
	void AddGold(uint32 Gold) const;

	uint32 ConvertCost(uint32 Gold, bool bIncrease) const;

	UFUNCTION()
	bool AddItemToInventory(UItem* Item) const;

	UFUNCTION()
	bool FindItem(const FName& ItemName, bool RemoveItem);

	// Gives each party member experience. Use Force to give dead members experience too.
	UFUNCTION()
	void GiveExperience(float Experience, bool Force = false);

	UFUNCTION()
	void Escape();

	UFUNCTION()
	void MoveForward(float Val);

	UFUNCTION()
	void MoveRight(float Val);

	UFUNCTION()
	void TurnUp(float Val);

	UFUNCTION()
	void TurnRight(float Val);

	UFUNCTION()
	void OnStartJump();

	UFUNCTION()
	void OnEndJump();

	UFUNCTION()
	void OnStartWalk();

	UFUNCTION()
	void OnEndWalk();

	UFUNCTION()
	void ToggleTurnMode();
	void ForceExitTurnMode();

	UFUNCTION()
	void Interact();

	// Display HUD windows
	UFUNCTION()
	void ToggleInventory();

	UFUNCTION()
	void ToggleStats();

	UFUNCTION()
	void ToggleSkills();

	UFUNCTION()
	void ToggleQuests();

	UFUNCTION()
	void ToggleBestiary();

	UFUNCTION()
	void ToggleMap();

	// Set active character to the requested player. Doesn't work in turn mode
	UFUNCTION()
	void OnPlayer1();

	UFUNCTION()
	void OnPlayer2();

	UFUNCTION()
	void OnPlayer3();

	UFUNCTION()
	void OnPlayer4();

	UFUNCTION()
	void SwitchToPartyMember(int32 PartyMember);

	UFUNCTION()
	void OnCast();

	UFUNCTION()
	void OnRecast();

	UFUNCTION()
	virtual void Attack() override;

	void StartSwimming();
	void StopSwimming();

	virtual void DealDamage(UDamageData* Damage, bool DamageAll = false) override;
	virtual void DealDamage(UDamageData* Damage, UCharacterSheet* Character) override;

	void CheckDeath();

	virtual void AddStatusEffect(EPartyStatusEffects Status, int32 Duration = StatusEffectConstants::kMaxDuration, int32 Strength = 1) override;
	virtual void RemoveStatusEffect(EPartyStatusEffects Status) override;

	// Instead of actor forward direction, get camera forward direction
	virtual FVector GetProjectileSpawnDirection() const;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay)
	UPointLightComponent* TorchComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay)
	UPawnNoiseEmitterComponent* NoiseComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Menu)
	TSubclassOf<class UUserWidget> EscapeMenuClass;

	UPROPERTY()
	UUserWidget* EscapeMenu;

	UPROPERTY(BlueprintReadOnly, Category = "Movement")
	bool bIsWalking;

	UPROPERTY(BlueprintReadOnly, Category = "Movement")
	bool bIsMovingBackwards;

	UPROPERTY()
	bool bIsSwimming;

	UFUNCTION(BlueprintNativeEvent, Category = "Movement")
	void PlayFootstep(bool bIsLeftFoot, EPhysicalSurface Surface);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float FootstepInterval;

	UPROPERTY(EditDefaultsOnly, Category = "Audio")
	USoundWave* GoldSound;

	UPROPERTY()
	USoundWave* BattleStartSound;

	UPROPERTY()
	USoundWave* BattleEndSound;

	UPROPERTY()
	USoundWave* DeathSound;

	// Item the party currently has, but is not in an inventory yet
	UPROPERTY()
	UItem* HeldItem;

	bool GetActorUnderMouse(FName Tag, float Distance, AActor*& Actor, float& ActorDistance);
	bool GetActorUnderMouse(TArray<FName> TagList, float Distance, AActor*& Actor, float& ActorDistance);

	bool GetNearestActorByTag(FName Tag, float Distance, AActor*& NearestActor, float& NearestActorDistance);
	bool GetNearestActorByTag(TArray<FName> TagList, float Distance, AActor*& NearestActor, float& NearestActorDistance);

	static const float kMaxTargetDistance;

	void Quicksave();
	void Quickload();

	virtual bool CastSpell(ESpells Spell, UCharacterSheet* Target = nullptr) override;

private:
	void VisibilityCheck();
	
	void UpdateVoice();

	UFUNCTION()
	bool IsOnScreen(FVector WorldLocation) const;

	UFUNCTION()
	void AttackPrivate(ANNSCharacter* Target = nullptr);

	UFUNCTION()
	void RemoveHighlighting(AActor*& Previous, AActor* Next);
	UFUNCTION()
	void AddHighlighting(AActor*& Previous, AActor* Next, bool bAsInteractable = false);

	virtual void UpdateWalkSpeed() override;

	UFUNCTION()
	void TriggerFootstep();

	UPROPERTY()
	AActor* HighlightActorAttackable;
	UPROPERTY()
	AActor* HighlightActorInteractable;

	UPROPERTY()
	TArray<FName> TickTags;

	UPROPERTY()
	bool bWasMoving;

	UPROPERTY()
	FTimerHandle FootstepHandle;

	UPROPERTY()
	bool bFootstepLeft;

	static const float kPlayerWalkSpeedFactor;
	static const float kPlayerRunSpeed;
	static const float kPlayerCombatSpeedFactor;
	static const float kPlayerFleeSpeedFactor;
	static const float kPlayerBackwardsSpeedFactor;
	static const float kMaxPickupDistance;
};
