// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Globals.h"
#include "QuestManager.generated.h"

class APlayerPartyCharacter;

UCLASS()
class UQuestTask : public UObject
{
	GENERATED_BODY()

public:
	UQuestTask() : Super(), bIsCompleted(false) {}

	virtual void SaveLoadCommon(FArchive& Archive)
	{
		Archive << bIsCompleted;
	}

	UFUNCTION()
	virtual FString ToString() const { return TEXT(""); }

	UPROPERTY()
	bool bIsCompleted;
};

UCLASS()
class UDialogQuestTask : public UQuestTask
{
	GENERATED_BODY()

public:
	UDialogQuestTask() : Super() {}

	static UDialogQuestTask* Create(const FString& CName) {
		UDialogQuestTask* Task = NewObject<UDialogQuestTask>();
		Task->CharacterName = CName;
		Task->ItemName = FName();
		Task->bDestroyItem = false;
		return Task;
	}

	static UDialogQuestTask* Create(const FString& CName, const FName& IName, bool Destroy) {
		UDialogQuestTask* Task = NewObject<UDialogQuestTask>();
		Task->CharacterName = CName;
		Task->ItemName = IName;
		Task->bDestroyItem = Destroy;
		return Task;
	}

	virtual FString ToString() const override {
		if (!ItemName.IsNone()) {
			if (bDestroyItem) {
				return FString::Printf(TEXT("Give a %s to %s"), *ItemName.ToString(), *CharacterName);
			} else {
				return FString::Printf(TEXT("Show %s that you have a %s"), *CharacterName, *ItemName.ToString());
			}
		} else {
			return FString::Printf(TEXT("Speak to %s"), *CharacterName);
		}
	}

	UPROPERTY()
	FString CharacterName;

	UPROPERTY()
	FName ItemName;

	UPROPERTY()
	bool bDestroyItem;
};

UCLASS()
class UKillQuestTask : public UQuestTask
{
	GENERATED_BODY()

public:
	UKillQuestTask() : Super() {}
	UKillQuestTask(const FString& Name, int32 Amount)
		: UQuestTask(), CharacterName(Name), AmountToKill(Amount), TotalToKill(Amount) {}

	static UKillQuestTask* Create(const FString& Name, int32 Amount) {
		UKillQuestTask* Task = NewObject<UKillQuestTask>();
		Task->CharacterName = Name;
		Task->AmountToKill = Amount;
		Task->TotalToKill = Amount;
		return Task;
	}

	virtual void SaveLoadCommon(FArchive& Archive) override {
		Super::SaveLoadCommon(Archive);
		Archive << AmountToKill;
	}

	virtual FString ToString() const override {
		return FString::Printf(TEXT("Kill %ss: %i / %i"), *CharacterName, (TotalToKill - AmountToKill), TotalToKill);
	}

	UPROPERTY()
	FString CharacterName;

	UPROPERTY()
	int32 AmountToKill;

	UPROPERTY()
	int32 TotalToKill;
};

UCLASS()
class UFinishQuestTask : public UQuestTask
{
	GENERATED_BODY()

public:
	UFinishQuestTask() : Super() {}
	UFinishQuestTask(EQuestFlags Quest)
		: UQuestTask(), QuestFlag(Quest) {}

	static UFinishQuestTask* Create(EQuestFlags Quest) {
		UFinishQuestTask* Task = NewObject<UFinishQuestTask>();
		Task->QuestFlag = Quest;
		return Task;
	}

	virtual FString ToString() const override {
		return TEXT("Finish a different quest");
	}

	UPROPERTY()
	EQuestFlags QuestFlag;
};


UCLASS()
class UInteractQuestTask : public UQuestTask
{
	GENERATED_BODY()

public:
	UInteractQuestTask() : Super() {}
	UInteractQuestTask(const FString& Desc)
		: UQuestTask(), Description(Desc) {}

	static UInteractQuestTask* Create(const FString& Description) {
		UInteractQuestTask* Task = NewObject<UInteractQuestTask>();
		Task->Description = Description;
		return Task;
	}

	virtual FString ToString() const override {
		return Description;
	}

	UPROPERTY()
	FString Description;
};

USTRUCT(BlueprintType)
struct FQuestReward
{
	GENERATED_USTRUCT_BODY()

public:
	FQuestReward() {}
	FQuestReward(int32 G, int32 XP) : Gold(G), Experience(XP) {}

	UPROPERTY()
	int32 Gold;

	UPROPERTY()
	int32 Experience;
};

UCLASS()
class UQuest : public UObject
{
	GENERATED_BODY()

public:
	UQuest() : Super() {}

	void SaveLoadCommon(FArchive& Archive) {
		for (UQuestTask* Task : Tasks) {
			Task->SaveLoadCommon(Archive);
		}
	}

	UPROPERTY()
	int32 Quest;
	
	UPROPERTY()
	TArray<UQuestTask*> Tasks;

	UPROPERTY()
	FString Name;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FQuestFinished, EQuestFlags, Quest);

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API UQuestManager : public UObject
{
	GENERATED_BODY()
	
public:
	UQuestManager(const FObjectInitializer& ObjectInitializer);

	virtual void SaveLoadCommon(FArchive& Archive);
	virtual void Save(FArchive& Archive);
	virtual void Load(FArchive& Archive);
	
	UFUNCTION(BlueprintCallable, Category = "Quests")
	void UpdateQuestFromDialog(EQuestFlags Quest, const FString& CharacterName);
	UFUNCTION(BlueprintCallable, Category = "Quests")
	void UpdateQuestsFromKill(const FString& CharacterName);
	UFUNCTION(BlueprintCallable, Category = "Quests")
	void UpdateQuestsFromFinish(EQuestFlags QuestFlag);
	UFUNCTION(BlueprintCallable, Category = "Quests")
	void UpdateQuestsFromInteract(EQuestFlags QuestFlag);

	// Update quest progress until the next uncompleted task
	void UpdateQuestState(int32 Quest);

	// Check if the next step in a quest is talking to the requested character
	bool IsCurrentOrPreviousStepCharacter(EQuestFlags Quest, const FString& CharacterName, int32 FlagNum) const;
	bool IsCharacterDialogFinished(EQuestFlags Quest, const FString& CharacterName, int32 FlagNum) const;

	UFUNCTION(BlueprintPure, Category = "Quests")
	bool IsCompleted(int32 Quest) const;

	UQuest* GetQuest(EQuestFlags Quest) const;
	const FQuestReward& GetQuestReward(EQuestFlags Quest) const;
	bool IsCompleted(EQuestFlags Quest) const;
	int32 GetCurrentTask(EQuestFlags Quest) const;
	UQuest* GetBestiaryQuest(EBestiaryFlags Quest) const;
	const FQuestReward& GetBestiaryReward(EBestiaryFlags Quest) const;
	bool IsCompleted(EBestiaryFlags Quest) const;

	TWeakObjectPtr<APlayerPartyCharacter> PlayerParty;

	UFUNCTION(BlueprintPure, Category = "Quests")
	TArray<FText> GetQuestText(bool bCompletedQuests, bool bUseBestiary) const;

	UPROPERTY(BlueprintAssignable, Category = "Quests")
	FQuestFinished QuestFinishedDelegate;

	UPROPERTY()
	USoundWave* QuestTaskSound;

	UPROPERTY()
	USoundWave* QuestFinishedSound;

	TWeakObjectPtr<UNNSGameInstance> GameInstance;

private:
	void AddQuestTask(EQuestFlags Quest, UQuestTask* Task);
	void AddQuestReward(EQuestFlags Quest, int32 Gold, int32 Experience);

	void AddBestiaryTask(EBestiaryFlags Quest, UQuestTask* Task);
	void AddBestiaryReward(EBestiaryFlags Quest, int32 Gold, int32 Experience);

	UPROPERTY()
	TArray<UQuest*> Quests;

	TMap<int, int32> QuestProgress;
	TMap<int, FQuestReward> QuestRewards;
};
