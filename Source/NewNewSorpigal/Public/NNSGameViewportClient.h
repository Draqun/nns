// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameViewportClient.h"
#include "NNSGameViewportClient.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API UNNSGameViewportClient : public UGameViewportClient
{
	GENERATED_BODY()
	
public:
	UNNSGameViewportClient(const FObjectInitializer& ObjectInitializer);
	
	virtual void LostFocus(FViewport* InViewport) override;
	virtual void ReceivedFocus(FViewport* InViewport) override;

private:
	bool bMouseWasLocked;
};
