// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Globals.h"
#include "Item.generated.h"

/**
 * 
 */
UCLASS(abstract)
class NEWNEWSORPIGAL_API UItem : public UObject
{
	GENERATED_BODY()
	
public:
	UItem();

	void Save(FArchive& Archive);
	virtual void SaveLoadCommon(FArchive& Archive);
	// Spawns a new UItem according to the saved class, and then initializes it
	static UItem* Load(FArchive& Archive);
	
	virtual void Init() {}

	// Name of item for use in tooltips
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FName Name;

	// String representation of item for use in tooltips
	UFUNCTION()
	virtual TArray<FString> ToString() const;

	UFUNCTION()
	virtual void Identify(int SkillValue, EMastery Mastery);

	virtual bool MatchesShop(EShopTypes ShopType);

	FName GetName() const;

	virtual uint32 GetBuyCost() const;
	uint32 GetSellCost() const;
	uint32 GetIDCost() const;

	// DON'T USE THIS DIRECTLY. Pass it into GameInstance::GetTexture()
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TAssetPtr<UTexture> Icon;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	int32 IconWidth;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	int32 IconHeight;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	int32 Tier;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TSubclassOf<class APickup> PickupClass;

	UPROPERTY()
	bool bIsIdentified;
};
