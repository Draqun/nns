// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "TimeOfDayManager.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API UTimeOfDayManager : public UObject
{
	GENERATED_BODY()
	
public:
	UTimeOfDayManager();

	void Save(FArchive& Archive);
	void Load(FArchive& Archive);
	void SaveLoadCommon(FArchive& Archive);

	void StartTimer();
	void EndTimer();

	void TickTimer();

	UFUNCTION(BlueprintCallable, Category = "Time")
	FString GetAsString() const;
	UFUNCTION(BlueprintCallable, Category = "Time")
	float GetAsFloat() const;

	UFUNCTION(BlueprintCallable, Category = "Time")
	int32 GetMinutesSinceGameStart() const { return MinutesSinceGameStart; }

	// normalized from 0 (just about to tick) to 1 (just ticked)
	UFUNCTION(BlueprintCallable, Category = "Time")
	float TimeRemainingUntilTick() const;

	void AdvanceTurnTimer(float SecondsToAdvance);

	void AdvanceTimeOfDay(float MinutesToAdvance);

	bool IsTimerActive() const { return World.IsValid() && World->GetTimerManager().IsTimerActive(TickHandle); }

	void SetTime(int32 H, int32 M);

	UPROPERTY()
	int32 Hours;

	UPROPERTY()
	int32 Minutes;

	UPROPERTY()
	float TurnTimer;

	UPROPERTY()
	FTimerHandle TickHandle;

	TWeakObjectPtr<UWorld> World;

private:
	float MinutesSinceGameStart;

	// Number of seconds between each tick
	static const float TimeScale;

	// Number of minutes to increment every tick
	static const int32 MinutesPerTick;
};
