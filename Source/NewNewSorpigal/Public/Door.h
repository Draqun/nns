// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StaticInteractable.h"
#include "Door.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API ADoor : public AStaticInteractable
{
	GENERATED_BODY()
	
public:
	ADoor(const FObjectInitializer& ObjectInitializer);

	virtual void Tick(float DeltaSeconds) override;

	virtual void SaveLoadCommon(FArchive& Archive) override;
	virtual void Load(FArchive& Archive) override;

	virtual void Interact(ANNSCharacter* Character) override;

	// True to open door left. False to open door right.
	UPROPERTY(EditAnywhere, Category = "Door")
	bool bOpenXPositive;

	UPROPERTY(EditAnywhere, Category = "Door")
	float SecondsToOpen;

	UPROPERTY(EditAnywhere, Category = "Door")
	USoundBase* OpeningSound;

	// Currently does nothing
	UPROPERTY(EditAnywhere, Category = "Door")
	bool bLoopOpeningSound;

	// Door will open to this percent of its total width
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door")
	float OpenPercent;

	// Name of a key necessary to open this door. Key will be destroyed on use
	UPROPERTY(EditAnywhere, Category = "Door")
	FName Key;

private:
	UPROPERTY()
	bool bIsOpen;

	UPROPERTY()
	float TimeElapsed;
};
