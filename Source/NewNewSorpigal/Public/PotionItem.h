// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ConsumableItem.h"
#include "PotionItem.generated.h"

class APlayerPartyCharacter;
class UCharacterSheet;

UENUM(BlueprintType)
enum class EPotionTypes : uint8
{
	PotionEmptyBottle,
	PotionReagentRed,
	PotionReagentYellow,
	PotionReagentBlue,
	PotionRed,
	PotionYellow,
	PotionBlue,
	PotionDarkRed,
	PotionDarkYellow,
	PotionDarkBlue,
	PotionOrange,
	PotionGreen,
	PotionPurple,
	PotionWhite1,
	PotionWhite2,
	PotionWhite3,
	PotionWhite4,
	PotionWhite5,
	PotionWhite6,
	PotionBlack1,
	PotionBlack2,
	PotionBlack3,
	PotionBlack4,
	PotionBlack5,
	PotionBlack6,
};

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API UPotionItem : public UConsumableItem
{
	GENERATED_BODY()
	
public:
	UPotionItem();

	UFUNCTION(BlueprintCallable, Category = "ItemCreate")
	static UPotionItem* Create(const FName& Potion);

	virtual void Init() override;

	virtual bool MatchesShop(EShopTypes ShopType) override;

	virtual TArray<FString> ToString() const override;

	virtual bool Consume(UCharacterSheet* Character);

	bool Mix(APlayerPartyCharacter* Player, UPotionItem* Item);

	virtual uint32 GetBuyCost() const override;

	bool IsReagent() const { return Type == EPotionTypes::PotionReagentRed || Type == EPotionTypes::PotionReagentYellow || Type == EPotionTypes::PotionReagentBlue; }
	bool IsEmpty() const { return Type == EPotionTypes::PotionEmptyBottle; }

	UPROPERTY()
	EPotionTypes Type;

	UPROPERTY()
	FString Tooltip;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TSubclassOf<class APickup> RedReagentPickupClass;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TSubclassOf<class APickup> YellowReagentPickupClass;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TSubclassOf<class APickup> BlueReagentPickupClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Gameplay")
	UParticleSystem* Explosion;

	UPROPERTY()
	USoundWave* MixSound;

	UPROPERTY()
	USoundWave* DrinkSound;

	UPROPERTY()
	USoundWave* ExplodeSound;

	static const FName kEmptyString;
	static const FName kRedReagentString;
	static const FName kYellowReagentString;
	static const FName kBlueReagentString;

private:
	static const FStringAssetReference kRedReagentIcon;
	static const FStringAssetReference kYellowReagentIcon;
	static const FStringAssetReference kBlueReagentIcon;
	static const FStringAssetReference kEmptyIcon;
};
