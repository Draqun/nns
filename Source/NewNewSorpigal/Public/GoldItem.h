// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Item.h"
#include "GoldItem.generated.h"

/**
 * UI representation for gold, basically only used for chests
 */
UCLASS()
class NEWNEWSORPIGAL_API UGoldItem : public UItem
{
	GENERATED_BODY()
	
public:
	UGoldItem();

	virtual void SaveLoadCommon(FArchive& Archive) override;

	static UGoldItem* Create(uint32 Amount);

	virtual TArray<FString> ToString() const override;
	
	UPROPERTY()
	uint32 Amount;	
};
