// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StaticInteractable.h"
#include "ZoneChangerInteractable.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API AZoneChangerInteractable : public AStaticInteractable
{
	GENERATED_BODY()
	
public:
	AZoneChangerInteractable(const FObjectInitializer& ObjectInitializer);

	virtual void Interact(ANNSCharacter* Character) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay")
	FName LevelName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	FName OtherSideObjectName;
};
