// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StaticInteractable.h"
#include "NPCBuilding.generated.h"

class ANNSCharacter;
class UDialogComponent;

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API ANPCBuilding : public AStaticInteractable
{
	GENERATED_BODY()
	
public:
	ANPCBuilding(const FObjectInitializer& ObjectInitializer);
	
	virtual void Interact(ANNSCharacter* Character) override;

	UPROPERTY(VisibleAnywhere, Category = "Dialog")
	UDialogComponent* DialogComponent;
};
