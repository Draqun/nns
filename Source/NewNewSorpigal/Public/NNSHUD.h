// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "Globals.h"
#include "NNSHUD.generated.h"

class ABaseEnemyCharacter;
class AChest;
class APickup;
class APlayerPartyCharacter;
class AStaticInteractable;
class UCharacterSheet;
class UDamageData;
class UDialogComponent;
class UInventory;
class UItem;
class UNNSGameInstance;

UENUM(BlueprintType)
enum class EMenuType : uint8
{
	MenuNone,
	MenuInventory,
	MenuStats,
	MenuSkills,
	MenuQuests,
	MenuBestiary,
	MenuSpellbook
};

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API ANNSHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	ANNSHUD(const FObjectInitializer& ObjectInitializer);

	void Reset();

	virtual void BeginPlay() override;

	virtual void DrawHUD() override;

	UFUNCTION(BlueprintCallable, Category="Pause")
	void Pause(bool SetPaused);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Font")
	UFont* Font;

	UFUNCTION()
	void ToggleHUDMenuType(EMenuType Type, bool bPlaySound = true);

	UFUNCTION()
	bool Escape();

	UFUNCTION()
	void EnterDialog(UDialogComponent* Dialog);

	UFUNCTION()
	void OpenChest(AChest* InChest);

	UFUNCTION()
	void EnterBuilding(AStaticInteractable* InBuilding);

	void Log(const FString& Line);
	void Log(UCharacterSheet* Character, float Amount, const FString& Source);

	// Clear and recreate hitboxes depending on menu type
	UFUNCTION()
	void RefreshHitboxes();

	UPROPERTY(BlueprintReadOnly, Category = "Menu")
	EMenuType MenuType;

	// Copy of non-virtual function in AHUD to handle right-click instead of left-click
	bool UpdateAndDispatchHitBoxRightClickEvents(FVector2D ClickLocation, const EInputEvent InEventType);

	static TArray<FString> StatNames;
	static TArray<FString> SkillNames;
	static TArray<FString> ClassNames;
	static TArray<FString> StatusEffectNames;
	static TArray<FString> PartyStatusEffectNames;

	UPROPERTY()
	UTextureRenderTarget2D* Minimap;

	UPROPERTY()
	float MinimapSize;

	UPROPERTY()
	FVector MinimapOrigin;

	UPROPERTY()
	UTexture2D* Arrow;

	UPROPERTY()
	UTexture2D* TurnIcon;

	UPROPERTY()
	UTexture2D* LevelUpIcon;

	UPROPERTY()
	UTexture2D* DeathIcon;

	UPROPERTY()
	UTexture2D* UnconsciousIcon;

	UPROPERTY()
	bool bDrawMap;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Menu)
	TSubclassOf<class UUserWidget> QuestsMenuClass;

	UPROPERTY()
	UUserWidget* QuestsMenu;

	UPROPERTY(EditAnywhere, Category = "Audio")
	USoundWave* OpenMenuSound;

	UPROPERTY(EditAnywhere, Category = "Audio")
	USoundWave* CloseMenuSound;

	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* BackgroundMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* InnerBackgroundMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* SlotBackgroundMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* LogBackgroundMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* SpellbookBackgroundMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* PlayerBackgroundMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* TooltipBackgroundMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* DialogBackgroundMaterial;

	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* InnerBorderMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* SlotBorderMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* LogBorderMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* SpellbookBorderMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* PlayerBorderMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* TooltipBorderMaterial;
	UPROPERTY(EditAnywhere, Category = "Background")
	UMaterialInterface* DialogBorderMaterial;

	UPROPERTY(EditAnywhere, Category = "Background")
	float InnerBorderWidth;
	UPROPERTY(EditAnywhere, Category = "Background")
	float SlotBorderWidth;
	UPROPERTY(EditAnywhere, Category = "Background")
	float LogBorderWidth;
	UPROPERTY(EditAnywhere, Category = "Background")
	float SpellbookBorderWidth;
	UPROPERTY(EditAnywhere, Category = "Background")
	float TooltipBorderWidth;
	UPROPERTY(EditAnywhere, Category = "Background")
	float DialogBorderWidth;

	UPROPERTY(EditAnywhere, Category = "Font")
	FLinearColor FontColor;
	UPROPERTY(EditAnywhere, Category = "Font")
	FLinearColor HeaderColor;

	FName ClickedHitbox;

	virtual void NotifyHitBoxClick(FName BoxName) override;
	virtual void NotifyHitBoxRelease(FName BoxName) override;
	virtual void NotifyHitBoxRightClick(FName BoxName);
	virtual void NotifyHitBoxBeginCursorOver(FName BoxName) override;
	virtual void NotifyHitBoxEndCursorOver(FName BoxName) override;

	UPROPERTY()
	USoundWave* DoorSound;

	UPROPERTY()
	UTexture2D* NPCPortrait;
	
private:
	// Current Dialog to display
	TWeakObjectPtr<UDialogComponent> DialogComponent;
	FName CurrentDialogKey;
	// Current chest being opened
	TWeakObjectPtr<AChest> Chest;
	// Shop being browsed
	TWeakObjectPtr<AStaticInteractable> Building;

	ESkills SpellbookSchool;
	ESpells SelectedSpell;
	ETargetMode TargetMode;

	TArray<FString> LogLines;
	static const int32 kMaxLogLines;
	static const float LogWidth;
	float LogHeight;
	float LogX;
	float LogY;

	float MinimapX;
	static const float MinimapY;
	static const float MinimapWidth;
	static const float MinimapHeight;

	float TimeX;
	static const float TimeWidth;
	float TimeHeight;
	static const float TimeY;

	float ChestX;
	float ChestY;
	float ChestCloseX;
	float ChestCloseY;
	static const float ChestCloseWidth;
	static const float ChestCloseHeight;
	static const FName ChestCloseName;

	float CrosshairX;
	float CrosshairY;

	// General layout variables; vary depending on viewport size
	float BackgroundX;
	float BackgroundY;
	float BackgroundWidth;
	float BackgroundHeight;
	float TypeX;
	float TypeY;
	float CharacterX;
	float CharacterY;
	float StatusX;
	float StatusY;
	static const int32 NumStatusRows;
	static const FString StatusEffectTooltip;
	float TextHeight;
	float DialogSidebarKeyY;
	float SpellbookX;
	float SpellbookY;
	float SpellbookButtonX;

	float DialogSidebarHeight;
	float DialogSidebarX;
	float DialogSidebarExitY;
	float DialogBackgroundX;
	float DialogBackgroundY;
	static const float DialogSidebarWidth;
	static const float DialogSidebarSpacing;
	static const float DialogSidebarPortraitSize;
	static const float DialogTextScale;
	static const float DialogBackgroundWidth;
	static const float DialogBackgroundHeight;
	// Name of the button to exit dialog mode
	static const FName DialogExit;
	static const FName StoreBuy;
	static const FName StoreInventory;
	static const FName StoreSell;
	static const FName StoreIdentify;
	static const FName StoreSkills;

	static const float SpellbookWidth;
	static const float SpellbookHeight;
	static const float SpellbookSpellSize;
	static const float SpellbookButtonWidth;
	static const float SpellbookButtonHeight;

	static const float MapSize;
	float MapX;
	float MapY;

	// The current hitbox hovered over
	FName HoveredHitbox;

	bool IsRightMousePressed() const;

	// Recalculate general layout variables above; happens every frame in case of resize
	void UpdateFromViewportCoordinates();

	// After casting a spell or canceling targeting, reset selected spell to null
	void ResetSpell();

	void DrawDialog();
	void DrawChest();
	void DrawShop();
	void DrawTemple();
	void DrawTavern();

	void DrawDialogSidebarBase(const FString& Name);
	float DrawDialogSidebarText(const FString& Name, float Y, bool Available = true, bool Hovered = false);
	float DrawDialogComponent(UDialogComponent* Dialog, float Y);

	// Functions for drawing the HUD pieces
	void DrawHUDBackground();
	void DrawBorder(UMaterialInterface* Background, UTexture2D* TLCorner, UTexture2D* TRCorner, UMaterialInterface* Line, float LineWidth, float X, float Y, float Width, float Height);
	void DrawInventory(float X, float Y, UInventory* Inventory);
	void DrawCharacter();
	void DrawStat(EStats Stat, float X, float Y);
	void DrawStats();
	bool DrawSkill(ESkills Skill, float X, float Y);
	void DrawSkills();
	void DrawSpellbook();
	void DrawMap();

	// Draw tooltips for the various HUD menus
	void DrawTooltip(UItem* Item);
	void DrawTooltip(EStats Stat);
	void DrawTooltip(ESkills Skill);
	void DrawTooltip(ESpells Spell);
	void DrawTooltip(EStatusEffects Effect);
	void DrawTooltip(EPartyStatusEffects Effect);
	void DrawEffectTooltip(int32 Status, const FStatusEffect& Effect, bool bPartyEffect);
	void DrawTooltip(APickup* Pickup);
	void DrawTooltip(ABaseEnemyCharacter* Character);
	void DrawTooltipBorder(float Width, float Height, float& OutX, float& OutY);

	// Helper functions to convert to/from Hitbox names
	static FName CoordsToName(const TPair<int32, int32>& Coords);
	static TPair<int32, int32> NameToCoords(const FName& Name);

	float AddDialogHitBox(const FName& BoxName, float Y);
	float AddDialogComponentHitBox(UDialogComponent* Dialog, float Y);
	void AddInventoryHitBox(float StartX, float StartY);

	void HandleDialogHitbox(UDialogComponent* Dialog, const FName& BoxName);

	// Pure copy of private function in AHUD
	FVector2D GetCoordinateOffset() const;

	TWeakObjectPtr<APlayerPartyCharacter> PlayerParty;
	TWeakObjectPtr<UNNSGameInstance> GameInstance;

	static const FLinearColor kWhiteColor;
	static const FLinearColor kBlackColor;
	static const FLinearColor kRedColor;
	static const FLinearColor kGreenColor;
	static const FLinearColor kBlueColor;
	static const FLinearColor kGoldColor;
	static const FLinearColor kBrownColor;
	static const FLinearColor kLightGrayColor;
	static const FLinearColor kMediumGrayColor;
	static const FLinearColor kDarkGrayColor;

	static const FString DialogSkillLearn;
	static const FString DialogSkillTrain;
	static const FString DialogSkillTrainTooHigh;
	static const FString DialogSkillCannotTrain;
	static const FName DialogSkillBasic;
	static const FName DialogSkillExpert;
	static const FName DialogSkillMaster;

	static const FString TempleHeal;
	static const FString TempleHealFull;

	static const FName TavernRestName;
	static const FString TavernRestString;

	static const FString StoreWrongType;
	static const FString StoreBuyPrice;
	static const FString StoreSellPrice;
	static const FString StoreIdentifyPrice;
	static const float StoreWidth;
	static const float StoreHeight;
	float StoreX;
	float StoreY;

	static const int32 InventoryRows;
	static const int32 InventoryColumns;
	static const float InventoryBoxSize;
	static const float InventoryHeight;
	static const float TypeWidth;
	static const float TypeHeight;

	static const float CharacterWidth;
	static const float CharacterHeight;

	static const float Margin;
	static const float SmallMargin;

	static const FName ScreenHitbox;

	static const float PlayerXStart;
	static const float PlayerYStart;
	static const float PlayerSpacing;
	static const float PlayerPortraitSize;
	static const float PlayerTitleHeight;
	static const float PlayerStatusHeight;
	static const float PlayerBarWidth;
	static const float PlayerBackgroundWidth;
	static const float PlayerBackgroundHeight;

	float HotkeyHintX;
	float HotkeyHintY;

	struct HUDPosition {
		HUDPosition(const FString& Name, float X, float Y, float Width, float Height)
		{
			this->Name = Name;
			this->X = X;
			this->Y = Y;
			this->Width = Width;
			this->Height = Height;
		}
		float X, Y, Width, Height;

		UPROPERTY()
		FString Name;
	};

	// Position of equipment hitboxes to draw in Character section
	TMap<EEquipType, HUDPosition> EquipPositions;
	// Order to draw hitboxes in, as they can overlap
	TArray<EEquipType> EquipOrder;

	EStats StatFromName(const FString& Name);
	ESkills SkillFromName(const FString& Name);
	ESpells SpellFromName(const FString& Name);

	struct MasteryTooltip {
		MasteryTooltip(FString D, FString B, FString E, FString M)
		{
			Description = D;
			Basic = B;
			Expert = E;
			Master = M;
		}
		FString Description;
		FString Basic;
		FString Expert;
		FString Master;
	};

	void DrawMasteryTooltip(const FString& Name, const MasteryTooltip& Tooltip, const EMastery& Mastery, const EMastery& MaxAllowed = EMastery::MasteryMaster);
	void DrawMasteryTooltip(const FString& Name, const MasteryTooltip& Tooltip, const EMastery& Mastery, int32 MaxAllowed);

	TArray<FString> StatTooltips;
	TArray<MasteryTooltip> SkillTooltips;
	TArray<MasteryTooltip> SpellTooltips;
	TArray<FString> StatusEffectTooltips;
	TArray<FString> PartyStatusEffectTooltips;

	TMap<EStatusEffects, UTexture2D*> StatusIcons;
	TMap<EPartyStatusEffects, UTexture2D*> PartyStatusIcons;

	int32 ActiveCharacterWhenPaused;
};
