// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Projectile.h"
#include "ArrowProjectile.generated.h"

class ANNSCharacter;

UCLASS()
class NEWNEWSORPIGAL_API AArrowProjectile : public AProjectile
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AArrowProjectile(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual void HitCallback(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void OnOverlapMesh(ANNSCharacter* HitCharacter, FHitResult& Hit) override;

	// When arrow collides with something, swap out the dynamic actor with this static one
	UPROPERTY(EditDefaultsOnly, Category = "Static Projectile")
	TSubclassOf<class AActor> StaticArrowClass;
};
