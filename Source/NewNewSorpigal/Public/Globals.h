// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Globals.generated.h"

namespace ActorTags {
	extern const FName kFriendlyTag;
	extern const FName kEnemyTag;
	extern const FName kAttackableTag;
	extern const FName kPickupableTag;
	extern const FName kInteractableTag;
	extern const FName kIdentifiableTag;
	extern const FName kExcludeTag;
	extern const FName kVisibleTag;
}

namespace UtilFunctions {
	static float RollDice(int32 Dice, int32 Sides)
	{
		float Value = 0.0f;
		for (int32 i = 0; i < Dice; ++i) {
			Value += FMath::RandRange(1, Sides);
		}
		return Value;
	}
}

USTRUCT(BlueprintType)
struct FSkillCost
{
	GENERATED_BODY()

	FSkillCost() : SkillPoints(0), Gold(0) {}
	FSkillCost(int32 Points, uint32 G) : SkillPoints(Points), Gold(G) {}

	UPROPERTY()
	int32 SkillPoints;

	UPROPERTY()
	uint32 Gold;
};

UENUM(BlueprintType)
enum class EMastery : uint8
{
	MasteryBasic,
	MasteryExpert,
	MasteryMaster
};

UENUM(BlueprintType)
enum class EClass : uint8
{
	ClassKnight,
	ClassPaladin,
	ClassArcher,
	ClassDruid,
	ClassCleric,
	ClassSorcerer,
	ClassTotalClasses
};

FORCEINLINE FArchive& operator<<(FArchive &Ar, EClass& Class)
{
	uint8 AsByte = (uint8)Class;
	Ar << AsByte;

	if (Ar.IsLoading()) {
		Class = (EClass)AsByte;
	}
	return Ar;
};

UENUM(BlueprintType)
enum class ESpells : uint8
{
	SpellTorchLight,
	SpellFireShield,
	SpellFlameBolt,
	SpellFireSpikes,
	SpellFireBall,
	SpellWizardsEye,
	SpellAirShield,
	SpellShock,
	SpellFeatherFall,
	SpellWaterWalk,
	SpellJump,
	SpellFly,
	SpellTeleportTown,
	SpellCureLight,
	SpellHeroism,
	SpellCauseLight,
	SpellFlee,
	SpellRegen,
	SpellCureHeavy,
	SpellCauseHeavy,
	SpellTotalSpells
};

FORCEINLINE FArchive& operator<<(FArchive &Ar, ESpells& Spell)
{
	uint8 AsByte = (uint8)Spell;
	Ar << AsByte;

	if (Ar.IsLoading()) {
		Spell = (ESpells)AsByte;
	}
	return Ar;
}

UENUM(BlueprintType)
enum class ETargetMode : uint8
{
	TargetNone,
	TargetEnemy,
	TargetAlly
};

UENUM(BlueprintType)
enum class EDamageTypes : uint8
{
	DamagePure,
	DamagePhysical,
	DamageFire,
	DamageAir,
	DamageTotalDamage
};

UENUM(BlueprintType)
enum class ETrapTypes : uint8
{
	TrapNone,
	TrapFire,
	TrapAir,
	TrapTotalTraps
};

FORCEINLINE FArchive& operator<<(FArchive &Ar, ETrapTypes& Trap)
{
	uint8 AsByte = (uint8)Trap;
	Ar << AsByte;

	if (Ar.IsLoading()) {
		Trap = (ETrapTypes)AsByte;
	}
	return Ar;
}

UENUM(BlueprintType)
enum class ETrapLevel : uint8
{
	TrapBasic,
	TrapExpert,
	TrapMaster
};

FORCEINLINE FArchive& operator<<(FArchive &Ar, ETrapLevel& Trap)
{
	uint8 AsByte = (uint8)Trap;
	Ar << AsByte;

	if (Ar.IsLoading()) {
		Trap = (ETrapLevel)AsByte;
	}
	return Ar;
}

UENUM(BlueprintType)
enum class EEquipType : uint8
{
	EquipMainHand			UMETA(DisplayName = "Main Hand"),
	EquipOffHand			UMETA(DisplayName = "Off Hand"),
	EquipRanged				UMETA(DisplayName = "Ranged"),
	EquipHead				UMETA(DisplayName = "Head"),
	EquipBody				UMETA(DisplayName = "Body"),
	EquipHands				UMETA(DisplayName = "Hands"),
	EquipLegs				UMETA(DisplayName = "Legs"),
	EquipFeet				UMETA(DisplayName = "Feet"),
	EquipBack				UMETA(DisplayName = "Back"),
	EquipNeck				UMETA(DisplayName = "Necklace"),
	EquipFinger1			UMETA(DisplayName = "Ring1"),
	EquipFinger2			UMETA(DisplayName = "Ring2"),
	EquipFinger3			UMETA(DisplayName = "Ring3"),
	EquipFinger4			UMETA(DisplayName = "Ring4"),
	EquipTotalSlots
};

UENUM(BlueprintType)
enum class EEquipStyle : uint8
{
	EquipSword				UMETA(DisplayName = "Sword"),
	EquipAxe				UMETA(DisplayName = "Axe"),
	EquipDagger				UMETA(DisplayName = "Dagger"),
	EquipBow				UMETA(DisplayName = "Bow"),
	EquipShield				UMETA(DisplayName = "Shield"),
	EquipLeather			UMETA(DisplayName = "Leather"),
	EquipChain				UMETA(DisplayName = "Chain"),
	EquipPlate				UMETA(DisplayName = "Plate"),
	EquipOther				UMETA(DisplayName = "Other")
};

UENUM(BlueprintType)
enum class EStats : uint8
{
	StatStrength			UMETA(DisplayName = "Strength"),
	StatIntelligence		UMETA(DisplayName = "Intelligence"),
	StatDexterity			UMETA(DisplayName = "Dexterity"),
	StatSpeed				UMETA(DisplayName = "Speed"),
	StatVitality			UMETA(DisplayName = "Vitality"),
	StatLuck				UMETA(DisplayName = "Luck"),
	StatHealth				UMETA(DisplayName = "Max Health"),
	StatMana				UMETA(DisplayName = "Max Mana"),
	StatAccuracy			UMETA(DisplayName = "Accuracy"),
	StatEvasion				UMETA(DisplayName = "Evasion"),
	StatResistFire			UMETA(DisplayName = "Fire Resist"),
	StatResistAir			UMETA(DisplayName = "Air Resist"),
	StatTotalStats,
	StatCurrentHealth,
	StatCurrentMana
};

namespace StatusEffectConstants {
	extern const int32 kMaxDuration;
}

USTRUCT(BlueprintType)
struct FStatusEffect
{
	GENERATED_BODY()

	FStatusEffect() : Duration(0), Strength(0) {}
	FStatusEffect(int32 Dur, int32 Str)
		: Duration(Dur), Strength(Str) {}

	bool IsPermanent() const { return Duration > 144000; }

	UPROPERTY()
	int32 Duration; // in minutes

	UPROPERTY()
	int32 Strength;
};

FORCEINLINE FArchive& operator<<(FArchive &Ar, FStatusEffect& Effect)
{
	Ar << Effect.Duration;
	Ar << Effect.Strength;
	return Ar;
}

UENUM(BlueprintType)
enum class EStatusEffects : uint8
{
	StatusPoison				UMETA(DisplayName = "Poison"),
	StatusRegen,
	StatusResistFire,
	StatusResistAir,
	StatusFireSpikes,
	StatusHeroism,
	StatusStatsUp,
	StatusInWater,
	StatusTotalStatusEffects	UMETA(DisplayName = "None")
};

FORCEINLINE FArchive& operator<<(FArchive &Ar, EStatusEffects& Status)
{
	uint8 AsByte = (uint8)Status;
	Ar << AsByte;

	if (Ar.IsLoading()) {
		Status = (EStatusEffects)AsByte;
	}
	return Ar;
}

UENUM(BlueprintType)
enum class EPartyStatusEffects : uint8
{
	StatusWizardsEye,
	StatusWaterWalk,
	StatusFly,
	StatusFeatherFall,
	StatusTorchLight,
	StatusInCombat,
	StatusFlee,
	StatusTotalPartyStatusEffects
};

FORCEINLINE FArchive& operator<<(FArchive &Ar, EPartyStatusEffects& Status)
{
	uint8 AsByte = (uint8)Status;
	Ar << AsByte;

	if (Ar.IsLoading()) {
		Status = (EPartyStatusEffects)AsByte;
	}
	return Ar;
}

UENUM(BlueprintType)
enum class ESkills : uint8
{
	SkillWeaponSword, // weapons
	SkillWeaponAxe,
	SkillWeaponDagger,
	SkillWeaponBow,
	SkillArmorShield, // armor
	SkillArmorLeather,
	SkillArmorChain,
	SkillArmorPlate,
	SkillAlchemy, // misc
	SkillArmsmaster,
	SkillBodybuilding,
	SkillDisarmTrap,
	SkillIdentifyItem,
	SkillIdentifyMonster,
	SkillLearning,
	SkillMeditation,
	SkillMerchant,
	SkillSwimming,
	SkillFireMagic, // magic
	SkillAirMagic,
	SkillBodyMagic,
	SkillTotalSkills
};

UENUM(BlueprintType)
enum class EShopTypes : uint8
{
	ShopWeapon,
	ShopArmorChest,
	ShopSpellbook,
	ShopArmorOther,
	ShopAlchemy,
	ShopTemple,
	ShopTavern
};

FORCEINLINE FArchive& operator<<(FArchive &Ar, EShopTypes& Shop)
{
	uint8 AsByte = (uint8)Shop;
	Ar << AsByte;

	if (Ar.IsLoading()) {
		Shop = (EShopTypes)AsByte;
	}
	return Ar;
}

FORCEINLINE FArchive& operator<<(FArchive &Ar, ESkills& Skill)
{
	uint8 AsByte = (uint8)Skill;
	Ar << AsByte;

	if (Ar.IsLoading()) {
		Skill = (ESkills)AsByte;
	}
	return Ar;
}

USTRUCT(BlueprintType)
struct FSpellInfo
{
	GENERATED_BODY()

	FSpellInfo() : Name(TEXT("")), ManaCost(1), RequiredMastery(EMastery::MasteryBasic), Delay(1.0f), Skill(ESkills::SkillTotalSkills), TargetMode(ETargetMode::TargetEnemy), SpellbookTexture(nullptr), Sound(nullptr) {}
	FSpellInfo(const FString& N, int32 Mana, EMastery Mastery, float D, ESkills S, ETargetMode Target, const FStringAssetReference& Texture, const FStringAssetReference& Audio)
		: Name(N), ManaCost(Mana), RequiredMastery(Mastery), Delay(D), TargetMode(Target), Skill(S)
	{
		SpellbookTexture = Texture;
		Sound = Audio;
	}

	UPROPERTY()
	FString Name;

	UPROPERTY()
	int32 ManaCost;

	UPROPERTY()
	EMastery RequiredMastery;

	UPROPERTY()
	float Delay;

	UPROPERTY()
	ESkills Skill;

	UPROPERTY()
	ETargetMode TargetMode;

	UPROPERTY()
	TAssetPtr<UTexture> SpellbookTexture;

	UPROPERTY()
	TAssetPtr<USoundWave> Sound;
};

UENUM(BlueprintType)
enum class EQuestFlags : uint8
{
	QuestFreeMoney			UMETA(DisplayName = "Free Money"),
	QuestSisterChat			UMETA(DisplayName = "Sister Chat"),
	QuestSpiderQueen		UMETA(DisplayName = "Kill Spider Queen"),
	QuestTrollKing			UMETA(DisplayName = "Kill Troll King"),
	QuestScavengerHuntStart	UMETA(DisplayName = "Scavenger Hunt Start"),
	QuestScavengerHuntTown	UMETA(DisplayName = "Scavenger Hunt Town"),
	QuestScavengerHuntForest	UMETA(DisplayName = "Scavenger Hunt Forest"),
	QuestScavengerHuntMaze	UMETA(DisplayName = "Scavenger Hunt Maze"),
	QuestScavengerHuntMountain	UMETA(DisplayName = "Scavenger Hunt Mountain"),
	QuestMainTalkMayor,
	QuestMainStatueTown,
	QuestMainStatueMountain,
	QuestMainStatueForest,
	QuestMainStatueMaze,
	QuestMainDemonDoor,
	QuestTotalQuests		UMETA(DisplayName = "N/A")
};

UENUM(BlueprintType)
enum class EBestiaryFlags : uint8
{
	BestiarySpiders,
	BestiaryGoblins,
	BestiaryDragonflies,
	BestiaryTrolls,
	BestiaryVampires,
	BestiaryTotalQuests
};

FORCEINLINE FArchive& operator<<(FArchive &Ar, EQuestFlags& Quest)
{
	uint8 AsByte = (uint8)Quest;
	Ar << AsByte;

	if (Ar.IsLoading()) {
		Quest = (EQuestFlags)AsByte;
	}
	return Ar;
}

namespace EnumMaps {
	extern TMap<EDamageTypes, EStats> DamageResistances;
	extern TMap<EStats, EStatusEffects> StatusResistances;
	extern TMap<ETrapTypes, EDamageTypes> TrapDamages;
	extern TMap<ESkills, TArray<ESpells>> SchoolSpells;
	extern TMap<ESpells, FSpellInfo> SpellInfo;
	extern TMap<EMastery, FSkillCost> SkillCosts;
	extern TMap<EShopTypes, TArray<ESkills>> ShopSkills;
	extern TMap<EEquipStyle, ESkills> StyleToSkill;
}
